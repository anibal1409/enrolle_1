﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class actcerti1 : Form
    {
        public actcerti1()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void actcerti1_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo4();
            combo2();
            combo1();
            comboBox1.SelectedIndex = 0;
        }
        DataTable dt = new DataTable();
        void combo4()
        {

            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox4.ValueMember = "cod";
            comboBox4.DisplayMember = "periodo";
            comboBox4.DataSource = dt;
        }
        public string obser, usu;
        string pago, estado;
        void cargar()
        {
            string buscarcx = "SELECT TOP 1 * FROM pago ORDER BY cod DESC;";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                double ccod;
                ccod = Double.Parse(leex["cod"].ToString()) + 1;
                textBox1.Text = ccod.ToString();
            }
            else
            {
                textBox1.Text = "1";
            }
        }
        void combo1()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Cliente' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "banco";
            comboBox2.DataSource = dt;
        }
        void combo5(string com)
        {
            string ctc = "SELECT cod,modalidad FROM modalidad WHERE estatus = 'DISPONIBLE' AND periodo = " + com + " ORDER BY modalidad";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dtx = new DataTable();
            DATA.Fill(dtx);

            DataRow nuevaFila = dtx.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";

            dtx.Rows.InsertAt(nuevaFila, 0);

            comboBox5.ValueMember = "cod";
            comboBox5.DisplayMember = "modalidad";
            comboBox5.DataSource = dtx;
        }

        void combo2()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Instituto' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "banco";
            comboBox3.DataSource = dt;
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox4.SelectedIndex != 0)
            {

                combo5(comboBox4.SelectedValue.ToString());


                string buscarcx = "SELECT * FROM precio WHERE periodo = " + comboBox4.SelectedValue.ToString() + " AND servicio= 'Certificado';";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read() == true)
                {
                    textBox5.Text = leex["precio"].ToString();
                    comboBox5.Enabled = true;
                }
                else
                {
                    MessageBox.Show("El periodo académico seleccionado no tiene precio definido para este servicio, comuníquese con el Coordinador Académico.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    comboBox5.Enabled = false;
                }


            }
            else
            {
                textBox5.Clear();
                textBox10.Clear();
                textBox11.Clear();
                //clearc();
                comboBox5.Enabled = false;
                dt = new DataTable();
                dt.Columns.Add("cod");
                dt.Columns.Add("modalidad");
                DataRow nuevaFila = dt.NewRow();
                nuevaFila["cod"] = 0;
                nuevaFila["modalidad"] = "Seleccione una disciplina";

                dt.Rows.InsertAt(nuevaFila, 0);

                comboBox5.ValueMember = "cod";
                comboBox5.DisplayMember = "modalidad";
                comboBox5.DataSource = dt;
            }
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox5.SelectedIndex != 0 && v == "1")
            {
                estatica.Mod = "1";
                estatica.Certi = comboBox5.SelectedValue.ToString();
                select a = new select();
                a.ShowDialog();
                if (a.DialogResult == DialogResult.Yes)
                {
                    textBox11.Text = estatica.Estudiante;
                    textBox10.Text = estatica.Ne;
                    estatica.Mod = "";
                }
                else
                {
                    textBox10.Clear();
                    textBox11.Clear();
                }
            }
            if (comboBox5.SelectedIndex == 0 && v == "1")
            {
                textBox10.Clear();
                textBox11.Clear();

            }
        }
        int ep = 0;
        void buscarp()
        {
            if (textBox1.Text != "")
            {
                ocultar();
                desbloquear();
                string buscarc = "SELECT * FROM pago WHERE cod = " + textBox1.Text + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    ep = 1;
                    pago = textBox1.Text;
                    ocultar();
                    label4.Visible = true;
                    comboBox1.Visible = true;
                    //button7.Visible = false;
                    button5.Visible = false;
                    comboBox1.SelectedItem = lee["tipo"].ToString();
                    dateTimePicker1.Text = lee["fecha"].ToString();
                    textBox2.Text = lee["monto"].ToString();
                    textBox3.Text = lee["pago"].ToString();
                    textBox4.Text = lee["disponible"].ToString();
                    comboBox2.SelectedValue = lee["origen"].ToString();
                    comboBox3.SelectedValue = lee["destino"].ToString();
                    estado = lee["estatus"].ToString();
                    bloquear();


                }
                else
                {
                    MessageBox.Show("El código ingresado no pertenece a ningún pago registrado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Focus();
                    ocultar();
                    desbloquear();
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código de pago para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox1.Focus();
                ocultar();
                desbloquear();
            }
        }
        void ocultar()
        {
            textBox2.Visible = false;
            textBox3.Visible = false;
            label2.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            dateTimePicker1.Visible = false;
            comboBox2.Visible = false;
            comboBox3.Visible = false;
            textBox2.Clear();
            textBox3.Clear();
            dateTimePicker1.Value = DateTime.Now;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            label17.Visible = false;
            textBox4.Visible = false;
        }
        void bloquear()
        {
            comboBox1.Enabled = false;
            comboBox2.Enabled = false;
            comboBox3.Enabled = false;
            textBox2.ReadOnly = true;
            textBox3.ReadOnly = true;
            textBox4.ReadOnly = true;
            dateTimePicker1.Enabled = false;
        }
        void desbloquear()
        {
            comboBox1.Enabled = true;
            comboBox2.Enabled = true;
            comboBox3.Enabled = true;
            textBox2.ReadOnly = false;
            textBox3.ReadOnly = false;
            textBox4.ReadOnly = false;
            dateTimePicker1.Enabled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            buscarp();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text != "")
            {
                if (comboBox1.Text == "Efectivo")
                {
                    ocultar();
                    textBox2.Visible = true;
                    label2.Visible = true;
                    dateTimePicker1.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = false;
                    if (ep == 1)
                    {

                        label17.Visible = true;
                        textBox4.Visible = true;
                    }


                }
                if (comboBox1.Text == "Exoneración")
                {
                    ocultar();
                    dateTimePicker1.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = false;
                    textBox4.Visible = false;
                    label17.Visible = false;
                }
                if (comboBox1.Text == "Deposito")
                {
                    ocultar();
                    dateTimePicker1.Visible = true;
                    textBox2.Visible = true;
                    label2.Visible = true;
                    textBox3.Visible = true;
                    label6.Visible = true;
                    comboBox3.Visible = true;
                    label7.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = true;
                    if (ep == 1)
                    {

                        label17.Visible = true;
                        textBox4.Visible = true;
                    }

                }
                if (comboBox1.Text == "Trans. Bancaria" || comboBox1.Text == "Pago Rápido" || comboBox1.Text == "Otro")
                {
                    ocultar();
                    dateTimePicker1.Visible = true;
                    textBox2.Visible = true;
                    label2.Visible = true;
                    textBox3.Visible = true;
                    label6.Visible = true;
                    comboBox2.Visible = true;
                    label5.Visible = true;
                    comboBox3.Visible = true;
                    label7.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = true;
                    if (ep == 1)
                    {

                        label17.Visible = true;
                        textBox4.Visible = true;
                    }

                }
            }
            else
            {
                ocultar();
                desbloquear();
            }
        }
        string ci;
        void estu(string x)
        {
            ci = "";
            string buscarcx = "SELECT TOP 1 * FROM estudiante WHERE ci = '" + x + "';";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                textBox11.Text= ci = leex["ci"].ToString();
                textBox10.Text = leex["nombre"].ToString() + " " + leex["apellido"].ToString(); 
            }
        }

        void per(string x)
        {
            string buscarcx = "SELECT TOP 1 * FROM modalidad WHERE cod = " + x + ";";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                comboBox4.SelectedValue =  leex["periodo"].ToString();
            }
        }
        string certi, v = "", pp= "", prec = "";
        private void button3_Click(object sender, EventArgs e)
        {
            ocultar();
            desbloquear();
            certi = "";
            v = "";
            if(textBox6.Text != "")
            {

                string buscarcx = "SELECT TOP 1 * FROM certi WHERE cod = " + textBox6.Text + " AND tipo = 'REGULAR';";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read() == true)
                {
                    certi = leex["cod"].ToString();
                    estu(leex["estudiante"].ToString());
                    per(leex["modalidad"].ToString());
                    textBox5.Text = leex["precio"].ToString();
                    comboBox5.SelectedValue = leex["modalidad"].ToString();
                    textBox1.Text = leex["pago"].ToString();
                    pp = leex["pago"].ToString();
                    prec = leex["precio"].ToString();
                    comboBox6.Text = leex["estatus"].ToString();
                    buscarp();
                    string buscarcxx = "SELECT nombre, apellido FROM empleado WHERE  ci = '" + leex["empleado"].ToString() + "'";
                    OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
                    OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
                    OleDbDataReader leexx = comandoxx.ExecuteReader();
                    if (leexx.Read())
                    {
                        textBox14.Text = "(" + leex["empleado"].ToString() + ") " + leexx["nombre"].ToString() + " " + leexx["apellido"].ToString();
                    }
                    v = "1";
                }
                else
                {
                    MessageBox.Show("El código ingresado no pertenece a ninguna solicitud de certificado registrada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox11.Text != "" && textBox5.Text != "" && pago != "" && pago != null && textBox6.Text != "")
            {

                if (pp != pago)
                {
                    DateTime hora = new DateTime();
                    double dis = 0, p = 0, to;
                    dis = Double.Parse(textBox4.Text);
                    if (comboBox1.Text != "Exoneración")
                    {

                        dis = Double.Parse(textBox4.Text);
                        p = Double.Parse(textBox5.Text);
                        if (dis >= p)
                        {
                            try
                            {
                                to = dis - p;
                                hora = DateTime.Now;
                                if (estado == "VERIFICADO")
                                {
                                    string insertar = "UPDATE certi  SET estudiante = '" + textBox11.Text + "' , modalidad = " + comboBox5.SelectedValue.ToString() + ", pago = " + pago + ",  precio =  '" + textBox5.Text + "', estatus =  '"+estado+"', empleado = '" + estatica.Ci + "' WHERE cod = " + certi + ";";
                                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                    cmd.ExecuteNonQuery();

                                    string insertarxx = "UPDATE  pago SET disponible = '" + to.ToString() + "' WHERE cod = " + pago + ";";
                                    OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                    cmdxx.ExecuteNonQuery();

                                    textBox4.Text = to.ToString();
                                    button8.Visible = true;
                                    MessageBox.Show("Los datos de solicitud del certificado se modificaron con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    string insertar = "UPDATE certi  SET estudiante = '" + textBox11.Text + "' , modalidad = " + comboBox5.SelectedValue.ToString() + ", pago = " + pago + ",  precio =  '" + textBox5.Text + "', estatus =  '" + estado + "', empleado = '" + estatica.Ci + "' WHERE cod = " + certi + ";";
                                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                    cmd.ExecuteNonQuery();

                                    string insertarxx = "UPDATE  pago SET disponible = '" + to.ToString() + "' WHERE cod = " + pago + ";";
                                    OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                    cmdxx.ExecuteNonQuery();

                                    textBox4.Text = to.ToString();
                                    button8.Visible = true;
                                    MessageBox.Show("Los datos de solicitud del certificado se modificaron con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }




                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("La disponibilidad del pago ingresado no cubre el precio del servicio. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBox1.Focus();
                        }
                    }
                    else
                    {
                        try
                        {
                            if (dis == 0)
                            {
                                hora = DateTime.Now;
                                if (estado == "VERIFICADO")
                                {
                                    string insertar = "UPDATE certi  SET estudiante = '" + textBox11.Text + "' , modalidad = " + comboBox5.SelectedValue.ToString() + ", pago = " + pago + ",  precio =  '" + textBox5.Text + "', estatus =  '" + estado + "', empleado = '" + estatica.Ci + "' WHERE cod = " + certi + ";";
                                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                    cmd.ExecuteNonQuery();

                                    string insertarxx = "UPDATE  pago SET disponible = '-1' WHERE cod = " + pago + ";";
                                    OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                    cmdxx.ExecuteNonQuery();
                                    button8.Visible = true;
                                    MessageBox.Show("Los datos de solicitud del certificado se modificaron con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    string insertar = "UPDATE certi  SET estudiante = '" + textBox11.Text + "' , modalidad = " + comboBox5.SelectedValue.ToString() + ", pago = " + pago + ",  precio =  '" + textBox5.Text + "', estatus =  '" + estado + "', empleado = '" + estatica.Ci + "' WHERE cod = " + certi + ";";
                                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                    cmd.ExecuteNonQuery();

                                    string insertarxx = "UPDATE  pago SET disponible = '-1' WHERE cod = " + pago + ";";
                                    OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                    cmdxx.ExecuteNonQuery();
                                    button8.Visible = true;

                                    MessageBox.Show("Los datos de solicitud del certificado se modificaron con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("La Exoneración ingresada ya fue utilizada en otra formalización. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                textBox1.Focus();
                            }




                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                }
                else
                {
                    DateTime hora = new DateTime();
                    double dis = 0, p = 0, to, pr = 0;
                    dis = Double.Parse(textBox4.Text);
                    if (comboBox1.Text != "Exoneración")
                    {

                        dis = Double.Parse(textBox4.Text);
                        p = Double.Parse(textBox5.Text);
                        pr = Double.Parse(prec);
                        if (dis+pr >= p)
                        {
                            try
                            {
                                to = (dis+pr) - p;
                                hora = DateTime.Now;
                                if (estado == "VERIFICADO")
                                {
                                    string insertar = "UPDATE certi  SET estudiante = '" + textBox11.Text + "' , modalidad = " + comboBox5.SelectedValue.ToString() + ", pago = " + pago + ",  precio =  '" + textBox5.Text + "', estatus =  '" + estado + "', empleado = '" + estatica.Ci + "' WHERE cod = " + certi + ";";
                                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                    cmd.ExecuteNonQuery();

                                    string insertarxx = "UPDATE  pago SET disponible = '" + to.ToString() + "' WHERE cod = " + pago + ";";
                                    OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                    cmdxx.ExecuteNonQuery();

                                    textBox4.Text = to.ToString();
                                    button8.Visible = true;
                                    MessageBox.Show("Los datos de solicitud del certificado se modificaron con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    string insertar = "UPDATE certi  SET estudiante = '" + textBox11.Text + "' , modalidad = " + comboBox5.SelectedValue.ToString() + ", pago = " + pago + ",  precio =  '" + textBox5.Text + "', estatus =  '" + estado + "', empleado = '" + estatica.Ci + "' WHERE cod = " + certi + ";";
                                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                    cmd.ExecuteNonQuery();

                                    string insertarxx = "UPDATE  pago SET disponible = '" + to.ToString() + "' WHERE cod = " + pago + ";";
                                    OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                    cmdxx.ExecuteNonQuery();

                                    textBox4.Text = to.ToString();
                                    button8.Visible = true;
                                    MessageBox.Show("Los datos de solicitud del certificado se modificaron con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }




                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("La disponibilidad del pago ingresado no cubre el precio del servicio. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBox1.Focus();
                        }
                    }
                    else
                    {
                        try
                        {
                                hora = DateTime.Now;
                                if (estado == "VERIFICADO")
                                {
                                    string insertar = "UPDATE certi  SET estudiante = '" + textBox11.Text + "' , modalidad = " + comboBox5.SelectedValue.ToString() + ", pago = " + pago + ",  precio =  '" + textBox5.Text + "', estatus =  '" + estado + "', empleado = '" + estatica.Ci + "' WHERE cod = " + certi + ";";
                                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                    cmd.ExecuteNonQuery();

                                    string insertarxx = "UPDATE  pago SET disponible = '-1' WHERE cod = " + pago + ";";
                                    OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                    cmdxx.ExecuteNonQuery();
                                    button8.Visible = true;
                                    MessageBox.Show("Los datos de solicitud del certificado se modificaron con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    string insertar = "UPDATE certi  SET estudiante = '" + textBox11.Text + "' , modalidad = " + comboBox5.SelectedValue.ToString() + ", pago = " + pago + ",  precio =  '" + textBox5.Text + "', estatus =  '" + estado + "', empleado = '" + estatica.Ci + "' WHERE cod = " + certi + ";";
                                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                    cmd.ExecuteNonQuery();

                                    string insertarxx = "UPDATE  pago SET disponible = '-1' WHERE cod = " + pago + ";";
                                    OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                    cmdxx.ExecuteNonQuery();
                                    button8.Visible = true;

                                    MessageBox.Show("Los datos de solicitud del certificado se modificaron con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }




                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar la modificación.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox4.SelectedIndex != 0)
            {
                string buscarcx = "SELECT * FROM precio WHERE periodo = " + comboBox4.SelectedValue.ToString() + " AND servicio= 'Certificado';";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read() == true)
                {
                    textBox5.Text = leex["precio"].ToString();
                    comboBox5.Enabled = true;
                }
                else
                {
                    MessageBox.Show("El periodo académico seleccionado no tiene precio definido para este servicio, comuníquese con el Coordinador Académico.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    comboBox5.Enabled = false;
                }


            }
        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            estado = comboBox6.Text;
        }

        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox10.Clear();
            textBox11.Clear();
            ocultar();
            bloquear();
            certi = "";
            pago = "";
            v = "";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            clear();
        }

        #region menu1
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea salir de su sesión?", "Notificación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                isesion a = new isesion();
                a.Show();
                this.Close();
            }
        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            estudiante a = new estudiante();
            a.Show();
            this.Close();
        }

        private void modificarRestContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actusuario a = new actusuario();
            a.Show();
            this.Close();
        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actpas a = new actpas();
            a.ShowDialog();
        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            historial a = new historial();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            usuario a = new usuario();
            a.Show();
            this.Close();
        }

        private void asignacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void deduccionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pagoe a = new pagoe();
            a.Show();
            this.Close();
        }

        private void disciplinasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            admin a = new admin();
            a.Show();
            this.Close();
        }



        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cargarnota a = new cargarnota();
            a.Show();
            this.Close();
        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actnotae a = new actnotae();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            modalidad a = new modalidad();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            actmodalidad a = new actmodalidad();
            a.Show();
            this.Close();
        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mdisponibles a = new mdisponibles();
            a.Show();
            this.Close();
        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resumencurso a = new resumencurso();
            a.Show();
            this.Close();
        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listaestudiantes a = new listaestudiantes();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            tipom a = new tipom();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            acttipom a = new acttipom();
            a.Show();
            this.Close();
        }



        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            forma a = new forma();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            actforma a = new actforma();
            a.Show();
            this.Close();
        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cambio a = new cambio();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            periodo a = new periodo();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            actperiodo a = new actperiodo();
            a.Show();
            this.Close();
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cerrar a = new cerrar();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            pre a = new pre();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            actpre a = new actpre();
            a.Show();
            this.Close();
        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            prepen a = new prepen();
            a.Show();
            this.Close();
        }


        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actestudiante a = new actestudiante();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            actempleado a = new actempleado();
            a.Show();
            this.Close();
        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            trabajo a = new trabajo();
            a.Show();
            this.Close();
        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actividad a = new actividad();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            cargo a = new cargo();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            actcargo a = new actcargo();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            banco a = new banco();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            actbanco a = new actbanco();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            pago a = new pago();
            a.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            conpago a = new conpago();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            actpago a = new actpago();
            a.Show();
            this.Close();
        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            veripago a = new veripago();
            a.Show();
            this.Close();
        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listapv a = new listapv();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {
            precio a = new precio();
            a.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            conprecio a = new conprecio();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            actprecio a = new actprecio();
            a.Show();
            this.Close();
        }

        private void salarioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            abonos a = new abonos();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            actabonos a = new actabonos();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            dedu a = new dedu();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            actdedu a = new actdedu();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {
            notas a = new notas();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {
            actnotas a = new actnotas();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {
            certi2 a = new certi2();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {
            actcerti2 a = new actcerti2();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem16_Click(object sender, EventArgs e)
        {
            certi1 a = new certi1();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {
            actcerti1 a = new actcerti1();
            a.Show();
            this.Close();
        }

        private void listarCertificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listacerti a = new listacerti();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem17_Click(object sender, EventArgs e)
        {
            estudio a = new estudio();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem16_Click(object sender, EventArgs e)
        {
            actestudio a = new actestudio();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem18_Click(object sender, EventArgs e)
        {
            postu a = new postu();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem17_Click(object sender, EventArgs e)
        {
            actpostu a = new actpostu();
            a.Show();
            this.Close();
        }
        #endregion

        


    }
}
