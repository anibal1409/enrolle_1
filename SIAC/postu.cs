﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Drawing.Printing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;
using System.Globalization;

namespace SIAC
{
    public partial class postu : Form
    {
        public postu()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");

        private void postu_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
            cargar();
            comboBox1.SelectedIndex = 2;
            comboBox4.SelectedIndex = 0;
            cc2();

        }
        void cc2()
        {
            dt = new DataTable();
            dt.Columns.Add("cod");
            dt.Columns.Add("modalidad");
            DataRow nuevaFila = dt.NewRow();
            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "modalidad";
            comboBox2.DataSource = dt;

        }
        void cargar()
        {
            string buscarcx = "SELECT TOP 1 * FROM postu ORDER BY cod DESC;";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                double ccod;
                ccod = Double.Parse(leex["cod"].ToString()) + 1;
                textBox3.Text = ccod.ToString();
            }
            else
            {
                textBox3.Text = "1";
            }
        }
        void combo1()
        {
            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dtx = new DataTable();
            dtx = new DataTable();
            DATA.Fill(dtx);

            DataRow nuevaFila = dtx.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dtx.Rows.InsertAt(nuevaFila, 0);

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "periodo";
            comboBox3.DataSource = dtx;
        }
        string cie = "";
        void combo3()
        {
            if (textBox5.Text != "")
            {

                cie = "";
                var cadena = textBox5.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    cie = textBox5.Text;
                    string buscarcx = "SELECT nombre, apellido, cargo, estatus FROM empleado WHERE  ci = '" + cie + "'";
                    OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                    OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read())
                    {
                            textBox13.Text = leex["nombre"].ToString() + " " + leex["apellido"].ToString();

                            string buscarc = "SELECT cargo FROM cargo WHERE cod = " + leex["cargo"].ToString() + "";
                            OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                            OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                            OleDbDataReader lee = comando.ExecuteReader();
                            if (lee.Read())
                            {
                                textBox4.Text = lee["cargo"].ToString();
                            }
                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad '" + textBox5.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox5.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad válido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox5.Focus();
                }


            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para poder realizar una búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox5.Focus();
            }

        }
        DataTable dt = new DataTable();
        void combo2()
        {
            DataTable dt = new DataTable();
            string buscarcx = "SELECT * FROM pre WHERE estudiante= '" + ci + "' AND estatus = 'CULMINADO';";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            while (leex.Read())
            {
                string ctc = "SELECT cod,modalidad FROM modalidad WHERE  cod = " + leex["modalidad"].ToString() + "";
                OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
                DATA.Fill(dt);
            }


            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "modalidad";
            comboBox2.DataSource = dt;
        }
        string ci = "";
        void buscare()
        {
            if (comboBox3.SelectedIndex != 0 && textBox2.Text != "")
            {
                if (textBox7.Text != "")
                {
                    ci = "";
                    var cadena = textBox7.Text;
                    if (cadena.Length >= 7 && cadena.Length <= 8)
                    {
                        string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox7.Text + "';";
                        OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                        OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                        OleDbDataReader lee = comando.ExecuteReader();
                        if (lee.Read() == true)
                        {
                                ci = textBox7.Text;
                                textBox6.Text = lee["nombre"].ToString() + " " + lee["apellido"].ToString();
                                combo2();
                            

                        }
                        else
                        {
                            MessageBox.Show("El documento de identidad '" + textBox7.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            textBox7.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Debe ingresar un documento de identidad válido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un periodo académico para poder hacer uso de esta función", "Información)", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox3.SelectedIndex != 0)
            {
                string buscarcx = "SELECT * FROM precio WHERE periodo = " + comboBox3.SelectedValue.ToString() + " AND servicio= 'Postulación para Pasantías';";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read() == true)
                {
                    textBox2.Text = leex["precio"].ToString();
                }
                else
                {
                    MessageBox.Show("El periodo académico seleccionado no tiene precio definido para este servicio, comuníquese con el Coordinador Académico.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    textBox2.Clear();
                }

            }
            else
            {
                textBox2.Clear();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscare();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            combo3();
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox7_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buscare();
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox5_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                combo3();
            }
        }
        string per, tm;
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedValue.ToString() != "0")
            {
                per = "";
                string buscarc = "SELECT * FROM modalidad WHERE cod = " + comboBox2.SelectedValue.ToString() + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    string buscarcx = "SELECT * FROM periodo WHERE cod = " + lee["periodo"].ToString() + ";";
                    OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                    OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read() == true)
                    {
                        per = lee["periodo"].ToString();
                        textBox8.Text = leex["periodo"].ToString();
                    }
                    string buscarcxx = "SELECT * FROM pre WHERE estudiante = '" + ci + "' AND modalidad = " + lee["cod"].ToString() + ";";
                    OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
                    OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
                    OleDbDataReader leexx = comandoxx.ExecuteReader();
                    if (leexx.Read() == true)
                    {

                        textBox9.Text = leexx["nota"].ToString();
                    }
                    string buscarcxe = "SELECT * FROM tipom WHERE cod = " + lee["tipo"].ToString() + ";";
                    OleDbDataAdapter dataxe = new OleDbDataAdapter(buscarcxe, conexion);
                    OleDbCommand comandoxe = new OleDbCommand(buscarcxe, conexion);
                    OleDbDataReader leexe = comandoxe.ExecuteReader();
                    if (leexe.Read() == true)
                    {
                        tm = leexe["modalidad"].ToString();
                    }
                }
            }
            else
            {
                textBox8.Clear();
                textBox9.Clear();
            }
        }
        DateTime aa;
        private void button5_Click(object sender, EventArgs e)
        {
            if(ci != "" && cie != "" && textBox4.Text != "" && textBox3.Text != "" && textBox2.Text != "" && comboBox3.SelectedIndex != 0  && textBox1.Text != "" && textBox10.Text != "" && comboBox2.SelectedIndex != 0  && textBox9.Text != "" && textBox11.Text != "")
            {
                try
                {
                    aa = DateTime.Now;
                    string insertar = "INSERT INTO postu VALUES (" + textBox3.Text + ",'" + aa.ToString("dd-MM-yyyy") + "', '" + textBox2.Text + "', '" + ci + "'," + comboBox2.SelectedValue.ToString() + ", " + per + ", '"+textBox1.Text+"', '"+comboBox4.Text+"', '"+textBox10.Text+"' , '"+textBox11.Text+"' , '"+numericUpDown1.Value.ToString()+"', '"+comboBox1.Text+"' , '"+cie+"','" + estatica.Ci + "', 'GENERADA')";
                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                    cmd.ExecuteNonQuery();
                    button8.Visible = true;
                    button5.Visible = false;
                    MessageBox.Show("La carta de postulación para pasantías se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (DBConcurrencyException ex)
                {
                    MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        void clear()
        {
            ci = "";
            cie = "";
            per = "";
            comboBox3.SelectedIndex = 0;
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox8.Clear();
            textBox9.Clear();
            textBox10.Clear();
            textBox11.Clear();
            textBox13.Clear();
            cc2();
            comboBox1.SelectedIndex = 2;
            comboBox4.SelectedIndex = 0;
            numericUpDown1.Value = 1;
            button5.Visible = true;
            button8.Visible = false;
            cargar();

        }
        private void button6_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        #region crearPDF
        private void To_pdf()
        {
            string ab, bc;
            ab = textBox6.Text;
            bc = textBox13.Text;
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            Document doc = new Document(PageSize.LETTER, 30, 30, 30, 30);
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:";
            saveFileDialog1.Title = "Guardar Carta de Postulación para Pasantías ";
            saveFileDialog1.DefaultExt = "pdf";
            saveFileDialog1.Filter = "pdf Files (*.pdf)|*.pdf| All Files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.FileName = "CartaPostulaciónPasantías" + ci + myTI.ToTitleCase(ab) + DateTime.Now.ToString("dd-MMM-yyyy"); ;
            string filename = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filename = saveFileDialog1.FileName;
            }
            if (filename.Trim() != "")
            {
                FileStream file = new FileStream(filename,
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite,
                FileShare.ReadWrite);
                PdfWriter wri = PdfWriter.GetInstance(doc, file);
                wri.PageEvent = new Class1();
                wri.PageEvent = new menbrete();
                //PdfWriter.GetInstance(doc, file);

                doc.Open();
                string remito = "Generado por: (" + estatica.Ci + ") " + estatica.Nombre + " " + estatica.Ape + ", con ENROLL ONE.";
                string envio = "Fecha: " + DateTime.Now.ToString("dd MMMM yyyy, hh:mm:ss tt");

                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _standardFont22 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _standardFont2 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _standardFont23 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font cabe = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                // Creamos una tabla que contendrá el nombre, apellido y país
                // de nuestros visitante.
                PdfPTable tb1 = new PdfPTable(2);
                tb1.WidthPercentage = 100;


                //agregando una imagen
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("logo.jpg");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_CENTER;
                float percentage = 0.0f;
                percentage = 100 / imagen.Width;
                imagen.ScalePercent(percentage * 100);
                imagen.SetAbsolutePosition(20, 730);
                doc.Add(imagen);
                //PdfImage img = new PdfImage(iTextSharp.text.Image.
                PdfPCell ff2 = new PdfPCell();
                /*ff2.BorderWidth = 0;
                ff2.Colspan = 2;*/
                //tb1.AddCell(imagen);


                // Configuramos el título de las columnas de la tabla
                /*PdfPCell ff1 = new PdfPCell(new Phrase("\nRepública Bolivariana de Venezuela\nCentro de Adiestramiento Profesional para el Desarrollo de los Sistemas Informáticos y Afines \nTelf.: 0291-8966131/ 0414-7679330\nMaturín - Estado Monagas\nRIF: J-406338516", _standardFont23));
                ff1.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff1.BorderWidth = 0;*/


                // Añadimos las celdas a la tabla
                //tb1.AddCell(ff2);
                //tb1.AddCell(ff1);


                // Configuramos el título de las columnas de la tabla
                doc.Add(new Paragraph("                       "));
                doc.Add(new Paragraph("                       "));

                Chunk chunk = new Chunk("\n\n\n\nPOSTULACIÓN PARA PASANTÍAS.", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                ff2 = new PdfPCell(new Paragraph(chunk));
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff2.Padding = 4;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                Phrase p2 = new Phrase();

                ff2 = new PdfPCell();
                Chunk c1 = new Chunk("\n\nDIRIGIDA A:\n" , _standardFont2);
                Chunk c1x = new Chunk(textBox1.Text + "\n" + comboBox4.Text + " " + textBox10.Text, _standardFont22);
                Chunk c8 = new Chunk("", _standardFont22);
                Chunk c1xxx = new Chunk("\nPresente.-\n\n          Es grato dirigirme a usted,  en la oportunidad de presentarle a él o (la) Estudiante ", _standardFont2);
                ab = ab.ToLower();
                Chunk c2 = new Chunk(myTI.ToTitleCase(ab), _standardFont22);
                Chunk c3 = new Chunk(". Titular de la Cédula de Identidad: Nº ", _standardFont2);
                Chunk c4 = new Chunk(ci, _standardFont22);
                Chunk c5 = new Chunk(", quien fue estudiante regular de la Institución CAPEDSIA, C.A, y en la misma cursó el periodo " , _standardFont2);
                Chunk c6 = new Chunk(textBox8.Text, _standardFont22);
                Chunk c7 = new Chunk(" del "+ tm +": ", _standardFont2);
                Chunk c7x = new Chunk(comboBox2.Text, _standardFont22);
                //Chunk c8 = new Chunk("" , _standardFont22);
                //DateTime ent = DateTime.Parse(textBox9.Text);
                Chunk c9 = new Chunk(", con un promedio de ", _standardFont2);
                Chunk c8x = new Chunk(textBox9.Text + " puntos", _standardFont22);
                Chunk c9x = new Chunk(", en una escala valorativa de ", _standardFont2);
                string fe = "\n\nConstancia que se expide en la ciudad Maturín, ";
                if (DateTime.Now.ToString("dd") == "01")
                {
                    fe = fe + "el día 01 del mes " + DateTime.Now.ToString("MMMM") + " del " + DateTime.Now.ToString("yyyy") + ".";
                }
                else
                {
                    fe = fe + "a los " + DateTime.Now.ToString("dd") + " días del mes " + DateTime.Now.ToString("MMMM") + " del " + DateTime.Now.ToString("yyyy") + ".";
                }
                Chunk c10 = new Chunk("1 a 20 puntos", _standardFont22);
                Chunk c11 = new Chunk(", quien desea realizar su pasantía Ocupacional-Profesional en esa Institución. El periodo de duración de la pasantía será de mínimo " + numericUpDown1.Value.ToString() + " " + comboBox1.Text + ". A partir de la fecha estipulada por ustedes.\n\nAgradezco de antemano sus mejores oficios para la realización de esta labor de interacción.\nSin otro particular a que hacer referencia, quedo de Usted."+fe+"\n\nAtentamente.", _standardFont2);
                p2.Add(c1);
                p2.Add(c1x);
                p2.Add(c8);
                p2.Add(c1xxx);
                p2.Add(c2);
                p2.Add(c3);
                p2.Add(c4);
                p2.Add(c5);
                p2.Add(c6);
                p2.Add(c7);
                p2.Add(c7x);
                p2.Add(c9);
                p2.Add(c8x);
                p2.Add(c9x);
                p2.Add(c10);
                p2.Add(c11);
                Paragraph xa = new Paragraph(p2);

                //DateTime fin = DateTime.Parse("");
               
                //c3 = new Chunk(" (" + enletras(textBox1.Text) + ") semana(s) de clase(s), y debe cumplir de forma obligatoria para otorgar certificado de culminación del " + tm + ", esta constancia expira el " + fin.ToString("dd") + " de " + fin.ToString("MMMM") + " del " + fin.ToString("yyyy") + "." + fe, _standardFont2);
                c2 = new Chunk("", _standardFont2);
                p2.Add(c2);
                xa = new Paragraph(p2);
                xa.SetLeading(20, 0);
                xa.Alignment = Element.ALIGN_JUSTIFIED;
                ff2.AddElement(xa);
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);

                Phrase p1 = new Phrase();

                ff2 = new PdfPCell();
                bc = bc.ToLower();


                Chunk c1xee = new Chunk("\n\n\n\n\n________________________________\n" + myTI.ToTitleCase(bc), _standardFont2);
                Chunk c3x = new Chunk("\n" + textBox4.Text.ToUpper(), _standardFont22);
                p1.Add(c1xee);
                p1.Add(c3x);
                Paragraph x1 = new Paragraph(p1);
                x1.SetLeading(20, 0);
                x1.Alignment = Element.ALIGN_CENTER;
                ff2.AddElement(x1);
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                float[] medidaCeldas = { 0.25f, 2.85f };

                // ASIGNAS LAS MEDIDAS A LA TABLA (ANCHO)
                tb1.SetWidths(medidaCeldas);

                // Finalmente, añadimos la tabla al documento PDF y cerramos el documento
                doc.Add(tb1);

                //doc.Add(new Paragraph("                       "));


                doc.Add(new Paragraph("                       "));

                // Crea la imagen
                iTextSharp.text.Image imagen2 = iTextSharp.text.Image.GetInstance("fondo.jpg");
                // Cambia el tamaño de la imagen
                imagen2.ScaleToFit(700, 250);
                // Se indica que la imagen debe almacenarse como fondo
                imagen2.Alignment = iTextSharp.text.Image.UNDERLYING;
                // Coloca la imagen en una posición absoluta
                imagen2.SetAbsolutePosition(150, 280);
                // Imprime la imagen como fondo de página
                doc.Add(imagen2);



                doc.AddCreationDate();

                doc.Close();
                Process.Start(filename);//Esta parte se puede omitir, si solo se desea guardar el archivo, y que este no se ejecute al instante
            }

        }
        iTextSharp.text.Font _standardFont4 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        iTextSharp.text.Font _standardFont5 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);

        public float[] GetTamañoColumnas(DataGridView dg)
        {
            float[] values = new float[dg.ColumnCount];
            for (int i = 0; i < dg.ColumnCount; i++)
            {
                values[i] = (float)dg.Columns[i].Width;
            }
            return values;

        }
        #endregion

        private void button8_Click(object sender, EventArgs e)
        {
            To_pdf();
        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        
    }
}
