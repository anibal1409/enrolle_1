﻿namespace SIAC
{
    partial class abonos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(abonos));
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.académicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarModificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarAEstudianteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disciplinasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.disponiblesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resumenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaDeEstudiantesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.formalizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cambioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.periodoAcadémicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preinscripciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.pendientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estudianteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empleadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeTrabajoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeActividadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.finanzasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bancoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.pagosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarMiContraseñaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deduccionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.reciboDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitudToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónDeCalificacionesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónEgredadosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónRegularesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.listarCertificacionesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeEstudioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.postulaciónParaPasantíasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.usuarioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarRestContraseñaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem84 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem114 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem115 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem116 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem117 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem118 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem119 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem120 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem121 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem136 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem137 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem138 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem139 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem140 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem141 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem142 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem143 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem144 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem145 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem163 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem164 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem165 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem166 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.menuStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(302, 19);
            this.label1.TabIndex = 27;
            this.label1.Text = "Ingrese el nombre del asignación a registrar";
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(470, 49);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(44, 44);
            this.button1.TabIndex = 26;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridView1.Location = new System.Drawing.Point(11, 99);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(503, 158);
            this.dataGridView1.TabIndex = 25;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Cod";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Asignación";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 270;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Valor Unitario";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 58);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(302, 26);
            this.textBox1.TabIndex = 24;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(320, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 19);
            this.label2.TabIndex = 29;
            this.label2.Text = "Valor unitario";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(320, 58);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(144, 26);
            this.textBox2.TabIndex = 28;
            this.textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem,
            this.académicoToolStripMenuItem,
            this.estudianteToolStripMenuItem,
            this.empleadoToolStripMenuItem,
            this.finanzasToolStripMenuItem,
            this.perfilToolStripMenuItem,
            this.salarioToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(529, 24);
            this.menuStrip1.TabIndex = 30;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.inicioToolStripMenuItem.Text = "Inicio";
            this.inicioToolStripMenuItem.Click += new System.EventHandler(this.inicioToolStripMenuItem_Click);
            // 
            // académicoToolStripMenuItem
            // 
            this.académicoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calificacionesToolStripMenuItem,
            this.disciplinasToolStripMenuItem,
            this.formalizaciónToolStripMenuItem,
            this.periodoAcadémicoToolStripMenuItem,
            this.preinscripciónToolStripMenuItem});
            this.académicoToolStripMenuItem.Name = "académicoToolStripMenuItem";
            this.académicoToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.académicoToolStripMenuItem.Text = "Académico";
            // 
            // calificacionesToolStripMenuItem
            // 
            this.calificacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarModificarToolStripMenuItem,
            this.modificarAEstudianteToolStripMenuItem});
            this.calificacionesToolStripMenuItem.Name = "calificacionesToolStripMenuItem";
            this.calificacionesToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.calificacionesToolStripMenuItem.Text = "Calificaciones";
            // 
            // cargarModificarToolStripMenuItem
            // 
            this.cargarModificarToolStripMenuItem.Name = "cargarModificarToolStripMenuItem";
            this.cargarModificarToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.cargarModificarToolStripMenuItem.Text = "Cargar/Modificar";
            this.cargarModificarToolStripMenuItem.Click += new System.EventHandler(this.cargarModificarToolStripMenuItem_Click);
            // 
            // modificarAEstudianteToolStripMenuItem
            // 
            this.modificarAEstudianteToolStripMenuItem.Name = "modificarAEstudianteToolStripMenuItem";
            this.modificarAEstudianteToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.modificarAEstudianteToolStripMenuItem.Text = "Modificar a Estudiante";
            this.modificarAEstudianteToolStripMenuItem.Click += new System.EventHandler(this.modificarAEstudianteToolStripMenuItem_Click);
            // 
            // disciplinasToolStripMenuItem
            // 
            this.disciplinasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem4,
            this.modificarToolStripMenuItem3,
            this.disponiblesToolStripMenuItem,
            this.resumenToolStripMenuItem,
            this.listaDeEstudiantesToolStripMenuItem,
            this.tiposToolStripMenuItem});
            this.disciplinasToolStripMenuItem.Name = "disciplinasToolStripMenuItem";
            this.disciplinasToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.disciplinasToolStripMenuItem.Text = "Disciplina";
            // 
            // registrarToolStripMenuItem4
            // 
            this.registrarToolStripMenuItem4.Name = "registrarToolStripMenuItem4";
            this.registrarToolStripMenuItem4.Size = new System.Drawing.Size(182, 22);
            this.registrarToolStripMenuItem4.Text = "Registrar";
            this.registrarToolStripMenuItem4.Click += new System.EventHandler(this.registrarToolStripMenuItem4_Click);
            // 
            // modificarToolStripMenuItem3
            // 
            this.modificarToolStripMenuItem3.Name = "modificarToolStripMenuItem3";
            this.modificarToolStripMenuItem3.Size = new System.Drawing.Size(182, 22);
            this.modificarToolStripMenuItem3.Text = "Modificar";
            this.modificarToolStripMenuItem3.Click += new System.EventHandler(this.modificarToolStripMenuItem3_Click);
            // 
            // disponiblesToolStripMenuItem
            // 
            this.disponiblesToolStripMenuItem.Name = "disponiblesToolStripMenuItem";
            this.disponiblesToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.disponiblesToolStripMenuItem.Text = "Disponibles";
            this.disponiblesToolStripMenuItem.Click += new System.EventHandler(this.disponiblesToolStripMenuItem_Click);
            // 
            // resumenToolStripMenuItem
            // 
            this.resumenToolStripMenuItem.Name = "resumenToolStripMenuItem";
            this.resumenToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.resumenToolStripMenuItem.Text = "Resumen";
            this.resumenToolStripMenuItem.Click += new System.EventHandler(this.resumenToolStripMenuItem_Click);
            // 
            // listaDeEstudiantesToolStripMenuItem
            // 
            this.listaDeEstudiantesToolStripMenuItem.Name = "listaDeEstudiantesToolStripMenuItem";
            this.listaDeEstudiantesToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.listaDeEstudiantesToolStripMenuItem.Text = "Lista de Estudiantes";
            this.listaDeEstudiantesToolStripMenuItem.Click += new System.EventHandler(this.listaDeEstudiantesToolStripMenuItem_Click);
            // 
            // tiposToolStripMenuItem
            // 
            this.tiposToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem5,
            this.modificarToolStripMenuItem4});
            this.tiposToolStripMenuItem.Name = "tiposToolStripMenuItem";
            this.tiposToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.tiposToolStripMenuItem.Text = "Tipos";
            // 
            // registrarToolStripMenuItem5
            // 
            this.registrarToolStripMenuItem5.Name = "registrarToolStripMenuItem5";
            this.registrarToolStripMenuItem5.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem5.Text = "Registrar";
            this.registrarToolStripMenuItem5.Click += new System.EventHandler(this.registrarToolStripMenuItem5_Click);
            // 
            // modificarToolStripMenuItem4
            // 
            this.modificarToolStripMenuItem4.Name = "modificarToolStripMenuItem4";
            this.modificarToolStripMenuItem4.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem4.Text = "Modificar";
            this.modificarToolStripMenuItem4.Click += new System.EventHandler(this.modificarToolStripMenuItem4_Click);
            // 
            // formalizaciónToolStripMenuItem
            // 
            this.formalizaciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem6,
            this.modificarToolStripMenuItem5,
            this.cambioToolStripMenuItem});
            this.formalizaciónToolStripMenuItem.Name = "formalizaciónToolStripMenuItem";
            this.formalizaciónToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.formalizaciónToolStripMenuItem.Text = "Formalización";
            // 
            // registrarToolStripMenuItem6
            // 
            this.registrarToolStripMenuItem6.Name = "registrarToolStripMenuItem6";
            this.registrarToolStripMenuItem6.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem6.Text = "Registrar";
            this.registrarToolStripMenuItem6.Click += new System.EventHandler(this.registrarToolStripMenuItem6_Click);
            // 
            // modificarToolStripMenuItem5
            // 
            this.modificarToolStripMenuItem5.Name = "modificarToolStripMenuItem5";
            this.modificarToolStripMenuItem5.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem5.Text = "Modificar";
            this.modificarToolStripMenuItem5.Click += new System.EventHandler(this.modificarToolStripMenuItem5_Click);
            // 
            // cambioToolStripMenuItem
            // 
            this.cambioToolStripMenuItem.Name = "cambioToolStripMenuItem";
            this.cambioToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.cambioToolStripMenuItem.Text = "Cambio";
            this.cambioToolStripMenuItem.Click += new System.EventHandler(this.cambioToolStripMenuItem_Click);
            // 
            // periodoAcadémicoToolStripMenuItem
            // 
            this.periodoAcadémicoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem7,
            this.modificarToolStripMenuItem6,
            this.cerrarToolStripMenuItem});
            this.periodoAcadémicoToolStripMenuItem.Name = "periodoAcadémicoToolStripMenuItem";
            this.periodoAcadémicoToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.periodoAcadémicoToolStripMenuItem.Text = "Periodo Académico";
            // 
            // registrarToolStripMenuItem7
            // 
            this.registrarToolStripMenuItem7.Name = "registrarToolStripMenuItem7";
            this.registrarToolStripMenuItem7.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem7.Text = "Registrar";
            this.registrarToolStripMenuItem7.Click += new System.EventHandler(this.registrarToolStripMenuItem7_Click);
            // 
            // modificarToolStripMenuItem6
            // 
            this.modificarToolStripMenuItem6.Name = "modificarToolStripMenuItem6";
            this.modificarToolStripMenuItem6.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem6.Text = "Modificar";
            this.modificarToolStripMenuItem6.Click += new System.EventHandler(this.modificarToolStripMenuItem6_Click);
            // 
            // cerrarToolStripMenuItem
            // 
            this.cerrarToolStripMenuItem.Name = "cerrarToolStripMenuItem";
            this.cerrarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.cerrarToolStripMenuItem.Text = "Cerrar";
            this.cerrarToolStripMenuItem.Click += new System.EventHandler(this.cerrarToolStripMenuItem_Click);
            // 
            // preinscripciónToolStripMenuItem
            // 
            this.preinscripciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem8,
            this.modificarToolStripMenuItem7,
            this.pendientesToolStripMenuItem});
            this.preinscripciónToolStripMenuItem.Name = "preinscripciónToolStripMenuItem";
            this.preinscripciónToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.preinscripciónToolStripMenuItem.Text = "Preinscripción";
            // 
            // registrarToolStripMenuItem8
            // 
            this.registrarToolStripMenuItem8.Name = "registrarToolStripMenuItem8";
            this.registrarToolStripMenuItem8.Size = new System.Drawing.Size(133, 22);
            this.registrarToolStripMenuItem8.Text = "Registrar";
            this.registrarToolStripMenuItem8.Click += new System.EventHandler(this.registrarToolStripMenuItem8_Click);
            // 
            // modificarToolStripMenuItem7
            // 
            this.modificarToolStripMenuItem7.Name = "modificarToolStripMenuItem7";
            this.modificarToolStripMenuItem7.Size = new System.Drawing.Size(133, 22);
            this.modificarToolStripMenuItem7.Text = "Modificar";
            this.modificarToolStripMenuItem7.Click += new System.EventHandler(this.modificarToolStripMenuItem7_Click);
            // 
            // pendientesToolStripMenuItem
            // 
            this.pendientesToolStripMenuItem.Name = "pendientesToolStripMenuItem";
            this.pendientesToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.pendientesToolStripMenuItem.Text = "Pendientes";
            this.pendientesToolStripMenuItem.Click += new System.EventHandler(this.pendientesToolStripMenuItem_Click);
            // 
            // estudianteToolStripMenuItem
            // 
            this.estudianteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem,
            this.modificarToolStripMenuItem,
            this.historialToolStripMenuItem});
            this.estudianteToolStripMenuItem.Name = "estudianteToolStripMenuItem";
            this.estudianteToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.estudianteToolStripMenuItem.Text = "Estudiante";
            // 
            // registrarToolStripMenuItem
            // 
            this.registrarToolStripMenuItem.Name = "registrarToolStripMenuItem";
            this.registrarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem.Text = "Registrar";
            this.registrarToolStripMenuItem.Click += new System.EventHandler(this.registrarToolStripMenuItem_Click_1);
            // 
            // modificarToolStripMenuItem
            // 
            this.modificarToolStripMenuItem.Name = "modificarToolStripMenuItem";
            this.modificarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem.Text = "Modificar";
            this.modificarToolStripMenuItem.Click += new System.EventHandler(this.modificarToolStripMenuItem_Click);
            // 
            // historialToolStripMenuItem
            // 
            this.historialToolStripMenuItem.Name = "historialToolStripMenuItem";
            this.historialToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.historialToolStripMenuItem.Text = "Historial";
            this.historialToolStripMenuItem.Click += new System.EventHandler(this.historialToolStripMenuItem_Click_1);
            // 
            // empleadoToolStripMenuItem
            // 
            this.empleadoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem9,
            this.modificarToolStripMenuItem8,
            this.constanciaDeTrabajoToolStripMenuItem,
            this.constanciaDeActividadToolStripMenuItem,
            this.cargosToolStripMenuItem});
            this.empleadoToolStripMenuItem.Name = "empleadoToolStripMenuItem";
            this.empleadoToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.empleadoToolStripMenuItem.Text = "Empleado";
            // 
            // registrarToolStripMenuItem9
            // 
            this.registrarToolStripMenuItem9.Name = "registrarToolStripMenuItem9";
            this.registrarToolStripMenuItem9.Size = new System.Drawing.Size(205, 22);
            this.registrarToolStripMenuItem9.Text = "Registrar";
            this.registrarToolStripMenuItem9.Click += new System.EventHandler(this.registrarToolStripMenuItem9_Click);
            // 
            // modificarToolStripMenuItem8
            // 
            this.modificarToolStripMenuItem8.Name = "modificarToolStripMenuItem8";
            this.modificarToolStripMenuItem8.Size = new System.Drawing.Size(205, 22);
            this.modificarToolStripMenuItem8.Text = "Modificar";
            this.modificarToolStripMenuItem8.Click += new System.EventHandler(this.modificarToolStripMenuItem8_Click);
            // 
            // constanciaDeTrabajoToolStripMenuItem
            // 
            this.constanciaDeTrabajoToolStripMenuItem.Name = "constanciaDeTrabajoToolStripMenuItem";
            this.constanciaDeTrabajoToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.constanciaDeTrabajoToolStripMenuItem.Text = "Constancia de Trabajo";
            this.constanciaDeTrabajoToolStripMenuItem.Click += new System.EventHandler(this.constanciaDeTrabajoToolStripMenuItem_Click);
            // 
            // constanciaDeActividadToolStripMenuItem
            // 
            this.constanciaDeActividadToolStripMenuItem.Name = "constanciaDeActividadToolStripMenuItem";
            this.constanciaDeActividadToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.constanciaDeActividadToolStripMenuItem.Text = "Constancia de Actividad";
            this.constanciaDeActividadToolStripMenuItem.Click += new System.EventHandler(this.constanciaDeActividadToolStripMenuItem_Click);
            // 
            // cargosToolStripMenuItem
            // 
            this.cargosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem10,
            this.modificarToolStripMenuItem9});
            this.cargosToolStripMenuItem.Name = "cargosToolStripMenuItem";
            this.cargosToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.cargosToolStripMenuItem.Text = "Cargos";
            // 
            // registrarToolStripMenuItem10
            // 
            this.registrarToolStripMenuItem10.Name = "registrarToolStripMenuItem10";
            this.registrarToolStripMenuItem10.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem10.Text = "Registrar";
            this.registrarToolStripMenuItem10.Click += new System.EventHandler(this.registrarToolStripMenuItem10_Click);
            // 
            // modificarToolStripMenuItem9
            // 
            this.modificarToolStripMenuItem9.Name = "modificarToolStripMenuItem9";
            this.modificarToolStripMenuItem9.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem9.Text = "Modificar";
            this.modificarToolStripMenuItem9.Click += new System.EventHandler(this.modificarToolStripMenuItem9_Click);
            // 
            // finanzasToolStripMenuItem
            // 
            this.finanzasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bancoToolStripMenuItem,
            this.pagosToolStripMenuItem,
            this.preciosToolStripMenuItem});
            this.finanzasToolStripMenuItem.Name = "finanzasToolStripMenuItem";
            this.finanzasToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.finanzasToolStripMenuItem.Text = "Finanzas";
            // 
            // bancoToolStripMenuItem
            // 
            this.bancoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem11,
            this.modificarToolStripMenuItem10});
            this.bancoToolStripMenuItem.Name = "bancoToolStripMenuItem";
            this.bancoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.bancoToolStripMenuItem.Text = "Banco";
            // 
            // registrarToolStripMenuItem11
            // 
            this.registrarToolStripMenuItem11.Name = "registrarToolStripMenuItem11";
            this.registrarToolStripMenuItem11.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem11.Text = "Registrar";
            this.registrarToolStripMenuItem11.Click += new System.EventHandler(this.registrarToolStripMenuItem11_Click);
            // 
            // modificarToolStripMenuItem10
            // 
            this.modificarToolStripMenuItem10.Name = "modificarToolStripMenuItem10";
            this.modificarToolStripMenuItem10.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem10.Text = "Modificar";
            this.modificarToolStripMenuItem10.Click += new System.EventHandler(this.modificarToolStripMenuItem10_Click);
            // 
            // pagosToolStripMenuItem
            // 
            this.pagosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem12,
            this.consultarToolStripMenuItem,
            this.modificarToolStripMenuItem11,
            this.verificarToolStripMenuItem,
            this.listaToolStripMenuItem});
            this.pagosToolStripMenuItem.Name = "pagosToolStripMenuItem";
            this.pagosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pagosToolStripMenuItem.Text = "Pagos";
            // 
            // registrarToolStripMenuItem12
            // 
            this.registrarToolStripMenuItem12.Name = "registrarToolStripMenuItem12";
            this.registrarToolStripMenuItem12.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem12.Text = "Registrar";
            this.registrarToolStripMenuItem12.Click += new System.EventHandler(this.registrarToolStripMenuItem12_Click);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.consultarToolStripMenuItem.Text = "Consultar";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
            // 
            // modificarToolStripMenuItem11
            // 
            this.modificarToolStripMenuItem11.Name = "modificarToolStripMenuItem11";
            this.modificarToolStripMenuItem11.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem11.Text = "Modificar";
            this.modificarToolStripMenuItem11.Click += new System.EventHandler(this.modificarToolStripMenuItem11_Click);
            // 
            // verificarToolStripMenuItem
            // 
            this.verificarToolStripMenuItem.Name = "verificarToolStripMenuItem";
            this.verificarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.verificarToolStripMenuItem.Text = "Verificar";
            this.verificarToolStripMenuItem.Click += new System.EventHandler(this.verificarToolStripMenuItem_Click);
            // 
            // listaToolStripMenuItem
            // 
            this.listaToolStripMenuItem.Name = "listaToolStripMenuItem";
            this.listaToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.listaToolStripMenuItem.Text = "Lista";
            this.listaToolStripMenuItem.Click += new System.EventHandler(this.listaToolStripMenuItem_Click);
            // 
            // preciosToolStripMenuItem
            // 
            this.preciosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem13,
            this.consultarToolStripMenuItem1,
            this.modificarToolStripMenuItem12});
            this.preciosToolStripMenuItem.Name = "preciosToolStripMenuItem";
            this.preciosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.preciosToolStripMenuItem.Text = "Precios";
            // 
            // registrarToolStripMenuItem13
            // 
            this.registrarToolStripMenuItem13.Name = "registrarToolStripMenuItem13";
            this.registrarToolStripMenuItem13.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem13.Text = "Registrar";
            this.registrarToolStripMenuItem13.Click += new System.EventHandler(this.registrarToolStripMenuItem13_Click);
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.consultarToolStripMenuItem1.Text = "Consultar";
            this.consultarToolStripMenuItem1.Click += new System.EventHandler(this.consultarToolStripMenuItem1_Click);
            // 
            // modificarToolStripMenuItem12
            // 
            this.modificarToolStripMenuItem12.Name = "modificarToolStripMenuItem12";
            this.modificarToolStripMenuItem12.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem12.Text = "Modificar";
            this.modificarToolStripMenuItem12.Click += new System.EventHandler(this.modificarToolStripMenuItem12_Click);
            // 
            // perfilToolStripMenuItem
            // 
            this.perfilToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificarMiContraseñaToolStripMenuItem});
            this.perfilToolStripMenuItem.Name = "perfilToolStripMenuItem";
            this.perfilToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.perfilToolStripMenuItem.Text = "Perfil";
            // 
            // modificarMiContraseñaToolStripMenuItem
            // 
            this.modificarMiContraseñaToolStripMenuItem.Name = "modificarMiContraseñaToolStripMenuItem";
            this.modificarMiContraseñaToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.modificarMiContraseñaToolStripMenuItem.Text = "Modificar mi contraseña";
            this.modificarMiContraseñaToolStripMenuItem.Click += new System.EventHandler(this.modificarMiContraseñaToolStripMenuItem_Click);
            // 
            // salarioToolStripMenuItem
            // 
            this.salarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asignacionesToolStripMenuItem,
            this.deduccionesToolStripMenuItem,
            this.reciboDePagoToolStripMenuItem});
            this.salarioToolStripMenuItem.Name = "salarioToolStripMenuItem";
            this.salarioToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.salarioToolStripMenuItem.Text = "Salario";
            // 
            // asignacionesToolStripMenuItem
            // 
            this.asignacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem2,
            this.modificarToolStripMenuItem1});
            this.asignacionesToolStripMenuItem.Name = "asignacionesToolStripMenuItem";
            this.asignacionesToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.asignacionesToolStripMenuItem.Text = "Asignaciones";
            // 
            // registrarToolStripMenuItem2
            // 
            this.registrarToolStripMenuItem2.Name = "registrarToolStripMenuItem2";
            this.registrarToolStripMenuItem2.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem2.Text = "Registrar";
            this.registrarToolStripMenuItem2.Click += new System.EventHandler(this.registrarToolStripMenuItem2_Click);
            // 
            // modificarToolStripMenuItem1
            // 
            this.modificarToolStripMenuItem1.Name = "modificarToolStripMenuItem1";
            this.modificarToolStripMenuItem1.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem1.Text = "Modificar";
            this.modificarToolStripMenuItem1.Click += new System.EventHandler(this.modificarToolStripMenuItem1_Click);
            // 
            // deduccionesToolStripMenuItem
            // 
            this.deduccionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem3,
            this.modificarToolStripMenuItem2});
            this.deduccionesToolStripMenuItem.Name = "deduccionesToolStripMenuItem";
            this.deduccionesToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.deduccionesToolStripMenuItem.Text = "Deducciones";
            // 
            // registrarToolStripMenuItem3
            // 
            this.registrarToolStripMenuItem3.Name = "registrarToolStripMenuItem3";
            this.registrarToolStripMenuItem3.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem3.Text = "Registrar";
            this.registrarToolStripMenuItem3.Click += new System.EventHandler(this.registrarToolStripMenuItem3_Click);
            // 
            // modificarToolStripMenuItem2
            // 
            this.modificarToolStripMenuItem2.Name = "modificarToolStripMenuItem2";
            this.modificarToolStripMenuItem2.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem2.Text = "Modificar";
            this.modificarToolStripMenuItem2.Click += new System.EventHandler(this.modificarToolStripMenuItem2_Click);
            // 
            // reciboDePagoToolStripMenuItem
            // 
            this.reciboDePagoToolStripMenuItem.Name = "reciboDePagoToolStripMenuItem";
            this.reciboDePagoToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.reciboDePagoToolStripMenuItem.Text = "Recibo de Pago";
            this.reciboDePagoToolStripMenuItem.Click += new System.EventHandler(this.reciboDePagoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.solicitudToolStripMenuItem1,
            this.usuarioToolStripMenuItem1,
            this.salirToolStripMenuItem1});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(26, 20);
            this.toolStripMenuItem1.Text = "+";
            // 
            // solicitudToolStripMenuItem1
            // 
            this.solicitudToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.certificaciónDeCalificacionesToolStripMenuItem1,
            this.certificaciónEgredadosToolStripMenuItem1,
            this.certificaciónRegularesToolStripMenuItem1,
            this.listarCertificacionesToolStripMenuItem1,
            this.constanciaDeEstudioToolStripMenuItem1,
            this.postulaciónParaPasantíasToolStripMenuItem1});
            this.solicitudToolStripMenuItem1.Name = "solicitudToolStripMenuItem1";
            this.solicitudToolStripMenuItem1.Size = new System.Drawing.Size(121, 22);
            this.solicitudToolStripMenuItem1.Text = "Solicitud";
            // 
            // certificaciónDeCalificacionesToolStripMenuItem1
            // 
            this.certificaciónDeCalificacionesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem20,
            this.modificarToolStripMenuItem18});
            this.certificaciónDeCalificacionesToolStripMenuItem1.Name = "certificaciónDeCalificacionesToolStripMenuItem1";
            this.certificaciónDeCalificacionesToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.certificaciónDeCalificacionesToolStripMenuItem1.Text = "Certificación de Calificaciones";
            // 
            // registrarToolStripMenuItem20
            // 
            this.registrarToolStripMenuItem20.Name = "registrarToolStripMenuItem20";
            this.registrarToolStripMenuItem20.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem20.Text = "Registrar";
            this.registrarToolStripMenuItem20.Click += new System.EventHandler(this.registrarToolStripMenuItem20_Click);
            // 
            // modificarToolStripMenuItem18
            // 
            this.modificarToolStripMenuItem18.Name = "modificarToolStripMenuItem18";
            this.modificarToolStripMenuItem18.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem18.Text = "Modificar";
            this.modificarToolStripMenuItem18.Click += new System.EventHandler(this.modificarToolStripMenuItem18_Click);
            // 
            // certificaciónEgredadosToolStripMenuItem1
            // 
            this.certificaciónEgredadosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem21,
            this.modificarToolStripMenuItem19});
            this.certificaciónEgredadosToolStripMenuItem1.Name = "certificaciónEgredadosToolStripMenuItem1";
            this.certificaciónEgredadosToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.certificaciónEgredadosToolStripMenuItem1.Text = "Certificación Egredados";
            // 
            // registrarToolStripMenuItem21
            // 
            this.registrarToolStripMenuItem21.Name = "registrarToolStripMenuItem21";
            this.registrarToolStripMenuItem21.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem21.Text = "Registrar";
            this.registrarToolStripMenuItem21.Click += new System.EventHandler(this.registrarToolStripMenuItem21_Click);
            // 
            // modificarToolStripMenuItem19
            // 
            this.modificarToolStripMenuItem19.Name = "modificarToolStripMenuItem19";
            this.modificarToolStripMenuItem19.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem19.Text = "Modificar";
            this.modificarToolStripMenuItem19.Click += new System.EventHandler(this.modificarToolStripMenuItem19_Click);
            // 
            // certificaciónRegularesToolStripMenuItem1
            // 
            this.certificaciónRegularesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem22,
            this.modificarToolStripMenuItem20});
            this.certificaciónRegularesToolStripMenuItem1.Name = "certificaciónRegularesToolStripMenuItem1";
            this.certificaciónRegularesToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.certificaciónRegularesToolStripMenuItem1.Text = "Certificación Regulares";
            // 
            // registrarToolStripMenuItem22
            // 
            this.registrarToolStripMenuItem22.Name = "registrarToolStripMenuItem22";
            this.registrarToolStripMenuItem22.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem22.Text = "Registrar";
            this.registrarToolStripMenuItem22.Click += new System.EventHandler(this.registrarToolStripMenuItem22_Click);
            // 
            // modificarToolStripMenuItem20
            // 
            this.modificarToolStripMenuItem20.Name = "modificarToolStripMenuItem20";
            this.modificarToolStripMenuItem20.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem20.Text = "Modificar";
            this.modificarToolStripMenuItem20.Click += new System.EventHandler(this.modificarToolStripMenuItem20_Click);
            // 
            // listarCertificacionesToolStripMenuItem1
            // 
            this.listarCertificacionesToolStripMenuItem1.Name = "listarCertificacionesToolStripMenuItem1";
            this.listarCertificacionesToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.listarCertificacionesToolStripMenuItem1.Text = "Listar Certificaciones";
            this.listarCertificacionesToolStripMenuItem1.Click += new System.EventHandler(this.listarCertificacionesToolStripMenuItem1_Click);
            // 
            // constanciaDeEstudioToolStripMenuItem1
            // 
            this.constanciaDeEstudioToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem23,
            this.modificarToolStripMenuItem21});
            this.constanciaDeEstudioToolStripMenuItem1.Name = "constanciaDeEstudioToolStripMenuItem1";
            this.constanciaDeEstudioToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.constanciaDeEstudioToolStripMenuItem1.Text = "Constancia de Estudio";
            // 
            // registrarToolStripMenuItem23
            // 
            this.registrarToolStripMenuItem23.Name = "registrarToolStripMenuItem23";
            this.registrarToolStripMenuItem23.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem23.Text = "Registrar";
            this.registrarToolStripMenuItem23.Click += new System.EventHandler(this.registrarToolStripMenuItem23_Click);
            // 
            // modificarToolStripMenuItem21
            // 
            this.modificarToolStripMenuItem21.Name = "modificarToolStripMenuItem21";
            this.modificarToolStripMenuItem21.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem21.Text = "Modificar";
            this.modificarToolStripMenuItem21.Click += new System.EventHandler(this.modificarToolStripMenuItem21_Click);
            // 
            // postulaciónParaPasantíasToolStripMenuItem1
            // 
            this.postulaciónParaPasantíasToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem24,
            this.modificarToolStripMenuItem22});
            this.postulaciónParaPasantíasToolStripMenuItem1.Name = "postulaciónParaPasantíasToolStripMenuItem1";
            this.postulaciónParaPasantíasToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.postulaciónParaPasantíasToolStripMenuItem1.Text = "Postulación para Pasantías";
            // 
            // registrarToolStripMenuItem24
            // 
            this.registrarToolStripMenuItem24.Name = "registrarToolStripMenuItem24";
            this.registrarToolStripMenuItem24.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem24.Text = "Registrar";
            this.registrarToolStripMenuItem24.Click += new System.EventHandler(this.registrarToolStripMenuItem24_Click);
            // 
            // modificarToolStripMenuItem22
            // 
            this.modificarToolStripMenuItem22.Name = "modificarToolStripMenuItem22";
            this.modificarToolStripMenuItem22.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem22.Text = "Modificar";
            this.modificarToolStripMenuItem22.Click += new System.EventHandler(this.modificarToolStripMenuItem22_Click);
            // 
            // usuarioToolStripMenuItem1
            // 
            this.usuarioToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem19,
            this.modificarRestContraseñaToolStripMenuItem1});
            this.usuarioToolStripMenuItem1.Name = "usuarioToolStripMenuItem1";
            this.usuarioToolStripMenuItem1.Size = new System.Drawing.Size(121, 22);
            this.usuarioToolStripMenuItem1.Text = "Usuario";
            // 
            // registrarToolStripMenuItem19
            // 
            this.registrarToolStripMenuItem19.Name = "registrarToolStripMenuItem19";
            this.registrarToolStripMenuItem19.Size = new System.Drawing.Size(221, 22);
            this.registrarToolStripMenuItem19.Text = "Registrar";
            this.registrarToolStripMenuItem19.Click += new System.EventHandler(this.registrarToolStripMenuItem19_Click);
            // 
            // modificarRestContraseñaToolStripMenuItem1
            // 
            this.modificarRestContraseñaToolStripMenuItem1.Name = "modificarRestContraseñaToolStripMenuItem1";
            this.modificarRestContraseñaToolStripMenuItem1.Size = new System.Drawing.Size(221, 22);
            this.modificarRestContraseñaToolStripMenuItem1.Text = "Modificar/Rest. Contraseña";
            this.modificarRestContraseñaToolStripMenuItem1.Click += new System.EventHandler(this.modificarRestContraseñaToolStripMenuItem1_Click);
            // 
            // salirToolStripMenuItem1
            // 
            this.salirToolStripMenuItem1.Name = "salirToolStripMenuItem1";
            this.salirToolStripMenuItem1.Size = new System.Drawing.Size(121, 22);
            this.salirToolStripMenuItem1.Text = "Salir";
            this.salirToolStripMenuItem1.Click += new System.EventHandler(this.salirToolStripMenuItem1_Click);
            // 
            // menuStrip3
            // 
            this.menuStrip3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem84,
            this.toolStripMenuItem114,
            this.toolStripMenuItem136,
            this.toolStripMenuItem138,
            this.toolStripMenuItem163,
            this.toolStripMenuItem166});
            this.menuStrip3.Location = new System.Drawing.Point(0, 0);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Size = new System.Drawing.Size(529, 24);
            this.menuStrip3.TabIndex = 31;
            this.menuStrip3.Text = "menuStrip3";
            this.menuStrip3.Visible = false;
            // 
            // toolStripMenuItem84
            // 
            this.toolStripMenuItem84.Name = "toolStripMenuItem84";
            this.toolStripMenuItem84.Size = new System.Drawing.Size(49, 20);
            this.toolStripMenuItem84.Text = "Inicio";
            this.toolStripMenuItem84.Click += new System.EventHandler(this.toolStripMenuItem84_Click);
            // 
            // toolStripMenuItem114
            // 
            this.toolStripMenuItem114.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem115,
            this.toolStripMenuItem116,
            this.toolStripMenuItem117,
            this.toolStripMenuItem118,
            this.toolStripMenuItem119});
            this.toolStripMenuItem114.Name = "toolStripMenuItem114";
            this.toolStripMenuItem114.Size = new System.Drawing.Size(71, 20);
            this.toolStripMenuItem114.Text = "Empleado";
            // 
            // toolStripMenuItem115
            // 
            this.toolStripMenuItem115.Name = "toolStripMenuItem115";
            this.toolStripMenuItem115.Size = new System.Drawing.Size(205, 22);
            this.toolStripMenuItem115.Text = "Registrar";
            this.toolStripMenuItem115.Click += new System.EventHandler(this.toolStripMenuItem115_Click);
            // 
            // toolStripMenuItem116
            // 
            this.toolStripMenuItem116.Name = "toolStripMenuItem116";
            this.toolStripMenuItem116.Size = new System.Drawing.Size(205, 22);
            this.toolStripMenuItem116.Text = "Modificar";
            this.toolStripMenuItem116.Click += new System.EventHandler(this.toolStripMenuItem116_Click);
            // 
            // toolStripMenuItem117
            // 
            this.toolStripMenuItem117.Name = "toolStripMenuItem117";
            this.toolStripMenuItem117.Size = new System.Drawing.Size(205, 22);
            this.toolStripMenuItem117.Text = "Constancia de Trabajo";
            this.toolStripMenuItem117.Click += new System.EventHandler(this.toolStripMenuItem117_Click);
            // 
            // toolStripMenuItem118
            // 
            this.toolStripMenuItem118.Name = "toolStripMenuItem118";
            this.toolStripMenuItem118.Size = new System.Drawing.Size(205, 22);
            this.toolStripMenuItem118.Text = "Constancia de Actividad";
            this.toolStripMenuItem118.Click += new System.EventHandler(this.toolStripMenuItem118_Click);
            // 
            // toolStripMenuItem119
            // 
            this.toolStripMenuItem119.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem120,
            this.toolStripMenuItem121});
            this.toolStripMenuItem119.Name = "toolStripMenuItem119";
            this.toolStripMenuItem119.Size = new System.Drawing.Size(205, 22);
            this.toolStripMenuItem119.Text = "Cargos";
            // 
            // toolStripMenuItem120
            // 
            this.toolStripMenuItem120.Name = "toolStripMenuItem120";
            this.toolStripMenuItem120.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem120.Text = "Registrar";
            this.toolStripMenuItem120.Click += new System.EventHandler(this.toolStripMenuItem120_Click);
            // 
            // toolStripMenuItem121
            // 
            this.toolStripMenuItem121.Name = "toolStripMenuItem121";
            this.toolStripMenuItem121.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem121.Text = "Modificar";
            this.toolStripMenuItem121.Click += new System.EventHandler(this.toolStripMenuItem121_Click);
            // 
            // toolStripMenuItem136
            // 
            this.toolStripMenuItem136.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem137});
            this.toolStripMenuItem136.Name = "toolStripMenuItem136";
            this.toolStripMenuItem136.Size = new System.Drawing.Size(46, 20);
            this.toolStripMenuItem136.Text = "Perfil";
            // 
            // toolStripMenuItem137
            // 
            this.toolStripMenuItem137.Name = "toolStripMenuItem137";
            this.toolStripMenuItem137.Size = new System.Drawing.Size(203, 22);
            this.toolStripMenuItem137.Text = "Modificar mi contraseña";
            this.toolStripMenuItem137.Click += new System.EventHandler(this.toolStripMenuItem137_Click);
            // 
            // toolStripMenuItem138
            // 
            this.toolStripMenuItem138.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem139,
            this.toolStripMenuItem142,
            this.toolStripMenuItem145});
            this.toolStripMenuItem138.Name = "toolStripMenuItem138";
            this.toolStripMenuItem138.Size = new System.Drawing.Size(55, 20);
            this.toolStripMenuItem138.Text = "Salario";
            // 
            // toolStripMenuItem139
            // 
            this.toolStripMenuItem139.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem140,
            this.toolStripMenuItem141});
            this.toolStripMenuItem139.Name = "toolStripMenuItem139";
            this.toolStripMenuItem139.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem139.Text = "Asignaciones";
            // 
            // toolStripMenuItem140
            // 
            this.toolStripMenuItem140.Name = "toolStripMenuItem140";
            this.toolStripMenuItem140.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem140.Text = "Registrar";
            this.toolStripMenuItem140.Click += new System.EventHandler(this.toolStripMenuItem140_Click);
            // 
            // toolStripMenuItem141
            // 
            this.toolStripMenuItem141.Name = "toolStripMenuItem141";
            this.toolStripMenuItem141.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem141.Text = "Modificar";
            this.toolStripMenuItem141.Click += new System.EventHandler(this.toolStripMenuItem141_Click);
            // 
            // toolStripMenuItem142
            // 
            this.toolStripMenuItem142.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem143,
            this.toolStripMenuItem144});
            this.toolStripMenuItem142.Name = "toolStripMenuItem142";
            this.toolStripMenuItem142.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem142.Text = "Deducciones";
            // 
            // toolStripMenuItem143
            // 
            this.toolStripMenuItem143.Name = "toolStripMenuItem143";
            this.toolStripMenuItem143.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem143.Text = "Registrar";
            this.toolStripMenuItem143.Click += new System.EventHandler(this.toolStripMenuItem143_Click);
            // 
            // toolStripMenuItem144
            // 
            this.toolStripMenuItem144.Name = "toolStripMenuItem144";
            this.toolStripMenuItem144.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem144.Text = "Modificar";
            this.toolStripMenuItem144.Click += new System.EventHandler(this.toolStripMenuItem144_Click);
            // 
            // toolStripMenuItem145
            // 
            this.toolStripMenuItem145.Name = "toolStripMenuItem145";
            this.toolStripMenuItem145.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem145.Text = "Recibo de Pago";
            this.toolStripMenuItem145.Click += new System.EventHandler(this.toolStripMenuItem145_Click);
            // 
            // toolStripMenuItem163
            // 
            this.toolStripMenuItem163.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem164,
            this.toolStripMenuItem165});
            this.toolStripMenuItem163.Name = "toolStripMenuItem163";
            this.toolStripMenuItem163.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem163.Text = "Usuario";
            // 
            // toolStripMenuItem164
            // 
            this.toolStripMenuItem164.Name = "toolStripMenuItem164";
            this.toolStripMenuItem164.Size = new System.Drawing.Size(221, 22);
            this.toolStripMenuItem164.Text = "Registrar";
            this.toolStripMenuItem164.Click += new System.EventHandler(this.toolStripMenuItem164_Click);
            // 
            // toolStripMenuItem165
            // 
            this.toolStripMenuItem165.Name = "toolStripMenuItem165";
            this.toolStripMenuItem165.Size = new System.Drawing.Size(221, 22);
            this.toolStripMenuItem165.Text = "Modificar/Rest. Contraseña";
            this.toolStripMenuItem165.Click += new System.EventHandler(this.toolStripMenuItem165_Click);
            // 
            // toolStripMenuItem166
            // 
            this.toolStripMenuItem166.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenuItem166.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolStripMenuItem166.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.toolStripMenuItem166.Name = "toolStripMenuItem166";
            this.toolStripMenuItem166.Size = new System.Drawing.Size(42, 20);
            this.toolStripMenuItem166.Text = "Salir";
            this.toolStripMenuItem166.Click += new System.EventHandler(this.toolStripMenuItem166_Click);
            // 
            // abonos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(529, 276);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menuStrip3);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "abonos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registrar Asignación";
            this.Load += new System.EventHandler(this.abonos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem académicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarModificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarAEstudianteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disciplinasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem disponiblesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resumenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaDeEstudiantesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tiposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem formalizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem cambioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem periodoAcadémicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem cerrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preinscripciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem pendientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estudianteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empleadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeTrabajoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeActividadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem finanzasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bancoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem pagosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem verificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem perfilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarMiContraseñaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deduccionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem reciboDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem solicitudToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem usuarioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem certificaciónDeCalificacionesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem certificaciónEgredadosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem certificaciónRegularesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem listarCertificacionesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeEstudioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem modificarRestContraseñaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem postulaciónParaPasantíasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem22;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem84;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem114;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem115;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem116;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem117;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem118;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem119;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem120;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem121;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem136;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem137;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem138;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem139;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem140;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem141;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem142;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem143;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem144;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem145;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem163;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem164;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem165;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem166;
    }
}