﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Windows.Forms;

namespace SIAC
{
    public partial class actestudiante : Form
    {
        public actestudiante()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void actestudiante_Load(object sender, EventArgs e)
        {
            conexion.Open();
            comboBox3.SelectedIndex = 0;
            if (estatica.Nivel == "Asesor de Ventas")
            {
                comboBox3.Visible = false;
            }
            else
            {
                comboBox3.Visible = true;
            }
        }

        string ci = "";
        DateTime var;
        void buscar()
        {
            if (textBox1.Text != "")
            {
                ci = "";
                var cadena = textBox1.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox1.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                            ci = textBox1.Text;
                            textBox2.Text = lee["nombre"].ToString();
                            textBox3.Text = lee["apellido"].ToString();
                            dateTimePicker1.Text = lee["fecha"].ToString();
                            textBox4.Text = lee["direc"].ToString();
                            textBox5.Text = lee["tlf"].ToString();
                            textBox6.Text = lee["obser"].ToString();
                            comboBox3.Text = lee["estatus"].ToString();
                            var = dateTimePicker1.Value.Date;
                            int edad = DateTime.Today.AddTicks(-var.Ticks).Year - 1;
                            textBox99.Text = edad.ToString();
                            button3.Visible = true;

                      

                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad '" + textBox1.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            buscar();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buscar();
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            comboBox3.SelectedIndex = 0;
            dateTimePicker1.Value = DateTime.Now;
            textBox99.Clear();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox1.Text != "" && textBox1.Text != "" && textBox1.Text != "" && textBox1.Text != "" && comboBox3.SelectedIndex != 0)
            {
                try
                {
                    string insertar = "UPDATE  estudiante SET  nombre = '" + textBox2.Text + "', apellido = '"+textBox3.Text+"', fecha = '"+dateTimePicker1.Text+"', direc = '"+textBox4.Text+"', tlf = '"+textBox5.Text+"', estatus = '"+comboBox3.Text+"', obser = '"+textBox6.Text+"' WHERE ci = '" + ci + "' ";
                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                    cmd.ExecuteNonQuery();
                    clear();
                    MessageBox.Show("Los datos del estudiante fueron modificados con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);


                }

                catch (DBConcurrencyException ex)
                {
                    MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar la modificación.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker1.Text != DateTime.Now.ToString("dd-MM-yyyy"))
            {
                var = dateTimePicker1.Value.Date;
                int edad = DateTime.Today.AddTicks(-var.Ticks).Year - 1;
                textBox99.Text = edad.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clear();
        }

        #region menu1

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            admin a = new admin();
            a.Show();
            this.Close();
        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cargarnota a = new cargarnota();
            a.Show();
            this.Close();
        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actnotae a = new actnotae();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            modalidad a = new modalidad();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            actmodalidad a = new actmodalidad();
            a.Show();
            this.Close();
        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mdisponibles a = new mdisponibles();
            a.Show();
            this.Close();
        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resumencurso a = new resumencurso();
            a.Show();
            this.Close();
        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listaestudiantes a = new listaestudiantes();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            tipom a = new tipom();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            acttipom a = new acttipom();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            forma a = new forma();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            actforma a = new actforma();
            a.Show();
            this.Close();
        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cambio a = new cambio();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            periodo a = new periodo();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            actperiodo a = new actperiodo();
            a.Show();
            this.Close();
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cerrar a = new cerrar();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            pre a = new pre();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            actpre a = new actpre();
            a.Show();
            this.Close();
        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            prepen a = new prepen();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            estudiante a = new estudiante();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actestudiante a = new actestudiante();
            a.Show();
            this.Close();
        }

        private void historialToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            historial a = new historial();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            actempleado a = new actempleado();
            a.Show();
            this.Close();
        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            trabajo a = new trabajo();
            a.Show();
            this.Close();
        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actividad a = new actividad();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            cargo a = new cargo();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            actcargo a = new actcargo();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            banco a = new banco();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            actbanco a = new actbanco();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            pago a = new pago();
            a.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            conpago a = new conpago();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            actbanco a = new actbanco();
            a.Show();
            this.Close();
        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            veripago a = new veripago();
            a.Show();
            this.Close();
        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listapv a = new listapv();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {
            precio a = new precio();
            a.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            conprecio a = new conprecio();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            actprecio a = new actprecio();
            a.Show();
            this.Close();
        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actpas a = new actpas();
            a.ShowDialog();
        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            abonos a = new abonos();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            actabonos a = new actabonos();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            dedu a = new dedu();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            actdedu a = new actdedu();
            a.Show();
            this.Close();
        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pagoe a = new pagoe();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem20_Click(object sender, EventArgs e)
        {
            notas a = new notas();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem18_Click(object sender, EventArgs e)
        {
            actnotas a = new actnotas();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem21_Click(object sender, EventArgs e)
        {
            certi2 a = new certi2();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem19_Click(object sender, EventArgs e)
        {
            actcerti2 a = new actcerti2();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem22_Click(object sender, EventArgs e)
        {
            certi1 a = new certi1();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem20_Click(object sender, EventArgs e)
        {
            actcerti1 a = new actcerti1();
            a.Show();
            this.Close();
        }

        private void listarCertificacionesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            listacerti a = new listacerti();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem23_Click(object sender, EventArgs e)
        {
            estudio a = new estudio();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem21_Click(object sender, EventArgs e)
        {
            actestudio a = new actestudio();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem24_Click(object sender, EventArgs e)
        {
            postu a = new postu();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem22_Click(object sender, EventArgs e)
        {
            actpostu a = new actpostu();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem19_Click(object sender, EventArgs e)
        {
            usuario a = new usuario();
            a.Show();
            this.Close();
        }

        private void modificarRestContraseñaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            actusuario a = new actusuario();
            a.Show();
            this.Close();
        }

        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea salir de su sesión?", "Notificación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                isesion a = new isesion();
                a.Show();
                this.Close();
            }
        }
        #endregion

        
    }
}
