﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class pres : Form
    {
        public pres()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void pres_Load(object sender, EventArgs e)
        {
            conexion.Open();
            dataGridView1.AllowUserToAddRows = false;//eliminar filas por defecto   
            cargar();
        }
        string[,] comen;
        int can;
        void cargar()
        {
            label1.Text = "(" + estatica.Estudiante + ") " + estatica.Ne;
            try
            {

                string buscarc = "SELECT * FROM pre WHERE estudiante = '" + estatica.Estudiante + "' AND estatus = 'PENDIENTE' ORDER BY cod;";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                can = 0;
                    comen = new string[4, 1000000];

                    dataGridView1.Rows.Clear();
                    dataGridView1.Refresh();
                    while (lee.Read())
                    {
                            comen[0, can] = lee["cod"].ToString();
                            string buscarcx = "SELECT * FROM modalidad WHERE cod = '" + lee["modalidad"].ToString() + "';";
                            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                            OleDbDataReader leex = comandox.ExecuteReader();
                            if (leex.Read() == true)
                            {
                                comen[1, can] = leex["modalidad"].ToString();
                            }
                            comen[2, can] = lee["preciom"].ToString();

                            string buscarcxx = "SELECT * FROM periodo WHERE cod = '" + lee["periodo"].ToString() + "';";
                            OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
                            OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
                            OleDbDataReader leexx = comandoxx.ExecuteReader();
                            if (leexx.Read() == true)
                            {

                                comen[3, can] = leexx["periodo"].ToString();
                            }
                            can++;

                    }
                    if (can > 0)
                    {
                        dataGridView1.Rows.Clear();
                        dataGridView1.Refresh();
                        for (int i = 0; i < can; i++)
                        {
                            dataGridView1.Rows.Add();
                            dataGridView1.Rows[i].Cells[0].Value = comen[0, i];
                            dataGridView1.Rows[i].Cells[1].Value = comen[1, i];
                            dataGridView1.Rows[i].Cells[2].Value = comen[2, i];
                            dataGridView1.Rows[i].Cells[3].Value = comen[3, i];
                            dataGridView1.Rows[i].Cells[4].Value = "Seleccionar";

                        }
                    }
            }
            catch (DBConcurrencyException ex)
            {
                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                estatica.Pre = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                this.DialogResult = DialogResult.Yes;
            }
        }
    }
}
