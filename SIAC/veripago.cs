﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class veripago : Form
    {
        public veripago()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void veripago_Load(object sender, EventArgs e)
        {
            conexion.Open();
            dataGridView1.AllowUserToAddRows = false;//eliminar filas por defecto  
            cargar();
        }
        string[,] comen;
        int can;
        void cargar()
        {

            string buscarc = "SELECT * FROM pago WHERE estatus = 'PENDIENTE';";
            OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
            OleDbCommand comando = new OleDbCommand(buscarc, conexion);
            OleDbDataReader lee = comando.ExecuteReader();
            can = 0;
            comen = new string[7, 10000000];

            
            while (lee.Read())
            {
                    comen[0, can] = lee["cod"].ToString();
                    comen[1, can] = lee["tipo"].ToString();
                    comen[2, can] = lee["fecha"].ToString();
                    comen[3, can] = lee["monto"].ToString();
                    comen[4, can] = lee["pago"].ToString();
                    string buscarcz = "SELECT * FROM banco WHERE cod = " + lee["origen"].ToString() + ";";
                    OleDbDataAdapter dataz = new OleDbDataAdapter(buscarcz, conexion);
                    OleDbCommand comandoz = new OleDbCommand(buscarcz, conexion);
                    OleDbDataReader leez = comandoz.ExecuteReader();
                    if (leez.Read())
                    {
                        comen[5, can] = leez["banco"].ToString();
                    }

                    buscarcz = "SELECT * FROM banco WHERE cod = " + lee["destino"].ToString() + ";";
                    dataz = new OleDbDataAdapter(buscarcz, conexion);
                    comandoz = new OleDbCommand(buscarcz, conexion);
                    leez = comandoz.ExecuteReader();
                    if (leez.Read())
                    {
                        comen[6, can] = leez["banco"].ToString();
                    }
                    can++;
            }
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();

           if (can > 0)
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
               DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)dataGridView1.Columns[7].CellTemplate;
               DataTable dt = new DataTable();

                    DataRow nuevaFila = dt.NewRow();
                    dt.Columns.Add("modalidad");

                    nuevaFila["modalidad"] = "Seleccione un estatus";

                    dt.Rows.InsertAt(nuevaFila, 0);
                    DataRow nuevaFilaX = dt.NewRow();
                    nuevaFilaX["modalidad"] = "VERIFICADO";

                    dt.Rows.InsertAt(nuevaFilaX, 1);
                    DataRow nuevaFilaXX = dt.NewRow();

                    nuevaFilaXX["modalidad"] = "NO VERIFICADO";

                    dt.Rows.InsertAt(nuevaFilaXX, 2);
                            
			        cell.DisplayMember = "modalidad";
                    cell.ValueMember = "modalidad";
                    cell.Value = "Seleccione un estatus";
                    
                    cell.DataSource = dt;

                for (int i = 0; i < can; i++)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells[0].Value = comen[0, i];
                    dataGridView1.Rows[i].Cells[1].Value = comen[1, i];
                    dataGridView1.Rows[i].Cells[2].Value = comen[2, i];
                    dataGridView1.Rows[i].Cells[3].Value = comen[3, i];
                    dataGridView1.Rows[i].Cells[4].Value = comen[4, i];
                    dataGridView1.Rows[i].Cells[5].Value = comen[5, i];
                    dataGridView1.Rows[i].Cells[6].Value = comen[6, i];
                    
			
                    /*DataGridViewComboBoxColumn esta = dataGridView1.Columns["Estatus"] as DataGridViewComboBoxColumn;
                    esta.Items.Add("Seleccione un estatus");
                    esta.Items.Add("VERIFICADO");
                    esta.Items.Add("NO VERIFICADO");*/
                    
                    
                    
                    dataGridView1.Rows[i].Cells[8].Value = " ";

                    
                    

                }
            }
           for (int i = 0; i < dataGridView1.Rows.Count; i++)
           {
               dataGridView1.Rows[i].Cells[7].Value = "Seleccione un estatus";
           }
        }
        string control;
        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                control = "";
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (dataGridView1.Rows[i].Cells[7].Value.ToString() != "Seleccione un estatus")
                    {
                        if (dataGridView1.Rows[i].Cells[7].Value.ToString() == "VERIFICADO")
                        {
                            try
                            {
                                string insertar = "UPDATE  pago SET  estatus = 'VERIFICADO' WHERE cod =  " + dataGridView1.Rows[i].Cells[0].Value.ToString() + "";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();

                                string buscarc = "SELECT * FROM forma WHERE pago = " + dataGridView1.Rows[i].Cells[0].Value.ToString() + ";";
                                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                                OleDbDataReader lee = comando.ExecuteReader();
                                while (lee.Read())
                                {
                                    string insertarx = "UPDATE  forma SET  estatus = 'VERIFICADO' WHERE cod =  '" + lee["cod"].ToString() + "'";
                                    OleDbCommand cmdx = new OleDbCommand(insertarx, conexion);
                                    cmdx.ExecuteNonQuery();

                                    string insertarxx = "UPDATE  pre SET  estatus = 'VERIFICADO' WHERE cod =  '" + lee["cod"].ToString() + "'";
                                    OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                    cmdxx.ExecuteNonQuery();
                                    control = "1";
                                    string buscarcxx = "SELECT * FROM pre WHERE cod = '" + lee["cod"].ToString() + "';";
                                    OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
                                    OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
                                    OleDbDataReader leexx = comandoxx.ExecuteReader();
                                    while (leexx.Read())
                                    {
                                        string insertarxxx = "UPDATE  estudiante SET  estatus = 'ACTIVO' WHERE ci =  '" + leexx["estudiante"].ToString() + "'";
                                        OleDbCommand cmdxxx = new OleDbCommand(insertarxxx, conexion);
                                        cmdxxx.ExecuteNonQuery();
                                    }

                                }

                                string buscarcx = "SELECT * FROM certi WHERE pago = " + dataGridView1.Rows[i].Cells[0].Value.ToString() + ";";
                                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                                OleDbDataReader leex = comandox.ExecuteReader();
                                while (leex.Read())
                                {
                                    string insertarx = "UPDATE  certi SET  estatus = 'VERIFICADO' WHERE cod =  " + leex["cod"].ToString() + "";
                                    OleDbCommand cmdxa = new OleDbCommand(insertarx, conexion);
                                    cmdxa.ExecuteNonQuery();


                                }
                                control = "1";

                            }
                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        if (dataGridView1.Rows[i].Cells[7].Value.ToString() == "NO VERIFICADO")
                        {
                            try
                            {
                                string insertar = "UPDATE  pago SET  estatus = 'NO VERIFICADO' WHERE cod =  " + dataGridView1.Rows[i].Cells[0].Value.ToString() + "";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();

                                string buscarc = "SELECT * FROM forma WHERE pago = " + dataGridView1.Rows[i].Cells[0].Value.ToString() + ";";
                                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                                OleDbDataReader lee = comando.ExecuteReader();
                                while (lee.Read())
                                {
                                    string insertarx = "UPDATE  forma SET  estatus = 'NO VERIFICADO' WHERE cod = '" + lee["cod"].ToString() + "'";
                                    OleDbCommand cmdx = new OleDbCommand(insertarx, conexion);
                                    cmdx.ExecuteNonQuery();

                                    string insertarxx = "UPDATE  pre SET  estatus = 'NO VERIFICAO' WHERE cod = '" + lee["cod"].ToString() + "'";
                                    OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                    cmdxx.ExecuteNonQuery();

                                }

                                string buscarcx = "SELECT * FROM certi WHERE pago = " + dataGridView1.Rows[i].Cells[0].Value.ToString() + ";";
                                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                                OleDbDataReader leex = comandox.ExecuteReader();
                                while (leex.Read())
                                {
                                    string insertarx = "UPDATE  certi SET  estatus = 'NO VERIFICADO' WHERE cod =  " + leex["cod"].ToString() + "";
                                    OleDbCommand cmdxa = new OleDbCommand(insertarx, conexion);
                                    cmdxa.ExecuteNonQuery();


                                }
                                control = "1";

                            }
                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }

                    }
                }
                if (control == "1")
                {
                    MessageBox.Show("Los cambios fueron guardados con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cargar();

                }
                if (control == "")
                {
                    MessageBox.Show("No hay ningún cambio que guardar.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
