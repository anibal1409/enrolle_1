﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class historial : Form
    {
        public historial()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void historial_Load(object sender, EventArgs e)
        {
            conexion.Open();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscar();
        }

        string ci = "";
        void buscar()
        {
            if (textBox7.Text != "")
            {
                ci = "";
                var cadena = textBox7.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox7.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                            ci = textBox7.Text;
                            textBox6.Text = lee["nombre"].ToString() + " "+ lee["apellido"].ToString();
                            cargar();
                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad '" + textBox7.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                        
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        string[,] comen;
        int can;
        void cargar()
        {
            can = 0;
            comen = new string[5, 10000000];

            string buscarc = "SELECT * FROM pre WHERE estudiante = '"+ci+"';";
            OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
            OleDbCommand comando = new OleDbCommand(buscarc, conexion);
            OleDbDataReader lee = comando.ExecuteReader();
            while (lee.Read())
            {
                comen[0, can] = lee["cod"].ToString();
                comen[1, can] = "Inscrpción";
                string buscarcx= "SELECT * FROM modalidad WHERE cod = " + lee["modalidad"].ToString() + ";";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read())
                {
                    comen[2, can] = leex["modalidad"].ToString();
                }
                comen[3, can] = lee["estatus"].ToString();
                comen[4, can] = lee["fecha"].ToString();
                can++;
            }

            string buscarc2 = "SELECT * FROM estudio WHERE estudiante = '" + ci + "';";
            OleDbDataAdapter data2 = new OleDbDataAdapter(buscarc2, conexion);
            OleDbCommand comando2 = new OleDbCommand(buscarc2, conexion);
            OleDbDataReader lee2 = comando2.ExecuteReader();
            while (lee2.Read())
            {
                comen[0, can] = lee2["cod"].ToString();
                comen[1, can] = "Constancia de estudio";
                string buscarcx = "SELECT * FROM modalidad WHERE cod = " + lee2["modalidad"].ToString() + ";";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read())
                {
                    comen[2, can] = leex["modalidad"].ToString();
                }
                comen[3, can] = lee2["estatus"].ToString();
                comen[4, can] = lee2["fecha"].ToString();
                can++;
            }

            string buscarc3 = "SELECT * FROM nota WHERE estudiante = '" + ci + "';";
            OleDbDataAdapter data3 = new OleDbDataAdapter(buscarc3, conexion);
            OleDbCommand comando3 = new OleDbCommand(buscarc3, conexion);
            OleDbDataReader lee3 = comando3.ExecuteReader();
            while (lee3.Read())
            {
                comen[0, can] = lee3["cod"].ToString();
                comen[1, can] = "Certificación de notas";
                string buscarcx = "SELECT * FROM modalidad WHERE cod = " + lee3["modalidad"].ToString() + ";";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read())
                {
                    comen[2, can] = leex["modalidad"].ToString();
                }
                comen[3, can] = lee3["estatus"].ToString();
                comen[4, can] = lee3["fecha"].ToString();
                can++;
            }
            string buscarc4 = "SELECT * FROM postu WHERE estudiante = '" + ci + "';";
            OleDbDataAdapter data4 = new OleDbDataAdapter(buscarc4, conexion);
            OleDbCommand comando4 = new OleDbCommand(buscarc4, conexion);
            OleDbDataReader lee4 = comando4.ExecuteReader();
            while (lee4.Read())
            {
                comen[0, can] = lee4["cod"].ToString();
                comen[1, can] = "Carta de postulación";
                string buscarcx = "SELECT * FROM modalidad WHERE cod = " + lee4["modalidad"].ToString() + ";";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read())
                {
                    comen[2, can] = leex["modalidad"].ToString();
                }
                comen[3, can] = lee4["estatus"].ToString();
                comen[4, can] = lee4["fecha"].ToString();
                can++;
            }

            string buscarc5 = "SELECT * FROM certi WHERE estudiante = '" + ci + "';";
            OleDbDataAdapter data5 = new OleDbDataAdapter(buscarc5, conexion);
            OleDbCommand comando5 = new OleDbCommand(buscarc5, conexion);
            OleDbDataReader lee5 = comando5.ExecuteReader();
            while (lee5.Read())
            {
                comen[0, can] = lee5["cod"].ToString();
                comen[1, can] = "Certificación";
                string buscarcx = "SELECT * FROM modalidad WHERE cod = " + lee5["modalidad"].ToString() + ";";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read())
                {
                    comen[2, can] = leex["modalidad"].ToString();
                }
                comen[3, can] = lee5["estatus"].ToString();
                comen[4, can] = lee5["fecha"].ToString();
                can++;
            }
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();

            if (can > 1)
            {
                DateTime a, b;
                string ax, bx, cx, dx, ex, fx, gx, hx, ix, jx;
                for (int i = 0; i < can; i++)
                {
                    for (int j = 0; j < can - 1; j++)
                    {
                        a = DateTime.Parse(comen[4, j]);
                        b = DateTime.Parse(comen[4, j + 1]);
                        if (a<b)
                        {
                            ax = comen[0, j];
                            bx = comen[1, j];
                            cx = comen[2, j];
                            dx = comen[3, j];
                            ex = comen[4, j];
                            comen[0, j] = comen[0, j + 1];
                            comen[1, j] = comen[1, j + 1];
                            comen[2, j] = comen[2, j + 1];
                            comen[3, j] = comen[3, j + 1];
                            comen[4, j] = comen[4, j + 1];
                            comen[0, j + 1] = ax;
                            comen[1, j + 1] = bx;
                            comen[2, j + 1] = cx;
                            comen[3, j + 1] = dx;
                            comen[4, j + 1] = ex;
                        }
                    }
                }
            }
            /*if (can > 1)
            {
                string ax, bx, cx, dx, ex, fx, gx, hx, ix, jx;
                for (int i = 0; i < can; i++)
                {
                    for (int j = 0; j < can - 1; j++)
                    {
                        if (String.Compare(comen[4, j], comen[4, j + 1]) < 0)
                        {
                            ax = comen[0, j];
                            bx = comen[1, j];
                            cx = comen[2, j];
                            dx = comen[3, j];
                            ex = comen[4, j];
                            comen[0, j] = comen[0, j + 1];
                            comen[1, j] = comen[1, j + 1];
                            comen[2, j] = comen[2, j + 1];
                            comen[3, j] = comen[3, j + 1];
                            comen[4, j] = comen[4, j + 1];
                            comen[0, j + 1] = ax;
                            comen[1, j + 1] = bx;
                            comen[2, j + 1] = cx;
                            comen[3, j + 1] = dx;
                            comen[4, j + 1] = ex;
                        }
                    }
                }
            }*/

            if (can > 0)
            {
                

                for (int i = 0; i < can; i++)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells[0].Value = comen[0, i];
                    dataGridView1.Rows[i].Cells[1].Value = comen[1, i];
                    dataGridView1.Rows[i].Cells[2].Value = comen[2, i];
                    dataGridView1.Rows[i].Cells[3].Value = comen[3, i];
                    dataGridView1.Rows[i].Cells[4].Value = DateTime.Parse(comen[4, i]);




                }
            }
        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            estudiante a = new estudiante();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actestudiante a = new actestudiante();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            usuario a = new usuario();
            a.Show();
            this.Close();
        }
    }
}
