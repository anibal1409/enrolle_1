﻿namespace SIAC
{
    partial class cerrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cerrar));
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.académicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarModificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarAEstudianteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disciplinasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.disponiblesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resumenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaDeEstudiantesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.formalizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cambioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.periodoAcadémicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preinscripciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.pendientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estudianteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empleadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeTrabajoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeActividadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.finanzasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bancoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pagosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.listaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.preciosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarMiContraseñaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salarioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.asignacionesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.deduccionesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.reciboDePagoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitudToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónDeCalificacionesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónEgredadosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónRegularesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.listarCertificacionesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeEstudioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.postulaciónParaPasantíasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.usuarioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarRestContraseñaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem30 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem31 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem32 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem39 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem40 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem41 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem42 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem43 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem44 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem45 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem46 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem47 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem48 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem49 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem50 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem51 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem52 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem34 = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarMiContraseñaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitudToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónDeCalificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónEgredadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónRegularesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.listarCertificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeEstudioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.postulaciónParaPasantíasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.usuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarRestContraseñaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem35 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 19);
            this.label5.TabIndex = 220;
            this.label5.Text = "Periodo Académico";
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(12, 81);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(317, 27);
            this.comboBox3.TabIndex = 219;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.button1.Location = new System.Drawing.Point(134, 124);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 32);
            this.button1.TabIndex = 221;
            this.button1.Text = "Cerrar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem,
            this.académicoToolStripMenuItem,
            this.estudianteToolStripMenuItem,
            this.empleadoToolStripMenuItem,
            this.toolStripMenuItem2});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(341, 24);
            this.menuStrip1.TabIndex = 277;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.inicioToolStripMenuItem.Text = "Inicio";
            this.inicioToolStripMenuItem.Click += new System.EventHandler(this.inicioToolStripMenuItem_Click);
            // 
            // académicoToolStripMenuItem
            // 
            this.académicoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calificacionesToolStripMenuItem,
            this.disciplinasToolStripMenuItem,
            this.formalizaciónToolStripMenuItem,
            this.periodoAcadémicoToolStripMenuItem,
            this.preinscripciónToolStripMenuItem});
            this.académicoToolStripMenuItem.Name = "académicoToolStripMenuItem";
            this.académicoToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.académicoToolStripMenuItem.Text = "Académico";
            // 
            // calificacionesToolStripMenuItem
            // 
            this.calificacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarModificarToolStripMenuItem,
            this.modificarAEstudianteToolStripMenuItem});
            this.calificacionesToolStripMenuItem.Name = "calificacionesToolStripMenuItem";
            this.calificacionesToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.calificacionesToolStripMenuItem.Text = "Calificaciones";
            // 
            // cargarModificarToolStripMenuItem
            // 
            this.cargarModificarToolStripMenuItem.Name = "cargarModificarToolStripMenuItem";
            this.cargarModificarToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.cargarModificarToolStripMenuItem.Text = "Cargar/Modificar";
            this.cargarModificarToolStripMenuItem.Click += new System.EventHandler(this.cargarModificarToolStripMenuItem_Click);
            // 
            // modificarAEstudianteToolStripMenuItem
            // 
            this.modificarAEstudianteToolStripMenuItem.Name = "modificarAEstudianteToolStripMenuItem";
            this.modificarAEstudianteToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.modificarAEstudianteToolStripMenuItem.Text = "Modificar a Estudiante";
            this.modificarAEstudianteToolStripMenuItem.Click += new System.EventHandler(this.modificarAEstudianteToolStripMenuItem_Click);
            // 
            // disciplinasToolStripMenuItem
            // 
            this.disciplinasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem4,
            this.modificarToolStripMenuItem3,
            this.disponiblesToolStripMenuItem,
            this.resumenToolStripMenuItem,
            this.listaDeEstudiantesToolStripMenuItem,
            this.tiposToolStripMenuItem});
            this.disciplinasToolStripMenuItem.Name = "disciplinasToolStripMenuItem";
            this.disciplinasToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.disciplinasToolStripMenuItem.Text = "Disciplina";
            // 
            // registrarToolStripMenuItem4
            // 
            this.registrarToolStripMenuItem4.Name = "registrarToolStripMenuItem4";
            this.registrarToolStripMenuItem4.Size = new System.Drawing.Size(182, 22);
            this.registrarToolStripMenuItem4.Text = "Registrar";
            this.registrarToolStripMenuItem4.Click += new System.EventHandler(this.registrarToolStripMenuItem4_Click);
            // 
            // modificarToolStripMenuItem3
            // 
            this.modificarToolStripMenuItem3.Name = "modificarToolStripMenuItem3";
            this.modificarToolStripMenuItem3.Size = new System.Drawing.Size(182, 22);
            this.modificarToolStripMenuItem3.Text = "Modificar";
            this.modificarToolStripMenuItem3.Click += new System.EventHandler(this.modificarToolStripMenuItem3_Click);
            // 
            // disponiblesToolStripMenuItem
            // 
            this.disponiblesToolStripMenuItem.Name = "disponiblesToolStripMenuItem";
            this.disponiblesToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.disponiblesToolStripMenuItem.Text = "Disponibles";
            this.disponiblesToolStripMenuItem.Click += new System.EventHandler(this.disponiblesToolStripMenuItem_Click);
            // 
            // resumenToolStripMenuItem
            // 
            this.resumenToolStripMenuItem.Name = "resumenToolStripMenuItem";
            this.resumenToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.resumenToolStripMenuItem.Text = "Resumen";
            this.resumenToolStripMenuItem.Click += new System.EventHandler(this.resumenToolStripMenuItem_Click);
            // 
            // listaDeEstudiantesToolStripMenuItem
            // 
            this.listaDeEstudiantesToolStripMenuItem.Name = "listaDeEstudiantesToolStripMenuItem";
            this.listaDeEstudiantesToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.listaDeEstudiantesToolStripMenuItem.Text = "Lista de Estudiantes";
            this.listaDeEstudiantesToolStripMenuItem.Click += new System.EventHandler(this.listaDeEstudiantesToolStripMenuItem_Click);
            // 
            // tiposToolStripMenuItem
            // 
            this.tiposToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem5,
            this.modificarToolStripMenuItem4});
            this.tiposToolStripMenuItem.Name = "tiposToolStripMenuItem";
            this.tiposToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.tiposToolStripMenuItem.Text = "Tipos";
            // 
            // registrarToolStripMenuItem5
            // 
            this.registrarToolStripMenuItem5.Name = "registrarToolStripMenuItem5";
            this.registrarToolStripMenuItem5.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem5.Text = "Registrar";
            this.registrarToolStripMenuItem5.Click += new System.EventHandler(this.registrarToolStripMenuItem5_Click);
            // 
            // modificarToolStripMenuItem4
            // 
            this.modificarToolStripMenuItem4.Name = "modificarToolStripMenuItem4";
            this.modificarToolStripMenuItem4.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem4.Text = "Modificar";
            this.modificarToolStripMenuItem4.Click += new System.EventHandler(this.modificarToolStripMenuItem4_Click);
            // 
            // formalizaciónToolStripMenuItem
            // 
            this.formalizaciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem6,
            this.modificarToolStripMenuItem5,
            this.cambioToolStripMenuItem});
            this.formalizaciónToolStripMenuItem.Name = "formalizaciónToolStripMenuItem";
            this.formalizaciónToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.formalizaciónToolStripMenuItem.Text = "Formalización";
            // 
            // registrarToolStripMenuItem6
            // 
            this.registrarToolStripMenuItem6.Name = "registrarToolStripMenuItem6";
            this.registrarToolStripMenuItem6.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem6.Text = "Registrar";
            this.registrarToolStripMenuItem6.Click += new System.EventHandler(this.registrarToolStripMenuItem6_Click);
            // 
            // modificarToolStripMenuItem5
            // 
            this.modificarToolStripMenuItem5.Name = "modificarToolStripMenuItem5";
            this.modificarToolStripMenuItem5.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem5.Text = "Modificar";
            this.modificarToolStripMenuItem5.Click += new System.EventHandler(this.modificarToolStripMenuItem5_Click);
            // 
            // cambioToolStripMenuItem
            // 
            this.cambioToolStripMenuItem.Name = "cambioToolStripMenuItem";
            this.cambioToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cambioToolStripMenuItem.Text = "Cambio";
            this.cambioToolStripMenuItem.Click += new System.EventHandler(this.cambioToolStripMenuItem_Click);
            // 
            // periodoAcadémicoToolStripMenuItem
            // 
            this.periodoAcadémicoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem7,
            this.modificarToolStripMenuItem6,
            this.cerrarToolStripMenuItem});
            this.periodoAcadémicoToolStripMenuItem.Name = "periodoAcadémicoToolStripMenuItem";
            this.periodoAcadémicoToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.periodoAcadémicoToolStripMenuItem.Text = "Periodo Académico";
            // 
            // registrarToolStripMenuItem7
            // 
            this.registrarToolStripMenuItem7.Name = "registrarToolStripMenuItem7";
            this.registrarToolStripMenuItem7.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem7.Text = "Registrar";
            this.registrarToolStripMenuItem7.Click += new System.EventHandler(this.registrarToolStripMenuItem7_Click);
            // 
            // modificarToolStripMenuItem6
            // 
            this.modificarToolStripMenuItem6.Name = "modificarToolStripMenuItem6";
            this.modificarToolStripMenuItem6.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem6.Text = "Modificar";
            this.modificarToolStripMenuItem6.Click += new System.EventHandler(this.modificarToolStripMenuItem6_Click);
            // 
            // cerrarToolStripMenuItem
            // 
            this.cerrarToolStripMenuItem.Name = "cerrarToolStripMenuItem";
            this.cerrarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cerrarToolStripMenuItem.Text = "Cerrar";
            this.cerrarToolStripMenuItem.Click += new System.EventHandler(this.cerrarToolStripMenuItem_Click);
            // 
            // preinscripciónToolStripMenuItem
            // 
            this.preinscripciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem8,
            this.modificarToolStripMenuItem7,
            this.pendientesToolStripMenuItem});
            this.preinscripciónToolStripMenuItem.Name = "preinscripciónToolStripMenuItem";
            this.preinscripciónToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.preinscripciónToolStripMenuItem.Text = "Preinscripción";
            // 
            // registrarToolStripMenuItem8
            // 
            this.registrarToolStripMenuItem8.Name = "registrarToolStripMenuItem8";
            this.registrarToolStripMenuItem8.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem8.Text = "Registrar";
            this.registrarToolStripMenuItem8.Click += new System.EventHandler(this.registrarToolStripMenuItem8_Click);
            // 
            // modificarToolStripMenuItem7
            // 
            this.modificarToolStripMenuItem7.Name = "modificarToolStripMenuItem7";
            this.modificarToolStripMenuItem7.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem7.Text = "Modificar";
            this.modificarToolStripMenuItem7.Click += new System.EventHandler(this.modificarToolStripMenuItem7_Click);
            // 
            // pendientesToolStripMenuItem
            // 
            this.pendientesToolStripMenuItem.Name = "pendientesToolStripMenuItem";
            this.pendientesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pendientesToolStripMenuItem.Text = "Pendientes";
            this.pendientesToolStripMenuItem.Click += new System.EventHandler(this.pendientesToolStripMenuItem_Click);
            // 
            // estudianteToolStripMenuItem
            // 
            this.estudianteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem,
            this.modificarToolStripMenuItem,
            this.historialToolStripMenuItem});
            this.estudianteToolStripMenuItem.Name = "estudianteToolStripMenuItem";
            this.estudianteToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.estudianteToolStripMenuItem.Text = "Estudiante";
            // 
            // registrarToolStripMenuItem
            // 
            this.registrarToolStripMenuItem.Name = "registrarToolStripMenuItem";
            this.registrarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem.Text = "Registrar";
            this.registrarToolStripMenuItem.Click += new System.EventHandler(this.registrarToolStripMenuItem_Click);
            // 
            // modificarToolStripMenuItem
            // 
            this.modificarToolStripMenuItem.Name = "modificarToolStripMenuItem";
            this.modificarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem.Text = "Modificar";
            this.modificarToolStripMenuItem.Click += new System.EventHandler(this.modificarToolStripMenuItem_Click);
            // 
            // historialToolStripMenuItem
            // 
            this.historialToolStripMenuItem.Name = "historialToolStripMenuItem";
            this.historialToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.historialToolStripMenuItem.Text = "Historial";
            this.historialToolStripMenuItem.Click += new System.EventHandler(this.historialToolStripMenuItem_Click);
            // 
            // empleadoToolStripMenuItem
            // 
            this.empleadoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem9,
            this.modificarToolStripMenuItem8,
            this.constanciaDeTrabajoToolStripMenuItem,
            this.constanciaDeActividadToolStripMenuItem,
            this.cargosToolStripMenuItem});
            this.empleadoToolStripMenuItem.Name = "empleadoToolStripMenuItem";
            this.empleadoToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.empleadoToolStripMenuItem.Text = "Empleado";
            // 
            // registrarToolStripMenuItem9
            // 
            this.registrarToolStripMenuItem9.Name = "registrarToolStripMenuItem9";
            this.registrarToolStripMenuItem9.Size = new System.Drawing.Size(205, 22);
            this.registrarToolStripMenuItem9.Text = "Registrar";
            this.registrarToolStripMenuItem9.Click += new System.EventHandler(this.registrarToolStripMenuItem9_Click);
            // 
            // modificarToolStripMenuItem8
            // 
            this.modificarToolStripMenuItem8.Name = "modificarToolStripMenuItem8";
            this.modificarToolStripMenuItem8.Size = new System.Drawing.Size(205, 22);
            this.modificarToolStripMenuItem8.Text = "Modificar";
            this.modificarToolStripMenuItem8.Click += new System.EventHandler(this.modificarToolStripMenuItem8_Click);
            // 
            // constanciaDeTrabajoToolStripMenuItem
            // 
            this.constanciaDeTrabajoToolStripMenuItem.Name = "constanciaDeTrabajoToolStripMenuItem";
            this.constanciaDeTrabajoToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.constanciaDeTrabajoToolStripMenuItem.Text = "Constancia de Trabajo";
            this.constanciaDeTrabajoToolStripMenuItem.Click += new System.EventHandler(this.constanciaDeTrabajoToolStripMenuItem_Click);
            // 
            // constanciaDeActividadToolStripMenuItem
            // 
            this.constanciaDeActividadToolStripMenuItem.Name = "constanciaDeActividadToolStripMenuItem";
            this.constanciaDeActividadToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.constanciaDeActividadToolStripMenuItem.Text = "Constancia de Actividad";
            this.constanciaDeActividadToolStripMenuItem.Click += new System.EventHandler(this.constanciaDeActividadToolStripMenuItem_Click);
            // 
            // cargosToolStripMenuItem
            // 
            this.cargosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem10,
            this.modificarToolStripMenuItem9});
            this.cargosToolStripMenuItem.Name = "cargosToolStripMenuItem";
            this.cargosToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.cargosToolStripMenuItem.Text = "Cargos";
            // 
            // registrarToolStripMenuItem10
            // 
            this.registrarToolStripMenuItem10.Name = "registrarToolStripMenuItem10";
            this.registrarToolStripMenuItem10.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem10.Text = "Registrar";
            this.registrarToolStripMenuItem10.Click += new System.EventHandler(this.registrarToolStripMenuItem10_Click);
            // 
            // modificarToolStripMenuItem9
            // 
            this.modificarToolStripMenuItem9.Name = "modificarToolStripMenuItem9";
            this.modificarToolStripMenuItem9.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem9.Text = "Modificar";
            this.modificarToolStripMenuItem9.Click += new System.EventHandler(this.modificarToolStripMenuItem9_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.finanzasToolStripMenuItem1,
            this.perfilToolStripMenuItem1,
            this.salarioToolStripMenuItem1,
            this.solicitudToolStripMenuItem1,
            this.usuarioToolStripMenuItem1,
            this.salirToolStripMenuItem1});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(26, 20);
            this.toolStripMenuItem2.Text = "+";
            // 
            // finanzasToolStripMenuItem1
            // 
            this.finanzasToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bancoToolStripMenuItem1,
            this.pagosToolStripMenuItem1,
            this.preciosToolStripMenuItem1});
            this.finanzasToolStripMenuItem1.Name = "finanzasToolStripMenuItem1";
            this.finanzasToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.finanzasToolStripMenuItem1.Text = "Finanzas";
            // 
            // bancoToolStripMenuItem1
            // 
            this.bancoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem3,
            this.modificarToolStripMenuItem1});
            this.bancoToolStripMenuItem1.Name = "bancoToolStripMenuItem1";
            this.bancoToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.bancoToolStripMenuItem1.Text = "Banco";
            // 
            // registrarToolStripMenuItem3
            // 
            this.registrarToolStripMenuItem3.Name = "registrarToolStripMenuItem3";
            this.registrarToolStripMenuItem3.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem3.Text = "Registrar";
            this.registrarToolStripMenuItem3.Click += new System.EventHandler(this.registrarToolStripMenuItem3_Click);
            // 
            // modificarToolStripMenuItem1
            // 
            this.modificarToolStripMenuItem1.Name = "modificarToolStripMenuItem1";
            this.modificarToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem1.Text = "Modificar";
            this.modificarToolStripMenuItem1.Click += new System.EventHandler(this.modificarToolStripMenuItem1_Click);
            // 
            // pagosToolStripMenuItem1
            // 
            this.pagosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem15,
            this.consultarToolStripMenuItem2,
            this.modificarToolStripMenuItem2,
            this.verificarToolStripMenuItem1,
            this.listaToolStripMenuItem1});
            this.pagosToolStripMenuItem1.Name = "pagosToolStripMenuItem1";
            this.pagosToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.pagosToolStripMenuItem1.Text = "Pagos";
            // 
            // registrarToolStripMenuItem15
            // 
            this.registrarToolStripMenuItem15.Name = "registrarToolStripMenuItem15";
            this.registrarToolStripMenuItem15.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem15.Text = "Registrar";
            this.registrarToolStripMenuItem15.Click += new System.EventHandler(this.registrarToolStripMenuItem15_Click);
            // 
            // consultarToolStripMenuItem2
            // 
            this.consultarToolStripMenuItem2.Name = "consultarToolStripMenuItem2";
            this.consultarToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.consultarToolStripMenuItem2.Text = "Consultar";
            this.consultarToolStripMenuItem2.Click += new System.EventHandler(this.consultarToolStripMenuItem2_Click);
            // 
            // modificarToolStripMenuItem2
            // 
            this.modificarToolStripMenuItem2.Name = "modificarToolStripMenuItem2";
            this.modificarToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem2.Text = "Modificar";
            this.modificarToolStripMenuItem2.Click += new System.EventHandler(this.modificarToolStripMenuItem2_Click);
            // 
            // verificarToolStripMenuItem1
            // 
            this.verificarToolStripMenuItem1.Name = "verificarToolStripMenuItem1";
            this.verificarToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.verificarToolStripMenuItem1.Text = "Verificar";
            this.verificarToolStripMenuItem1.Click += new System.EventHandler(this.verificarToolStripMenuItem1_Click);
            // 
            // listaToolStripMenuItem1
            // 
            this.listaToolStripMenuItem1.Name = "listaToolStripMenuItem1";
            this.listaToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.listaToolStripMenuItem1.Text = "Lista";
            this.listaToolStripMenuItem1.Click += new System.EventHandler(this.listaToolStripMenuItem1_Click);
            // 
            // preciosToolStripMenuItem1
            // 
            this.preciosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem16,
            this.consultarToolStripMenuItem3,
            this.modificarToolStripMenuItem15});
            this.preciosToolStripMenuItem1.Name = "preciosToolStripMenuItem1";
            this.preciosToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.preciosToolStripMenuItem1.Text = "Precios";
            // 
            // registrarToolStripMenuItem16
            // 
            this.registrarToolStripMenuItem16.Name = "registrarToolStripMenuItem16";
            this.registrarToolStripMenuItem16.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem16.Text = "Registrar";
            // 
            // consultarToolStripMenuItem3
            // 
            this.consultarToolStripMenuItem3.Name = "consultarToolStripMenuItem3";
            this.consultarToolStripMenuItem3.Size = new System.Drawing.Size(152, 22);
            this.consultarToolStripMenuItem3.Text = "Consultar";
            this.consultarToolStripMenuItem3.Click += new System.EventHandler(this.consultarToolStripMenuItem3_Click);
            // 
            // modificarToolStripMenuItem15
            // 
            this.modificarToolStripMenuItem15.Name = "modificarToolStripMenuItem15";
            this.modificarToolStripMenuItem15.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem15.Text = "Modificar";
            this.modificarToolStripMenuItem15.Click += new System.EventHandler(this.modificarToolStripMenuItem15_Click);
            // 
            // perfilToolStripMenuItem1
            // 
            this.perfilToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificarMiContraseñaToolStripMenuItem1});
            this.perfilToolStripMenuItem1.Name = "perfilToolStripMenuItem1";
            this.perfilToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.perfilToolStripMenuItem1.Text = "Perfil";
            // 
            // modificarMiContraseñaToolStripMenuItem1
            // 
            this.modificarMiContraseñaToolStripMenuItem1.Name = "modificarMiContraseñaToolStripMenuItem1";
            this.modificarMiContraseñaToolStripMenuItem1.Size = new System.Drawing.Size(203, 22);
            this.modificarMiContraseñaToolStripMenuItem1.Text = "Modificar mi contraseña";
            this.modificarMiContraseñaToolStripMenuItem1.Click += new System.EventHandler(this.modificarMiContraseñaToolStripMenuItem1_Click);
            // 
            // salarioToolStripMenuItem1
            // 
            this.salarioToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asignacionesToolStripMenuItem1,
            this.deduccionesToolStripMenuItem1,
            this.reciboDePagoToolStripMenuItem1});
            this.salarioToolStripMenuItem1.Name = "salarioToolStripMenuItem1";
            this.salarioToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.salarioToolStripMenuItem1.Text = "Salario";
            // 
            // asignacionesToolStripMenuItem1
            // 
            this.asignacionesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem1,
            this.modificarToolStripMenuItem13});
            this.asignacionesToolStripMenuItem1.Name = "asignacionesToolStripMenuItem1";
            this.asignacionesToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.asignacionesToolStripMenuItem1.Text = "Asignaciones";
            // 
            // registrarToolStripMenuItem1
            // 
            this.registrarToolStripMenuItem1.Name = "registrarToolStripMenuItem1";
            this.registrarToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem1.Text = "Registrar";
            this.registrarToolStripMenuItem1.Click += new System.EventHandler(this.registrarToolStripMenuItem1_Click);
            // 
            // modificarToolStripMenuItem13
            // 
            this.modificarToolStripMenuItem13.Name = "modificarToolStripMenuItem13";
            this.modificarToolStripMenuItem13.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem13.Text = "Modificar";
            this.modificarToolStripMenuItem13.Click += new System.EventHandler(this.modificarToolStripMenuItem13_Click);
            // 
            // deduccionesToolStripMenuItem1
            // 
            this.deduccionesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem14,
            this.modificarToolStripMenuItem14});
            this.deduccionesToolStripMenuItem1.Name = "deduccionesToolStripMenuItem1";
            this.deduccionesToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.deduccionesToolStripMenuItem1.Text = "Deducciones";
            // 
            // registrarToolStripMenuItem14
            // 
            this.registrarToolStripMenuItem14.Name = "registrarToolStripMenuItem14";
            this.registrarToolStripMenuItem14.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem14.Text = "Registrar";
            this.registrarToolStripMenuItem14.Click += new System.EventHandler(this.registrarToolStripMenuItem14_Click);
            // 
            // modificarToolStripMenuItem14
            // 
            this.modificarToolStripMenuItem14.Name = "modificarToolStripMenuItem14";
            this.modificarToolStripMenuItem14.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem14.Text = "Modificar";
            this.modificarToolStripMenuItem14.Click += new System.EventHandler(this.modificarToolStripMenuItem14_Click);
            // 
            // reciboDePagoToolStripMenuItem1
            // 
            this.reciboDePagoToolStripMenuItem1.Name = "reciboDePagoToolStripMenuItem1";
            this.reciboDePagoToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.reciboDePagoToolStripMenuItem1.Text = "Recibo de Pago";
            this.reciboDePagoToolStripMenuItem1.Click += new System.EventHandler(this.reciboDePagoToolStripMenuItem1_Click);
            // 
            // solicitudToolStripMenuItem1
            // 
            this.solicitudToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.certificaciónDeCalificacionesToolStripMenuItem1,
            this.certificaciónEgredadosToolStripMenuItem1,
            this.certificaciónRegularesToolStripMenuItem1,
            this.listarCertificacionesToolStripMenuItem1,
            this.constanciaDeEstudioToolStripMenuItem1,
            this.postulaciónParaPasantíasToolStripMenuItem1});
            this.solicitudToolStripMenuItem1.Name = "solicitudToolStripMenuItem1";
            this.solicitudToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.solicitudToolStripMenuItem1.Text = "Solicitud";
            // 
            // certificaciónDeCalificacionesToolStripMenuItem1
            // 
            this.certificaciónDeCalificacionesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem20,
            this.modificarToolStripMenuItem18});
            this.certificaciónDeCalificacionesToolStripMenuItem1.Name = "certificaciónDeCalificacionesToolStripMenuItem1";
            this.certificaciónDeCalificacionesToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.certificaciónDeCalificacionesToolStripMenuItem1.Text = "Certificación de Calificaciones";
            // 
            // registrarToolStripMenuItem20
            // 
            this.registrarToolStripMenuItem20.Name = "registrarToolStripMenuItem20";
            this.registrarToolStripMenuItem20.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem20.Text = "Registrar";
            this.registrarToolStripMenuItem20.Click += new System.EventHandler(this.registrarToolStripMenuItem20_Click);
            // 
            // modificarToolStripMenuItem18
            // 
            this.modificarToolStripMenuItem18.Name = "modificarToolStripMenuItem18";
            this.modificarToolStripMenuItem18.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem18.Text = "Modificar";
            this.modificarToolStripMenuItem18.Click += new System.EventHandler(this.modificarToolStripMenuItem18_Click);
            // 
            // certificaciónEgredadosToolStripMenuItem1
            // 
            this.certificaciónEgredadosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem21,
            this.modificarToolStripMenuItem19});
            this.certificaciónEgredadosToolStripMenuItem1.Name = "certificaciónEgredadosToolStripMenuItem1";
            this.certificaciónEgredadosToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.certificaciónEgredadosToolStripMenuItem1.Text = "Certificación Egredados";
            // 
            // registrarToolStripMenuItem21
            // 
            this.registrarToolStripMenuItem21.Name = "registrarToolStripMenuItem21";
            this.registrarToolStripMenuItem21.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem21.Text = "Registrar";
            this.registrarToolStripMenuItem21.Click += new System.EventHandler(this.registrarToolStripMenuItem21_Click);
            // 
            // modificarToolStripMenuItem19
            // 
            this.modificarToolStripMenuItem19.Name = "modificarToolStripMenuItem19";
            this.modificarToolStripMenuItem19.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem19.Text = "Modificar";
            this.modificarToolStripMenuItem19.Click += new System.EventHandler(this.modificarToolStripMenuItem19_Click);
            // 
            // certificaciónRegularesToolStripMenuItem1
            // 
            this.certificaciónRegularesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem22,
            this.modificarToolStripMenuItem20});
            this.certificaciónRegularesToolStripMenuItem1.Name = "certificaciónRegularesToolStripMenuItem1";
            this.certificaciónRegularesToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.certificaciónRegularesToolStripMenuItem1.Text = "Certificación Regulares";
            // 
            // registrarToolStripMenuItem22
            // 
            this.registrarToolStripMenuItem22.Name = "registrarToolStripMenuItem22";
            this.registrarToolStripMenuItem22.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem22.Text = "Registrar";
            this.registrarToolStripMenuItem22.Click += new System.EventHandler(this.registrarToolStripMenuItem22_Click);
            // 
            // modificarToolStripMenuItem20
            // 
            this.modificarToolStripMenuItem20.Name = "modificarToolStripMenuItem20";
            this.modificarToolStripMenuItem20.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem20.Text = "Modificar";
            this.modificarToolStripMenuItem20.Click += new System.EventHandler(this.modificarToolStripMenuItem20_Click);
            // 
            // listarCertificacionesToolStripMenuItem1
            // 
            this.listarCertificacionesToolStripMenuItem1.Name = "listarCertificacionesToolStripMenuItem1";
            this.listarCertificacionesToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.listarCertificacionesToolStripMenuItem1.Text = "Listar Certificaciones";
            this.listarCertificacionesToolStripMenuItem1.Click += new System.EventHandler(this.listarCertificacionesToolStripMenuItem1_Click);
            // 
            // constanciaDeEstudioToolStripMenuItem1
            // 
            this.constanciaDeEstudioToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem23,
            this.modificarToolStripMenuItem21});
            this.constanciaDeEstudioToolStripMenuItem1.Name = "constanciaDeEstudioToolStripMenuItem1";
            this.constanciaDeEstudioToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.constanciaDeEstudioToolStripMenuItem1.Text = "Constancia de Estudio";
            // 
            // registrarToolStripMenuItem23
            // 
            this.registrarToolStripMenuItem23.Name = "registrarToolStripMenuItem23";
            this.registrarToolStripMenuItem23.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem23.Text = "Registrar";
            this.registrarToolStripMenuItem23.Click += new System.EventHandler(this.registrarToolStripMenuItem23_Click);
            // 
            // modificarToolStripMenuItem21
            // 
            this.modificarToolStripMenuItem21.Name = "modificarToolStripMenuItem21";
            this.modificarToolStripMenuItem21.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem21.Text = "Modificar";
            this.modificarToolStripMenuItem21.Click += new System.EventHandler(this.modificarToolStripMenuItem21_Click);
            // 
            // postulaciónParaPasantíasToolStripMenuItem1
            // 
            this.postulaciónParaPasantíasToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem24,
            this.modificarToolStripMenuItem22});
            this.postulaciónParaPasantíasToolStripMenuItem1.Name = "postulaciónParaPasantíasToolStripMenuItem1";
            this.postulaciónParaPasantíasToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.postulaciónParaPasantíasToolStripMenuItem1.Text = "Postulación para Pasantías";
            // 
            // registrarToolStripMenuItem24
            // 
            this.registrarToolStripMenuItem24.Name = "registrarToolStripMenuItem24";
            this.registrarToolStripMenuItem24.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem24.Text = "Registrar";
            this.registrarToolStripMenuItem24.Click += new System.EventHandler(this.registrarToolStripMenuItem24_Click);
            // 
            // modificarToolStripMenuItem22
            // 
            this.modificarToolStripMenuItem22.Name = "modificarToolStripMenuItem22";
            this.modificarToolStripMenuItem22.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem22.Text = "Modificar";
            this.modificarToolStripMenuItem22.Click += new System.EventHandler(this.modificarToolStripMenuItem22_Click);
            // 
            // usuarioToolStripMenuItem1
            // 
            this.usuarioToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem19,
            this.modificarRestContraseñaToolStripMenuItem1});
            this.usuarioToolStripMenuItem1.Name = "usuarioToolStripMenuItem1";
            this.usuarioToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.usuarioToolStripMenuItem1.Text = "Usuario";
            // 
            // registrarToolStripMenuItem19
            // 
            this.registrarToolStripMenuItem19.Name = "registrarToolStripMenuItem19";
            this.registrarToolStripMenuItem19.Size = new System.Drawing.Size(221, 22);
            this.registrarToolStripMenuItem19.Text = "Registrar";
            this.registrarToolStripMenuItem19.Click += new System.EventHandler(this.registrarToolStripMenuItem19_Click);
            // 
            // modificarRestContraseñaToolStripMenuItem1
            // 
            this.modificarRestContraseñaToolStripMenuItem1.Name = "modificarRestContraseñaToolStripMenuItem1";
            this.modificarRestContraseñaToolStripMenuItem1.Size = new System.Drawing.Size(221, 22);
            this.modificarRestContraseñaToolStripMenuItem1.Text = "Modificar/Rest. Contraseña";
            this.modificarRestContraseñaToolStripMenuItem1.Click += new System.EventHandler(this.modificarRestContraseñaToolStripMenuItem1_Click);
            // 
            // salirToolStripMenuItem1
            // 
            this.salirToolStripMenuItem1.Name = "salirToolStripMenuItem1";
            this.salirToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.salirToolStripMenuItem1.Text = "Salir";
            this.salirToolStripMenuItem1.Click += new System.EventHandler(this.salirToolStripMenuItem1_Click);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem29,
            this.toolStripMenuItem39,
            this.toolStripMenuItem34});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(341, 24);
            this.menuStrip2.TabIndex = 278;
            this.menuStrip2.Text = "menuStrip2";
            this.menuStrip2.Visible = false;
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(49, 20);
            this.toolStripMenuItem3.Text = "Inicio";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.toolStripMenuItem8,
            this.toolStripMenuItem17,
            this.toolStripMenuItem21,
            this.toolStripMenuItem25});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem4.Text = "Académico";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem6,
            this.toolStripMenuItem7});
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem5.Text = "Calificaciones";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(195, 22);
            this.toolStripMenuItem6.Text = "Cargar/Modificar";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(195, 22);
            this.toolStripMenuItem7.Text = "Modificar a Estudiante";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem9,
            this.toolStripMenuItem10,
            this.toolStripMenuItem11,
            this.toolStripMenuItem12,
            this.toolStripMenuItem13,
            this.toolStripMenuItem14});
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem8.Text = "Disciplina";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem9.Text = "Registrar";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem10.Text = "Modificar";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem11.Text = "Disponibles";
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem12.Text = "Resumen";
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem13.Text = "Lista de Estudiantes";
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem15,
            this.toolStripMenuItem16});
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem14.Text = "Tipos";
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem15.Text = "Registrar";
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem16.Text = "Modificar";
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem18,
            this.toolStripMenuItem19,
            this.toolStripMenuItem20});
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem17.Text = "Formalización";
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem18.Text = "Registrar";
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem19.Text = "Modificar";
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem20.Text = "Cambio";
            // 
            // toolStripMenuItem21
            // 
            this.toolStripMenuItem21.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem22,
            this.toolStripMenuItem23,
            this.toolStripMenuItem24});
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            this.toolStripMenuItem21.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem21.Text = "Periodo Académico";
            // 
            // toolStripMenuItem22
            // 
            this.toolStripMenuItem22.Name = "toolStripMenuItem22";
            this.toolStripMenuItem22.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem22.Text = "Registrar";
            // 
            // toolStripMenuItem23
            // 
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            this.toolStripMenuItem23.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem23.Text = "Modificar";
            // 
            // toolStripMenuItem24
            // 
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            this.toolStripMenuItem24.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem24.Text = "Cerrar";
            // 
            // toolStripMenuItem25
            // 
            this.toolStripMenuItem25.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem26,
            this.toolStripMenuItem27,
            this.toolStripMenuItem28});
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            this.toolStripMenuItem25.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem25.Text = "Preinscripción";
            // 
            // toolStripMenuItem26
            // 
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            this.toolStripMenuItem26.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem26.Text = "Registrar";
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem27.Text = "Modificar";
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem28.Text = "Pendientes";
            // 
            // toolStripMenuItem29
            // 
            this.toolStripMenuItem29.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem30,
            this.toolStripMenuItem31,
            this.toolStripMenuItem32});
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            this.toolStripMenuItem29.Size = new System.Drawing.Size(76, 20);
            this.toolStripMenuItem29.Text = "Estudiante";
            // 
            // toolStripMenuItem30
            // 
            this.toolStripMenuItem30.Name = "toolStripMenuItem30";
            this.toolStripMenuItem30.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem30.Text = "Registrar";
            // 
            // toolStripMenuItem31
            // 
            this.toolStripMenuItem31.Name = "toolStripMenuItem31";
            this.toolStripMenuItem31.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem31.Text = "Modificar";
            // 
            // toolStripMenuItem32
            // 
            this.toolStripMenuItem32.Name = "toolStripMenuItem32";
            this.toolStripMenuItem32.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem32.Text = "Historial";
            // 
            // toolStripMenuItem39
            // 
            this.toolStripMenuItem39.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem40,
            this.toolStripMenuItem43,
            this.toolStripMenuItem49});
            this.toolStripMenuItem39.Name = "toolStripMenuItem39";
            this.toolStripMenuItem39.Size = new System.Drawing.Size(66, 20);
            this.toolStripMenuItem39.Text = "Finanzas";
            // 
            // toolStripMenuItem40
            // 
            this.toolStripMenuItem40.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem41,
            this.toolStripMenuItem42});
            this.toolStripMenuItem40.Name = "toolStripMenuItem40";
            this.toolStripMenuItem40.Size = new System.Drawing.Size(113, 22);
            this.toolStripMenuItem40.Text = "Banco";
            // 
            // toolStripMenuItem41
            // 
            this.toolStripMenuItem41.Name = "toolStripMenuItem41";
            this.toolStripMenuItem41.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem41.Text = "Registrar";
            // 
            // toolStripMenuItem42
            // 
            this.toolStripMenuItem42.Name = "toolStripMenuItem42";
            this.toolStripMenuItem42.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem42.Text = "Modificar";
            // 
            // toolStripMenuItem43
            // 
            this.toolStripMenuItem43.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem44,
            this.toolStripMenuItem45,
            this.toolStripMenuItem46,
            this.toolStripMenuItem47,
            this.toolStripMenuItem48});
            this.toolStripMenuItem43.Name = "toolStripMenuItem43";
            this.toolStripMenuItem43.Size = new System.Drawing.Size(113, 22);
            this.toolStripMenuItem43.Text = "Pagos";
            // 
            // toolStripMenuItem44
            // 
            this.toolStripMenuItem44.Name = "toolStripMenuItem44";
            this.toolStripMenuItem44.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem44.Text = "Registrar";
            // 
            // toolStripMenuItem45
            // 
            this.toolStripMenuItem45.Name = "toolStripMenuItem45";
            this.toolStripMenuItem45.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem45.Text = "Consultar";
            // 
            // toolStripMenuItem46
            // 
            this.toolStripMenuItem46.Name = "toolStripMenuItem46";
            this.toolStripMenuItem46.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem46.Text = "Modificar";
            // 
            // toolStripMenuItem47
            // 
            this.toolStripMenuItem47.Name = "toolStripMenuItem47";
            this.toolStripMenuItem47.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem47.Text = "Verificar";
            // 
            // toolStripMenuItem48
            // 
            this.toolStripMenuItem48.Name = "toolStripMenuItem48";
            this.toolStripMenuItem48.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem48.Text = "Lista";
            // 
            // toolStripMenuItem49
            // 
            this.toolStripMenuItem49.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem50,
            this.toolStripMenuItem51,
            this.toolStripMenuItem52});
            this.toolStripMenuItem49.Name = "toolStripMenuItem49";
            this.toolStripMenuItem49.Size = new System.Drawing.Size(113, 22);
            this.toolStripMenuItem49.Text = "Precios";
            // 
            // toolStripMenuItem50
            // 
            this.toolStripMenuItem50.Name = "toolStripMenuItem50";
            this.toolStripMenuItem50.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem50.Text = "Registrar";
            // 
            // toolStripMenuItem51
            // 
            this.toolStripMenuItem51.Name = "toolStripMenuItem51";
            this.toolStripMenuItem51.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem51.Text = "Consultar";
            // 
            // toolStripMenuItem52
            // 
            this.toolStripMenuItem52.Name = "toolStripMenuItem52";
            this.toolStripMenuItem52.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem52.Text = "Modificar";
            // 
            // toolStripMenuItem34
            // 
            this.toolStripMenuItem34.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.perfilToolStripMenuItem,
            this.solicitudToolStripMenuItem,
            this.usuarioToolStripMenuItem,
            this.toolStripMenuItem35});
            this.toolStripMenuItem34.Name = "toolStripMenuItem34";
            this.toolStripMenuItem34.Size = new System.Drawing.Size(26, 20);
            this.toolStripMenuItem34.Text = "+";
            // 
            // perfilToolStripMenuItem
            // 
            this.perfilToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificarMiContraseñaToolStripMenuItem});
            this.perfilToolStripMenuItem.Name = "perfilToolStripMenuItem";
            this.perfilToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.perfilToolStripMenuItem.Text = "Perfil";
            // 
            // modificarMiContraseñaToolStripMenuItem
            // 
            this.modificarMiContraseñaToolStripMenuItem.Name = "modificarMiContraseñaToolStripMenuItem";
            this.modificarMiContraseñaToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.modificarMiContraseñaToolStripMenuItem.Text = "Modificar mi contraseña";
            // 
            // solicitudToolStripMenuItem
            // 
            this.solicitudToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.certificaciónDeCalificacionesToolStripMenuItem,
            this.certificaciónEgredadosToolStripMenuItem,
            this.certificaciónRegularesToolStripMenuItem,
            this.listarCertificacionesToolStripMenuItem,
            this.constanciaDeEstudioToolStripMenuItem,
            this.postulaciónParaPasantíasToolStripMenuItem});
            this.solicitudToolStripMenuItem.Name = "solicitudToolStripMenuItem";
            this.solicitudToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.solicitudToolStripMenuItem.Text = "Solicitud";
            // 
            // certificaciónDeCalificacionesToolStripMenuItem
            // 
            this.certificaciónDeCalificacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem11,
            this.modificarToolStripMenuItem10});
            this.certificaciónDeCalificacionesToolStripMenuItem.Name = "certificaciónDeCalificacionesToolStripMenuItem";
            this.certificaciónDeCalificacionesToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.certificaciónDeCalificacionesToolStripMenuItem.Text = "Certificación de Calificaciones";
            // 
            // registrarToolStripMenuItem11
            // 
            this.registrarToolStripMenuItem11.Name = "registrarToolStripMenuItem11";
            this.registrarToolStripMenuItem11.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem11.Text = "Registrar";
            // 
            // modificarToolStripMenuItem10
            // 
            this.modificarToolStripMenuItem10.Name = "modificarToolStripMenuItem10";
            this.modificarToolStripMenuItem10.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem10.Text = "Modificar";
            // 
            // certificaciónEgredadosToolStripMenuItem
            // 
            this.certificaciónEgredadosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem12,
            this.modificarToolStripMenuItem11});
            this.certificaciónEgredadosToolStripMenuItem.Name = "certificaciónEgredadosToolStripMenuItem";
            this.certificaciónEgredadosToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.certificaciónEgredadosToolStripMenuItem.Text = "Certificación Egredados";
            // 
            // registrarToolStripMenuItem12
            // 
            this.registrarToolStripMenuItem12.Name = "registrarToolStripMenuItem12";
            this.registrarToolStripMenuItem12.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem12.Text = "Registrar";
            // 
            // modificarToolStripMenuItem11
            // 
            this.modificarToolStripMenuItem11.Name = "modificarToolStripMenuItem11";
            this.modificarToolStripMenuItem11.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem11.Text = "Modificar";
            // 
            // certificaciónRegularesToolStripMenuItem
            // 
            this.certificaciónRegularesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem13,
            this.modificarToolStripMenuItem12});
            this.certificaciónRegularesToolStripMenuItem.Name = "certificaciónRegularesToolStripMenuItem";
            this.certificaciónRegularesToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.certificaciónRegularesToolStripMenuItem.Text = "Certificación Regulares";
            // 
            // registrarToolStripMenuItem13
            // 
            this.registrarToolStripMenuItem13.Name = "registrarToolStripMenuItem13";
            this.registrarToolStripMenuItem13.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem13.Text = "Registrar";
            // 
            // modificarToolStripMenuItem12
            // 
            this.modificarToolStripMenuItem12.Name = "modificarToolStripMenuItem12";
            this.modificarToolStripMenuItem12.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem12.Text = "Modificar";
            // 
            // listarCertificacionesToolStripMenuItem
            // 
            this.listarCertificacionesToolStripMenuItem.Name = "listarCertificacionesToolStripMenuItem";
            this.listarCertificacionesToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.listarCertificacionesToolStripMenuItem.Text = "Listar Certificaciones";
            // 
            // constanciaDeEstudioToolStripMenuItem
            // 
            this.constanciaDeEstudioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem17,
            this.modificarToolStripMenuItem16});
            this.constanciaDeEstudioToolStripMenuItem.Name = "constanciaDeEstudioToolStripMenuItem";
            this.constanciaDeEstudioToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.constanciaDeEstudioToolStripMenuItem.Text = "Constancia de Estudio";
            // 
            // registrarToolStripMenuItem17
            // 
            this.registrarToolStripMenuItem17.Name = "registrarToolStripMenuItem17";
            this.registrarToolStripMenuItem17.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem17.Text = "Registrar";
            // 
            // modificarToolStripMenuItem16
            // 
            this.modificarToolStripMenuItem16.Name = "modificarToolStripMenuItem16";
            this.modificarToolStripMenuItem16.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem16.Text = "Modificar";
            // 
            // postulaciónParaPasantíasToolStripMenuItem
            // 
            this.postulaciónParaPasantíasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registarToolStripMenuItem,
            this.modificarToolStripMenuItem17});
            this.postulaciónParaPasantíasToolStripMenuItem.Name = "postulaciónParaPasantíasToolStripMenuItem";
            this.postulaciónParaPasantíasToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.postulaciónParaPasantíasToolStripMenuItem.Text = "Postulación para Pasantías";
            // 
            // registarToolStripMenuItem
            // 
            this.registarToolStripMenuItem.Name = "registarToolStripMenuItem";
            this.registarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.registarToolStripMenuItem.Text = "Registar";
            // 
            // modificarToolStripMenuItem17
            // 
            this.modificarToolStripMenuItem17.Name = "modificarToolStripMenuItem17";
            this.modificarToolStripMenuItem17.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem17.Text = "Modificar";
            // 
            // usuarioToolStripMenuItem
            // 
            this.usuarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem2,
            this.modificarRestContraseñaToolStripMenuItem});
            this.usuarioToolStripMenuItem.Name = "usuarioToolStripMenuItem";
            this.usuarioToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.usuarioToolStripMenuItem.Text = "Usuario";
            // 
            // registrarToolStripMenuItem2
            // 
            this.registrarToolStripMenuItem2.Name = "registrarToolStripMenuItem2";
            this.registrarToolStripMenuItem2.Size = new System.Drawing.Size(221, 22);
            this.registrarToolStripMenuItem2.Text = "Registrar";
            // 
            // modificarRestContraseñaToolStripMenuItem
            // 
            this.modificarRestContraseñaToolStripMenuItem.Name = "modificarRestContraseñaToolStripMenuItem";
            this.modificarRestContraseñaToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.modificarRestContraseñaToolStripMenuItem.Text = "Modificar/Rest. Contraseña";
            // 
            // toolStripMenuItem35
            // 
            this.toolStripMenuItem35.Name = "toolStripMenuItem35";
            this.toolStripMenuItem35.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItem35.Text = "Salir";
            // 
            // cerrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(341, 184);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox3);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "cerrar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cerrar Periodo Académico";
            this.Load += new System.EventHandler(this.cerrar_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem académicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarModificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarAEstudianteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disciplinasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem disponiblesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resumenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaDeEstudiantesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tiposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem formalizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem cambioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem periodoAcadémicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem cerrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preinscripciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem pendientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estudianteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empleadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeTrabajoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeActividadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem finanzasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bancoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pagosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem verificarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem listaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem preciosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem perfilToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem modificarMiContraseñaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salarioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem asignacionesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem deduccionesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem reciboDePagoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem solicitudToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem certificaciónDeCalificacionesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem certificaciónEgredadosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem certificaciónRegularesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem listarCertificacionesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeEstudioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem postulaciónParaPasantíasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem usuarioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem modificarRestContraseñaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem29;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem30;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem31;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem32;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem39;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem40;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem41;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem42;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem43;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem44;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem45;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem46;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem47;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem48;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem49;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem50;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem51;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem52;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem34;
        private System.Windows.Forms.ToolStripMenuItem perfilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarMiContraseñaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solicitudToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem certificaciónDeCalificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem certificaciónEgredadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem certificaciónRegularesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem listarCertificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeEstudioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem postulaciónParaPasantíasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem usuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem modificarRestContraseñaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem35;
    }
}