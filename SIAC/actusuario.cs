﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class actusuario : Form
    {
        public actusuario()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void actusuario_Load(object sender, EventArgs e)
        {
            conexion.Open();
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
        }
        string ci;

        void buscar()
        {
            if (textBox1.Text != "")
            {
                ci = "";
                var cadena = textBox1.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM usuario WHERE nombre = '" + textBox1.Text + "'";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        comboBox3.Enabled = true;
                        comboBox2.Enabled = true;
                        comboBox2.Text = lee["nivel"].ToString();
                        comboBox3.Text = lee["estatus"].ToString();
                        string buscarcxx = "SELECT * FROM empleado WHERE ci = '" + textBox1.Text + "';";
                        OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
                        OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
                        OleDbDataReader leexx = comandoxx.ExecuteReader();
                        if (leexx.Read() == true)
                        {
                            ci = textBox1.Text;
                            textBox2.Text = leexx["nombre"].ToString() + " " + leexx["apellido"].ToString();
                            string buscarcx = "SELECT * FROM cargo WHERE cod = " + leexx["cargo"].ToString() + ";";
                            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                            OleDbDataReader leex = comandox.ExecuteReader();
                            if (leex.Read() == true)
                            {
                                textBox3.Text = leex["cargo"].ToString();
                            }
                            button5.Visible = true;
                            button2.Visible = true;
                            button3.Visible = true;
                        }
                        
                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad '" + textBox1.Text + "' no se encuentra registrada como usuario de acceso al sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para realizar una búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscar();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        void clear()
        {
            ci = "";
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox1.Focus();
            comboBox3.Text = "";
            comboBox2.Text = "";
            comboBox3.Enabled = false;
            comboBox2.Enabled = false;
            button2.Visible = false;
            button3.Visible = false;
            button5.Visible = false;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (ci != "")
            {
                if (MessageBox.Show("¿Desea reiniciar la contraseña del usuario '" + ci + "'?", "Restaurar contraseña del usuario", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string insertar = "UPDATE  usuario SET  pass = '" + ci + "' WHERE nombre = '" + ci + "' ";
                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("La contraseña fue reiniciada con éxito. La nueva contraseña del usuario es su Documento de identidad.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Debe hacer la búsqueda de un usuario previamente para poder utilizar esta función.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ci != "")
            {
                string x = comboBox3.Text;
                if (comboBox3.Text != "" && comboBox2.Text != "")
                {
                    try
                    {

                        string insertarx = "UPDATE  usuario SET  nivel = '" + comboBox2.Text + "', estatus = '" + comboBox3.Text + "' WHERE nombre = '" + ci + "';";
                        OleDbCommand cmdx = new OleDbCommand(insertarx, conexion);
                        cmdx.ExecuteNonQuery();
                        //insertarx = "UPDATE  usuario SET  nivel = '" + comboBox2.Text + "' AND estatus = '" + comboBox3.Text + "' WHERE nombre = '" + ci + "';";
                        MessageBox.Show("Los datos del usuario fueron modificados con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        clear();
                    }
                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Todos los campos son requeridos para poder utilizar esta función.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Debe hacer la búsqueda de un usuario previamente para poder utilizar esta función.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            usuario a = new usuario();
            a.Show();
            this.Close();
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


        
    }
}
