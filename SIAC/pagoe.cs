﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Drawing.Printing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;
using System.Globalization;

namespace SIAC
{
    public partial class pagoe : Form
    {
        public pagoe()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        
        private void pagoe_Load(object sender, EventArgs e)
        {
            conexion.Open();
            textBox1.Focus();
            combo1();
            combo2();
            combo3();
            combo4();
            label13.Text = "Bs. " + String.Format("{0:0,0.00}", (sa));
            label14.Text = "Bs. " + String.Format("{0:0,0.00}", (sd));
            tota();
        }
        string ci = "", fechai;
        DateTime var;
        void buscar()
        {
            if (textBox1.Text != "")
            {
                ci = "";
                var cadena = textBox1.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM empleado WHERE ci = '" + textBox1.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        if (lee["estatus"].ToString() == "ACTIVO")
                        {
                            ci = textBox1.Text;
                            textBox2.Text = lee["nombre"].ToString() + " " + lee["apellido"].ToString();
                            fechai = lee["fechai"].ToString();
                            string buscarcx = "SELECT * FROM cargo WHERE cod = " + lee["cargo"].ToString() + ";";
                            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                            OleDbDataReader leex = comandox.ExecuteReader();
                            if (leex.Read() == true)
                            {
                                textBox3.Text = leex["cargo"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("El documento de identidad '" + textBox1.Text + "' no se encuentra activo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            textBox1.Focus();
                        }


                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad '" + textBox1.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad válido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        void combo1()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Cliente' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox4.ValueMember = "cod";
            comboBox4.DisplayMember = "banco";
            comboBox4.DataSource = dt;
        }

        void combo2()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Instituto' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "banco";
            comboBox3.DataSource = dt;
        }

        void combo3()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM abono ORDER BY abono";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["abono"] = "Seleccione una asignación";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox1.ValueMember = "cod";
            comboBox1.DisplayMember = "abono";
            comboBox1.DataSource = dt;
        }

        void combo4()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM dedu ORDER BY dedu";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["dedu"] = "Seleccione una deducción";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "dedu";
            comboBox2.DataSource = dt;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            buscar();
        }

        void ocultar()
        {
            textBox8.Visible = false;
            comboBox3.Visible = false; 
            comboBox4.Visible = false;
            label18.Visible = false;
            label17.Visible = false;
            label16.Visible = false;
        }
       
        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox5.Text != "")
            {
                if (comboBox5.Text == "Efectivo")
                {
                    ocultar();
                }
                if (comboBox5.Text == "Cheque")
                {
                    ocultar();
                    textBox8.Visible = true;
                    comboBox3.Visible = true;
                    label18.Visible = true;
                    label17.Visible = true;
                }
                if (comboBox5.Text == "Deposito")
                {
                    ocultar();
                    textBox8.Visible = true;
                    comboBox4.Visible = true;
                    label16.Visible = true;
                    label17.Visible = true;
                }
                if (comboBox5.Text == "Trans. Bancaria" || comboBox5.Text == "Pago Rápido")
                {
                    ocultar();
                    textBox8.Visible = true;
                    comboBox4.Visible = true;
                    comboBox3.Visible = true;
                    label16.Visible = true;
                    label17.Visible = true;
                    label18.Visible = true;
                }
            }
            else
            {
                ocultar();
            }
        }
        double va = 0, vd = 0, sa= 0, sd = 0, total = 0, cana = 0, cand = 0;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 0)
            {
                va = 0;
                string buscarc = "SELECT * FROM abono WHERE cod = " + comboBox1.SelectedValue.ToString() + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    textBox4.Text = String.Format("{0:0,0.00}", Double.Parse(lee["valor"].ToString()));
                    va = Double.Parse(lee["valor"].ToString());
                    textBox5.Focus();

                }
            }
            else
            {
                textBox4.Clear();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex != 0)
            {
                vd = 0;
                string buscarc = "SELECT * FROM dedu WHERE cod = " + comboBox2.SelectedValue.ToString() + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    textBox7.Text = lee["valor"].ToString();
                    vd = Double.Parse(lee["valor"].ToString());
                    textBox6.Focus();

                }
            }
            else
            {
                textBox7.Clear();
            }
        }
        private static void OnlyNumber(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        public bool IsNumeric(string x)
        {

            double retNum;

            try
            {
                retNum = Double.Parse(x);
                return true;
            }
            catch
            {
                return false;

            }



        }

        private void textBox5_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }
        int fila = 0, fila2 = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 0 && textBox5.Text != "" && textBox4.Text != "")
            {
                if(IsNumeric(textBox5.Text))
                {
                    cana = Double.Parse(textBox5.Text);
                    sa = sa + (cana * va);
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[fila].Cells[0].Value = comboBox1.Text;
                    dataGridView1.Rows[fila].Cells[1].Value = String.Format("{0:0,0.00}", (Double.Parse(textBox4.Text)));
                    dataGridView1.Rows[fila].Cells[2].Value = textBox5.Text;
                    dataGridView1.Rows[fila].Cells[3].Value = String.Format("{0:0,0.00}", (cana * va));
                    fila++;
                    label13.Text = "Bs. " + String.Format("{0:0,0.00}", (sa));
                    tota();
                    comboBox1.SelectedIndex = 0;
                    textBox4.Clear();
                    textBox5.Clear();
                }
                else
                {
                    MessageBox.Show("La cantidad ingresada no es válida. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox5.Focus();
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para agregar la asignación.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        void tota()
        {
            total = sa - sd;
            textBox9.Text = "Bs."+String.Format("{0:0,0.00}", (total));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex != 0 && textBox6.Text != "" && textBox7.Text != "")
            {
                if (IsNumeric(textBox6.Text))
                {
                    cand = Double.Parse(textBox6.Text);
                    sd = sd + (cand * vd);
                    dataGridView2.Rows.Add();
                    dataGridView2.Rows[fila2].Cells[0].Value = comboBox2.Text;
                    dataGridView2.Rows[fila2].Cells[1].Value = String.Format("{0:0,0.00}", (Double.Parse(textBox7.Text)));
                    dataGridView2.Rows[fila2].Cells[2].Value = textBox6.Text;
                    dataGridView2.Rows[fila2].Cells[3].Value = String.Format("{0:0,0.00}", (cand * vd));
                    fila2++;
                    label14.Text = "Bs. " + String.Format("{0:0,0.00}", (sd));
                    tota();
                    comboBox2.SelectedIndex = 0;
                    textBox6.Clear();
                    textBox7.Clear();
                }
                else
                {
                    MessageBox.Show("La cantidad ingresada no es valida. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox5.Focus();
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para agregar la deducción.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        void date1()
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            fila = 0;
            sa = 0;
            label13.Text = "Bs. " + String.Format("{0:0,0.00}", (sa));
            tota();
        }
        void date2()
        {
            dataGridView2.Rows.Clear();
            dataGridView2.Refresh();
            fila2 = 0;
            sd = 0;
            label14.Text = "Bs. " + String.Format("{0:0,0.00}", (sd));
            tota();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            date1();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            date2();
        }

        #region crearPDF
        private void To_pdf()
        {
            string ab, bc;
            ab = textBox2.Text;
            bc = textBox2.Text;
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            Document doc = new Document(PageSize.LETTER, 30, 30, 30, 30);
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:";
            saveFileDialog1.Title = "Guardar Recibo de Pago";
            saveFileDialog1.DefaultExt = "pdf";
            saveFileDialog1.Filter = "pdf Files (*.pdf)|*.pdf| All Files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.FileName = "ReciboPago" + ci + myTI.ToTitleCase(ab) + DateTime.Now.ToString("dd-MMM-yyyy"); ;
            string filename = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filename = saveFileDialog1.FileName;
            }
            if (filename.Trim() != "")
            {
                FileStream file = new FileStream(filename,
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite,
                FileShare.ReadWrite);
                PdfWriter wri = PdfWriter.GetInstance(doc, file);
                //wri.PageEvent = new Class1();
                //wri.PageEvent = new recibo();
                //PdfWriter.GetInstance(doc, file);

                doc.Open();
                string remito = "Generado por: (" + estatica.Ci + ") " + estatica.Nombre + " " + estatica.Ape + ", con ENROLL ONE.";
                string envio = "Fecha: " + DateTime.Now.ToString("dd MMMM yyyy, hh:mm:ss tt");

                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _standardFont22 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _standardFont22e = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _standardFont2 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _standardFont23 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font cabe = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                // Creamos una tabla que contendrá el nombre, apellido y país
                // de nuestros visitante.
                PdfPTable tb1 = new PdfPTable(2);
                tb1.WidthPercentage = 100;


                //agregando una imagen
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("logo.jpg");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_CENTER;
                float percentage = 0.0f;
                percentage = 100 / imagen.Width;
                imagen.ScalePercent(percentage * 100);
                imagen.SetAbsolutePosition(20, 730);
                PdfPCell ff1 = new PdfPCell(imagen);
                PdfPTable tbx = new PdfPTable(2);
                tbx.WidthPercentage = 100;
                //ff1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                ff1.BorderWidth = 0;

                Chunk chunk = new Chunk("                     RECIBO DE PAGO", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                PdfPCell ff2 = new PdfPCell(new Paragraph(chunk));
                ff2.BorderWidth = 0;
                //ff2.Colspan = 2;
                //ff2.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff2.Padding = 4;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tbx.AddCell(ff1);
                tbx.AddCell(ff2);
                float[] medidaCeldas = { 1f, 3f };

                // ASIGNAS LAS MEDIDAS A LA TABLA (ANCHO)
                tbx.SetWidths(medidaCeldas);
                doc.Add(tbx);
                //doc.Add(new Paragraph("                       "));


                ff2 = new PdfPCell(new Phrase("PERIODO: " + dateTimePicker1.Text + " al " + dateTimePicker2.Text, _standardFont2));
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                doc.Add(new Paragraph("                       "));

                ff2 = new PdfPCell(new Phrase("\nEMPLEADO: " + myTI.ToTitleCase(bc), _standardFont22e));
                ff2.BorderWidth = 0;
                //ff2.HorizontalAlignment = PdfPCell.ALIGN_JUSTIFIED;
                //tb1.AddCell(imagen);

                // Configuramos el título de las columnas de la tabla
                 ff1 = new PdfPCell(new Phrase("\nDOC. DE IDENTIDAD: "+ ci, _standardFont22e));
                ff1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                ff1.BorderWidth = 0;


                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                tb1.AddCell(ff1);


                ff2 = new PdfPCell(new Phrase("Fecha de Ingreso: " + fechai, _standardFont2));
                ff2.BorderWidth = 0;
                //ff2.HorizontalAlignment = PdfPCell.ALIGN_JUSTIFIED;
                //tb1.AddCell(imagen);

                // Configuramos el título de las columnas de la tabla
                ff1 = new PdfPCell(new Phrase("Cargo: " + textBox3.Text, _standardFont2));
                ff1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                ff1.BorderWidth = 0;


                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                tb1.AddCell(ff1);
                doc.Add(tb1);

                GenerarDocumento(doc);
                tb1 = new PdfPTable(2);
                tb1.WidthPercentage = 100;

                Phrase p1 = new Phrase();

                ff2 = new PdfPCell();
                bc = bc.ToLower();


                Chunk c1x = new Chunk("________________________________\n" + myTI.ToTitleCase(bc) + "\n"+ci, _standardFont2);
                //Chunk c3x = new Chunk("\n" + textBox4.Text.ToUpper(), _standardFont22);
                p1.Add(c1x);
                //p1.Add(c3x);
                Paragraph x1 = new Paragraph(p1);
                x1.SetLeading(20, 0);
                x1.Alignment = Element.ALIGN_CENTER;
                ff2.AddElement(x1);
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                tb1.AddCell(ff2);


                Phrase p3 = new Phrase();

                ff2 = new PdfPCell();
                 string fpago;
                fpago = "Forma de Pago: "+ comboBox5.Text;



                if (comboBox5.Text == "Deposito" || comboBox5.Text == "Trans. Bancaria" || comboBox5.Text == "Pago Rápido" || comboBox5.Text == "Cheque")
                {
                    fpago = fpago + ". N° " + textBox8.Text;
                }
                if (comboBox5.Text == "Deposito")
                {
                    fpago = fpago + ". A " + comboBox4.Text;
                }
                if (comboBox5.Text == "Cheque")
                {
                    fpago = fpago + ". De " + comboBox3.Text;
                }
                if (comboBox5.Text == "Trans. Bancaria" || comboBox5.Text == "Pago Rápido")
                {
                    fpago = fpago + ". De  " + comboBox3.Text + " A " + comboBox4.Text;
                }

                Chunk cp = new Chunk(fpago, _standardFont2);
                
                p3.Add(cp);
                Paragraph x3 = new Paragraph(p3);
                x3.SetLeading(20, 0);
                ff2.AddElement(x3);
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                //float[] medidaCeldas = { 0.25f, 2.85f };

                // ASIGNAS LAS MEDIDAS A LA TABLA (ANCHO)
                //tb1.SetWidths(medidaCeldas);

                // Finalmente, añadimos la tabla al documento PDF y cerramos el documento
                doc.Add(tb1);

                doc.Add(new Paragraph("__________________________________________________________________________________"));
                doc.Add(new Paragraph(" "));
                tbx = new PdfPTable(2);
                tbx.WidthPercentage = 100;


                tb1 = new PdfPTable(2);
                tb1.WidthPercentage = 100;

                //doc.Add(imagen);
                //PdfImage img = new PdfImage(iTextSharp.text.Image.
                ff2 = new PdfPCell();
                ff1 = new PdfPCell();
                /*ff2.BorderWidth = 0;
                ff2.Colspan = 2;*/
                //tb1.AddCell(imagen);


                // Configuramos el título de las columnas de la tabla
                /*PdfPCell ff1 = new PdfPCell(new Phrase("\nRepública Bolivariana de Venezuela\nCentro de Adiestramiento Profesional para el Desarrollo de los Sistemas Informáticos y Afines \nTelf.: 0291-8966131/ 0414-7679330\nMaturín - Estado Monagas\nRIF: J-406338516", _standardFont23));
                ff1.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff1.BorderWidth = 0;*/


                // Añadimos las celdas a la tabla
                //tb1.AddCell(ff2);
                //tb1.AddCell(ff1);


                // Configuramos el título de las columnas de la tabla
                //doc.Add(new Paragraph("                       "));
                //doc.Add(new Paragraph("                       "));

                ff1 = new PdfPCell(imagen);
                //ff1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                ff1.BorderWidth = 0;

                chunk = new Chunk("                     RECIBO DE PAGO", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                ff2 = new PdfPCell(new Paragraph(chunk));
                ff2.BorderWidth = 0;
                //ff2.Colspan = 2;
                //ff2.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff2.Padding = 4;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tbx.AddCell(ff1);
                tbx.AddCell(ff2);

                // ASIGNAS LAS MEDIDAS A LA TABLA (ANCHO)
                tbx.SetWidths(medidaCeldas);
                doc.Add(tbx);
                //doc.Add(new Paragraph("                       "));


                ff2 = new PdfPCell(new Phrase("PERIODO: " + dateTimePicker1.Text + " al " + dateTimePicker2.Text, _standardFont2));
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                //doc.Add(new Paragraph("                       "));

                ff2 = new PdfPCell(new Phrase("\nEMPLEADO: " + myTI.ToTitleCase(bc), _standardFont22e));
                ff2.BorderWidth = 0;
                //ff2.HorizontalAlignment = PdfPCell.ALIGN_JUSTIFIED;
                //tb1.AddCell(imagen);

                // Configuramos el título de las columnas de la tabla
                ff1 = new PdfPCell(new Phrase("\nDOC. DE IDENTIDAD: " + ci, _standardFont22e));
                ff1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                ff1.BorderWidth = 0;


                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                tb1.AddCell(ff1);


                ff2 = new PdfPCell(new Phrase("Fecha de Ingreso: " + fechai, _standardFont2));
                ff2.BorderWidth = 0;
                //ff2.HorizontalAlignment = PdfPCell.ALIGN_JUSTIFIED;
                //tb1.AddCell(imagen);

                // Configuramos el título de las columnas de la tabla
                ff1 = new PdfPCell(new Phrase("Cargo: " + textBox3.Text, _standardFont2));
                ff1.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                ff1.BorderWidth = 0;


                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                tb1.AddCell(ff1);
                doc.Add(tb1);

                GenerarDocumento(doc);
                tb1 = new PdfPTable(2);
                tb1.WidthPercentage = 100;

                p1 = new Phrase();

                ff2 = new PdfPCell();
                bc = bc.ToLower();


                c1x = new Chunk("________________________________\n" + myTI.ToTitleCase(bc) + "\n" + ci, _standardFont2);
                //Chunk c3x = new Chunk("\n" + textBox4.Text.ToUpper(), _standardFont22);
                p1.Add(c1x);
                //p1.Add(c3x);
                x1 = new Paragraph(p1);
                x1.SetLeading(20, 0);
                x1.Alignment = Element.ALIGN_CENTER;
                ff2.AddElement(x1);
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                tb1.AddCell(ff2);


                p3 = new Phrase();

                ff2 = new PdfPCell();
                fpago = "Forma de Pago: " + comboBox5.Text;



                if (comboBox5.Text == "Deposito" || comboBox5.Text == "Trans. Bancaria" || comboBox5.Text == "Pago Rápido" || comboBox5.Text == "Cheque")
                {
                    fpago = fpago + ". N° " + textBox8.Text;
                }
                if (comboBox5.Text == "Deposito")
                {
                    fpago = fpago + ". A " + comboBox4.Text;
                }
                if (comboBox5.Text == "Cheque")
                {
                    fpago = fpago + ". De " + comboBox3.Text;
                }
                if (comboBox5.Text == "Trans. Bancaria" || comboBox5.Text == "Pago Rápido")
                {
                    fpago = fpago + ". De  " + comboBox3.Text + " A " + comboBox4.Text;
                }

                cp = new Chunk(fpago, _standardFont2);

                p3.Add(cp);
                x3 = new Paragraph(p3);
                x3.SetLeading(20, 0);
                ff2.AddElement(x3);
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                //float[] medidaCeldas = { 0.25f, 2.85f };

                // ASIGNAS LAS MEDIDAS A LA TABLA (ANCHO)
                //tb1.SetWidths(medidaCeldas);

                // Finalmente, añadimos la tabla al documento PDF y cerramos el documento
                doc.Add(tb1);

                //doc.Add(new Paragraph("                       "));


                //doc.Add(new Paragraph("                       "));

                // Crea la imagen
                iTextSharp.text.Image imagen2 = iTextSharp.text.Image.GetInstance("fondo.jpg");
                // Cambia el tamaño de la imagen
                imagen2.ScaleToFit(700, 250);
                // Se indica que la imagen debe almacenarse como fondo
                imagen2.Alignment = iTextSharp.text.Image.UNDERLYING;
                // Coloca la imagen en una posición absoluta
                imagen2.SetAbsolutePosition(150, 280);
                // Imprime la imagen como fondo de página
                //doc.Add(imagen2);


                
                doc.AddCreationDate();

                doc.Close();
                Process.Start(filename);//Esta parte se puede omitir, si solo se desea guardar el archivo, y que este no se ejecute al instante
            }

        }
        iTextSharp.text.Font _standardFont4 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        iTextSharp.text.Font _standardFont5 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        public void GenerarDocumento(Document document)
        {
            int i, j;
            PdfPTable datatable = new PdfPTable(5);
            datatable.DefaultCell.Padding = 3;
            float[] headerwidths = GetTamañoColumnas(dataGridView1);
            float[] medidaCeldas = { 1.2f,  0.5f, 0.3f, 0.8f, 0.8f };
            datatable.SetWidths(medidaCeldas);
            datatable.WidthPercentage = 100;
            datatable.DefaultCell.BorderWidth = 0.75f;
            datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            /*for (i = 0; i < dataGridView1.ColumnCount; i++)
            {
                datatable.AddCell(new Phrase(dataGridView1.Columns[i].HeaderText, _standardFont5));
            }*/
            datatable.AddCell(new Phrase("Descripción", _standardFont5));
            datatable.AddCell(new Phrase("Bs.", _standardFont5));
            datatable.AddCell(new Phrase("Cant.", _standardFont5));
            datatable.AddCell(new Phrase("Asignación", _standardFont5));
            datatable.AddCell(new Phrase("Deducción", _standardFont5));
            datatable.HeaderRows = 1;
            datatable.DefaultCell.BorderWidth = 0.75f;
            for (i = 0; i < dataGridView1.Rows.Count; i++)
            {
                for (j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1[j, i].Value != null)
                    {
                        if (j == 0)
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(dataGridView1[j, i].Value.ToString(), _standardFont4));
                            cell.VerticalAlignment = Element.ALIGN_CENTER;
                            datatable.AddCell(cell);//En esta parte, se esta agregando un renglon por cada registro en el datagrid
                        }
                        else
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(dataGridView1[j, i].Value.ToString(), _standardFont4));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_CENTER;
                            datatable.AddCell(cell);//En esta parte, se esta agregando un renglon por cada registro en el datagrid

                        }
                    }
                }
                datatable.CompleteRow();
            }
            for (i = 0; i < dataGridView2.Rows.Count; i++)
            {
                for (j = 0; j < dataGridView2.Columns.Count; j++)
                {
                    if (dataGridView2[j, i].Value != null)
                    {
                        if (j == 3)
                        {
                            PdfPCell cell = new PdfPCell(new Phrase("", _standardFont4));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_CENTER;
                            datatable.AddCell(cell);//En est
                        }
                        else
                        {
                            if(j == 0)
                            {

                                PdfPCell cell = new PdfPCell(new Phrase(dataGridView2[j, i].Value.ToString(), _standardFont4));
                                cell.VerticalAlignment = Element.ALIGN_CENTER;
                                datatable.AddCell(cell);//En esta parte, se esta agregando un renglon por cada registro en el datagrid
                            }
                            else
                            {
                                PdfPCell cell = new PdfPCell(new Phrase(dataGridView2[j, i].Value.ToString(), _standardFont4));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_CENTER;
                                datatable.AddCell(cell);//En esta parte, se esta agregando un renglon por cada registro en el datagrid
                            }
                        }
                        if (j == 3)
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(dataGridView2[j, i].Value.ToString(), _standardFont4));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_CENTER;
                            datatable.AddCell(cell);//En esta parte, se esta agregando un renglon por cada registro en el datagrid
                        }
                    }
                }
                datatable.CompleteRow();
            }
            PdfPCell cellx = new PdfPCell(new Phrase("SUBTOTAL", _standardFont4));
            cellx.HorizontalAlignment = Element.ALIGN_RIGHT;
            cellx.VerticalAlignment = Element.ALIGN_CENTER;
            cellx.Colspan = 3;
            datatable.AddCell(cellx);
            PdfPCell cella = new PdfPCell(new Phrase("Bs. " + String.Format("{0:0,0.00}", (sa)), _standardFont4));
            cella.HorizontalAlignment = Element.ALIGN_CENTER;
            cella.VerticalAlignment = Element.ALIGN_CENTER;
            datatable.AddCell(cella);
            PdfPCell cellb = new PdfPCell(new Phrase("Bs. " + String.Format("{0:0,0.00}", (sd)), _standardFont4));
            cellb.HorizontalAlignment = Element.ALIGN_CENTER;
            cellb.VerticalAlignment = Element.ALIGN_CENTER;
            datatable.AddCell(cellb);
            datatable.CompleteRow();
            PdfPCell cellxx = new PdfPCell(new Phrase("TOTAL A PAGAR", _standardFont5));
            cellxx.HorizontalAlignment = Element.ALIGN_RIGHT;
            cellxx.VerticalAlignment = Element.ALIGN_CENTER;
            cellxx.Colspan = 4;
            datatable.AddCell(cellxx);
            PdfPCell cellt = new PdfPCell(new Phrase("Bs." + String.Format("{0:0,0.00}", (total)), _standardFont5));
            cellt.HorizontalAlignment = Element.ALIGN_CENTER;
            cellt.VerticalAlignment = Element.ALIGN_CENTER;
            datatable.AddCell(cellt);
            datatable.CompleteRow();
            document.Add(datatable);
        }
        public float[] GetTamañoColumnas(DataGridView dg)
        {
            float[] values = new float[dg.ColumnCount];
            for (int i = 0; i < dg.ColumnCount; i++)
            {
                values[i] = (float)dg.Columns[i].Width;
            }
            return values;

        }
        #endregion

        private void button8_Click(object sender, EventArgs e)
        {
            To_pdf();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox8.Clear();
            textBox9.Clear();
            ci = "";
            total = 0.00f;

            sa = 0.00f;
            sd = 0.00f;
            tota();
            date2();
            date1();
            comboBox5.SelectedIndex = 0;
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            comboBox4.SelectedIndex = 0;
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
        }
    }
}
