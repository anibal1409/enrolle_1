﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class concerti1 : Form
    {
        public concerti1()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");

        private void concerti1_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo4();
            combo2();
            combo1();
            comboBox1.SelectedIndex = 0;
        }
        DataTable dt = new DataTable();
        void combo4()
        {

            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox4.ValueMember = "cod";
            comboBox4.DisplayMember = "periodo";
            comboBox4.DataSource = dt;
        }
        public string obser, usu;
        string pago, estado;
        void cargar()
        {
            string buscarcx = "SELECT TOP 1 * FROM pago ORDER BY cod DESC;";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                double ccod;
                ccod = Double.Parse(leex["cod"].ToString()) + 1;
                textBox1.Text = ccod.ToString();
            }
            else
            {
                textBox1.Text = "1";
            }
        }
        void combo1()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Cliente' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "banco";
            comboBox2.DataSource = dt;
        }
        void combo5(string com)
        {
            string ctc = "SELECT cod,modalidad FROM modalidad WHERE estatus = 'DISPONIBLE' AND periodo = '" + com + "' ORDER BY modalidad";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dtx = new DataTable();
            DATA.Fill(dtx);

            DataRow nuevaFila = dtx.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";

            dtx.Rows.InsertAt(nuevaFila, 0);

            comboBox5.ValueMember = "cod";
            comboBox5.DisplayMember = "modalidad";
            comboBox5.DataSource = dtx;
        }

        void combo2()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Instituto' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "banco";
            comboBox3.DataSource = dt;
        }
        string certi, v = "", pp = "", prec = "";
        private void button3_Click(object sender, EventArgs e)
        {
            certi = "";
            v = "";
            if (textBox6.Text != "")
            {

                string buscarcx = "SELECT TOP 1 * FROM certi WHERE cod = '" + textBox6.Text + "';";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read() == true)
                {
                    certi = leex["cod"].ToString();
                    estu(leex["estudiante"].ToString());
                    per(leex["modalidad"].ToString());
                    textBox5.Text = leex["precio"].ToString();
                    comboBox5.SelectedValue = leex["modalidad"].ToString();
                    textBox1.Text = leex["pago"].ToString();
                    pp = leex["pago"].ToString();
                    prec = leex["precio"].ToString();
                    comboBox6.Text = leex["estatus"].ToString();
                    buscarp();
                    string buscarcxx = "SELECT nombre, apellido FROM empleado WHERE  ci = '" + leex["empleado"].ToString() + "'";
                    OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
                    OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
                    OleDbDataReader leexx = comandoxx.ExecuteReader();
                    if (leexx.Read())
                    {
                        textBox14.Text = "(" + leex["empleado"].ToString() + ") " + leexx["nombre"].ToString() + " " + leexx["apellido"].ToString();
                    }
                    v = "1";
                }
                else
                {
                    MessageBox.Show("El código ingresad el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código para poder realizar la busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        string ci;
        void estu(string x)
        {
            ci = "";
            string buscarcx = "SELECT TOP 1 * FROM estudiante WHERE ci = '" + x + "';";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                textBox11.Text = ci = leex["ci"].ToString();
                textBox10.Text = leex["nombre"].ToString() + " " + leex["apellido"].ToString();
            }
        }

        void per(string x)
        {
            string buscarcx = "SELECT TOP 1 * FROM modalidad WHERE cod = '" + x + "';";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                comboBox4.SelectedValue = leex["periodo"].ToString();
            }
        }

        int ep = 0;
        void buscarp()
        {
            if (textBox1.Text != "")
            {
                string buscarc = "SELECT * FROM pago WHERE cod = '" + textBox1.Text + "';";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    ep = 1;
                    pago = textBox1.Text;
                    ocultar();
                    label4.Visible = true;
                    comboBox1.Visible = true;
                    //button7.Visible = false;
                    comboBox1.SelectedItem = lee["tipo"].ToString();
                    dateTimePicker1.Text = lee["fecha"].ToString();
                    textBox2.Text = lee["monto"].ToString();
                    textBox3.Text = lee["pago"].ToString();
                    textBox4.Text = lee["disponible"].ToString();
                    comboBox2.SelectedValue = lee["origen"].ToString();
                    comboBox3.SelectedValue = lee["destino"].ToString();
                    estado = lee["estatus"].ToString();
                    bloquear();


                }
                else
                {
                    MessageBox.Show("El código ingresado no pertenece a ningún pago registrado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Focus();
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código de pago para poder realizar la busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox1.Focus();
            }
        }
        void ocultar()
        {
            textBox2.Visible = false;
            textBox3.Visible = false;
            label2.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            dateTimePicker1.Visible = false;
            comboBox2.Visible = false;
            comboBox3.Visible = false;
            textBox2.Clear();
            textBox3.Clear();
            dateTimePicker1.Value = DateTime.Now;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            label17.Visible = false;
            textBox4.Visible = false;
        }
        void bloquear()
        {
            comboBox1.Enabled = false;
            comboBox2.Enabled = false;
            comboBox3.Enabled = false;
            textBox2.ReadOnly = true;
            textBox3.ReadOnly = true;
            textBox4.ReadOnly = true;
            dateTimePicker1.Enabled = false;
        }
        void desbloquear()
        {
            comboBox1.Enabled = true;
            comboBox2.Enabled = true;
            comboBox3.Enabled = true;
            textBox2.ReadOnly = false;
            textBox3.ReadOnly = false;
            textBox4.ReadOnly = false;
            dateTimePicker1.Enabled = true;
        }

        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox10.Clear();
            textBox11.Clear();
            ocultar();
            bloquear();
            certi = "";
            pago = "";
            v = "";
        }
        private void button9_Click(object sender, EventArgs e)
        {
            clear();
        }
    }
}
