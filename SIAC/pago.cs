﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class pago : Form
    {
        public pago()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void pago_Load(object sender, EventArgs e)
        {
            conexion.Open();
            cargar();
            combo1();
            combo2();
        }
        public string obser, usu;
        void cargar()
        {
            string buscarcx = "SELECT TOP 1 * FROM pago ORDER BY cod DESC;";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                double ccod;
                ccod = Double.Parse(leex["cod"].ToString()) + 1;
                textBox1.Text= ccod.ToString();
            }
            else
            {
                textBox1.Text = "1";
            }
        }

        void combo1()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Cliente' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "banco";
            comboBox2.DataSource = dt;
        }

        void combo2()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Instituto' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "banco";
            comboBox3.DataSource = dt;
        }


        void ocultar()
        {
            textBox2.Visible = false;
            textBox3.Visible = false;
            label2.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            button1.Visible = false;
            button2.Visible = false;
            dateTimePicker1.Visible = false;
            comboBox2.Visible = false;
            comboBox3.Visible = false;
            textBox2.Clear();
            textBox3.Clear();
            dateTimePicker1.Value = DateTime.Now;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text != "")
            {
                if (comboBox1.Text == "Efectivo")
                {
                    ocultar();
                    this.Size = new Size(445, 211);
                    textBox2.Visible = true;
                    label2.Visible = true;
                    button1.Location = new Point(316, 118);
                    button2.Location = new Point(366, 118);
                    button1.Visible = true;
                    button2.Visible = true;
                    dateTimePicker1.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = false;

                    
                }
                if (comboBox1.Text == "Exoneración")
                {
                    ocultar();
                    this.Size = new Size(445, 211);
                    button1.Location = new Point(316, 118);
                    button2.Location = new Point(366, 118);
                    button1.Visible = true;
                    button2.Visible = true;
                    dateTimePicker1.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = false;
                }
                if (comboBox1.Text == "Deposito" )
                {
                    ocultar();
                    this.Size = new Size(445, 309);
                    button1.Location = new Point(316, 217);
                    button2.Location = new Point(366, 217);
                    button1.Visible = true;
                    button2.Visible = true;
                    dateTimePicker1.Visible = true;
                    label2.Visible = true;
                    textBox3.Visible = true;
                    label6.Visible = true;
                    textBox2.Visible = true;
                    comboBox3.Visible = true;
                    label7.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = true;

                }
                if (comboBox1.Text == "Trans. Bancaria" || comboBox1.Text == "Pago Rápido" || comboBox1.Text == "Otro")
                {
                    ocultar();
                    this.Size = new Size(445, 309);
                    button1.Location = new Point(316, 217);
                    button2.Location = new Point(366, 217);
                    button1.Visible = true;
                    button2.Visible = true;
                    dateTimePicker1.Visible = true;
                    textBox2.Visible = true;
                    label2.Visible = true;
                    textBox3.Visible = true;
                    label6.Visible = true;
                    comboBox2.Visible = true;
                    label5.Visible = true;
                    comboBox3.Visible = true;
                    label7.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = true;

                }
            }
            else
            {
                ocultar();
                this.Size = new Size(445, 159);
            }
        }
        public bool IsNumeric(string x)
        {
            double retNum;
            try
            {
                retNum = Double.Parse(x);
                return true;
            }
            catch
            {
                return false;
            }   
        }

        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            comboBox1.Text = "";
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            dateTimePicker1.Value = DateTime.Now;
            obser = "";
            usu = "";
            cargar();
            estatica.Usu = "";
            estatica.Obser = "";


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (comboBox1.Text == "Efectivo")
            {
                if (textBox2.Text != "")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        try
                        {
                            string insertar = "INSERT INTO pago  VALUES (" + textBox1.Text + ",'" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '0', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', 'Sin Observaciones', '" + estatica.Ci + "')";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            clear();
                            MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            
                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                        
                    else
                    {
                        MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Ingrese el monto para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (comboBox1.Text == "Exoneración")
            {
                exo a = new exo();
                a.ShowDialog();

                if(a.DialogResult == DialogResult.Yes)
                {

                   
                        try
                        {
                            string insertar = "INSERT INTO pago  VALUES (" + textBox1.Text + ",'" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '0', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '0', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', '" + estatica.Obser + "', '" + estatica.Usu + "')";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            clear();
                            MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
               }
            
            }
            if (comboBox1.Text == "Deposito")
            {
                if (textBox2.Text != "" && textBox3.Text != "" && comboBox3.Text != "Seleccione un banco")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        var cadena = textBox3.Text;
                        if (cadena.Length >= 4)
                        {
                            try
                            {
                                string insertar = "INSERT INTO pago  VALUES (" + textBox1.Text + ",'" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + comboBox1.Text + "', '" + comboBox3.SelectedValue.ToString() + "', '" + textBox2.Text + "', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', 'Sin Observaciones', '" + estatica.Ci + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                clear();
                                MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Debe ingresar al menos los ultimos cuatro dígitos del número de referencia.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBox3.Focus();
                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es valido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (comboBox1.Text == "Trans. Bancaria" || comboBox1.Text == "Pago Rápido")
            {
                if (textBox2.Text != "" && textBox3.Text != "" && comboBox2.Text != "Seleccione un banco" && comboBox3.Text != "Seleccione un banco")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        var cadena = textBox3.Text;
                        if (cadena.Length >= 4)
                        {
                            try
                            {
                                string insertar = "INSERT INTO pago  VALUES (" + textBox1.Text + ",'" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + comboBox2.SelectedValue.ToString() + "', '" + comboBox3.SelectedValue.ToString() + "', '" + textBox2.Text + "', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', 'Sin Observaciones', '" + estatica.Ci + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                clear();
                                MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Debe ingresar al menos los ultimos cuatro dígitos del número de referencia.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBox3.Focus();
                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es valido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            if (comboBox1.Text == "Otro")
            {
                if (textBox2.Text != "")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        exo a = new exo();
                        a.ShowDialog();

                        if (a.DialogResult == DialogResult.Yes)
                        {


                            try
                            {
                                string insertar = "INSERT INTO pago  VALUES (" + textBox1.Text + ",'" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + comboBox2.SelectedValue.ToString() + "', '" + comboBox3.SelectedValue.ToString() + "', '" + textBox2.Text + "', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', '" + estatica.Obser + "', '" + estatica.Usu + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                clear();
                                MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es valido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Ingrese el monto para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private static void OnlyNumber(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void toolStripMenuItem249_Click(object sender, EventArgs e)
        {

        }



        
    }
}
