﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Drawing.Printing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;

namespace SIAC
{
    public partial class certi2 : Form
    {
        public certi2()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void certi2_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo4();
            ccargar();
            combo2();
            combo1();
            comboBox1.SelectedIndex = 0;
        }

        int ep = 0;
        void ccargar()
        {
            string buscarcx = "SELECT TOP 1 * FROM certi ORDER BY cod DESC;";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                double ccod;
                ccod = Double.Parse(leex["cod"].ToString()) + 1;
                textBox12.Text = ccod.ToString();
            }
            else
            {
                textBox12.Text = "1";
            }
        }
        DataTable dt = new DataTable();
        void combo4()
        {

            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox4.ValueMember = "cod";
            comboBox4.DisplayMember = "periodo";
            comboBox4.DataSource = dt;
        }
        public string obser, usu;
        string pago, estado;
        void cargar()
        {
            string buscarcx = "SELECT TOP 1 * FROM pago ORDER BY cod DESC;";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                double ccod;
                ccod = Double.Parse(leex["cod"].ToString()) + 1;
                textBox1.Text = ccod.ToString();
            }
            else
            {
                textBox1.Text = "1";
            }
        }
        void combo1()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Cliente' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "banco";
            comboBox2.DataSource = dt;
        }

        void combo2()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Instituto' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "banco";
            comboBox3.DataSource = dt;
        }

        public bool IsNumeric(string x)
        {

            double retNum;

            try
            {
                retNum = Double.Parse(x);
                return true;
            }
            catch
            {
                return false;

            }



        }
        void ocultar()
        {
            textBox2.Visible = false;
            textBox3.Visible = false;
            label2.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            dateTimePicker1.Visible = false;
            comboBox2.Visible = false;
            comboBox3.Visible = false;
            textBox2.Clear();
            textBox3.Clear();
            dateTimePicker1.Value = DateTime.Now;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            label17.Visible = false;
            textBox4.Visible = false;
        }

        void buscarp()
        {
            if (textBox1.Text != "")
            {
                string buscarc = "SELECT * FROM pago WHERE cod = " + textBox1.Text + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    ep = 1;
                    pago = textBox1.Text;
                    ocultar();
                    label4.Visible = true;
                    comboBox1.Visible = true;
                    button7.Visible = false;
                    button5.Visible = false;
                    comboBox1.SelectedItem = lee["tipo"].ToString();
                    dateTimePicker1.Text = lee["fecha"].ToString();
                    textBox2.Text = lee["monto"].ToString();
                    textBox3.Text = lee["pago"].ToString();
                    textBox4.Text = lee["disponible"].ToString();
                    comboBox2.SelectedValue = lee["origen"].ToString();
                    comboBox3.SelectedValue = lee["destino"].ToString();
                    estado = lee["estatus"].ToString();
                    bloquear();
                    textBox1.ReadOnly = true;

                }
                else
                {
                    MessageBox.Show("El código ingresado no pertenece a ningún pago registrado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Focus();
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código de pago para poder realizar la busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox1.Focus();
            }
        }
        void bloquear()
        {
            comboBox1.Enabled = false;
            comboBox2.Enabled = false;
            comboBox3.Enabled = false;
            textBox2.ReadOnly = true;
            textBox3.ReadOnly = true;
            textBox4.ReadOnly = true;
            dateTimePicker1.Enabled = false;
        }
        void desbloquear()
        {
            comboBox1.Enabled = true;
            comboBox2.Enabled = true;
            comboBox3.Enabled = true;
            textBox2.ReadOnly = false;
            textBox3.ReadOnly = false;
            textBox4.ReadOnly = false;
            dateTimePicker1.Enabled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void lineShape1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text != "")
            {
                if (comboBox1.Text == "Efectivo")
                {
                    ocultar();
                    textBox2.Visible = true;
                    label2.Visible = true;
                    dateTimePicker1.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = false;
                    if (ep == 1)
                    {

                        label17.Visible = true;
                        textBox4.Visible = true;
                    }


                }
                if (comboBox1.Text == "Exoneración")
                {
                    ocultar();
                    dateTimePicker1.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = false;
                    textBox4.Visible = false;
                    label17.Visible = false;
                }
                if (comboBox1.Text == "Deposito")
                {
                    ocultar();
                    dateTimePicker1.Visible = true;
                    textBox2.Visible = true;
                    label2.Visible = true;
                    textBox3.Visible = true;
                    label6.Visible = true;
                    comboBox3.Visible = true;
                    label7.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = true;
                    if (ep == 1)
                    {

                        label17.Visible = true;
                        textBox4.Visible = true;
                    }

                }
                if (comboBox1.Text == "Trans. Bancaria" || comboBox1.Text == "Pago Rápido" || comboBox1.Text == "Otro")
                {
                    ocultar();
                    dateTimePicker1.Visible = true;
                    textBox2.Visible = true;
                    label2.Visible = true;
                    textBox3.Visible = true;
                    label6.Visible = true;
                    comboBox2.Visible = true;
                    label5.Visible = true;
                    comboBox3.Visible = true;
                    label7.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = true;
                    if (ep == 1)
                    {

                        label17.Visible = true;
                        textBox4.Visible = true;
                    }

                }
            }
            else
            {
                ocultar();
            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            buscarp();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ocultar();
            label4.Visible = true;
            comboBox1.Visible = true;
            button7.Visible = false;
            button5.Visible = false;
            cargar();
            textBox1.ReadOnly = true;
            button4.Visible = true;
            ep = 0;
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            textBox1.Clear();
            ocultar();
            label4.Visible = false;
            comboBox1.Visible = false;
            button7.Visible = true;
            button5.Visible = true;
            textBox1.ReadOnly = false;
            desbloquear();
            comboBox1.SelectedIndex = 0;
            pago = "";
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox4.SelectedIndex != 0)
            {


                string buscarcx = "SELECT * FROM precio WHERE periodo = " + comboBox4.SelectedValue.ToString() + " AND servicio= 'Certificado';";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read() == true)
                {
                    textBox9.Text = leex["precio"].ToString();
                }
                else
                {
                    MessageBox.Show("El periodo académico seleccionado no tiene precio definido para este servicio, comuníquese con el Coordinador Académico.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }


            }
            else
            {
                textBox9.Clear();
                //clearc();
            }
        }

        void cleard()
        {
            textBox8.Clear();
            textBox7.Clear();
            textBox6.Clear();
            textBox5.Clear();
            estatica.Ci = "";
            estatica.Mod = "";
            estatica.Incio = "";
            estatica.Fin = "";
            estatica.Periodo = "";
            curso = ""; 
        }

        string ci = "";
        DateTime var;
        void buscar()
        {
            if (textBox11.Text != "")
            {
                ci = "";
                var cadena = textBox11.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox11.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        if (lee["estatus"].ToString() == "ACTIVO" || lee["estatus"].ToString() == "INACTIVO")
                        {
                            ci = textBox11.Text;
                            textBox10.Text = lee["nombre"].ToString() +" "+lee["apellido"].ToString();
                            estatica.Estudiante= ci;
                            cursoe a = new cursoe();
                            a.ShowDialog();
                            if (a.DialogResult == DialogResult.Yes)
                            {
                                textBox8.Text = estatica.Mod;
                                textBox7.Text = estatica.Periodo;
                                textBox6.Text = estatica.Incio;
                                textBox5.Text = estatica.Fin;
                            }
                            else
                            {
                                cleard();
                            }

                        }
                        else
                        {
                            MessageBox.Show("El estudiante con el documento de identidad '" + textBox11.Text + "' se encuentra actualmente suspendido. Comuníquese con el Coordinador Académico.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }

                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad '" + textBox7.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        
                            textBox11.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            buscar();
        }

        string curso = "";
        private void button11_Click(object sender, EventArgs e)
        {
            estatica.Estudiante = ci;
            cursose a = new cursose();
            a.ShowDialog();
            if (a.DialogResult == DialogResult.Yes)
            {
                curso = "";
                string buscarcx = "SELECT * FROM modalidad WHERE cod = " + estatica.Certi + ";";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read() == true)
                {
                    curso = leex["cod"].ToString();
                    textBox8.Text = leex["modalidad"].ToString();
                    textBox6.Text = leex["inicio"].ToString();
                    textBox5.Text = leex["fin"].ToString();

                    string buscarc = "SELECT * FROM periodo WHERE cod = " + leex["periodo"].ToString() + ";";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        textBox7.Text = lee["periodo"].ToString();
                    }
                }
            }
            else
            {
                cleard();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            cleard();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if(textBox12.Text != "" && textBox9.Text != "" && ci != "" && ci != null && curso != "" && curso != null && pago != "" && pago != null && textBox4.Text != "")
            {
                DateTime hora = new DateTime();
                double dis = 0, p = 0, to;
                dis = Double.Parse(textBox4.Text);
                if (comboBox1.Text != "Exoneración")
                {

                    dis = Double.Parse(textBox4.Text);
                    p = Double.Parse(textBox9.Text);
                    if (dis >= p)
                    {
                        try
                        {
                            to = dis - p;
                            hora = DateTime.Now;
                            if (estado == "VERIFICADO")
                            {
                                string insertar = "INSERT INTO certi  VALUES (" + textBox12.Text + ", '" + ci + "', '" + curso + "', '" + pago + "', '" + hora.ToString("dd-MM-yyyy") + "', '" + hora.ToString("hh:mm:ss") + "', '" + textBox9.Text + "', 'EGRESADO', 'VERIFICADO', '" + estatica.Ci + "', " + comboBox4.SelectedValue.ToString() + ")";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();

                                string insertarxx = "UPDATE  pago SET disponible = '" + to.ToString() + "' WHERE cod = " + pago + ";";
                                OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                cmdxx.ExecuteNonQuery();

                                textBox4.Text = to.ToString();
                                button10.Visible = false;
                                button8.Visible = true;
                                MessageBox.Show("La solicitud del certificado se realizó con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                string insertar = "INSERT INTO certi  VALUES (" + textBox12.Text + ", '" + ci + "', '" + curso + "', '" + pago + "', '" + hora.ToString("dd-MM-yyyy") + "', '" + hora.ToString("hh:mm:ss") + "', '" + textBox9.Text + "', 'EGRESADO', 'PENDIENTE', '" + estatica.Ci + "', " + comboBox4.SelectedValue.ToString() + ")";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();

                                string insertarxx = "UPDATE  pago SET disponible = '" + to.ToString() + "' WHERE cod = " + pago + ";";
                                OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                cmdxx.ExecuteNonQuery();

                                textBox4.Text = to.ToString();
                                button10.Visible = false;
                                button8.Visible = true;
                                MessageBox.Show("La solicitud del certificado se realizó con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }




                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("La disponibilidad del pago ingresado no cubre el precio del servicio. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox1.Focus();
                    }
                }
                else
                {
                    try
                    {
                        if (dis == 0)
                        {
                            hora = DateTime.Now;
                            if (estado == "VERIFICADO")
                            {
                                string insertar = "INSERT INTO certi  VALUES (" + textBox12.Text + ", '" + ci + "', '" + curso + "', '" + pago + "', '" + hora.ToString("dd-MM-yyyy") + "', '" + hora.ToString("hh:mm:ss") + "', '" + textBox9.Text + "', 'EGRESADO', 'PENDIENTE', '" + estatica.Ci + "', " + comboBox4.SelectedValue.ToString() + ")";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);

                                string insertarxx = "UPDATE  pago SET disponible = '-1' WHERE cod = " + pago + ";";
                                OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                cmdxx.ExecuteNonQuery();
                                button10.Visible = false;
                                button8.Visible = true;
                                MessageBox.Show("La solicitud del certificado se realizó con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                string insertar = "INSERT INTO certi  VALUES (" + textBox12.Text + ", '" + ci + "', '" + curso + "', '" + pago + "', '" + hora.ToString("dd-MM-yyyy") + "', '" + hora.ToString("hh:mm:ss") + "', '" + textBox9.Text + "', 'EGRESADO', 'PENDIENTE', '" + estatica.Ci + "', " + comboBox4.SelectedValue.ToString() + ")";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);

                                string insertarxx = "UPDATE  pago SET disponible = '-1' WHERE cod = " + pago + ";";
                                OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                cmdxx.ExecuteNonQuery();
                                button8.Visible = true;
                                button10.Visible = false;

                                MessageBox.Show("La solicitud del certificado se realizó con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("La Exoneración ingresada ya fue utilizada en otra formalización. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            textBox1.Focus();
                        }




                    }

                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        void clear()
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (comboBox1.Text == "Efectivo")
            {
                if (textBox2.Text != "")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        try
                        {
                            string insertar = "INSERT INTO pago  VALUES (" + textBox1.Text + ",'" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '0', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', 'Sin Observaciones', '" + estatica.Ci + "')";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            button4.Visible = false;
                            textBox4.Visible = true;
                            label17.Visible = true;
                            textBox4.Text = textBox2.Text;
                            pago = textBox1.Text;
                            estado = "PENDIENTE";

                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es valido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Ingrese el monto para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (comboBox1.Text == "Exoneración")
            {
                exo a = new exo();
                a.ShowDialog();

                if (a.DialogResult == DialogResult.Yes)
                {


                    try
                    {
                        string insertar = "INSERT INTO pago  VALUES (" + textBox1.Text + ",'" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '0', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '0', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', '" + estatica.Obser + "', '" + estatica.Usu + "')";
                        OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        button4.Visible = false;
                        textBox4.Text = "0";
                        pago = textBox1.Text;
                        estado = "PENDIENTE";
                    }

                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            if (comboBox1.Text == "Deposito")
            {
                if (textBox2.Text != "" && textBox3.Text != "" && comboBox3.Text != "Seleccione un banco")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        var cadena = textBox3.Text;
                        if (cadena.Length >= 4)
                        {
                            try
                            {
                                string insertar = "INSERT INTO pago  VALUES (" + textBox1.Text + ",'" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + comboBox1.Text + "', '" + comboBox3.SelectedValue.ToString() + "', '" + textBox2.Text + "', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', 'Sin Observaciones', '" + estatica.Ci + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                clear();
                                MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Debe ingresar al menos los ultimos cuatro dígitos del número de referencia.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBox3.Focus();
                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (comboBox1.Text == "Trans. Bancaria" || comboBox1.Text == "Pago Rápido")
            {
                if (textBox2.Text != "" && textBox3.Text != "" && comboBox2.Text != "Seleccione un banco" && comboBox3.Text != "Seleccione un banco")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        var cadena = textBox3.Text;
                        if (cadena.Length >= 4)
                        {
                            try
                            {
                                string insertar = "INSERT INTO pago  VALUES (" + textBox1.Text + ",'" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + comboBox2.SelectedValue.ToString() + "', '" + comboBox3.SelectedValue.ToString() + "', '" + textBox2.Text + "', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', 'Sin Observaciones', '" + estatica.Ci + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                button4.Visible = false;
                                label17.Visible = true;
                                textBox4.Text = textBox2.Text;
                                pago = textBox1.Text;
                                estado = "PENDIENTE";
                                textBox4.Visible = true;
                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Debe ingresar al menos los ultimos cuatro dígitos del número de referencia.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBox3.Focus();
                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }



            if (comboBox1.Text == "Otro")
            {
                if (textBox2.Text != "")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        exo a = new exo();
                        a.ShowDialog();

                        if (a.DialogResult == DialogResult.Yes)
                        {


                            try
                            {
                                string insertar = "INSERT INTO pago  VALUES (" + textBox1.Text + ",'" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + comboBox2.SelectedValue.ToString() + "', '" + comboBox3.SelectedValue.ToString() + "', '" + textBox2.Text + "', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', '" + estatica.Obser + "', '" + estatica.Usu + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                button4.Visible = false;
                                label17.Visible = true;
                                textBox4.Text = textBox2.Text;
                                pago = textBox1.Text;
                                estado = "PENDIENTE";
                                textBox4.Visible = true;
                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Ingrese el monto para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        #region crearPDF
        private void To_pdf()
        {
            Document doc = new Document(PageSize.A8, 2, 2, 2, 2);
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:";
            saveFileDialog1.Title = "Guardar Solicitud de Certificado" + textBox10.Text;
            saveFileDialog1.DefaultExt = "pdf";
            saveFileDialog1.Filter = "pdf Files (*.pdf)|*.pdf| All Files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.FileName = "SolicitudCertificado" + textBox10.Text + DateTime.Now.ToString("dd-MMM-yyyy"); ;
            string filename = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filename = saveFileDialog1.FileName;
            }
            if (filename.Trim() != "")
            {
                FileStream file = new FileStream(filename,
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite,
                FileShare.ReadWrite);
                PdfWriter wri = PdfWriter.GetInstance(doc, file);
                wri.PageEvent = new PageEventHelper();
                //PdfWriter.GetInstance(doc, file);
                doc.AddAuthor("ENROLL ONE");
                doc.AddCreator("ENROLL ONE");
                doc.AddTitle("Pago de Certificado");

                doc.Open();
                string remito = "Generado por: (" + estatica.Ci + ") " + estatica.Nombre + " " + estatica.Ape + ", con ENROLL ONE.";
                string envio = "Fecha: " + DateTime.Now.ToString("dd MMMM yyyy");

                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                iTextSharp.text.Font _standardFont2 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font cabe = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                // Creamos una tabla que contendrá el nombre, apellido y país
                // de nuestros visitante.
                PdfPTable tb1 = new PdfPTable(1);
                tb1.WidthPercentage = 100;



                //agregando una imagen
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("logo.jpg");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_CENTER;
                float percentage = 0.0f;
                percentage = 350 / imagen.Width;
                imagen.ScalePercent(percentage * 100);
                //PdfImage img = new PdfImage(iTextSharp.text.Image.
                PdfPCell ff2 = new PdfPCell(imagen);
                ff2.BorderWidth = 0;
                ff2.BorderWidthTop = 1;
                ff2.BorderWidthRight = 1;
                ff2.BorderWidthLeft = 1;
                ff2.Padding = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                //tb1.AddCell(imagen);


                // Configuramos el título de las columnas de la tabla
                PdfPCell ff1 = new PdfPCell(new Phrase("PAGO DE CERTIFICADO", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)));
                ff1.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff1.BorderWidth = 0;
                ff1.Padding = 2;
                ff1.BorderWidthRight = 1;
                ff1.BorderWidthLeft = 1;


                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                tb1.AddCell(ff1);


                // Configuramos el título de las columnas de la tabla
                Chunk chunk = new Chunk("Código: " + textBox6.Text, _standardFont2);
                ff2 = new PdfPCell(new Paragraph(chunk));
                ff2.BorderWidth = 0;
                ff2.Padding = 2;
                ff2.BorderWidthRight = 1;
                ff2.BorderWidthLeft = 1;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);

                ff2 = new PdfPCell(new Paragraph(envio, _standardFont2));
                ff2.BorderWidth = 0;
                ff2.BorderWidthRight = 1;
                ff2.BorderWidthLeft = 1;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);

                // Configuramos el título de las columnas de la tabla
                chunk = new Chunk("Disciplina: (" + estatica.Certi + ") " + textBox8.Text, _standardFont2);
                ff2 = new PdfPCell(new Paragraph(chunk));
                ff2.BorderWidth = 0;
                ff2.Padding = 2;
                ff2.BorderWidthRight = 1;
                ff2.BorderWidthLeft = 1;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);

                // Configuramos el título de las columnas de la tabla
                chunk = new Chunk("\n0291-8966131\n", _standardFont2);
                ff2 = new PdfPCell(new Paragraph(chunk));
                ff2.BorderWidth = 0;
                ff2.Padding = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff2.BorderWidthRight = 1;
                ff2.BorderWidthLeft = 1;
                ff2.BorderWidthBottom = 1;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);

                //[] medidaCeldas = { 0.25f, 2.85f };

                // ASIGNAS LAS MEDIDAS A LA TABLA (ANCHO)
                // tb1.SetWidths(medidaCeldas);

                // Finalmente, añadimos la tabla al documento PDF y cerramos el documento
                doc.Add(tb1);





                doc.AddCreationDate();

                doc.Close();
                Process.Start(filename);//Esta parte se puede omitir, si solo se desea guardar el archivo, y que este no se ejecute al instante
            }

        }
        #endregion

        private void button8_Click(object sender, EventArgs e)
        {
            To_pdf();
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
