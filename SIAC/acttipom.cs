﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class acttipom : Form
    {
        public acttipom()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void acttipom_Load(object sender, EventArgs e)
        {
            conexion.Open();
            bus();
        }

        string[,] comen;
        int can;
        string num;
        void bus()
        {
            can = 0;
            comen = new string[2, 1000000];
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            string buscarc = "SELECT * FROM tipom ORDER BY modalidad;";
            OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
            OleDbCommand comando = new OleDbCommand(buscarc, conexion);
            OleDbDataReader lee = comando.ExecuteReader();
            while (lee.Read())
            {
                comen[0, can] = lee["cod"].ToString();
                comen[1, can] = lee["modalidad"].ToString();
                can++;
            }
            if (can > 0)
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                for (int i = 0; i < can; i++)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells[0].Value = comen[0, i];
                    dataGridView1.Rows[i].Cells[1].Value = comen[1, i];

                }
            }

        }
        string tipom;
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            tipom = "";
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            tipom = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox1.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tipom != "" && textBox1.Text != "")
            {
                try
                {
                    string insertar = "UPDATE  tipom SET  modalidad = " + textBox1.Text + " WHERE cod = " + tipom + " ";
                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("El tipo de modalidad fue modificado con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBox1.Clear();
                    bus();
                }

                catch (DBConcurrencyException ex)
                {
                    MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un tipo de modalidad para poder utilizar esta función.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem20_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem21_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem19_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem22_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem20_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem23_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem21_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem24_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem22_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem19_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }
    }
}
