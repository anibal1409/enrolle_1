﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class dedu : Form
    {
        public dedu()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");

        private void dedu_Load(object sender, EventArgs e)
        {
            conexion.Open();
            bus();
            if (estatica.Nivel == "Administrador")
            {
                //menuStrip1.Visible = true;
            }
            if (estatica.Nivel == "Asesor de Ventas")
            {
                //menuStrip4.Visible = true;
            }
            if (estatica.Nivel == "Coordinador Académico")
            {
                //menuStrip2.Visible = true;
            }
            if (estatica.Nivel == "Coordinador de Personal")
            {
                //menuStrip3.Visible = true;
            }
        }
        string[,] comen;
        int can;
        string num;
        void bus()
        {
            can = 0;
            comen = new string[3, 1000000];
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            string buscarc = "SELECT * FROM dedu ORDER BY dedu;";
            OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
            OleDbCommand comando = new OleDbCommand(buscarc, conexion);
            OleDbDataReader lee = comando.ExecuteReader();
            while (lee.Read())
            {
                comen[0, can] = lee["cod"].ToString();
                comen[1, can] = lee["dedu"].ToString();
                comen[2, can] = lee["valor"].ToString();
                can++;
            }

            if (can > 0)
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                for (int i = 0; i < can; i++)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells[0].Value = comen[0, i];
                    dataGridView1.Rows[i].Cells[1].Value = comen[1, i];
                    dataGridView1.Rows[i].Cells[2].Value = comen[2, i];

                }
            }

            string buscarcx = "SELECT TOP 1 * FROM dedu ORDER BY cod DESC;";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                double ccod;
                ccod = Double.Parse(leex["cod"].ToString()) + 1;
                num = ccod.ToString();
            }
            else
            {
                num = "1";
            }


        }
        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox1.Focus();
            bus();
        }
        public bool IsNumeric(string x)
        {

            double retNum;

            try
            {
                retNum = Double.Parse(x);
                return true;
            }
            catch
            {
                return false;

            }



        }

        private static void OnlyNumber(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                if (IsNumeric(textBox2.Text))
                {
                    string buscarc = "SELECT * FROM dedu WHERE dedu LIKE '" + textBox1.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        MessageBox.Show("La deducción '" + textBox1.Text + "' ya se encuentra registrada en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox1.Focus();
                    }
                    else
                    {

                        try
                        {
                            string insertar = "INSERT INTO dedu  VALUES (" + num + ",'" + textBox1.Text + "', '" + textBox2.Text + "')";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            clear();
                            MessageBox.Show("La deducción se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            bus();
                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox2.Focus();
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

    }
}
