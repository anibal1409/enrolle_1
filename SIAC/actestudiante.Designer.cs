﻿namespace SIAC
{
    partial class actestudiante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(actestudiante));
            this.button2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox99 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.académicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarModificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarAEstudianteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disciplinasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.disponiblesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resumenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaDeEstudiantesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.formalizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cambioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.periodoAcadémicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preinscripciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.pendientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estudianteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empleadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeTrabajoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeActividadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.finanzasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bancoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.pagosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarMiContraseñaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deduccionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.reciboDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitudToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónDeCalificacionesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónEgredadosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónRegularesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.listarCertificacionesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeEstudioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.postulaciónParaPasantíasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.usuarioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarRestContraseñaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem167 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem168 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem181 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem182 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem183 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem184 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem189 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem190 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem191 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem192 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem193 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem194 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem195 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem196 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem205 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem209 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem210 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem211 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem215 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem217 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem219 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem220 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem229 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem230 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem231 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem232 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem233 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem234 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem235 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem236 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem237 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem238 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem239 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem240 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem241 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem242 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem243 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem244 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem245 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem249 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem30 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem31 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem39 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem40 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem41 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem42 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem43 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem44 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem45 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem46 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem47 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem48 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem49 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem50 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem51 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem52 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem53 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem54 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem63 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem64 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem65 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem66 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem67 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem68 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem69 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem70 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem71 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem72 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem73 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem74 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem75 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem76 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem77 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem78 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem79 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem80 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem81 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem82 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem83 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.menuStrip4.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.Location = new System.Drawing.Point(490, 200);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(44, 44);
            this.button2.TabIndex = 44;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 19);
            this.label6.TabIndex = 42;
            this.label6.Text = "Teléfono";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(12, 160);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(169, 26);
            this.textBox5.TabIndex = 41;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(184, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 19);
            this.label8.TabIndex = 40;
            this.label8.Text = "Dirección";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(187, 105);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(347, 26);
            this.textBox4.TabIndex = 39;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(135, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 19);
            this.label5.TabIndex = 38;
            this.label5.Text = "Edad";
            // 
            // textBox99
            // 
            this.textBox99.Location = new System.Drawing.Point(138, 105);
            this.textBox99.Name = "textBox99";
            this.textBox99.ReadOnly = true;
            this.textBox99.Size = new System.Drawing.Size(40, 26);
            this.textBox99.TabIndex = 37;
            this.textBox99.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 19);
            this.label4.TabIndex = 36;
            this.label4.Text = "Fecha de Nac.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(358, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 19);
            this.label3.TabIndex = 35;
            this.label3.Text = "Apellido(s)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(184, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 19);
            this.label2.TabIndex = 34;
            this.label2.Text = "Nombre(s)";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(12, 105);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(123, 26);
            this.dateTimePicker1.TabIndex = 33;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(362, 54);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(169, 26);
            this.textBox3.TabIndex = 32;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(187, 54);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(169, 26);
            this.textBox2.TabIndex = 31;
            this.textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox2_KeyDown);
            this.textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            this.textBox2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox2_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 19);
            this.label1.TabIndex = 30;
            this.label1.Text = "Doc. de Identidad";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 54);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(169, 26);
            this.textBox1.TabIndex = 29;
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Location = new System.Drawing.Point(440, 200);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(44, 44);
            this.button3.TabIndex = 159;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(189, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 19);
            this.label7.TabIndex = 161;
            this.label7.Text = "Observación";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(188, 160);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(346, 26);
            this.textBox6.TabIndex = 160;
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "",
            "ACTIVO",
            "INACTIVO",
            "BLOQUEADO"});
            this.comboBox3.Location = new System.Drawing.Point(12, 217);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(169, 27);
            this.comboBox3.TabIndex = 163;
            this.comboBox3.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 195);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 19);
            this.label9.TabIndex = 162;
            this.label9.Text = "Estatus";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(157, 54);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 26);
            this.button1.TabIndex = 164;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem,
            this.académicoToolStripMenuItem,
            this.estudianteToolStripMenuItem,
            this.empleadoToolStripMenuItem,
            this.finanzasToolStripMenuItem,
            this.perfilToolStripMenuItem,
            this.salarioToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(549, 24);
            this.menuStrip1.TabIndex = 274;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.inicioToolStripMenuItem.Text = "Inicio";
            this.inicioToolStripMenuItem.Click += new System.EventHandler(this.inicioToolStripMenuItem_Click);
            // 
            // académicoToolStripMenuItem
            // 
            this.académicoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calificacionesToolStripMenuItem,
            this.disciplinasToolStripMenuItem,
            this.formalizaciónToolStripMenuItem,
            this.periodoAcadémicoToolStripMenuItem,
            this.preinscripciónToolStripMenuItem});
            this.académicoToolStripMenuItem.Name = "académicoToolStripMenuItem";
            this.académicoToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.académicoToolStripMenuItem.Text = "Académico";
            // 
            // calificacionesToolStripMenuItem
            // 
            this.calificacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarModificarToolStripMenuItem,
            this.modificarAEstudianteToolStripMenuItem});
            this.calificacionesToolStripMenuItem.Name = "calificacionesToolStripMenuItem";
            this.calificacionesToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.calificacionesToolStripMenuItem.Text = "Calificaciones";
            // 
            // cargarModificarToolStripMenuItem
            // 
            this.cargarModificarToolStripMenuItem.Name = "cargarModificarToolStripMenuItem";
            this.cargarModificarToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.cargarModificarToolStripMenuItem.Text = "Cargar/Modificar";
            this.cargarModificarToolStripMenuItem.Click += new System.EventHandler(this.cargarModificarToolStripMenuItem_Click);
            // 
            // modificarAEstudianteToolStripMenuItem
            // 
            this.modificarAEstudianteToolStripMenuItem.Name = "modificarAEstudianteToolStripMenuItem";
            this.modificarAEstudianteToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.modificarAEstudianteToolStripMenuItem.Text = "Modificar a Estudiante";
            this.modificarAEstudianteToolStripMenuItem.Click += new System.EventHandler(this.modificarAEstudianteToolStripMenuItem_Click);
            // 
            // disciplinasToolStripMenuItem
            // 
            this.disciplinasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem4,
            this.modificarToolStripMenuItem3,
            this.disponiblesToolStripMenuItem,
            this.resumenToolStripMenuItem,
            this.listaDeEstudiantesToolStripMenuItem,
            this.tiposToolStripMenuItem});
            this.disciplinasToolStripMenuItem.Name = "disciplinasToolStripMenuItem";
            this.disciplinasToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.disciplinasToolStripMenuItem.Text = "Disciplina";
            // 
            // registrarToolStripMenuItem4
            // 
            this.registrarToolStripMenuItem4.Name = "registrarToolStripMenuItem4";
            this.registrarToolStripMenuItem4.Size = new System.Drawing.Size(182, 22);
            this.registrarToolStripMenuItem4.Text = "Registrar";
            this.registrarToolStripMenuItem4.Click += new System.EventHandler(this.registrarToolStripMenuItem4_Click);
            // 
            // modificarToolStripMenuItem3
            // 
            this.modificarToolStripMenuItem3.Name = "modificarToolStripMenuItem3";
            this.modificarToolStripMenuItem3.Size = new System.Drawing.Size(182, 22);
            this.modificarToolStripMenuItem3.Text = "Modificar";
            this.modificarToolStripMenuItem3.Click += new System.EventHandler(this.modificarToolStripMenuItem3_Click);
            // 
            // disponiblesToolStripMenuItem
            // 
            this.disponiblesToolStripMenuItem.Name = "disponiblesToolStripMenuItem";
            this.disponiblesToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.disponiblesToolStripMenuItem.Text = "Disponibles";
            this.disponiblesToolStripMenuItem.Click += new System.EventHandler(this.disponiblesToolStripMenuItem_Click);
            // 
            // resumenToolStripMenuItem
            // 
            this.resumenToolStripMenuItem.Name = "resumenToolStripMenuItem";
            this.resumenToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.resumenToolStripMenuItem.Text = "Resumen";
            this.resumenToolStripMenuItem.Click += new System.EventHandler(this.resumenToolStripMenuItem_Click);
            // 
            // listaDeEstudiantesToolStripMenuItem
            // 
            this.listaDeEstudiantesToolStripMenuItem.Name = "listaDeEstudiantesToolStripMenuItem";
            this.listaDeEstudiantesToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.listaDeEstudiantesToolStripMenuItem.Text = "Lista de Estudiantes";
            this.listaDeEstudiantesToolStripMenuItem.Click += new System.EventHandler(this.listaDeEstudiantesToolStripMenuItem_Click);
            // 
            // tiposToolStripMenuItem
            // 
            this.tiposToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem5,
            this.modificarToolStripMenuItem4});
            this.tiposToolStripMenuItem.Name = "tiposToolStripMenuItem";
            this.tiposToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.tiposToolStripMenuItem.Text = "Tipos";
            // 
            // registrarToolStripMenuItem5
            // 
            this.registrarToolStripMenuItem5.Name = "registrarToolStripMenuItem5";
            this.registrarToolStripMenuItem5.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem5.Text = "Registrar";
            this.registrarToolStripMenuItem5.Click += new System.EventHandler(this.registrarToolStripMenuItem5_Click);
            // 
            // modificarToolStripMenuItem4
            // 
            this.modificarToolStripMenuItem4.Name = "modificarToolStripMenuItem4";
            this.modificarToolStripMenuItem4.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem4.Text = "Modificar";
            this.modificarToolStripMenuItem4.Click += new System.EventHandler(this.modificarToolStripMenuItem4_Click);
            // 
            // formalizaciónToolStripMenuItem
            // 
            this.formalizaciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem6,
            this.modificarToolStripMenuItem5,
            this.cambioToolStripMenuItem});
            this.formalizaciónToolStripMenuItem.Name = "formalizaciónToolStripMenuItem";
            this.formalizaciónToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.formalizaciónToolStripMenuItem.Text = "Formalización";
            // 
            // registrarToolStripMenuItem6
            // 
            this.registrarToolStripMenuItem6.Name = "registrarToolStripMenuItem6";
            this.registrarToolStripMenuItem6.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem6.Text = "Registrar";
            this.registrarToolStripMenuItem6.Click += new System.EventHandler(this.registrarToolStripMenuItem6_Click);
            // 
            // modificarToolStripMenuItem5
            // 
            this.modificarToolStripMenuItem5.Name = "modificarToolStripMenuItem5";
            this.modificarToolStripMenuItem5.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem5.Text = "Modificar";
            this.modificarToolStripMenuItem5.Click += new System.EventHandler(this.modificarToolStripMenuItem5_Click);
            // 
            // cambioToolStripMenuItem
            // 
            this.cambioToolStripMenuItem.Name = "cambioToolStripMenuItem";
            this.cambioToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cambioToolStripMenuItem.Text = "Cambio";
            this.cambioToolStripMenuItem.Click += new System.EventHandler(this.cambioToolStripMenuItem_Click);
            // 
            // periodoAcadémicoToolStripMenuItem
            // 
            this.periodoAcadémicoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem7,
            this.modificarToolStripMenuItem6,
            this.cerrarToolStripMenuItem});
            this.periodoAcadémicoToolStripMenuItem.Name = "periodoAcadémicoToolStripMenuItem";
            this.periodoAcadémicoToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.periodoAcadémicoToolStripMenuItem.Text = "Periodo Académico";
            // 
            // registrarToolStripMenuItem7
            // 
            this.registrarToolStripMenuItem7.Name = "registrarToolStripMenuItem7";
            this.registrarToolStripMenuItem7.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem7.Text = "Registrar";
            this.registrarToolStripMenuItem7.Click += new System.EventHandler(this.registrarToolStripMenuItem7_Click);
            // 
            // modificarToolStripMenuItem6
            // 
            this.modificarToolStripMenuItem6.Name = "modificarToolStripMenuItem6";
            this.modificarToolStripMenuItem6.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem6.Text = "Modificar";
            this.modificarToolStripMenuItem6.Click += new System.EventHandler(this.modificarToolStripMenuItem6_Click);
            // 
            // cerrarToolStripMenuItem
            // 
            this.cerrarToolStripMenuItem.Name = "cerrarToolStripMenuItem";
            this.cerrarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cerrarToolStripMenuItem.Text = "Cerrar";
            this.cerrarToolStripMenuItem.Click += new System.EventHandler(this.cerrarToolStripMenuItem_Click);
            // 
            // preinscripciónToolStripMenuItem
            // 
            this.preinscripciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem8,
            this.modificarToolStripMenuItem7,
            this.pendientesToolStripMenuItem});
            this.preinscripciónToolStripMenuItem.Name = "preinscripciónToolStripMenuItem";
            this.preinscripciónToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.preinscripciónToolStripMenuItem.Text = "Preinscripción";
            // 
            // registrarToolStripMenuItem8
            // 
            this.registrarToolStripMenuItem8.Name = "registrarToolStripMenuItem8";
            this.registrarToolStripMenuItem8.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem8.Text = "Registrar";
            this.registrarToolStripMenuItem8.Click += new System.EventHandler(this.registrarToolStripMenuItem8_Click);
            // 
            // modificarToolStripMenuItem7
            // 
            this.modificarToolStripMenuItem7.Name = "modificarToolStripMenuItem7";
            this.modificarToolStripMenuItem7.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem7.Text = "Modificar";
            this.modificarToolStripMenuItem7.Click += new System.EventHandler(this.modificarToolStripMenuItem7_Click);
            // 
            // pendientesToolStripMenuItem
            // 
            this.pendientesToolStripMenuItem.Name = "pendientesToolStripMenuItem";
            this.pendientesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pendientesToolStripMenuItem.Text = "Pendientes";
            this.pendientesToolStripMenuItem.Click += new System.EventHandler(this.pendientesToolStripMenuItem_Click);
            // 
            // estudianteToolStripMenuItem
            // 
            this.estudianteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem,
            this.modificarToolStripMenuItem,
            this.historialToolStripMenuItem});
            this.estudianteToolStripMenuItem.Name = "estudianteToolStripMenuItem";
            this.estudianteToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.estudianteToolStripMenuItem.Text = "Estudiante";
            // 
            // registrarToolStripMenuItem
            // 
            this.registrarToolStripMenuItem.Name = "registrarToolStripMenuItem";
            this.registrarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem.Text = "Registrar";
            this.registrarToolStripMenuItem.Click += new System.EventHandler(this.registrarToolStripMenuItem_Click);
            // 
            // modificarToolStripMenuItem
            // 
            this.modificarToolStripMenuItem.Name = "modificarToolStripMenuItem";
            this.modificarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem.Text = "Modificar";
            this.modificarToolStripMenuItem.Click += new System.EventHandler(this.modificarToolStripMenuItem_Click);
            // 
            // historialToolStripMenuItem
            // 
            this.historialToolStripMenuItem.Name = "historialToolStripMenuItem";
            this.historialToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.historialToolStripMenuItem.Text = "Historial";
            this.historialToolStripMenuItem.Click += new System.EventHandler(this.historialToolStripMenuItem_Click);
            // 
            // empleadoToolStripMenuItem
            // 
            this.empleadoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem9,
            this.modificarToolStripMenuItem8,
            this.constanciaDeTrabajoToolStripMenuItem,
            this.constanciaDeActividadToolStripMenuItem,
            this.cargosToolStripMenuItem});
            this.empleadoToolStripMenuItem.Name = "empleadoToolStripMenuItem";
            this.empleadoToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.empleadoToolStripMenuItem.Text = "Empleado";
            // 
            // registrarToolStripMenuItem9
            // 
            this.registrarToolStripMenuItem9.Name = "registrarToolStripMenuItem9";
            this.registrarToolStripMenuItem9.Size = new System.Drawing.Size(205, 22);
            this.registrarToolStripMenuItem9.Text = "Registrar";
            this.registrarToolStripMenuItem9.Click += new System.EventHandler(this.registrarToolStripMenuItem9_Click);
            // 
            // modificarToolStripMenuItem8
            // 
            this.modificarToolStripMenuItem8.Name = "modificarToolStripMenuItem8";
            this.modificarToolStripMenuItem8.Size = new System.Drawing.Size(205, 22);
            this.modificarToolStripMenuItem8.Text = "Modificar";
            this.modificarToolStripMenuItem8.Click += new System.EventHandler(this.modificarToolStripMenuItem8_Click);
            // 
            // constanciaDeTrabajoToolStripMenuItem
            // 
            this.constanciaDeTrabajoToolStripMenuItem.Name = "constanciaDeTrabajoToolStripMenuItem";
            this.constanciaDeTrabajoToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.constanciaDeTrabajoToolStripMenuItem.Text = "Constancia de Trabajo";
            this.constanciaDeTrabajoToolStripMenuItem.Click += new System.EventHandler(this.constanciaDeTrabajoToolStripMenuItem_Click);
            // 
            // constanciaDeActividadToolStripMenuItem
            // 
            this.constanciaDeActividadToolStripMenuItem.Name = "constanciaDeActividadToolStripMenuItem";
            this.constanciaDeActividadToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.constanciaDeActividadToolStripMenuItem.Text = "Constancia de Actividad";
            this.constanciaDeActividadToolStripMenuItem.Click += new System.EventHandler(this.constanciaDeActividadToolStripMenuItem_Click);
            // 
            // cargosToolStripMenuItem
            // 
            this.cargosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem10,
            this.modificarToolStripMenuItem9});
            this.cargosToolStripMenuItem.Name = "cargosToolStripMenuItem";
            this.cargosToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.cargosToolStripMenuItem.Text = "Cargos";
            // 
            // registrarToolStripMenuItem10
            // 
            this.registrarToolStripMenuItem10.Name = "registrarToolStripMenuItem10";
            this.registrarToolStripMenuItem10.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem10.Text = "Registrar";
            this.registrarToolStripMenuItem10.Click += new System.EventHandler(this.registrarToolStripMenuItem10_Click);
            // 
            // modificarToolStripMenuItem9
            // 
            this.modificarToolStripMenuItem9.Name = "modificarToolStripMenuItem9";
            this.modificarToolStripMenuItem9.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem9.Text = "Modificar";
            this.modificarToolStripMenuItem9.Click += new System.EventHandler(this.modificarToolStripMenuItem9_Click);
            // 
            // finanzasToolStripMenuItem
            // 
            this.finanzasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bancoToolStripMenuItem,
            this.pagosToolStripMenuItem,
            this.preciosToolStripMenuItem});
            this.finanzasToolStripMenuItem.Name = "finanzasToolStripMenuItem";
            this.finanzasToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.finanzasToolStripMenuItem.Text = "Finanzas";
            // 
            // bancoToolStripMenuItem
            // 
            this.bancoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem11,
            this.modificarToolStripMenuItem10});
            this.bancoToolStripMenuItem.Name = "bancoToolStripMenuItem";
            this.bancoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.bancoToolStripMenuItem.Text = "Banco";
            // 
            // registrarToolStripMenuItem11
            // 
            this.registrarToolStripMenuItem11.Name = "registrarToolStripMenuItem11";
            this.registrarToolStripMenuItem11.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem11.Text = "Registrar";
            this.registrarToolStripMenuItem11.Click += new System.EventHandler(this.registrarToolStripMenuItem11_Click);
            // 
            // modificarToolStripMenuItem10
            // 
            this.modificarToolStripMenuItem10.Name = "modificarToolStripMenuItem10";
            this.modificarToolStripMenuItem10.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem10.Text = "Modificar";
            this.modificarToolStripMenuItem10.Click += new System.EventHandler(this.modificarToolStripMenuItem10_Click);
            // 
            // pagosToolStripMenuItem
            // 
            this.pagosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem12,
            this.consultarToolStripMenuItem,
            this.modificarToolStripMenuItem11,
            this.verificarToolStripMenuItem,
            this.listaToolStripMenuItem});
            this.pagosToolStripMenuItem.Name = "pagosToolStripMenuItem";
            this.pagosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pagosToolStripMenuItem.Text = "Pagos";
            // 
            // registrarToolStripMenuItem12
            // 
            this.registrarToolStripMenuItem12.Name = "registrarToolStripMenuItem12";
            this.registrarToolStripMenuItem12.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem12.Text = "Registrar";
            this.registrarToolStripMenuItem12.Click += new System.EventHandler(this.registrarToolStripMenuItem12_Click);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.consultarToolStripMenuItem.Text = "Consultar";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
            // 
            // modificarToolStripMenuItem11
            // 
            this.modificarToolStripMenuItem11.Name = "modificarToolStripMenuItem11";
            this.modificarToolStripMenuItem11.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem11.Text = "Modificar";
            this.modificarToolStripMenuItem11.Click += new System.EventHandler(this.modificarToolStripMenuItem11_Click);
            // 
            // verificarToolStripMenuItem
            // 
            this.verificarToolStripMenuItem.Name = "verificarToolStripMenuItem";
            this.verificarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.verificarToolStripMenuItem.Text = "Verificar";
            this.verificarToolStripMenuItem.Click += new System.EventHandler(this.verificarToolStripMenuItem_Click);
            // 
            // listaToolStripMenuItem
            // 
            this.listaToolStripMenuItem.Name = "listaToolStripMenuItem";
            this.listaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.listaToolStripMenuItem.Text = "Lista";
            this.listaToolStripMenuItem.Click += new System.EventHandler(this.listaToolStripMenuItem_Click);
            // 
            // preciosToolStripMenuItem
            // 
            this.preciosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem13,
            this.consultarToolStripMenuItem1,
            this.modificarToolStripMenuItem12});
            this.preciosToolStripMenuItem.Name = "preciosToolStripMenuItem";
            this.preciosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.preciosToolStripMenuItem.Text = "Precios";
            // 
            // registrarToolStripMenuItem13
            // 
            this.registrarToolStripMenuItem13.Name = "registrarToolStripMenuItem13";
            this.registrarToolStripMenuItem13.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem13.Text = "Registrar";
            this.registrarToolStripMenuItem13.Click += new System.EventHandler(this.registrarToolStripMenuItem13_Click);
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.consultarToolStripMenuItem1.Text = "Consultar";
            this.consultarToolStripMenuItem1.Click += new System.EventHandler(this.consultarToolStripMenuItem1_Click);
            // 
            // modificarToolStripMenuItem12
            // 
            this.modificarToolStripMenuItem12.Name = "modificarToolStripMenuItem12";
            this.modificarToolStripMenuItem12.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem12.Text = "Modificar";
            this.modificarToolStripMenuItem12.Click += new System.EventHandler(this.modificarToolStripMenuItem12_Click);
            // 
            // perfilToolStripMenuItem
            // 
            this.perfilToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificarMiContraseñaToolStripMenuItem});
            this.perfilToolStripMenuItem.Name = "perfilToolStripMenuItem";
            this.perfilToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.perfilToolStripMenuItem.Text = "Perfil";
            // 
            // modificarMiContraseñaToolStripMenuItem
            // 
            this.modificarMiContraseñaToolStripMenuItem.Name = "modificarMiContraseñaToolStripMenuItem";
            this.modificarMiContraseñaToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.modificarMiContraseñaToolStripMenuItem.Text = "Modificar mi contraseña";
            this.modificarMiContraseñaToolStripMenuItem.Click += new System.EventHandler(this.modificarMiContraseñaToolStripMenuItem_Click);
            // 
            // salarioToolStripMenuItem
            // 
            this.salarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asignacionesToolStripMenuItem,
            this.deduccionesToolStripMenuItem,
            this.reciboDePagoToolStripMenuItem});
            this.salarioToolStripMenuItem.Name = "salarioToolStripMenuItem";
            this.salarioToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.salarioToolStripMenuItem.Text = "Salario";
            // 
            // asignacionesToolStripMenuItem
            // 
            this.asignacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem2,
            this.modificarToolStripMenuItem1});
            this.asignacionesToolStripMenuItem.Name = "asignacionesToolStripMenuItem";
            this.asignacionesToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.asignacionesToolStripMenuItem.Text = "Asignaciones";
            // 
            // registrarToolStripMenuItem2
            // 
            this.registrarToolStripMenuItem2.Name = "registrarToolStripMenuItem2";
            this.registrarToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem2.Text = "Registrar";
            this.registrarToolStripMenuItem2.Click += new System.EventHandler(this.registrarToolStripMenuItem2_Click);
            // 
            // modificarToolStripMenuItem1
            // 
            this.modificarToolStripMenuItem1.Name = "modificarToolStripMenuItem1";
            this.modificarToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem1.Text = "Modificar";
            this.modificarToolStripMenuItem1.Click += new System.EventHandler(this.modificarToolStripMenuItem1_Click);
            // 
            // deduccionesToolStripMenuItem
            // 
            this.deduccionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem3,
            this.modificarToolStripMenuItem2});
            this.deduccionesToolStripMenuItem.Name = "deduccionesToolStripMenuItem";
            this.deduccionesToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.deduccionesToolStripMenuItem.Text = "Deducciones";
            // 
            // registrarToolStripMenuItem3
            // 
            this.registrarToolStripMenuItem3.Name = "registrarToolStripMenuItem3";
            this.registrarToolStripMenuItem3.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem3.Text = "Registrar";
            this.registrarToolStripMenuItem3.Click += new System.EventHandler(this.registrarToolStripMenuItem3_Click);
            // 
            // modificarToolStripMenuItem2
            // 
            this.modificarToolStripMenuItem2.Name = "modificarToolStripMenuItem2";
            this.modificarToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem2.Text = "Modificar";
            this.modificarToolStripMenuItem2.Click += new System.EventHandler(this.modificarToolStripMenuItem2_Click);
            // 
            // reciboDePagoToolStripMenuItem
            // 
            this.reciboDePagoToolStripMenuItem.Name = "reciboDePagoToolStripMenuItem";
            this.reciboDePagoToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.reciboDePagoToolStripMenuItem.Text = "Recibo de Pago";
            this.reciboDePagoToolStripMenuItem.Click += new System.EventHandler(this.reciboDePagoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.solicitudToolStripMenuItem1,
            this.usuarioToolStripMenuItem1,
            this.salirToolStripMenuItem1});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(26, 20);
            this.toolStripMenuItem1.Text = "+";
            // 
            // solicitudToolStripMenuItem1
            // 
            this.solicitudToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.certificaciónDeCalificacionesToolStripMenuItem1,
            this.certificaciónEgredadosToolStripMenuItem1,
            this.certificaciónRegularesToolStripMenuItem1,
            this.listarCertificacionesToolStripMenuItem1,
            this.constanciaDeEstudioToolStripMenuItem1,
            this.postulaciónParaPasantíasToolStripMenuItem1});
            this.solicitudToolStripMenuItem1.Name = "solicitudToolStripMenuItem1";
            this.solicitudToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.solicitudToolStripMenuItem1.Text = "Solicitud";
            // 
            // certificaciónDeCalificacionesToolStripMenuItem1
            // 
            this.certificaciónDeCalificacionesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem20,
            this.modificarToolStripMenuItem18});
            this.certificaciónDeCalificacionesToolStripMenuItem1.Name = "certificaciónDeCalificacionesToolStripMenuItem1";
            this.certificaciónDeCalificacionesToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.certificaciónDeCalificacionesToolStripMenuItem1.Text = "Certificación de Calificaciones";
            // 
            // registrarToolStripMenuItem20
            // 
            this.registrarToolStripMenuItem20.Name = "registrarToolStripMenuItem20";
            this.registrarToolStripMenuItem20.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem20.Text = "Registrar";
            this.registrarToolStripMenuItem20.Click += new System.EventHandler(this.registrarToolStripMenuItem20_Click);
            // 
            // modificarToolStripMenuItem18
            // 
            this.modificarToolStripMenuItem18.Name = "modificarToolStripMenuItem18";
            this.modificarToolStripMenuItem18.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem18.Text = "Modificar";
            this.modificarToolStripMenuItem18.Click += new System.EventHandler(this.modificarToolStripMenuItem18_Click);
            // 
            // certificaciónEgredadosToolStripMenuItem1
            // 
            this.certificaciónEgredadosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem21,
            this.modificarToolStripMenuItem19});
            this.certificaciónEgredadosToolStripMenuItem1.Name = "certificaciónEgredadosToolStripMenuItem1";
            this.certificaciónEgredadosToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.certificaciónEgredadosToolStripMenuItem1.Text = "Certificación Egredados";
            // 
            // registrarToolStripMenuItem21
            // 
            this.registrarToolStripMenuItem21.Name = "registrarToolStripMenuItem21";
            this.registrarToolStripMenuItem21.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem21.Text = "Registrar";
            this.registrarToolStripMenuItem21.Click += new System.EventHandler(this.registrarToolStripMenuItem21_Click);
            // 
            // modificarToolStripMenuItem19
            // 
            this.modificarToolStripMenuItem19.Name = "modificarToolStripMenuItem19";
            this.modificarToolStripMenuItem19.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem19.Text = "Modificar";
            this.modificarToolStripMenuItem19.Click += new System.EventHandler(this.modificarToolStripMenuItem19_Click);
            // 
            // certificaciónRegularesToolStripMenuItem1
            // 
            this.certificaciónRegularesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem22,
            this.modificarToolStripMenuItem20});
            this.certificaciónRegularesToolStripMenuItem1.Name = "certificaciónRegularesToolStripMenuItem1";
            this.certificaciónRegularesToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.certificaciónRegularesToolStripMenuItem1.Text = "Certificación Regulares";
            // 
            // registrarToolStripMenuItem22
            // 
            this.registrarToolStripMenuItem22.Name = "registrarToolStripMenuItem22";
            this.registrarToolStripMenuItem22.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem22.Text = "Registrar";
            this.registrarToolStripMenuItem22.Click += new System.EventHandler(this.registrarToolStripMenuItem22_Click);
            // 
            // modificarToolStripMenuItem20
            // 
            this.modificarToolStripMenuItem20.Name = "modificarToolStripMenuItem20";
            this.modificarToolStripMenuItem20.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem20.Text = "Modificar";
            this.modificarToolStripMenuItem20.Click += new System.EventHandler(this.modificarToolStripMenuItem20_Click);
            // 
            // listarCertificacionesToolStripMenuItem1
            // 
            this.listarCertificacionesToolStripMenuItem1.Name = "listarCertificacionesToolStripMenuItem1";
            this.listarCertificacionesToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.listarCertificacionesToolStripMenuItem1.Text = "Listar Certificaciones";
            this.listarCertificacionesToolStripMenuItem1.Click += new System.EventHandler(this.listarCertificacionesToolStripMenuItem1_Click);
            // 
            // constanciaDeEstudioToolStripMenuItem1
            // 
            this.constanciaDeEstudioToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem23,
            this.modificarToolStripMenuItem21});
            this.constanciaDeEstudioToolStripMenuItem1.Name = "constanciaDeEstudioToolStripMenuItem1";
            this.constanciaDeEstudioToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.constanciaDeEstudioToolStripMenuItem1.Text = "Constancia de Estudio";
            // 
            // registrarToolStripMenuItem23
            // 
            this.registrarToolStripMenuItem23.Name = "registrarToolStripMenuItem23";
            this.registrarToolStripMenuItem23.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem23.Text = "Registrar";
            this.registrarToolStripMenuItem23.Click += new System.EventHandler(this.registrarToolStripMenuItem23_Click);
            // 
            // modificarToolStripMenuItem21
            // 
            this.modificarToolStripMenuItem21.Name = "modificarToolStripMenuItem21";
            this.modificarToolStripMenuItem21.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem21.Text = "Modificar";
            this.modificarToolStripMenuItem21.Click += new System.EventHandler(this.modificarToolStripMenuItem21_Click);
            // 
            // postulaciónParaPasantíasToolStripMenuItem1
            // 
            this.postulaciónParaPasantíasToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem24,
            this.modificarToolStripMenuItem22});
            this.postulaciónParaPasantíasToolStripMenuItem1.Name = "postulaciónParaPasantíasToolStripMenuItem1";
            this.postulaciónParaPasantíasToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.postulaciónParaPasantíasToolStripMenuItem1.Text = "Postulación para Pasantías";
            // 
            // registrarToolStripMenuItem24
            // 
            this.registrarToolStripMenuItem24.Name = "registrarToolStripMenuItem24";
            this.registrarToolStripMenuItem24.Size = new System.Drawing.Size(152, 22);
            this.registrarToolStripMenuItem24.Text = "Registrar";
            this.registrarToolStripMenuItem24.Click += new System.EventHandler(this.registrarToolStripMenuItem24_Click);
            // 
            // modificarToolStripMenuItem22
            // 
            this.modificarToolStripMenuItem22.Name = "modificarToolStripMenuItem22";
            this.modificarToolStripMenuItem22.Size = new System.Drawing.Size(152, 22);
            this.modificarToolStripMenuItem22.Text = "Modificar";
            this.modificarToolStripMenuItem22.Click += new System.EventHandler(this.modificarToolStripMenuItem22_Click);
            // 
            // usuarioToolStripMenuItem1
            // 
            this.usuarioToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem19,
            this.modificarRestContraseñaToolStripMenuItem1});
            this.usuarioToolStripMenuItem1.Name = "usuarioToolStripMenuItem1";
            this.usuarioToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.usuarioToolStripMenuItem1.Text = "Usuario";
            // 
            // registrarToolStripMenuItem19
            // 
            this.registrarToolStripMenuItem19.Name = "registrarToolStripMenuItem19";
            this.registrarToolStripMenuItem19.Size = new System.Drawing.Size(221, 22);
            this.registrarToolStripMenuItem19.Text = "Registrar";
            this.registrarToolStripMenuItem19.Click += new System.EventHandler(this.registrarToolStripMenuItem19_Click);
            // 
            // modificarRestContraseñaToolStripMenuItem1
            // 
            this.modificarRestContraseñaToolStripMenuItem1.Name = "modificarRestContraseñaToolStripMenuItem1";
            this.modificarRestContraseñaToolStripMenuItem1.Size = new System.Drawing.Size(221, 22);
            this.modificarRestContraseñaToolStripMenuItem1.Text = "Modificar/Rest. Contraseña";
            this.modificarRestContraseñaToolStripMenuItem1.Click += new System.EventHandler(this.modificarRestContraseñaToolStripMenuItem1_Click);
            // 
            // salirToolStripMenuItem1
            // 
            this.salirToolStripMenuItem1.Name = "salirToolStripMenuItem1";
            this.salirToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.salirToolStripMenuItem1.Text = "Salir";
            this.salirToolStripMenuItem1.Click += new System.EventHandler(this.salirToolStripMenuItem1_Click);
            // 
            // menuStrip4
            // 
            this.menuStrip4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem167,
            this.toolStripMenuItem168,
            this.toolStripMenuItem193,
            this.toolStripMenuItem205,
            this.toolStripMenuItem219,
            this.toolStripMenuItem229,
            this.toolStripMenuItem249});
            this.menuStrip4.Location = new System.Drawing.Point(0, 0);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.Size = new System.Drawing.Size(549, 24);
            this.menuStrip4.TabIndex = 276;
            this.menuStrip4.Text = "menuStrip4";
            this.menuStrip4.Visible = false;
            // 
            // toolStripMenuItem167
            // 
            this.toolStripMenuItem167.Name = "toolStripMenuItem167";
            this.toolStripMenuItem167.Size = new System.Drawing.Size(49, 20);
            this.toolStripMenuItem167.Text = "Inicio";
            // 
            // toolStripMenuItem168
            // 
            this.toolStripMenuItem168.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem181,
            this.toolStripMenuItem189});
            this.toolStripMenuItem168.Name = "toolStripMenuItem168";
            this.toolStripMenuItem168.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem168.Text = "Académico";
            // 
            // toolStripMenuItem181
            // 
            this.toolStripMenuItem181.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem182,
            this.toolStripMenuItem183,
            this.toolStripMenuItem184});
            this.toolStripMenuItem181.Name = "toolStripMenuItem181";
            this.toolStripMenuItem181.Size = new System.Drawing.Size(150, 22);
            this.toolStripMenuItem181.Text = "Formalización";
            // 
            // toolStripMenuItem182
            // 
            this.toolStripMenuItem182.Name = "toolStripMenuItem182";
            this.toolStripMenuItem182.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem182.Text = "Registrar";
            // 
            // toolStripMenuItem183
            // 
            this.toolStripMenuItem183.Name = "toolStripMenuItem183";
            this.toolStripMenuItem183.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem183.Text = "Modificar";
            // 
            // toolStripMenuItem184
            // 
            this.toolStripMenuItem184.Name = "toolStripMenuItem184";
            this.toolStripMenuItem184.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem184.Text = "Cambio";
            // 
            // toolStripMenuItem189
            // 
            this.toolStripMenuItem189.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem190,
            this.toolStripMenuItem191,
            this.toolStripMenuItem192});
            this.toolStripMenuItem189.Name = "toolStripMenuItem189";
            this.toolStripMenuItem189.Size = new System.Drawing.Size(150, 22);
            this.toolStripMenuItem189.Text = "Preinscripción";
            // 
            // toolStripMenuItem190
            // 
            this.toolStripMenuItem190.Name = "toolStripMenuItem190";
            this.toolStripMenuItem190.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem190.Text = "Registrar";
            // 
            // toolStripMenuItem191
            // 
            this.toolStripMenuItem191.Name = "toolStripMenuItem191";
            this.toolStripMenuItem191.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem191.Text = "Modificar";
            // 
            // toolStripMenuItem192
            // 
            this.toolStripMenuItem192.Name = "toolStripMenuItem192";
            this.toolStripMenuItem192.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem192.Text = "Pendientes";
            // 
            // toolStripMenuItem193
            // 
            this.toolStripMenuItem193.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem194,
            this.toolStripMenuItem195,
            this.toolStripMenuItem196});
            this.toolStripMenuItem193.Name = "toolStripMenuItem193";
            this.toolStripMenuItem193.Size = new System.Drawing.Size(76, 20);
            this.toolStripMenuItem193.Text = "Estudiante";
            // 
            // toolStripMenuItem194
            // 
            this.toolStripMenuItem194.Name = "toolStripMenuItem194";
            this.toolStripMenuItem194.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem194.Text = "Registrar";
            // 
            // toolStripMenuItem195
            // 
            this.toolStripMenuItem195.Name = "toolStripMenuItem195";
            this.toolStripMenuItem195.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem195.Text = "Modificar";
            // 
            // toolStripMenuItem196
            // 
            this.toolStripMenuItem196.Name = "toolStripMenuItem196";
            this.toolStripMenuItem196.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem196.Text = "Historial";
            // 
            // toolStripMenuItem205
            // 
            this.toolStripMenuItem205.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem209,
            this.toolStripMenuItem215});
            this.toolStripMenuItem205.Name = "toolStripMenuItem205";
            this.toolStripMenuItem205.Size = new System.Drawing.Size(66, 20);
            this.toolStripMenuItem205.Text = "Finanzas";
            // 
            // toolStripMenuItem209
            // 
            this.toolStripMenuItem209.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem210,
            this.toolStripMenuItem211});
            this.toolStripMenuItem209.Name = "toolStripMenuItem209";
            this.toolStripMenuItem209.Size = new System.Drawing.Size(113, 22);
            this.toolStripMenuItem209.Text = "Pagos";
            // 
            // toolStripMenuItem210
            // 
            this.toolStripMenuItem210.Name = "toolStripMenuItem210";
            this.toolStripMenuItem210.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem210.Text = "Registrar";
            // 
            // toolStripMenuItem211
            // 
            this.toolStripMenuItem211.Name = "toolStripMenuItem211";
            this.toolStripMenuItem211.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem211.Text = "Consultar";
            // 
            // toolStripMenuItem215
            // 
            this.toolStripMenuItem215.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem217});
            this.toolStripMenuItem215.Name = "toolStripMenuItem215";
            this.toolStripMenuItem215.Size = new System.Drawing.Size(113, 22);
            this.toolStripMenuItem215.Text = "Precios";
            // 
            // toolStripMenuItem217
            // 
            this.toolStripMenuItem217.Name = "toolStripMenuItem217";
            this.toolStripMenuItem217.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem217.Text = "Consultar";
            // 
            // toolStripMenuItem219
            // 
            this.toolStripMenuItem219.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem220});
            this.toolStripMenuItem219.Name = "toolStripMenuItem219";
            this.toolStripMenuItem219.Size = new System.Drawing.Size(46, 20);
            this.toolStripMenuItem219.Text = "Perfil";
            // 
            // toolStripMenuItem220
            // 
            this.toolStripMenuItem220.Name = "toolStripMenuItem220";
            this.toolStripMenuItem220.Size = new System.Drawing.Size(203, 22);
            this.toolStripMenuItem220.Text = "Modificar mi contraseña";
            // 
            // toolStripMenuItem229
            // 
            this.toolStripMenuItem229.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem230,
            this.toolStripMenuItem233,
            this.toolStripMenuItem236,
            this.toolStripMenuItem239,
            this.toolStripMenuItem240,
            this.toolStripMenuItem243});
            this.toolStripMenuItem229.Name = "toolStripMenuItem229";
            this.toolStripMenuItem229.Size = new System.Drawing.Size(66, 20);
            this.toolStripMenuItem229.Text = "Solicitud";
            // 
            // toolStripMenuItem230
            // 
            this.toolStripMenuItem230.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem231,
            this.toolStripMenuItem232});
            this.toolStripMenuItem230.Name = "toolStripMenuItem230";
            this.toolStripMenuItem230.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem230.Text = "Certificación de Calificaciones";
            // 
            // toolStripMenuItem231
            // 
            this.toolStripMenuItem231.Name = "toolStripMenuItem231";
            this.toolStripMenuItem231.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem231.Text = "Registrar";
            // 
            // toolStripMenuItem232
            // 
            this.toolStripMenuItem232.Name = "toolStripMenuItem232";
            this.toolStripMenuItem232.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem232.Text = "Modificar";
            // 
            // toolStripMenuItem233
            // 
            this.toolStripMenuItem233.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem234,
            this.toolStripMenuItem235});
            this.toolStripMenuItem233.Name = "toolStripMenuItem233";
            this.toolStripMenuItem233.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem233.Text = "Certificación Egredados";
            // 
            // toolStripMenuItem234
            // 
            this.toolStripMenuItem234.Name = "toolStripMenuItem234";
            this.toolStripMenuItem234.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem234.Text = "Registrar";
            // 
            // toolStripMenuItem235
            // 
            this.toolStripMenuItem235.Name = "toolStripMenuItem235";
            this.toolStripMenuItem235.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem235.Text = "Modificar";
            // 
            // toolStripMenuItem236
            // 
            this.toolStripMenuItem236.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem237,
            this.toolStripMenuItem238});
            this.toolStripMenuItem236.Name = "toolStripMenuItem236";
            this.toolStripMenuItem236.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem236.Text = "Certificación Regulares";
            // 
            // toolStripMenuItem237
            // 
            this.toolStripMenuItem237.Name = "toolStripMenuItem237";
            this.toolStripMenuItem237.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem237.Text = "Registrar";
            // 
            // toolStripMenuItem238
            // 
            this.toolStripMenuItem238.Name = "toolStripMenuItem238";
            this.toolStripMenuItem238.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem238.Text = "Modificar";
            // 
            // toolStripMenuItem239
            // 
            this.toolStripMenuItem239.Name = "toolStripMenuItem239";
            this.toolStripMenuItem239.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem239.Text = "Listar Certificaciones";
            // 
            // toolStripMenuItem240
            // 
            this.toolStripMenuItem240.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem241,
            this.toolStripMenuItem242});
            this.toolStripMenuItem240.Name = "toolStripMenuItem240";
            this.toolStripMenuItem240.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem240.Text = "Constancia de Estudio";
            // 
            // toolStripMenuItem241
            // 
            this.toolStripMenuItem241.Name = "toolStripMenuItem241";
            this.toolStripMenuItem241.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem241.Text = "Registrar";
            // 
            // toolStripMenuItem242
            // 
            this.toolStripMenuItem242.Name = "toolStripMenuItem242";
            this.toolStripMenuItem242.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem242.Text = "Modificar";
            // 
            // toolStripMenuItem243
            // 
            this.toolStripMenuItem243.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem244,
            this.toolStripMenuItem245});
            this.toolStripMenuItem243.Name = "toolStripMenuItem243";
            this.toolStripMenuItem243.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem243.Text = "Postulación para Pasantías";
            // 
            // toolStripMenuItem244
            // 
            this.toolStripMenuItem244.Name = "toolStripMenuItem244";
            this.toolStripMenuItem244.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem244.Text = "Registrar";
            // 
            // toolStripMenuItem245
            // 
            this.toolStripMenuItem245.Name = "toolStripMenuItem245";
            this.toolStripMenuItem245.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem245.Text = "Modificar";
            // 
            // toolStripMenuItem249
            // 
            this.toolStripMenuItem249.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenuItem249.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolStripMenuItem249.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.toolStripMenuItem249.Name = "toolStripMenuItem249";
            this.toolStripMenuItem249.Size = new System.Drawing.Size(42, 20);
            this.toolStripMenuItem249.Text = "Salir";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem28,
            this.toolStripMenuItem39,
            this.toolStripMenuItem53,
            this.toolStripMenuItem63,
            this.toolStripMenuItem80,
            this.toolStripMenuItem83});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(549, 24);
            this.menuStrip2.TabIndex = 277;
            this.menuStrip2.Text = "menuStrip2";
            this.menuStrip2.Visible = false;
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(49, 20);
            this.toolStripMenuItem2.Text = "Inicio";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripMenuItem7,
            this.toolStripMenuItem16,
            this.toolStripMenuItem20,
            this.toolStripMenuItem24});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem3.Text = "Académico";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem4.Text = "Calificaciones";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(195, 22);
            this.toolStripMenuItem5.Text = "Cargar/Modificar";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(195, 22);
            this.toolStripMenuItem6.Text = "Modificar a Estudiante";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.toolStripMenuItem10,
            this.toolStripMenuItem11,
            this.toolStripMenuItem12,
            this.toolStripMenuItem13});
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem7.Text = "Disciplina";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem8.Text = "Registrar";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem9.Text = "Modificar";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem10.Text = "Disponibles";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem11.Text = "Resumen";
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem12.Text = "Lista de Estudiantes";
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem14,
            this.toolStripMenuItem15});
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem13.Text = "Tipos";
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem14.Text = "Registrar";
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem15.Text = "Modificar";
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem17,
            this.toolStripMenuItem18,
            this.toolStripMenuItem19});
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem16.Text = "Formalización";
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem17.Text = "Registrar";
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem18.Text = "Modificar";
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem19.Text = "Cambio";
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem21,
            this.toolStripMenuItem22,
            this.toolStripMenuItem23});
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem20.Text = "Periodo Académico";
            // 
            // toolStripMenuItem21
            // 
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            this.toolStripMenuItem21.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem21.Text = "Registrar";
            // 
            // toolStripMenuItem22
            // 
            this.toolStripMenuItem22.Name = "toolStripMenuItem22";
            this.toolStripMenuItem22.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem22.Text = "Modificar";
            // 
            // toolStripMenuItem23
            // 
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            this.toolStripMenuItem23.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem23.Text = "Cerrar";
            // 
            // toolStripMenuItem24
            // 
            this.toolStripMenuItem24.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem25,
            this.toolStripMenuItem26,
            this.toolStripMenuItem27});
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            this.toolStripMenuItem24.Size = new System.Drawing.Size(177, 22);
            this.toolStripMenuItem24.Text = "Preinscripción";
            // 
            // toolStripMenuItem25
            // 
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            this.toolStripMenuItem25.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem25.Text = "Registrar";
            // 
            // toolStripMenuItem26
            // 
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            this.toolStripMenuItem26.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem26.Text = "Modificar";
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem27.Text = "Pendientes";
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem29,
            this.toolStripMenuItem30,
            this.toolStripMenuItem31});
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(76, 20);
            this.toolStripMenuItem28.Text = "Estudiante";
            // 
            // toolStripMenuItem29
            // 
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            this.toolStripMenuItem29.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem29.Text = "Registrar";
            // 
            // toolStripMenuItem30
            // 
            this.toolStripMenuItem30.Name = "toolStripMenuItem30";
            this.toolStripMenuItem30.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem30.Text = "Modificar";
            // 
            // toolStripMenuItem31
            // 
            this.toolStripMenuItem31.Name = "toolStripMenuItem31";
            this.toolStripMenuItem31.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem31.Text = "Historial";
            // 
            // toolStripMenuItem39
            // 
            this.toolStripMenuItem39.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem40,
            this.toolStripMenuItem43,
            this.toolStripMenuItem49});
            this.toolStripMenuItem39.Name = "toolStripMenuItem39";
            this.toolStripMenuItem39.Size = new System.Drawing.Size(66, 20);
            this.toolStripMenuItem39.Text = "Finanzas";
            // 
            // toolStripMenuItem40
            // 
            this.toolStripMenuItem40.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem41,
            this.toolStripMenuItem42});
            this.toolStripMenuItem40.Name = "toolStripMenuItem40";
            this.toolStripMenuItem40.Size = new System.Drawing.Size(113, 22);
            this.toolStripMenuItem40.Text = "Banco";
            // 
            // toolStripMenuItem41
            // 
            this.toolStripMenuItem41.Name = "toolStripMenuItem41";
            this.toolStripMenuItem41.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem41.Text = "Registrar";
            // 
            // toolStripMenuItem42
            // 
            this.toolStripMenuItem42.Name = "toolStripMenuItem42";
            this.toolStripMenuItem42.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem42.Text = "Modificar";
            // 
            // toolStripMenuItem43
            // 
            this.toolStripMenuItem43.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem44,
            this.toolStripMenuItem45,
            this.toolStripMenuItem46,
            this.toolStripMenuItem47,
            this.toolStripMenuItem48});
            this.toolStripMenuItem43.Name = "toolStripMenuItem43";
            this.toolStripMenuItem43.Size = new System.Drawing.Size(113, 22);
            this.toolStripMenuItem43.Text = "Pagos";
            // 
            // toolStripMenuItem44
            // 
            this.toolStripMenuItem44.Name = "toolStripMenuItem44";
            this.toolStripMenuItem44.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem44.Text = "Registrar";
            // 
            // toolStripMenuItem45
            // 
            this.toolStripMenuItem45.Name = "toolStripMenuItem45";
            this.toolStripMenuItem45.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem45.Text = "Consultar";
            // 
            // toolStripMenuItem46
            // 
            this.toolStripMenuItem46.Name = "toolStripMenuItem46";
            this.toolStripMenuItem46.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem46.Text = "Modificar";
            // 
            // toolStripMenuItem47
            // 
            this.toolStripMenuItem47.Name = "toolStripMenuItem47";
            this.toolStripMenuItem47.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem47.Text = "Verificar";
            // 
            // toolStripMenuItem48
            // 
            this.toolStripMenuItem48.Name = "toolStripMenuItem48";
            this.toolStripMenuItem48.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem48.Text = "Lista";
            // 
            // toolStripMenuItem49
            // 
            this.toolStripMenuItem49.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem50,
            this.toolStripMenuItem51,
            this.toolStripMenuItem52});
            this.toolStripMenuItem49.Name = "toolStripMenuItem49";
            this.toolStripMenuItem49.Size = new System.Drawing.Size(113, 22);
            this.toolStripMenuItem49.Text = "Precios";
            // 
            // toolStripMenuItem50
            // 
            this.toolStripMenuItem50.Name = "toolStripMenuItem50";
            this.toolStripMenuItem50.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem50.Text = "Registrar";
            // 
            // toolStripMenuItem51
            // 
            this.toolStripMenuItem51.Name = "toolStripMenuItem51";
            this.toolStripMenuItem51.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem51.Text = "Consultar";
            // 
            // toolStripMenuItem52
            // 
            this.toolStripMenuItem52.Name = "toolStripMenuItem52";
            this.toolStripMenuItem52.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem52.Text = "Modificar";
            // 
            // toolStripMenuItem53
            // 
            this.toolStripMenuItem53.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem54});
            this.toolStripMenuItem53.Name = "toolStripMenuItem53";
            this.toolStripMenuItem53.Size = new System.Drawing.Size(46, 20);
            this.toolStripMenuItem53.Text = "Perfil";
            // 
            // toolStripMenuItem54
            // 
            this.toolStripMenuItem54.Name = "toolStripMenuItem54";
            this.toolStripMenuItem54.Size = new System.Drawing.Size(203, 22);
            this.toolStripMenuItem54.Text = "Modificar mi contraseña";
            // 
            // toolStripMenuItem63
            // 
            this.toolStripMenuItem63.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem64,
            this.toolStripMenuItem67,
            this.toolStripMenuItem70,
            this.toolStripMenuItem73,
            this.toolStripMenuItem74,
            this.toolStripMenuItem77});
            this.toolStripMenuItem63.Name = "toolStripMenuItem63";
            this.toolStripMenuItem63.Size = new System.Drawing.Size(66, 20);
            this.toolStripMenuItem63.Text = "Solicitud";
            // 
            // toolStripMenuItem64
            // 
            this.toolStripMenuItem64.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem65,
            this.toolStripMenuItem66});
            this.toolStripMenuItem64.Name = "toolStripMenuItem64";
            this.toolStripMenuItem64.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem64.Text = "Certificación de Calificaciones";
            // 
            // toolStripMenuItem65
            // 
            this.toolStripMenuItem65.Name = "toolStripMenuItem65";
            this.toolStripMenuItem65.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem65.Text = "Registrar";
            // 
            // toolStripMenuItem66
            // 
            this.toolStripMenuItem66.Name = "toolStripMenuItem66";
            this.toolStripMenuItem66.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem66.Text = "Modificar";
            // 
            // toolStripMenuItem67
            // 
            this.toolStripMenuItem67.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem68,
            this.toolStripMenuItem69});
            this.toolStripMenuItem67.Name = "toolStripMenuItem67";
            this.toolStripMenuItem67.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem67.Text = "Certificación Egredados";
            // 
            // toolStripMenuItem68
            // 
            this.toolStripMenuItem68.Name = "toolStripMenuItem68";
            this.toolStripMenuItem68.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem68.Text = "Registrar";
            // 
            // toolStripMenuItem69
            // 
            this.toolStripMenuItem69.Name = "toolStripMenuItem69";
            this.toolStripMenuItem69.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem69.Text = "Modificar";
            // 
            // toolStripMenuItem70
            // 
            this.toolStripMenuItem70.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem71,
            this.toolStripMenuItem72});
            this.toolStripMenuItem70.Name = "toolStripMenuItem70";
            this.toolStripMenuItem70.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem70.Text = "Certificación Regulares";
            // 
            // toolStripMenuItem71
            // 
            this.toolStripMenuItem71.Name = "toolStripMenuItem71";
            this.toolStripMenuItem71.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem71.Text = "Registrar";
            // 
            // toolStripMenuItem72
            // 
            this.toolStripMenuItem72.Name = "toolStripMenuItem72";
            this.toolStripMenuItem72.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem72.Text = "Modificar";
            // 
            // toolStripMenuItem73
            // 
            this.toolStripMenuItem73.Name = "toolStripMenuItem73";
            this.toolStripMenuItem73.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem73.Text = "Listar Certificaciones";
            // 
            // toolStripMenuItem74
            // 
            this.toolStripMenuItem74.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem75,
            this.toolStripMenuItem76});
            this.toolStripMenuItem74.Name = "toolStripMenuItem74";
            this.toolStripMenuItem74.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem74.Text = "Constancia de Estudio";
            // 
            // toolStripMenuItem75
            // 
            this.toolStripMenuItem75.Name = "toolStripMenuItem75";
            this.toolStripMenuItem75.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem75.Text = "Registrar";
            // 
            // toolStripMenuItem76
            // 
            this.toolStripMenuItem76.Name = "toolStripMenuItem76";
            this.toolStripMenuItem76.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem76.Text = "Modificar";
            // 
            // toolStripMenuItem77
            // 
            this.toolStripMenuItem77.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem78,
            this.toolStripMenuItem79});
            this.toolStripMenuItem77.Name = "toolStripMenuItem77";
            this.toolStripMenuItem77.Size = new System.Drawing.Size(234, 22);
            this.toolStripMenuItem77.Text = "Postulación para Pasantías";
            // 
            // toolStripMenuItem78
            // 
            this.toolStripMenuItem78.Name = "toolStripMenuItem78";
            this.toolStripMenuItem78.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem78.Text = "Registrar";
            // 
            // toolStripMenuItem79
            // 
            this.toolStripMenuItem79.Name = "toolStripMenuItem79";
            this.toolStripMenuItem79.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem79.Text = "Modificar";
            // 
            // toolStripMenuItem80
            // 
            this.toolStripMenuItem80.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem81,
            this.toolStripMenuItem82});
            this.toolStripMenuItem80.Name = "toolStripMenuItem80";
            this.toolStripMenuItem80.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem80.Text = "Usuario";
            // 
            // toolStripMenuItem81
            // 
            this.toolStripMenuItem81.Name = "toolStripMenuItem81";
            this.toolStripMenuItem81.Size = new System.Drawing.Size(221, 22);
            this.toolStripMenuItem81.Text = "Registrar";
            // 
            // toolStripMenuItem82
            // 
            this.toolStripMenuItem82.Name = "toolStripMenuItem82";
            this.toolStripMenuItem82.Size = new System.Drawing.Size(221, 22);
            this.toolStripMenuItem82.Text = "Modificar/Rest. Contraseña";
            // 
            // toolStripMenuItem83
            // 
            this.toolStripMenuItem83.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenuItem83.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolStripMenuItem83.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.toolStripMenuItem83.Name = "toolStripMenuItem83";
            this.toolStripMenuItem83.Size = new System.Drawing.Size(42, 20);
            this.toolStripMenuItem83.Text = "Salir";
            // 
            // actestudiante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(549, 257);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.Controls.Add(this.menuStrip4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox99);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "actestudiante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modificar Estudiante";
            this.Load += new System.EventHandler(this.actestudiante_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox99;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem académicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarModificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarAEstudianteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disciplinasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem disponiblesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resumenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaDeEstudiantesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tiposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem formalizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem cambioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem periodoAcadémicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem cerrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preinscripciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem pendientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estudianteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empleadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeTrabajoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeActividadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem finanzasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bancoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem pagosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem verificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem perfilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarMiContraseñaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deduccionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem reciboDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem solicitudToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem certificaciónDeCalificacionesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem certificaciónEgredadosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem certificaciónRegularesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem listarCertificacionesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeEstudioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem postulaciónParaPasantíasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem usuarioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem modificarRestContraseñaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem1;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem167;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem168;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem181;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem182;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem183;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem184;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem189;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem190;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem191;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem192;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem193;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem194;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem195;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem196;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem205;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem209;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem210;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem211;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem215;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem217;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem219;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem220;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem229;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem230;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem231;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem232;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem233;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem234;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem235;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem236;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem237;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem238;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem239;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem240;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem241;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem242;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem243;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem244;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem245;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem249;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem29;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem30;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem31;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem39;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem40;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem41;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem42;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem43;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem44;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem45;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem46;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem47;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem48;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem49;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem50;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem51;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem52;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem53;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem54;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem63;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem64;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem65;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem66;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem67;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem68;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem69;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem70;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem71;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem72;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem73;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem74;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem75;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem76;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem77;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem78;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem79;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem80;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem81;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem82;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem83;
    }
}