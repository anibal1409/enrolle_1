﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIAC
{
    class estatica
    {
        private static string ci;

        public static string Ci
        {
            get { return estatica.ci; }
            set { estatica.ci = value; }
        }

        private static string nombre;

        public static string Nombre
        {
            get { return estatica.nombre; }
            set { estatica.nombre = value; }
        }

        private static string ape;

        public static string Ape
        {
            get { return estatica.ape; }
            set { estatica.ape = value; }
        }

        private static string nivel;

        public static string Nivel
        {
            get { return estatica.nivel; }
            set { estatica.nivel = value; }
        }

        private static string pas;

        public static string Pas
        {
            get { return estatica.pas; }
            set { estatica.pas = value; }
        }

        private static string obser;

        public static string Obser
        {
            get { return estatica.obser; }
            set { estatica.obser = value; }
        }

        private static string usu;

        public static string Usu
        {
            get { return estatica.usu; }
            set { estatica.usu = value; }
        }

        private static string estudiante;

        public static string Estudiante
        {
            get { return estatica.estudiante; }
            set { estatica.estudiante = value; }
        }

        private static string ne;

        public static string Ne
        {
            get { return estatica.ne; }
            set { estatica.ne = value; }
        }

        private static string pre;

        public static string Pre
        {
            get { return estatica.pre; }
            set { estatica.pre = value; }
        }

        private static string certi;

        public static string Certi
        {
            get { return estatica.certi; }
            set { estatica.certi = value; }
        }

        private static string mod;

        public static string Mod
        {
            get { return estatica.mod; }
            set { estatica.mod = value; }
        }

        private static string periodo;

        public static string Periodo
        {
            get { return estatica.periodo; }
            set { estatica.periodo = value; }
        }

        private static string incio;

        public static string Incio
        {
            get { return estatica.incio; }
            set { estatica.incio = value; }
        }

        private static string fin;

        public static string Fin
        {
            get { return estatica.fin; }
            set { estatica.fin = value; }
        }

    }
}
