﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class select : Form
    {
        public select()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void select_Load(object sender, EventArgs e)
        {
            conexion.Open();
            dataGridView1.AllowUserToAddRows = false;//eliminar filas por defecto  
            cargar();
        }
        string[,] comen;
        int can;
        void cargar()
        {
            string buscarcxx = "SELECT * FROM modalidad WHERE cod = " + estatica.Certi + ";";
            OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
            OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
            OleDbDataReader leexx = comandoxx.ExecuteReader();
            if (leexx.Read() == true)
            {
                label1.Text = "Lista de estudiantes de "+leexx["modalidad"].ToString();
            }

            string buscarc = "SELECT * FROM pre WHERE modalidad = "+estatica.Certi+" AND estatus = 'VERIFICADO';";
            OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
            OleDbCommand comando = new OleDbCommand(buscarc, conexion);
            OleDbDataReader lee = comando.ExecuteReader();
            can = 0;
            comen = new string[3, 1000000];

            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            while (lee.Read())
            {
                string buscarcx = "SELECT * FROM estudiante WHERE ci = '" + lee["estudiante"].ToString() + "';";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read() == true)
                {
                    comen[0, can] = leex["ci"].ToString();
                    comen[1, can] = leex["apellido"].ToString();
                    comen[2, can] = leex["nombre"].ToString();
                    can++;
                }
                

            }
            if (can > 1)
            {
                string ax, bx, cx;
                for (int i = 0; i < can; i++)
                {
                    for (int j = 0; j < can - 1; j++)
                    {
                        if (String.Compare(comen[1, j], comen[1, j + 1]) > 0)
                        {
                            ax = comen[0, j];
                            bx = comen[1, j];
                            cx = comen[2, j];
                            comen[0, j] = comen[0, j + 1];
                            comen[1, j] = comen[1, j + 1];
                            comen[2, j] = comen[2, j + 1];
                            comen[0, j + 1] = ax;
                            comen[1, j + 1] = bx;
                            comen[2, j + 1] = cx;
                        }
                    }
                }
            }

            if (can > 0)
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                for (int i = 0; i < can; i++)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells[0].Value = comen[0, i];
                    dataGridView1.Rows[i].Cells[1].Value = comen[1, i];
                    dataGridView1.Rows[i].Cells[2].Value = comen[2, i];
                    string buscarcx = "SELECT * FROM certi WHERE estudiante = '" + comen[0, i] + "' AND modalidad = "+estatica.Certi+";";
                    OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                    OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read() == true)
                    {
                        dataGridView1.Rows[i].Cells[3].Value = "Solicitado";
                    }
                    else
                    {
                        dataGridView1.Rows[i].Cells[3].Value = "Seleccionar";
                    }

                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                if (dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString() == "Seleccionar" || estatica.Mod == "1")
                {
                    estatica.Estudiante = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                    estatica.Ne = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString() + " " + dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                    this.DialogResult = DialogResult.Yes;
                }
                else
                {
                    MessageBox.Show("El estudiante ya solícito su certificado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
