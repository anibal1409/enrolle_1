﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class actmodalidad : Form
    {
        public actmodalidad()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void actmodalidad_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
            comboBox3.SelectedIndex = 0;
            comboBox4.SelectedIndex = 0;
        }
        void combo1()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM tipom ORDER BY modalidad";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione un tipo de modalidad";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox1.ValueMember = "cod";
            comboBox1.DisplayMember = "modalidad";
            comboBox1.DataSource = dt;
        }
        string ci;
        void buscar()
        {
            if (textBox5.Text != "")
            {
                ci = "";
                var cadena = textBox5.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM empleado WHERE ci = '" + textBox5.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        if (lee["estatus"].ToString() == "ACTIVO")
                        {

                            ci = textBox5.Text;
                            textBox6.Text = lee["nombre"].ToString() + " " + lee["apellido"].ToString();
                            textBox7.Text = lee["profesion"].ToString();
                        }
                        else
                        {
                            MessageBox.Show("Este empleado no se encuentra activo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad ingresado no pertenece a ningun empleado registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para realizar una busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        string mod, dispo, di;
        void busca()
        {
            if (textBox1.Text != "")
            {
                mod = "";
                string buscarc = "SELECT * FROM modalidad WHERE cod = " + textBox1.Text + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    if (lee["estatus"].ToString() == "DISPONIBLE" || lee["estatus"].ToString() == "NO DISPONIBLE")
                    {
                        mod = lee["cod"].ToString();
                        comboBox1.SelectedValue = lee["tipo"].ToString();
                        textBox2.Text = lee["modalidad"].ToString();
                        string buscarcx = "SELECT * FROM periodo WHERE cod = " + lee["periodo"].ToString() + ";";
                        OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                        OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                        OleDbDataReader leex = comandox.ExecuteReader();
                        if (leex.Read() == true)
                        {
                            textBox9.Text = leex["periodo"].ToString();
                            textBox3.Text = leex["inicio"].ToString();
                            textBox4.Text = leex["fin"].ToString();
                        }
                        comboBox3.Text = lee["dia"].ToString();
                        dateTimePicker1.Text = lee["desde"].ToString();
                        dateTimePicker2.Text = lee["hasta"].ToString();
                        dateTimePicker3.Text = lee["inicio"].ToString();
                        dateTimePicker4.Text = lee["fin"].ToString();
                        numericUpDown1.Value = int.Parse(lee["horas"].ToString());
                        numericUpDown2.Value = int.Parse(lee["semanas"].ToString());
                        numericUpDown3.Value = int.Parse(lee["capacidad"].ToString());
                        textBox5.Text = lee["profesor"].ToString();
                        buscar();
                        textBox8.Text = lee["precio"].ToString();
                        comboBox4.Text = lee["estatus"].ToString();
                        dispo = lee["capacidad"].ToString();
                        di = lee["disponible"].ToString();
                        button3.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("El código ingresado pertenece a una disciplina culminada y por lo tanto no puede ser modificada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        button1.Visible = false;
                        textBox1.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("El código ingresado no pertenece a una disciplina registrada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    textBox1.Focus();
                    button1.Visible = false;
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox1.Focus();
                button1.Visible = false;
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            busca();
        }

        private static void OnlyNumber(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox5_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buscar();
            }
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                busca();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            ci = "";
        }

        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox8.Clear();
            textBox9.Clear();
            ci = "";
            dispo = "";
            mod = "";
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
            dateTimePicker3.Value = DateTime.Now;
            dateTimePicker4.Value = DateTime.Now;
            comboBox1.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            comboBox4.SelectedIndex = 0;
            numericUpDown1.Value = 1;
            numericUpDown2.Value = 1;
            numericUpDown3.Value = 40;
            button3.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        public bool IsNumeric(string x)
        {

            double retNum;

            try
            {
                retNum = Double.Parse(x);
                return true;
            }
            catch
            {
                return false;

            }



        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ci != "" && textBox2.Text != "" && comboBox3.Text != "Seleccione un día" && textBox8.Text != "" && comboBox4.Text != "Seleccione un estatus")
            {
                if (IsNumeric(textBox8.Text))
                {
                    DateTime a, b;
                    if (dateTimePicker1.Value < dateTimePicker2.Value)
                    {
                        a = DateTime.Parse(textBox3.Text);
                        b = DateTime.Parse(textBox4.Text);
                        if(String.Compare(dateTimePicker3.Value.ToString("dd-MM-yyy"), dateTimePicker4.Value.ToString("dd-MM-yyy")) <=0)
                        {
                            if (String.Compare(dateTimePicker3.Value.ToString("dd-MM-yyy"), textBox3.Text) >= 0)
                            {
                                if (String.Compare(dateTimePicker4.Value.ToString("dd-MM-yyy"), textBox4.Text) <= 0)
                                {
                                    try
                                    {
                                        int tota = int.Parse(numericUpDown3.Value.ToString()) - int.Parse(dispo);
                                        int total = int.Parse(di) + tota;
                                        string insertar = "UPDATE  modalidad SET  tipo = " + comboBox1.SelectedValue.ToString() + ", modalidad = '" + textBox2.Text + "', dia = '" + comboBox3.Text + "', desde = '" + dateTimePicker1.Text + "', hasta = '" + dateTimePicker2.Text + "', inicio = '" + dateTimePicker3.Text + "', fin = '" + dateTimePicker4.Text + "', horas = '" + numericUpDown1.Value.ToString() + "', semanas = '" + numericUpDown2.Value.ToString() + "', capacidad = '" + numericUpDown3.Value.ToString() + "', profesor = '" + ci + "', precio = '" + textBox8.Text + "', estatus = '" + comboBox4.Text + "', disponible = '" + total.ToString() + "' WHERE cod = " + mod + " ";
                                        OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                        cmd.ExecuteNonQuery();
                                        clear();
                                        MessageBox.Show("Los datos de la disciplina fueron modificados con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);


                                    }

                                    catch (DBConcurrencyException ex)
                                    {
                                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("La fecha de finalización no puede ser mayor que la fecha de fin del periodo académico seleccionado. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    dateTimePicker4.Focus();
                                }
                            }
                            else
                            {
                                MessageBox.Show("La fecha de inicio no puede ser menor que la fecha de inicio del periodo académico seleccionado. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                dateTimePicker3.Focus();
                            }
                        }
                        else
                        {
                            MessageBox.Show("La fecha de inicio no puede ser mayor que la fecha de finalización. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            dateTimePicker3.Focus();
                        }

                    }
                    else
                    {
                        MessageBox.Show("La hora de salida no puede ser menor ni igual que la hora de entrada. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        dateTimePicker2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("El monto ingresado no es valido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox8.Focus();
                }

            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar la modificación.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
