﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class estudiante : Form
    {
        public estudiante()
        {
            InitializeComponent();
        }

        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void estudiante_Load(object sender, EventArgs e)
        {
            conexion.Open();
            if (estatica.Nivel == "Administrador")
            {
                //menuStrip1.Visible = true;
            }
            if (estatica.Nivel == "Asesor de Ventas")
            {
                //menuStrip4.Visible = true;
            }
            if (estatica.Nivel == "Coordinador Académico")
            {
                //menuStrip2.Visible = true;
            }
            if (estatica.Nivel == "Coordinador de Personal")
            {
                //menuStrip3.Visible = true;
            }
        }
        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox99.Clear();
            dateTimePicker1.Value = DateTime.Now;
            textBox1.Focus();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox5.Text != "" && textBox99.Text != "" && textBox4.Text != "" && dateTimePicker1.Text != DateTime.Now.ToString("dd-MM-yyyy"))
            {
                var cadena = textBox1.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox1.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        MessageBox.Show("El documento de identidad  '" + textBox1.Text + "' ya se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox1.Focus();
                    }
                    else
                    {

                        try
                        {
                            string insertar = "INSERT INTO estudiante VALUES ('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + dateTimePicker1.Text + "','" + textBox4.Text + "','" + textBox5.Text + "', 'ACTIVO', '')";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            clear();
                            MessageBox.Show("El estudiante se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        DateTime var;
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker1.Text != DateTime.Now.ToString("dd-MM-yyyy"))
            {
                var = dateTimePicker1.Value.Date;
                int edad = DateTime.Today.AddTicks(-var.Ticks).Year - 1;
                textBox99.Text = edad.ToString();
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        
        
    }
}
