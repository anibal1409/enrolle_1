﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class actpas : Form
    {
        public actpas()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void actpas_Load(object sender, EventArgs e)
        {
            conexion.Open();
        }
            

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text == textBox2.Text)
            {
                textBox2.BackColor = Color.Cyan;
                textBox3.BackColor = Color.Cyan;
            }
            else
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
            if (textBox2.Text == "" || textBox2.Text == " " || textBox2.Text == "  " || textBox2.Text == "  ")
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
            if (textBox2.Text == "" || textBox3.Text == " " || textBox3.Text == "  " || textBox3.Text == "  ")
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text == textBox2.Text)
            {
                textBox2.BackColor = Color.Cyan;
                textBox3.BackColor = Color.Cyan;
            }
            else
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
            if (textBox2.Text == "" || textBox2.Text == " " || textBox2.Text == "  " || textBox2.Text == "  ")
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
            if (textBox2.Text == "" || textBox3.Text == " " || textBox3.Text == "  " || textBox3.Text == "  ")
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                textBox2.PasswordChar = '\0';
                textBox3.PasswordChar = '\0';
            }
            else
            {
                textBox2.PasswordChar = '*';
                textBox3.PasswordChar = '*';
            
            }
        }

        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            checkBox1.Checked = false;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
            {
                if (MessageBox.Show("¿Desea continuar con la modificación de su contraseña de acceso al sistema?", "Notificación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (estatica.Pas == textBox1.Text && textBox2.Text == textBox3.Text)
                    {
                        try
                        {
                            string insertar = "UPDATE  usuario SET pass = '" + textBox2.Text + "' WHERE nombre = '" + estatica.Ci + "' ";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            estatica.Pas = textBox2.Text;
                            MessageBox.Show("Su contraseña ha sido modificada con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            clear();
                            this.Close();
                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        if (estatica.Pas != textBox1.Text)
                        {
                            MessageBox.Show("La contraseña actual ingresa es incorrecta.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            textBox1.Focus();
                        }
                        else
                        {
                            MessageBox.Show("La nueva contraseña y la confirmación son diferentes.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            textBox2.Focus();
                        }

                    }

                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para poder utilizar esta función.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
    }
}
