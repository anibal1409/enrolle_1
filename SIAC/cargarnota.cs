﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Drawing.Printing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;

namespace SIAC
{
    public partial class cargarnota : Form
    {
        public cargarnota()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");

        private void cargarnota_Load(object sender, EventArgs e)
        {
            conexion.Open();
            dataGridView1.AllowUserToAddRows = false;//eliminar filas por defecto 
            combo1();
            notas();

        }
        void combo1()
        {
            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dtx = new DataTable();
            dtx = new DataTable();
            DATA.Fill(dtx);

            DataRow nuevaFila = dtx.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dtx.Rows.InsertAt(nuevaFila, 0);

            comboBox1.ValueMember = "cod";
            comboBox1.DisplayMember = "periodo";
            comboBox1.DataSource = dtx;
        }
        DataTable dtx = new DataTable();
        void combo2(string com)
        {
            string ctc = "SELECT cod,modalidad FROM modalidad WHERE  periodo = " + com + " ORDER BY modalidad";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            dtx = new DataTable();
            DATA.Fill(dtx);
            DataRow nuevaFila = dtx.NewRow();
            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";
            dtx.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "modalidad";
            comboBox2.DataSource = dtx;
        }
        DataTable dt = new DataTable();
        void notas()
        {
            string ctc = "SELECT * FROM notas";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            dt = new DataTable();
            DATA.Fill(dt);
 
        }

        string[,] comen;
        int can;
        string profesor;
        void lista()
        {
            can = 0;
            comen = new string[5, 1000000];
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            string buscarcxx = "SELECT * FROM pre WHERE modalidad = " + comboBox2.SelectedValue.ToString() + " AND estatus = 'VERIFICADO';";
            OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
            OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
            OleDbDataReader leexx = comandoxx.ExecuteReader();
            while (leexx.Read())
            {

                
                comen[0, can] = leexx["estudiante"].ToString();
                string buscarcx1 = "SELECT * FROM estudiante WHERE ci = '" + leexx["estudiante"].ToString() + "';";
                OleDbDataAdapter datax1 = new OleDbDataAdapter(buscarcx1, conexion);
                OleDbCommand comandox1 = new OleDbCommand(buscarcx1, conexion);
                OleDbDataReader leex1 = comandox1.ExecuteReader();
                if (leex1.Read())
                {
                    comen[1, can] = leex1["nombre"].ToString();
                    comen[2, can] = leex1["apellido"].ToString();
                    comen[3, can] = leexx["nota"].ToString();
                    comen[4, can] = leexx["cod"].ToString();
                }
                can++;
                string buscarcx2 = "SELECT * FROM modalidad WHERE cod = " + comboBox2.SelectedValue.ToString() + ";";
                OleDbDataAdapter datax2 = new OleDbDataAdapter(buscarcx2, conexion);
                OleDbCommand comandox2 = new OleDbCommand(buscarcx2, conexion);
                OleDbDataReader leex2 = comandox2.ExecuteReader();
                if (leex2.Read())
                {
                    string buscarcx3 = "SELECT * FROM empleado WHERE ci = '" + leex2["profesor"].ToString() + "';";
                    OleDbDataAdapter datax3 = new OleDbDataAdapter(buscarcx3, conexion);
                    OleDbCommand comandox3 = new OleDbCommand(buscarcx3, conexion);
                    OleDbDataReader leex3 = comandox3.ExecuteReader();
                    if (leex3.Read())
                    {
                        profesor = leex3["nombre"].ToString() + " "+ leex3["apellido"].ToString();
                    }
                }
                

            } 
            if (can > 1)
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
               

                
                string ax, bx, cx, dx, ex;
                for (int i = 0; i < can; i++)
                {
                    for (int j = 0; j < can - 1; j++)
                    {
                        if (String.Compare(comen[2, j], comen[2, j + 1]) > 0)
                        {
                            ax = comen[0, j];
                            bx = comen[1, j];
                            cx = comen[2, j];
                            dx = comen[3, j];
                            ex = comen[4, j];
                            comen[0, j] = comen[0, j + 1];
                            comen[1, j] = comen[1, j + 1];
                            comen[2, j] = comen[2, j + 1];
                            comen[3, j] = comen[3, j + 1];
                            comen[4, j] = comen[4, j + 1];
                            comen[0, j + 1] = ax;
                            comen[1, j + 1] = bx;
                            comen[2, j + 1] = cx;
                            comen[3, j + 1] = dx;
                            comen[4, j + 1] = ex;
                        }
                    }
                }
            }

            if (can > 0)
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                for (int i = 0; i < can; i++)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells[0].Value = i + 1;
                    dataGridView1.Rows[i].Cells[1].Value = comen[0, i];
                    dataGridView1.Rows[i].Cells[3].Value = comen[1, i];
                    dataGridView1.Rows[i].Cells[2].Value = comen[2, i];
                    dataGridView1.Rows[i].Cells[4].Value = "";
                    dataGridView1.Rows[i].Cells[5].Value = comen[4, i];
                    DataGridViewComboBoxCell my_DGVCboColumn = dataGridView1.Rows[i].Cells[4] as DataGridViewComboBoxCell;
                    my_DGVCboColumn.DisplayMember = "cod";
                    my_DGVCboColumn.ValueMember = "valor";
                    my_DGVCboColumn.DataSource = dt.DefaultView;
                }
            }
            

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                dataGridView1.Rows[i].Cells[4].Value = comen[3, i];
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex != 0)
            {
                lista();
            }
            else
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 0)
            {
                combo2(comboBox1.SelectedValue.ToString());
                comboBox2.Enabled = true;
            }
            else
            {
                comboBox2.Enabled = false;
                dtx = new DataTable();
                dtx.Columns.Add("cod");
                dtx.Columns.Add("modalidad");
                DataRow nuevaFila = dtx.NewRow();
                nuevaFila["cod"] = 0;
                nuevaFila["modalidad"] = "Seleccione una disciplina";

                dtx.Rows.InsertAt(nuevaFila, 0);

                comboBox2.ValueMember = "cod";
                comboBox2.DisplayMember = "modalidad";
                comboBox2.DataSource = dtx;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    try
                    {
                        string insertar = "UPDATE  pre SET  nota = '" + dataGridView1.Rows[i].Cells[4].Value.ToString() + "' WHERE cod =  " + dataGridView1.Rows[i].Cells[5].Value.ToString() + "";
                        OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                        cmd.ExecuteNonQuery();
                    }
                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                MessageBox.Show("Los cambios fueron guardados con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                MessageBox.Show("Debe realizar una búsqueda para poder hacer uso de esta función.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #region crearPDF
        private void To_pdf()
        {
            Document doc = new Document(PageSize.LETTER, 20, 20, 20, 20);
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:";
            saveFileDialog1.Title = "Guardar Calificaciones Estudiante "+comboBox2.Text;
            saveFileDialog1.DefaultExt = "pdf";
            saveFileDialog1.Filter = "pdf Files (*.pdf)|*.pdf| All Files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.FileName = "CalificacionesEstudiante" + comboBox2.Text + DateTime.Now.ToString("dd-MMM-yyyy"); ;
            string filename = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filename = saveFileDialog1.FileName;
            }
            if (filename.Trim() != "")
            {
                FileStream file = new FileStream(filename,
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite,
                FileShare.ReadWrite);
                PdfWriter wri = PdfWriter.GetInstance(doc, file);
                wri.PageEvent = new PageEventHelper();
                //PdfWriter.GetInstance(doc, file);

                doc.Open();
                string remito = "Generado por: (" + estatica.Ci + ") " + estatica.Nombre + " " + estatica.Ape + ", con ENROLL ONE.";
                string envio = "Fecha: " + DateTime.Now.ToString("dd MMMM yyyy, hh:mm:ss tt");

                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                iTextSharp.text.Font _standardFont2 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font cabe = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                // Creamos una tabla que contendrá el nombre, apellido y país
                // de nuestros visitante.
                PdfPTable tb1 = new PdfPTable(2);
                tb1.WidthPercentage = 100;


                //agregando una imagen
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("logo.jpg");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_CENTER;
                float percentage = 0.0f;
                percentage = 1000 / imagen.Width;
                imagen.ScalePercent(percentage * 100);
                //PdfImage img = new PdfImage(iTextSharp.text.Image.
                PdfPCell ff2 = new PdfPCell(imagen);
                ff2.BorderWidth = 0;
                //tb1.AddCell(imagen);


                // Configuramos el título de las columnas de la tabla
                PdfPCell ff1 = new PdfPCell(new Phrase("\nRepública Bolivariana de Venezuela\nCentro de Adiestramiento Profesional para el Desarrollo de los Sistemas Informáticos y Afines \nTelf.: 0291-8966131/ 0414-7679330\nMaturín - Estado Monagas\nRIF: J-406338516", _standardFont2));
                ff1.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff1.BorderWidth = 0;


                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                tb1.AddCell(ff1);


                // Configuramos el título de las columnas de la tabla
                Chunk chunk = new Chunk("Calificaciones Estudiantes "+comboBox2.Text, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 20, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                ff2 = new PdfPCell(new Paragraph(chunk));
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff2.Padding = 4;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);

                ff2 = new PdfPCell(new Paragraph(envio, _standardFont2));
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);

                float[] medidaCeldas = { 0.25f, 2.85f };

                // ASIGNAS LAS MEDIDAS A LA TABLA (ANCHO)
                tb1.SetWidths(medidaCeldas);

                // Finalmente, añadimos la tabla al documento PDF y cerramos el documento
                doc.Add(tb1);

                //doc.Add(new Paragraph("                       "));

                doc.Add(new Paragraph(remito, _standardFont2));
                doc.Add(new Paragraph("Periodo académico: " + "(" + comboBox1.SelectedValue.ToString() + ") " + comboBox1.Text, _standardFont2));
                doc.Add(new Paragraph("Docente: " + profesor, _standardFont2));                
                doc.Add(new Paragraph("                       "));

                // Crea la imagen
                iTextSharp.text.Image imagen2 = iTextSharp.text.Image.GetInstance("fondo.jpg");
                // Cambia el tamaño de la imagen
                imagen2.ScaleToFit(700, 250);
                // Se indica que la imagen debe almacenarse como fondo
                imagen2.Alignment = iTextSharp.text.Image.UNDERLYING;
                // Coloca la imagen en una posición absoluta
                imagen2.SetAbsolutePosition(150, 280);
                // Imprime la imagen como fondo de página
                doc.Add(imagen2);

                if (dataGridView1.Rows.Count > 0)
                {
                    GenerarDocumento(doc);
                }



                doc.AddCreationDate();

                doc.Close();
                Process.Start(filename);//Esta parte se puede omitir, si solo se desea guardar el archivo, y que este no se ejecute al instante
            }

        }
        iTextSharp.text.Font _standardFont4 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        iTextSharp.text.Font _standardFont5 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        public void GenerarDocumento(Document document)
        {
            int i, j;
            PdfPTable datatable = new PdfPTable(dataGridView1.ColumnCount);
            datatable.DefaultCell.Padding = 3;
            float[] headerwidths = GetTamañoColumnas(dataGridView1);
            datatable.SetWidths(headerwidths);
            datatable.WidthPercentage = 100;
            datatable.DefaultCell.BorderWidth = 0.75f;
            datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            for (i = 0; i < dataGridView1.ColumnCount; i++)
            {
                datatable.AddCell(new Phrase(dataGridView1.Columns[i].HeaderText, _standardFont5));
            }
            datatable.HeaderRows = 1;
            datatable.DefaultCell.BorderWidth = 0.75f;
            for (i = 0; i < dataGridView1.Rows.Count; i++)
            {
                for (j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1[j, i].Value != null)
                    {
                        datatable.AddCell(new Phrase(dataGridView1[j, i].Value.ToString(), _standardFont4));//En esta parte, se esta agregando un renglon por cada registro en el datagrid
                    }
                }
                datatable.CompleteRow();
            }
            document.Add(datatable);
        }
        public float[] GetTamañoColumnas(DataGridView dg)
        {
            float[] values = new float[dg.ColumnCount];
            for (int i = 0; i < dg.ColumnCount-1; i++)
            {
                values[i] = (float)dg.Columns[i].Width;
            }
            return values;

        }
        #endregion

        private void button7_Click(object sender, EventArgs e)
        {
            To_pdf();
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}

