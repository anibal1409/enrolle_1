﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class actprecio : Form
    {
        public actprecio()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void actprecio_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
            comboBox2.SelectedIndex = 0;
        }
        void combo1()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' OR estatus = 'INACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox1.ValueMember = "cod";
            comboBox1.DisplayMember = "periodo";
            comboBox1.DataSource = dt;
        }
        string codp;
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 0 && comboBox2.SelectedIndex != 0)
            {
                string buscarc = "SELECT * FROM precio WHERE periodo = " + comboBox1.SelectedValue.ToString() + " AND servicio = '"+comboBox2.Text+"';";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read())
                {
                    textBox8.Text = lee["precio"].ToString();
                    codp =  lee["cod"].ToString();
                    textBox8.ReadOnly = false;
                    button3.Visible = true;
                }
                else
                {
                    MessageBox.Show("El servicio seleccionado no posee precio registrado en el periodo escogido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox8.ReadOnly = true;
                    button3.Visible = false;
                    codp = "";
                }
            }
            else
            {
                textBox8.Clear();
                codp = "";
            }

        }
        void clear()
        {
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
            textBox8.Clear();
            textBox8.ReadOnly = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clear();
        }

        public bool IsNumeric(string x)
        {

            double retNum;

            try
            {
                retNum = Double.Parse(x);
                return true;
            }
            catch
            {
                return false;

            }



        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 0 && comboBox2.SelectedIndex != 0 && textBox8.Text != "")
            {
                if (IsNumeric(textBox8.Text))
                {
                        try
                        {
                            string insertar = "UPDATE  precio SET  precio = '" + textBox8.Text + "' WHERE cod = " + codp + " ";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("El precio del servicio fue modificado con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            clear();
                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                            }

                        else
                        {
                            MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBox8.Focus();
                        }
                }
                else
                {
                    MessageBox.Show("Todos los campos son requeridos para realizar la modificación.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
        }

        private static void OnlyNumber(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
