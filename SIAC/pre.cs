﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class pre : Form
    {
        public pre()
        {
            InitializeComponent();
        }

        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void pre_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
            cargar();
        }
        DateTime dia = new DateTime();
        void cargar()
        {
            dia = DateTime.Now;
            textBox16.Text = dia.AddDays(10).ToString("dd-MM-yyyy");
        }
        DataTable dt = new DataTable();
        void combo1()
        {
            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dtx = new DataTable();
            dtx = new DataTable();
            DATA.Fill(dtx);

            DataRow nuevaFila = dtx.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dtx.Rows.InsertAt(nuevaFila, 0);

            comboBox1.ValueMember = "cod";
            comboBox1.DisplayMember = "periodo";
            comboBox1.DataSource = dtx;
        }

        void combo2(string com)
        {
            string ctc = "SELECT cod,modalidad FROM modalidad WHERE estatus = 'DISPONIBLE' AND periodo = "+com+" AND disponible DISCTIN '0' ORDER BY modalidad";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "modalidad";
            comboBox2.DataSource = dt;
        }
        string control = "", si= "0", cc = "";
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 0)
            {
                string buscarc = "SELECT * FROM control WHERE periodo = " + comboBox1.SelectedValue.ToString() + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                double xx = 0;
                if (lee.Read() == true)
                {
                    xx = Double.Parse(lee["control"].ToString()) + 1;
                    control = xx.ToString();
                    textBox1.Text = comboBox1.SelectedValue.ToString() + "," + control;
                    si = "0";
                    cc = lee["cod"].ToString();
                }
                else
                {
                    control = "1";
                    textBox1.Text = comboBox1.SelectedValue.ToString() + "," + control;
                    si = "1";
                }

                combo2(comboBox1.SelectedValue.ToString());
                /*if (ci != "")
                {*/
                    comboBox2.Enabled = true;
                //}

                string buscarcx = "SELECT * FROM precio WHERE periodo = " + comboBox1.SelectedValue.ToString() + " AND servicio= 'Preinscripción';";
                OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read() == true)
                {
                    textBox2.Text = leex["precio"].ToString();
                    comboBox2.Enabled = true;
                }
                else
                {
                    MessageBox.Show("El periodo académico seleccionado no tiene precio definido para este servicio, comuníquese con el Coordinador Académico.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    comboBox2.Enabled = false;
                    textBox1.Clear();
                    textBox2.Clear();
                }

            }
            else
            {
                textBox1.Clear();
                textBox2.Clear();
                clearc();
                comboBox2.Enabled = false;
                dt = new DataTable();
                dt.Columns.Add("cod");
                dt.Columns.Add("modalidad");
                DataRow nuevaFila = dt.NewRow(); 
                nuevaFila["cod"] = 0;
                nuevaFila["modalidad"] = "Seleccione una disciplina";

                dt.Rows.InsertAt(nuevaFila, 0);

                comboBox2.ValueMember = "cod";
                comboBox2.DisplayMember = "modalidad";
                comboBox2.DataSource = dt;
            }
            
        }
        string ci = "";
        DateTime var;
        void buscar()
        {
            if (textBox7.Text != "")
            {
                ci = "";
                var cadena = textBox7.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox7.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        if (lee["estatus"].ToString() == "ACTIVO" || lee["estatus"].ToString() == "INACTIVO")
                        {
                            ci = textBox7.Text;
                            textBox6.Text = lee["nombre"].ToString();
                            textBox3.Text = lee["apellido"].ToString();
                            dateTimePicker1.Text = lee["fecha"].ToString();
                            textBox4.Text = lee["direc"].ToString();
                            textBox5.Text = lee["tlf"].ToString();
                            var = dateTimePicker1.Value.Date;
                            int edad = DateTime.Today.AddTicks(-var.Ticks).Year - 1;
                            textBox99.Text = edad.ToString();
                            if (ci != "" && comboBox1.SelectedIndex != 0)
                            {

                                comboBox2.Enabled = true;
                            }

                        }
                        else
                        {
                            MessageBox.Show("El estudiante con el documento de identidad '"+textBox7.Text+"' se encuentra actualmente suspendido. Comuníquese con el Coordinador Académico.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        
                    }
                    else
                    {
                        if (MessageBox.Show("El documento de identidad '" + textBox7.Text + "' no se encuentra registrado en el sistema. ¿Desea registrarlo ahora?", "Información", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            button1.Visible = false;
                            button2.Visible = true;
                            textBox3.ReadOnly = false;
                            textBox4.ReadOnly = false;
                            textBox5.ReadOnly = false;
                            textBox6.ReadOnly = false;
                            dateTimePicker1.Enabled = true;
                            ci = "";
                            textBox3.Clear();
                            textBox4.Clear();
                            textBox5.Clear();
                            dateTimePicker1.Value = DateTime.Now;
                            textBox6.Clear();
                            textBox99.Clear();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad válido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscar();
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox7_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buscar();
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker1.Text != DateTime.Now.ToString("dd-MM-yyyy"))
            {
                var = dateTimePicker1.Value.Date;
                int edad = DateTime.Today.AddTicks(-var.Ticks).Year - 1;
                textBox99.Text = edad.ToString();
            }
        }

        void cleare()
        {
            ocultar();
            ci = "";
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            dateTimePicker1.Value = DateTime.Now;
            textBox6 .Clear();
            textBox7.Clear();
            textBox99.Clear();
            clearc();
            
        }

        void clearc()
        {
            textBox8.Clear();
            textBox9.Clear();
            textBox10.Clear();
            textBox11.Clear();
            textBox12.Clear();
            textBox13.Clear();
            textBox14.Clear();
            textBox15.Clear();
            ocultar();

        }

        void ocultar()
        {
            button1.Visible = true;
            button2.Visible = false;
            textBox3.ReadOnly = true;
            textBox4.ReadOnly = true;
            textBox5.ReadOnly = true;
            textBox6.ReadOnly = true;
            dateTimePicker1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox7.Text != "" && textBox6.Text != "" && textBox3.Text != "" && textBox5.Text != "" && textBox4.Text != "" && textBox99.Text != "" && dateTimePicker1.Text != DateTime.Now.ToString("dd-MM-yyyy"))
            {
                var cadena = textBox7.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox7.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        MessageBox.Show("El documento de identidad  '" + textBox7.Text + "' ya se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox7.Focus();
                    }
                    else
                    {

                        try
                        {
                            string insertar = "INSERT INTO estudiante VALUES ('" + textBox7.Text + "','" + textBox6.Text + "','" + textBox3.Text + "','" + dateTimePicker1.Text + "','" + textBox4.Text + "','" + textBox5.Text + "', 'ACTIVO', '')";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            ci = textBox7.Text;
                            MessageBox.Show("El estudiante se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (ci != "" && comboBox1.SelectedIndex != 0)
                            {

                                comboBox2.Enabled = true;
                            }
                            button1.Visible = true;
                            button2.Visible = false;
                            textBox3.ReadOnly = true;
                            textBox4.ReadOnly = true;
                            textBox5.ReadOnly = true;
                            textBox6.ReadOnly = true;
                            dateTimePicker1.Enabled = false;

                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            cleare();
            comboBox2.Enabled = false;
        }
        Double d = 0;
        string dis = "";
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedValue.ToString() != "0")
            {
                string buscarc = "SELECT * FROM modalidad WHERE cod = " + comboBox2.SelectedValue.ToString() + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    textBox8.Text = lee["dia"].ToString();
                    textBox9.Text = lee["desde"].ToString();
                    textBox10.Text = lee["hasta"].ToString();
                    textBox12.Text = lee["inicio"].ToString();
                    textBox11.Text = lee["fin"].ToString();
                    textBox13.Text = lee["precio"].ToString();
                    textBox14.Text = lee["disponible"].ToString();
                    d = Double.Parse(lee["disponible"].ToString()) -1;
                    dis = d.ToString();
                    string buscarcx = "SELECT * FROM empleado WHERE ci = '" + lee["profesor"].ToString() +"';";
                    OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                    OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read() == true)
                    {
                        textBox15.Text = leex["nombre"].ToString() + " " + leex["apellido"].ToString();
                    }

                }
            }
            else
            {
                clearc();
            }
        }


        void clear()
        {
            clearc();
            cleare();
            comboBox2.Enabled = false;
            comboBox1.Enabled = true;
            textBox1.Clear();
            textBox2.Clear();
            textBox16.Clear();
            cargar();
            comboBox1.SelectedIndex = 0;
            d = 0;
            dis = "";
            control = "";
            cc = "";
            si = "0";
            textBox7.ReadOnly = false;
            button1.Enabled = true;
            button4.Enabled = true;
            button6.Visible = true;
            button5.Visible = true;
            button7.Visible = false;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            clear();
        }
        DateTime a = new DateTime();
        void prei()
        {
            if (textBox1.Text != "" && comboBox1.SelectedIndex != 0 && comboBox2.SelectedIndex != 0 && ci != "" && textBox5.Text != "" && textBox9.Text != "")
            {
                if (textBox14.Text == "0")
                {
                    MessageBox.Show("La disciplina '" + comboBox2.Text + "' no posee capacidad para agregar a otro estudiante.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {
                        string buscarc = "SELECT * FROM pre WHERE estudiante = '" + ci + "' AND periodo = " + comboBox1.SelectedValue.ToString() + " AND modalidad = " + comboBox2.SelectedValue.ToString() + ";";
                        OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                        OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                        OleDbDataReader lee = comando.ExecuteReader();
                        if (lee.Read() == true)
                        {
                            MessageBox.Show("El estudiante '" + ci + "' ya se encuentra preinscrito en la disciplina '" + comboBox2.Text + "', periodo académico " + comboBox1.Text + ".", "Información", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        else
                        {
                            try
                            {
                                a = DateTime.Now;
                                string insertar = "INSERT INTO pre VALUES ('" + textBox1.Text + "','" + textBox2.Text + "', '" + ci + "', " + comboBox1.SelectedValue.ToString() + "," + comboBox2.SelectedValue.ToString() + ", '" + textBox13.Text + "', '" + a.ToString("dd-MM-yyyy") + "', '" + a.ToString("hh:mm:ss") + "', 'PENDIENTE', '" + estatica.Ci + "', '00')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();

                                if (si == "0")
                                {
                                    try
                                    {
                                        string insertaru = "UPDATE  control SET control = " + control + " WHERE cod = " + cc + " ";
                                        OleDbCommand cmdu = new OleDbCommand(insertaru, conexion);
                                        cmdu.ExecuteNonQuery();
                                    }
                                    catch (DBConcurrencyException ex)
                                    {
                                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        string insertarx = "INSERT INTO control (periodo, control) VALUES (" + comboBox1.SelectedValue.ToString() + ", " + control + ")";
                                        OleDbCommand cmdx = new OleDbCommand(insertarx, conexion);
                                        cmdx.ExecuteNonQuery();
                                    }
                                    catch (DBConcurrencyException ex)
                                    {
                                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }

                                }
                                try
                                {
                                    string insertarm = "UPDATE  modalidad SET disponible = '" + dis + "' WHERE cod = " + comboBox2.SelectedValue.ToString() + "";
                                    OleDbCommand cmdm = new OleDbCommand(insertarm, conexion);
                                    cmdm.ExecuteNonQuery();
                                }
                                catch (DBConcurrencyException ex)
                                {
                                    MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }


                                MessageBox.Show("La preinscripción se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //clear();
                                button5.Visible = false;
                                button6.Visible = false;
                                button7.Visible = true;
                                comboBox1.Enabled = false;
                                comboBox2.Enabled = false;
                                textBox7.ReadOnly = true;
                                button1.Enabled = false;
                                button4.Enabled = false;
                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            prei();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            prei();
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
