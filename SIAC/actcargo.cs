﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class actcargo : Form
    {
        public actcargo()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void actcargo_Load(object sender, EventArgs e)
        {
            conexion.Open();
            bus();
            if (estatica.Nivel == "Administrador")
            {
                menuStrip1.Visible = true;
            }
            if (estatica.Nivel == "Asesor de Ventas")
            {
                //menuStrip4.Visible = true;
            }
            if (estatica.Nivel == "Coordinador Académico")
            {
                //menuStrip2.Visible = true;
            }
            if (estatica.Nivel == "Coordinador de Personal")
            {
                menuStrip3.Visible = true;
            }
        }
        string[,] comen;
        int can;
        void bus()
        {
            can = 0;
            comen = new string[2, 1000000];
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            string buscarc = "SELECT * FROM cargo WHERE cod > 0  ORDER BY cargo;";
            OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
            OleDbCommand comando = new OleDbCommand(buscarc, conexion);
            OleDbDataReader lee = comando.ExecuteReader();
            while (lee.Read())
            {
                comen[0, can] = lee["cod"].ToString();
                comen[1, can] = lee["cargo"].ToString();
                can++;
            }
            if (can > 0)
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                for (int i = 0; i < can; i++)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells[0].Value = comen[0, i];
                    dataGridView1.Rows[i].Cells[1].Value = comen[1, i];

                }
            }


        }
        void ing()
        {
            try
            {
                string insertar = "UPDATE  cargo SET  cargo = '" + textBox1.Text + "' WHERE cod = " + cargo + " ";
                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                cmd.ExecuteNonQuery();
                MessageBox.Show("El cargo fue modificado  con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBox1.Clear();
                bus();
            }

            catch (DBConcurrencyException ex)
            {
                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        string cargo;
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            cargo = "";
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            cargo = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox1.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cargo != "" && textBox1.Text != "")
            {
                string buscarc = "SELECT * FROM cargo WHERE cargo LIKE '" + textBox1.Text + "';";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    if (lee["cod"].ToString() == cargo)
                    {
                        ing();
                    }
                    else
                    {
                        MessageBox.Show("El nombre de cargo que intenta actualizar ya se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox1.Focus();
                    }
                    
                }
                else
                {
                    ing();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un cargo para poder utilizar esta función.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
