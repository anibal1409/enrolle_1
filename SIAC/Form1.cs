﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void Form1_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo();
        }

        public struct Data
        {
            int Cte_ID { get; set; }
            string Cte_RazonSocial { get; set; }
        }

        void combo()
        {

            DataTable tc = new DataTable();
            string ctc = "select * from cargo WHERE cod > 0 order by cargo ";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["cargo"] = "Seleccione un cargo";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox1.ValueMember = "cod";
            comboBox1.DisplayMember = "cargo";
            comboBox1.DataSource = dt;

        }

        private static void OnlyNumber(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            comboBox1.SelectedIndex = 0;
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
            textBox1.Focus();


        }
        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "" && textBox6.Text != "" && textBox7.Text != "" && comboBox1.Text != "Seleccione un cargo" && dateTimePicker1.Text != DateTime.Now.ToString("dd-MM-yyyy"))
            {
                var cadena = textBox1.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM empleado WHERE ci = '" + textBox1.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        MessageBox.Show("El documento de identidad  '" + textBox1.Text + "' ya se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox1.Focus();
                    }
                    else
                    {

                        try
                        {
                            string insertar = "INSERT INTO empleado VALUES ('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + dateTimePicker1.Text + "','" + textBox5.Text + "','" + textBox4.Text + "', '" + textBox6.Text + "', " + comboBox1.SelectedValue.ToString() + ", '" + dateTimePicker2.Text + "', '" + textBox7.Text + "', 'ACTIVO')";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            clear();
                            MessageBox.Show("El empleado se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

                
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void periodoAcadémicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            periodo a = new periodo();
            a.Show();
            this.Close();
        }

    }
}
