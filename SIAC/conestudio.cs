﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Drawing.Printing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;
using System.Globalization;

namespace SIAC
{
    public partial class conestudio : Form
    {
        public conestudio()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");

        private void conestudio_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
        }
        void combo1()
        {
            string ctc = "SELECT * FROM periodo ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dtx = new DataTable();
            dtx = new DataTable();
            DATA.Fill(dtx);

            DataRow nuevaFila = dtx.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dtx.Rows.InsertAt(nuevaFila, 0);

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "periodo";
            comboBox3.DataSource = dtx;
        }
        string cod = "";
        DataTable dt = new DataTable();
        void buscar()
        {
            if (textBox3.Text != "")
            {
                cod = "";
                string buscarc = "SELECT * FROM estudio WHERE cod = '" + textBox3.Text + "';";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    cod = textBox3.Text;

                    textBox2.Text = lee["precio"].ToString();
                    textBox7.Text = lee["estudiante"].ToString();
                    buscare();
                    string ctc = "SELECT * FROM modalidad WHERE  cod = '" + lee["modalidad"].ToString() + "'";
                    OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
                    OleDbCommand comandox = new OleDbCommand(ctc, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read() == true)
                    {
                        comboBox3.SelectedValue = leex["periodo"].ToString();
                    }
                    DATA.Fill(dt);
                    /*DataRow nuevaFila = dt.NewRow();

                    nuevaFila["cod"] = 0;
                    nuevaFila["modalidad"] = "Seleccione una disciplina";

                    dt.Rows.InsertAt(nuevaFila, 0);*/

                    comboBox2.ValueMember = "cod";
                    comboBox2.DisplayMember = "modalidad";
                    comboBox2.DataSource = dt;
                    bm();
                    textBox5.Text = lee["suscribe"].ToString();
                    combo3();
                    cie = lee["empleado"].ToString();
                    comboBox1.Text = lee["estatus"].ToString();
                    string buscarcxx = "SELECT nombre, apellido FROM empleado WHERE  ci = '" + lee["empleado"].ToString() + "'";
                    OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
                    OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
                    OleDbDataReader leexx = comandoxx.ExecuteReader();
                    if (leexx.Read())
                    {
                        textBox14.Text = "(" + lee["empleado"].ToString() + ") " + leexx["nombre"].ToString() + " " + leexx["apellido"].ToString();
                    }

                    button8.Visible = true;

                }
                else
                {
                    MessageBox.Show("El código ingresado no pertene a una constancia de estudio registrada en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cod = "";
                    button8.Visible = false;

                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código para poder realizar la busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        string cie = "";
        void combo3()
        {
            if (textBox5.Text != "")
            {

                cie = "";
                var cadena = textBox5.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    cie = textBox5.Text;
                    string buscarcx = "SELECT nombre, apellido, cargo, estatus FROM empleado WHERE  ci = '" + cie + "'";
                    OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                    OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read())
                    {
                        if (leex["estatus"].ToString() == "ACTIVO")
                        {
                            textBox13.Text = leex["nombre"].ToString() + " " + leex["apellido"].ToString();

                            string buscarc = "SELECT cargo FROM cargo WHERE cod = '" + leex["cargo"].ToString() + "'";
                            OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                            OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                            OleDbDataReader lee = comando.ExecuteReader();
                            if (lee.Read())
                            {
                                textBox4.Text = lee["cargo"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("El documento de identidad '" + textBox5.Text + "' no se encuentra activo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            textBox5.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad '" + textBox5.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox5.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox5.Focus();
                }


            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para poder realizar una busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox5.Focus();
            }

        }
        string ci = "";
        void buscare()
        {
            if (textBox2.Text != "")
            {
                if (textBox7.Text != "")
                {
                    ci = "";
                    var cadena = textBox7.Text;
                    if (cadena.Length >= 7 && cadena.Length <= 8)
                    {
                        string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox7.Text + "';";
                        OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                        OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                        OleDbDataReader lee = comando.ExecuteReader();
                        if (lee.Read() == true)
                        {
                            if (lee["estatus"].ToString() == "ACTIVO")
                            {
                                ci = textBox7.Text;
                                textBox6.Text = lee["nombre"].ToString() + " " + lee["apellido"].ToString();
                                //combo2();
                            }
                            else
                            {
                                MessageBox.Show("El estudiante con el documento de identidad '" + textBox7.Text + "' no se encuentra activo actualmente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                textBox7.Focus();
                            }

                        }
                        else
                        {
                            MessageBox.Show("El documento de identidad '" + textBox7.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            textBox7.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad para poder realizar la busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un periodo académico para poder hacer uso de esta función", "Información)", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        void bm()
        {
            if (comboBox2.SelectedValue.ToString() != "0")
            {
                string buscarc = "SELECT * FROM modalidad WHERE cod = '" + comboBox2.SelectedValue.ToString() + "';";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    textBox8.Text = lee["dia"].ToString();
                    textBox9.Text = lee["desde"].ToString();
                    textBox10.Text = lee["hasta"].ToString();
                    textBox12.Text = lee["inicio"].ToString();
                    textBox11.Text = lee["fin"].ToString();
                    textBox1.Text = lee["semanas"].ToString();

                }
            }
            else
            {
                //clearc();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscar();
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buscar();
            }
        }

        #region crearPDF
        private void To_pdf()
        {
            string ab, bc;
            ab = textBox6.Text;
            bc = textBox13.Text;
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            Document doc = new Document(PageSize.LETTER, 30, 30, 30, 30);
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:";
            saveFileDialog1.Title = "Guardar Constancia de Estudios ";
            saveFileDialog1.DefaultExt = "pdf";
            saveFileDialog1.Filter = "pdf Files (*.pdf)|*.pdf| All Files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.FileName = "ConstanciaEstudio" + textBox7.Text+ myTI.ToTitleCase(ab) + DateTime.Now.ToString("dd-MMM-yyyy"); ;
            string filename = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filename = saveFileDialog1.FileName;
            }
            if (filename.Trim() != "")
            {
                FileStream file = new FileStream(filename,
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite,
                FileShare.ReadWrite);
                PdfWriter wri = PdfWriter.GetInstance(doc, file);
                wri.PageEvent = new Class1();
                wri.PageEvent = new menbrete();
                //PdfWriter.GetInstance(doc, file);

                doc.Open();
                string remito = "Generado por: (" + estatica.Ci + ") " + estatica.Nombre + " " + estatica.Ape + ", con ENROLL ONE.";
                string envio = "Fecha: " + DateTime.Now.ToString("dd MMMM yyyy, hh:mm:ss tt");

                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _standardFont22 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _standardFont2 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _standardFont23 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font cabe = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                // Creamos una tabla que contendrá el nombre, apellido y país
                // de nuestros visitante.
                PdfPTable tb1 = new PdfPTable(2);
                tb1.WidthPercentage = 100;


                //agregando una imagen
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("logo.jpg");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_CENTER;
                float percentage = 0.0f;
                percentage = 100 / imagen.Width;
                imagen.ScalePercent(percentage * 100);
                imagen.SetAbsolutePosition(20, 730);
                doc.Add(imagen);
                //PdfImage img = new PdfImage(iTextSharp.text.Image.
                PdfPCell ff2 = new PdfPCell();
                /*ff2.BorderWidth = 0;
                ff2.Colspan = 2;*/
                //tb1.AddCell(imagen);


                // Configuramos el título de las columnas de la tabla
                /*PdfPCell ff1 = new PdfPCell(new Phrase("\nRepública Bolivariana de Venezuela\nCentro de Adiestramiento Profesional para el Desarrollo de los Sistemas Informáticos y Afines \nTelf.: 0291-8966131/ 0414-7679330\nMaturín - Estado Monagas\nRIF: J-406338516", _standardFont23));
                ff1.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff1.BorderWidth = 0;*/


                // Añadimos las celdas a la tabla
                //tb1.AddCell(ff2);
                //tb1.AddCell(ff1);


                // Configuramos el título de las columnas de la tabla
                doc.Add(new Paragraph("                       "));
                doc.Add(new Paragraph("                       "));

                Chunk chunk = new Chunk("\n\n\n\nCONSTANCIA DE ESTUDIO.", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                ff2 = new PdfPCell(new Paragraph(chunk));
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff2.Padding = 4;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                Phrase p2 = new Phrase();

                ff2 = new PdfPCell();
                Chunk c1 = new Chunk("\n\n          Quien suscribe, la Directora DEL CENTRO DE ADIESTRAMIENTO PROFESIONAL PARA EL DESARROLLO DE LOS SISTEMAS INFORMÁTICOS Y AFINES  (CAPEDSIA), Hace constar que él o (la) estudiante  ", _standardFont2);
                ab = ab.ToLower();
                Chunk c2 = new Chunk(myTI.ToTitleCase(ab), _standardFont22);
                Chunk c3 = new Chunk(".   De la Cédula de Identidad: Nº  ", _standardFont2);
                Chunk c4 = new Chunk(textBox7.Text, _standardFont22);
                Chunk c5 = new Chunk(", presta servicios en el cargo de ", _standardFont2);
                Chunk c6 = new Chunk(textBox1.Text.ToUpper(), _standardFont22);
                Chunk c7 = new Chunk(", siendo su  salario básico devengado " + textBox2.Text + " (Bs. ", _standardFont2);
                Chunk c8 = new Chunk(textBox9.Text, _standardFont22);
                string fe = "). \n\nConstancia que se expide a petición de la parte interesada en la ciudad Maturín, ";

                if (DateTime.Now.ToString("dd") == "01")
                {
                    fe = fe + "el día 01 del mes " + DateTime.Now.ToString("MMMM") + " de " + DateTime.Now.ToString("yyyy") + ".";
                }
                else
                {
                    fe = fe + "a los " + DateTime.Now.ToString("dd") + " días del mes " + DateTime.Now.ToString("MMMM") + " de " + DateTime.Now.ToString("yyyy") + ".";
                }
                Chunk c9 = new Chunk(fe, _standardFont2);
                p2.Add(c1);
                p2.Add(c2);
                p2.Add(c3);
                p2.Add(c4);
                p2.Add(c5);
                p2.Add(c6);
                p2.Add(c7);
                p2.Add(c8);
                p2.Add(c9);
                Paragraph xa = new Paragraph(p2);
                xa.SetLeading(20, 0);
                xa.Alignment = Element.ALIGN_JUSTIFIED;
                ff2.AddElement(xa);
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);

                Phrase p1 = new Phrase();

                ff2 = new PdfPCell();
                bc = bc.ToLower();
                Chunk c1x = new Chunk("\n\n\n\n\n________________________________\n" + myTI.ToTitleCase(bc), _standardFont2);
                Chunk c3x = new Chunk("\n" + textBox4.Text.ToUpper(), _standardFont22);
                p1.Add(c1x);
                p1.Add(c3x);
                Paragraph x1 = new Paragraph(p1);
                x1.SetLeading(20, 0);
                x1.Alignment = Element.ALIGN_CENTER;
                ff2.AddElement(x1);
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                float[] medidaCeldas = { 0.25f, 2.85f };

                // ASIGNAS LAS MEDIDAS A LA TABLA (ANCHO)
                tb1.SetWidths(medidaCeldas);

                // Finalmente, añadimos la tabla al documento PDF y cerramos el documento
                doc.Add(tb1);

                //doc.Add(new Paragraph("                       "));


                doc.Add(new Paragraph("                       "));

                // Crea la imagen
                iTextSharp.text.Image imagen2 = iTextSharp.text.Image.GetInstance("fondo.jpg");
                // Cambia el tamaño de la imagen
                imagen2.ScaleToFit(700, 250);
                // Se indica que la imagen debe almacenarse como fondo
                imagen2.Alignment = iTextSharp.text.Image.UNDERLYING;
                // Coloca la imagen en una posición absoluta
                imagen2.SetAbsolutePosition(150, 280);
                // Imprime la imagen como fondo de página
                doc.Add(imagen2);



                doc.AddCreationDate();

                doc.Close();
                Process.Start(filename);//Esta parte se puede omitir, si solo se desea guardar el archivo, y que este no se ejecute al instante
            }

        }
        iTextSharp.text.Font _standardFont4 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        iTextSharp.text.Font _standardFont5 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);

        public float[] GetTamañoColumnas(DataGridView dg)
        {
            float[] values = new float[dg.ColumnCount];
            for (int i = 0; i < dg.ColumnCount; i++)
            {
                values[i] = (float)dg.Columns[i].Width;
            }
            return values;

        }
        #endregion

        private void button8_Click(object sender, EventArgs e)
        {
            To_pdf();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
