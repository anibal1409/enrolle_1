﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace SIAC
{
    class menbrete : PdfPageEventHelper
    {
        PdfContentByte cb;
        PdfTemplate template;


        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            cb = writer.DirectContent;
            template = cb.CreateTemplate(50, 50);
        }

        public override void OnEndPage(PdfWriter writer, Document doc)
        {

            BaseColor grey = new BaseColor(0, 0, 0);
            iTextSharp.text.Font font = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, grey);
            //tbl footer
            PdfPTable footerTbl = new PdfPTable(1);
            footerTbl.TotalWidth = doc.PageSize.Width;



            //numero de la page

            Chunk myFooter = new Chunk("\nRepública Bolivariana de Venezuela\nCentro de Adiestramiento Profesional para el Desarrollo de los Sistemas Informáticos y Afines \nTelf.: 0291-8966131/ 0414-7679330\nMaturín - Estado Monagas\nRIF: J-406338516", FontFactory.GetFont(FontFactory.TIMES_ROMAN, 9, grey));
            
            PdfPCell footer = new PdfPCell(new Phrase(myFooter));
            footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
            footer.HorizontalAlignment = Element.ALIGN_CENTER;
            footerTbl.AddCell(footer);


            footerTbl.WriteSelectedRows(0, -1, 0, (doc.Top +0), writer.DirectContent);
        }




        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

        }
    }
}
