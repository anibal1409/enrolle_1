﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class conprecio : Form
    {
        public conprecio()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void conprecio_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
            comboBox2.SelectedIndex = 0;
            if (estatica.Nivel == "Administrador")
            {
                menuStrip1.Visible = true;
            }
            if (estatica.Nivel == "Asesor de Ventas")
            {
                menuStrip4.Visible = true;
            }
            if (estatica.Nivel == "Coordinador Académico")
            {
                menuStrip2.Visible = true;
            }
            if (estatica.Nivel == "Coordinador de Personal")
            {
                //menuStrip3.Visible = true;
            }
        }
        void combo1()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' OR estatus = 'INACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox1.ValueMember = "cod";
            comboBox1.DisplayMember = "periodo";
            comboBox1.DataSource = dt;
        }
        string codp;
        void clear()
        {
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
            textBox8.Clear();
            textBox8.ReadOnly = true;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 0 && comboBox2.SelectedIndex != 0)
            {
                string buscarc = "SELECT * FROM precio WHERE periodo = " + comboBox1.SelectedValue.ToString() + " AND servicio = '" + comboBox2.Text + "';";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read())
                {
                    textBox8.Text = lee["precio"].ToString();
                    codp = lee["cod"].ToString();
                }
                else
                {
                    MessageBox.Show("El servicio seleccionado no posee precio registrado en el periodo escogido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox8.ReadOnly = true;
                    codp = "";
                }
            }
            else
            {
                textBox8.Clear();
                codp = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clear();
        }
    }
}
