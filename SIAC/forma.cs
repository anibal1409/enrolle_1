﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class forma : Form
    {
        public forma()
        {
            InitializeComponent();
        }

        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void forma_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
            combo2();
            comboBox4.SelectedIndex = 0;
            comboBox5.SelectedIndex = 0;
            comboBox6.SelectedIndex = 0;
            comboBox1.SelectedIndex = 0;
        }

        public string obser, usu;
        void cargar()
        {
            string buscarcx = "SELECT TOP 1 * FROM pago ORDER BY cod DESC;";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                double ccod;
                ccod = Double.Parse(leex["cod"].ToString()) + 1;
                textBox1.Text = ccod.ToString();
            }
            else
            {
                textBox1.Text = "1";
            }
        }
        void ocultar()
        {
            textBox2.Visible = false;
            textBox3.Visible = false;
            label2.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            dateTimePicker1.Visible = false;
            comboBox2.Visible = false;
            comboBox3.Visible = false;
            textBox2.Clear();
            textBox3.Clear();
            dateTimePicker1.Value = DateTime.Now;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            label17.Visible = false;
            textBox4.Visible = false;
        }

        void combo1()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Cliente' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "banco";
            comboBox2.DataSource = dt;
        }

        void combo2()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Instituto' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "banco";
            comboBox3.DataSource = dt;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text != "")
            {
                if (comboBox1.Text == "Efectivo")
                {
                    ocultar();
                    textBox2.Visible = true;
                    label2.Visible = true;
                    dateTimePicker1.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = false;
                    if (ep == 1)
                    {

                        label17.Visible = true;
                        textBox4.Visible = true;
                    }


                }
                if (comboBox1.Text == "Exoneración")
                {
                    ocultar();
                    dateTimePicker1.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = false;
                    textBox4.Visible = false;
                    label17.Visible = false;
                }
                if (comboBox1.Text == "Deposito")
                {
                    ocultar();
                    dateTimePicker1.Visible = true;
                    textBox2.Visible = true;
                    label2.Visible = true;
                    textBox3.Visible = true;
                    label6.Visible = true;
                    comboBox3.Visible = true;
                    label7.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = true;
                    if (ep == 1)
                    {

                        label17.Visible = true;
                        textBox4.Visible = true;
                    }

                }
                if (comboBox1.Text == "Trans. Bancaria" || comboBox1.Text == "Pago Rápido" || comboBox1.Text == "Otro")
                {
                    ocultar();
                    dateTimePicker1.Visible = true;
                    textBox2.Visible = true;
                    label2.Visible = true;
                    textBox3.Visible = true;
                    label6.Visible = true;
                    comboBox2.Visible = true;
                    label5.Visible = true;
                    comboBox3.Visible = true;
                    label7.Visible = true;
                    dateTimePicker1.Value = DateTime.Now;
                    dateTimePicker1.Enabled = true;
                    if (ep == 1)
                    {

                        label17.Visible = true;
                        textBox4.Visible = true;
                    }

                }
            }
            else
            {
                ocultar();
            }
        }

        int ep = 0;
        private void button7_Click(object sender, EventArgs e)
        {
            ocultar();
            ep = 0;
            label4.Visible = true;
            comboBox1.Visible = true;
            button7.Visible = false;
            button5.Visible = false;
            cargar();
            textBox1.ReadOnly = true;
            button4.Visible = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            ocultar();
            label4.Visible = false;
            comboBox1.Visible = false;
            button7.Visible = true;
            button5.Visible = true;
            textBox1.ReadOnly = false;
            desbloquear();
            comboBox1.SelectedIndex = 0;
            ep = 0;
            pago = "";
        }
        public bool IsNumeric(string x)
        {

            double retNum;

            try
            {
                retNum = Double.Parse(x);
                return true;
            }
            catch
            {
                return false;

            }



        }

        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            comboBox1.Text = "";
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            dateTimePicker1.Value = DateTime.Now;
            obser = "";
            usu = "";
            cargar();
            estatica.Usu = "";
            estatica.Obser = "";


        }
        string pago, estado;
        private void button4_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (comboBox1.Text == "Efectivo")
            {
                if (textBox2.Text != "")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        try
                        {
                            string insertar = "INSERT INTO pago  VALUES ('" + textBox1.Text + "','" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '0', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', 'Sin Observaciones', '" + estatica.Ci + "')";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            button4.Visible = false;
                            textBox4.Visible = true;
                            label17.Visible = true;
                            textBox4.Text = textBox2.Text;
                            pago = textBox1.Text;
                            estado = "PENDIENTE";

                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es valido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Ingrese el monto para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (comboBox1.Text == "Exoneración")
            {
                exo a = new exo();
                a.ShowDialog();

                if (a.DialogResult == DialogResult.Yes)
                {


                    try
                    {
                        string insertar = "INSERT INTO pago  VALUES ('" + textBox1.Text + "','" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '0', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '" + comboBox1.Text + "', '0', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', '" + estatica.Obser + "', '" + estatica.Usu + "')";
                        OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        button4.Visible = false;
                        textBox4.Text = "0";
                        pago = textBox1.Text;
                        estado = "PENDIENTE";
                    }

                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            if (comboBox1.Text == "Deposito")
            {
                if (textBox2.Text != "" && textBox3.Text != "" && comboBox3.Text != "Seleccione un banco")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        var cadena = textBox3.Text;
                        if (cadena.Length >= 4)
                        {
                            try
                            {
                                string insertar = "INSERT INTO pago  VALUES ('" + textBox1.Text + "','" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + comboBox1.Text + "', '" + comboBox3.SelectedValue.ToString() + "', '" + textBox2.Text + "', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', 'Sin Observaciones', '" + estatica.Ci + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                clear();
                                MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Debe ingresar al menos los ultimos cuatro dígitos del número de referencia.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBox3.Focus();
                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es valido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (comboBox1.Text == "Trans. Bancaria" || comboBox1.Text == "Pago Rápido")
            {
                if (textBox2.Text != "" && textBox3.Text != "" && comboBox2.Text != "Seleccione un banco" && comboBox3.Text != "Seleccione un banco")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        var cadena = textBox3.Text;
                        if (cadena.Length >= 4)
                        {
                            try
                            {
                                string insertar = "INSERT INTO pago  VALUES ('" + textBox1.Text + "','" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + comboBox2.SelectedValue.ToString() + "', '" + comboBox3.SelectedValue.ToString() + "', '" + textBox2.Text + "', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', 'Sin Observaciones', '" + estatica.Ci + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                button4.Visible = false;
                                label17.Visible = true;
                                textBox4.Text = textBox2.Text;
                                pago = textBox1.Text;
                                estado = "PENDIENTE";
                                textBox4.Visible = true;
                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Debe ingresar al menos los ultimos cuatro dígitos del número de referencia.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBox3.Focus();
                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es valido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }



            if (comboBox1.Text == "Otro")
            {
                if (textBox2.Text != "")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        exo a = new exo();
                        a.ShowDialog();

                        if (a.DialogResult == DialogResult.Yes)
                        {


                            try
                            {
                                string insertar = "INSERT INTO pago  VALUES ('" + textBox1.Text + "','" + comboBox1.Text + "', '" + dateTimePicker1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + comboBox2.SelectedValue.ToString() + "', '" + comboBox3.SelectedValue.ToString() + "', '" + textBox2.Text + "', 'PENDIENTE', '" + DateTime.Now.ToShortDateString() + "', '" + estatica.Obser + "', '" + estatica.Usu + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                MessageBox.Show("El pagó se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                button4.Visible = false;
                                label17.Visible = true;
                                textBox4.Text = textBox2.Text;
                                pago = textBox1.Text;
                                estado = "PENDIENTE";
                                textBox4.Visible = true;
                            }

                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es valido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Ingrese el monto para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        private static void OnlyNumber(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }

        private void textBox12_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }
        void bloquear()
        {
            comboBox1.Enabled = false;
            comboBox2.Enabled = false;
            comboBox3.Enabled = false;
            textBox2.ReadOnly = true;
            textBox3.ReadOnly = true;
            textBox4.ReadOnly = true;
            dateTimePicker1.Enabled = false;
        }
        void desbloquear()
        {
            comboBox1.Enabled = true;
            comboBox2.Enabled = true;
            comboBox3.Enabled = true;
            textBox2.ReadOnly = false;
            textBox3.ReadOnly = false;
            textBox4.ReadOnly = false;
            dateTimePicker1.Enabled = true;
        }

        void buscarp()
        {
            if (textBox1.Text != "")
            {
                string buscarc = "SELECT * FROM pago WHERE cod = " + textBox1.Text + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    ep = 1;
                    pago = textBox1.Text;
                    ocultar();
                    label4.Visible = true;
                    comboBox1.Visible = true;
                    button7.Visible = false;
                    button5.Visible = false;
                    comboBox1.SelectedItem = lee["tipo"].ToString();
                    dateTimePicker1.Text = lee["fecha"].ToString();
                    textBox2.Text = lee["monto"].ToString();
                    textBox3.Text = lee["pago"].ToString();
                    textBox4.Text = lee["disponible"].ToString();
                    comboBox2.SelectedValue = lee["origen"].ToString();
                    comboBox3.SelectedValue = lee["destino"].ToString();
                    estado = lee["estatus"].ToString();
                    bloquear();


                }
                else
                {
                    MessageBox.Show("El código ingresado no pertenece a ningún pago registrado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Focus();
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código de pago para poder realizar la busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox1.Focus();
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            buscarp();
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buscarp();
            }
        }
        string ci, pre;
        void buscarpre()
        {
            if (textBox12.Text != "")
            {
                try
                {

                    string buscarc = "SELECT * FROM pre WHERE cod = " + textBox12.Text + ";";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        if(lee["estatus"].ToString() == "PENDIENTE")
                        {
                            pre = textBox12.Text;
                            ci = lee["estudiante"].ToString();
                            textBox7.Text = ci;
                            textBox13.Text = lee["preciom"].ToString();
                             try
                             {
                                  buscarc = "SELECT * FROM estudiante WHERE ci = '" + ci + "';";
                                  data = new OleDbDataAdapter(buscarc, conexion);
                                  comando = new OleDbCommand(buscarc, conexion);
                                  OleDbDataReader leex = comando.ExecuteReader();
                                  if (leex.Read() == true)
                                  {
                                      textBox6.Text = leex["nombre"].ToString() + " " + leex["apellido"].ToString();
                                  }
                             }
                             catch (DBConcurrencyException ex)
                             {
                                 MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                             }
                             catch (Exception ex)
                             {
                                 MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                             }
                             try
                             {
                                 buscarc = "SELECT * FROM modalidad WHERE cod = " + lee["modalidad"].ToString() + ";";
                                 data = new OleDbDataAdapter(buscarc, conexion);
                                 comando = new OleDbCommand(buscarc, conexion);
                                 OleDbDataReader leexx = comando.ExecuteReader();
                                 if (leexx.Read() == true)
                                 {
                                     textBox22.Text = leexx["modalidad"].ToString();
                                     textBox8.Text = leexx["dia"].ToString();
                                     textBox9.Text = leexx["desde"].ToString();
                                     textBox10.Text = leexx["hasta"].ToString();
                                     textBox5.Text = leexx["inicio"].ToString();
                                 }
                             }
                             catch (DBConcurrencyException ex)
                             {
                                 MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                             }
                             catch (Exception ex)
                             {
                                 MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                             }
                        }
                        else
                        {
                            if(lee["estatus"].ToString() == "VERIFICADO" )
                            {
                                MessageBox.Show("El código ingresado pertenece a una preinscripción que ya se encuentra formalizada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                textBox12.Focus();
                            }
                            if (lee["estatus"].ToString() == "CULMINADO" || lee["estatus"].ToString() == "NO COMPLETADO")
                            {
                                MessageBox.Show("El código ingresado pertenece a una preinscripción de un periodo académico culminado y ya no puede ser formalizada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                textBox12.Focus();
                            }


                        }

                    }
                    else
                    {
                        MessageBox.Show("El código ingresado no pertenece a una preinscripción registrada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox1.Clear();
                    }
                }
                catch (DBConcurrencyException ex)
                {
                    MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código de pago para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox1.Clear();
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            buscarpre();
        }

        void be()
        {
            if (textBox7.Text != "")
            {
                try
                {

                    string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox7.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        estatica.Estudiante = lee["ci"].ToString();
                        estatica.Ne = lee["nombre"].ToString() + " " + lee["apellido"].ToString();
                        pres a = new pres();
                        a.ShowDialog();
                        if (a.DialogResult == DialogResult.Yes)
                        {
                            textBox12.Text = estatica.Pre;
                            buscarpre();

                        }

                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad ingresado no pertenece a ningún estudiante registrado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox7.Clear();
                    }
                }
                catch (DBConcurrencyException ex)
                {
                    MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para realizar una busqueda", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox7.Focus();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            be();
        }
        DateTime hora = new DateTime();
        private void button10_Click(object sender, EventArgs e)
        {
            if(pre != "" && pago != "" && pre != null && pago != null )
            {
                double dis = 0, p = 0, to;
                dis = Double.Parse(textBox4.Text);
                if (comboBox1.Text != "Exoneración")
                {
                    
                    dis = Double.Parse(textBox4.Text);
                    p = Double.Parse(textBox13.Text);
                    if (dis >= p)
                    {
                        try
                        {
                            to = dis - p;
                            hora = DateTime.Now;
                            if (estado == "VERIFICADO")
                            {
                                string insertar = "INSERT INTO forma  VALUES (" + pre + ",'" + hora.ToString("dd-MM-yyyy") + "', '" + hora.ToString("hh:mm:ss") + "', '" + comboBox4.Text + "', '" + comboBox5.Text + "', '" + comboBox6.Text + "', " + pago + ", '" + estatica.Ci + "', '"+estado+"')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                string insertarx = "UPDATE  pre SET estatus = 'VERIFICADA' WHERE cod = " + pre + ";";
                                OleDbCommand cmdx = new OleDbCommand(insertarx, conexion);
                                cmdx.ExecuteNonQuery();

                                string insertarxx = "UPDATE  pago SET disponible = '" + to.ToString() + "' WHERE cod = " + pago + ";";
                                OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                cmdxx.ExecuteNonQuery();


                                string insertarxxx = "UPDATE  estudiante SET estatus = 'ACTIVO' WHERE cI = '" + ci + "';";
                                OleDbCommand cmdxxx = new OleDbCommand(insertarxxx, conexion);
                                cmdxxx.ExecuteNonQuery();

                                textBox4.Text = to.ToString();
                                MessageBox.Show("La formalización se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                string insertar = "INSERT INTO forma  VALUES (" + pre + ",'" + hora.ToString("dd-MM-yyyy") + "', '" + hora.ToString("hh:mm:ss") + "', '" + comboBox4.Text + "', '" + comboBox5.Text + "', '" + comboBox6.Text + "', " + pago + ", '" + estatica.Ci + "', '" + estado + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();

                                string insertarx = "UPDATE  pre SET estatus = 'FORMALIZADA' WHERE cod = " + pre + ";";
                                OleDbCommand cmdx = new OleDbCommand(insertarx, conexion);
                                cmdx.ExecuteNonQuery();

                                string insertarxx = "UPDATE  pago SET disponible = '" + to.ToString() + "' WHERE cod = " + pago + ";";
                                OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                cmdxx.ExecuteNonQuery();

                                textBox4.Text = to.ToString();
                                MessageBox.Show("La formalización se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }




                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("La disponibilidad del pago ingresado no cubre el precio del servicio. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox1.Focus();
                    }
                }
                else
                {
                    try
                    {
                        if (dis == 0)
                        {
                            hora = DateTime.Now;
                            if (estado == "VERIFICADO")
                            {
                                string insertar = "INSERT INTO forma  VALUES (" + pre + ",'" + hora.ToString("dd-MM-yyyy") + "', '" + hora.ToString("hh:mm:ss") + "', '" + comboBox4.Text + "', '" + comboBox5.Text + "', '" + comboBox6.Text + "', " + pago + ", '" + estatica.Ci + "', '" + estado + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                string insertarx = "UPDATE  pre SET estatus = 'VERIFICADA' WHERE cod = " + pre + ";";
                                OleDbCommand cmdx = new OleDbCommand(insertarx, conexion);
                                cmdx.ExecuteNonQuery();

                                string insertarxx = "UPDATE  pago SET disponible = '-1' WHERE cod = " + pago + ";";
                                OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                cmdxx.ExecuteNonQuery();

                                string insertarxxx = "UPDATE  estudiante SET estatus = 'ACTIVO' WHERE cI = '" + ci + "';";
                                OleDbCommand cmdxxx = new OleDbCommand(insertarxxx, conexion);
                                cmdxxx.ExecuteNonQuery();

                                MessageBox.Show("La formalización se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                string insertar = "INSERT INTO forma  VALUES (" + pre + ",'" + hora.ToString("dd-MM-yyyy") + "', '" + hora.ToString("hh:mm:ss") + "', '" + comboBox4.Text + "', '" + comboBox5.Text + "', '" + comboBox6.Text + "', " + pago + ", '" + estatica.Ci + "', '" + estado + "')";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();

                                string insertarx = "UPDATE  pre SET estatus = 'FORMALIZADA' WHERE cod = " + pre + ";";
                                OleDbCommand cmdx = new OleDbCommand(insertarx, conexion);
                                cmdx.ExecuteNonQuery();

                                string insertarxx = "UPDATE  pago SET disponible = '-1' WHERE cod = " + pago + ";";
                                OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                                cmdxx.ExecuteNonQuery();

                                MessageBox.Show("La formalización se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("La Exoneración ingresada ya fue utilizada en otra formalización. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            textBox1.Focus();
                        }




                    }

                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }
        
    }
}
