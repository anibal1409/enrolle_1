﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class periodo : Form
    {
        public periodo()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void periodo_Load(object sender, EventArgs e)
        {
            conexion.Open();
            cargar();
        }

        void cargar()
        {

            string buscarc = "SELECT TOP 1 * FROM periodo ORDER BY cod DESC;";
            OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
            OleDbCommand comando = new OleDbCommand(buscarc, conexion);
            OleDbDataReader lee = comando.ExecuteReader();
            if (lee.Read() == true)
            {
                double ccod;
                ccod = Double.Parse(lee["cod"].ToString()) + 1;
                textBox1.Text = ccod.ToString();
            }
            else
            {
                textBox1.Text = "1";
            }
        }

        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            comboBox1.Text = "";
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
            cargar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && comboBox1.Text != "")
            {
                if (dateTimePicker1.Value <= dateTimePicker2.Value)
                {
                    try
                    {
                        string insertar = "INSERT INTO periodo VALUES (" + textBox1.Text + ",'" + textBox2.Text + "','" + dateTimePicker1.Text + "','" + dateTimePicker2.Text + "','" + comboBox1.Text + "')";
                        OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                        cmd.ExecuteNonQuery();
                        clear();
                        MessageBox.Show("El periodo académico se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }

                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("La fecha de inicio no puede ser mayor que la fecha de culminación.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            admin a = new admin();
            a.Show();
            this.Close();
        }
    }
}
