﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class cerrar : Form
    {
        public cerrar()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void cerrar_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
        }
        void combo1()
        {
            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dtx = new DataTable();
            dtx = new DataTable();
            DATA.Fill(dtx);

            DataRow nuevaFila = dtx.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dtx.Rows.InsertAt(nuevaFila, 0);

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "periodo";
            comboBox3.DataSource = dtx;
        }
        string pas;
        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox3.SelectedIndex != 0)
            {
                if (MessageBox.Show("¿Está seguro que desea cerrar el periodo académico seleccionada? Recuerde que una vez cerrado el periodo académico no se podrán realizar registro, modificaciones, solicitudes u otro en este periodo.", "ADVERTENCIA", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    pas = Microsoft.VisualBasic.Interaction.InputBox("Ingrese su clave de acceso al sistema para continuar", "Solicitud de clave", "****");
                    if (pas == estatica.Pas)
                    {
                        try
                        {
                            string insertar = "UPDATE  periodo SET   estatus = 'CULMINADO' WHERE cod = " + comboBox3.SelectedValue.ToString() + " ";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();



                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        try
                        {
                            string insertar = "UPDATE  pre SET   estatus = 'CULMINADO' WHERE periodo = " + comboBox3.SelectedValue.ToString() + " AND estatus = 'VERIFICADO'";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();



                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        try
                        {
                            string insertar = "UPDATE  pre SET   estatus = 'NO COMPLETADO' WHERE periodo = " + comboBox3.SelectedValue.ToString() + " AND estatus = 'NO VERIFICADO' OR estatus = 'PENDIENTE'";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();



                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        try
                        {
                            string insertar = "UPDATE  forma SET   estatus = 'CULMINADO' WHERE periodo = '" + comboBox3.SelectedValue.ToString() + "' AND estatus = 'VERIFICADO'";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();



                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        try
                        {
                            string insertar = "UPDATE  forma SET   estatus = 'NO COMPLETADO' WHERE periodo = " + comboBox3.SelectedValue.ToString() + " AND estatus = 'NO VERIFICADO' OR estatus = 'PENDIENTE'";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();



                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        try
                        {
                            string insertar = "UPDATE  modalidad SET   estatus = 'CULMINADO' WHERE periodo = " + comboBox3.SelectedValue.ToString() + "";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();



                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        MessageBox.Show("El periodo académico fue cerrado con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Contraseña incorrecta.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            else
            {
                MessageBox.Show("Debe seleccionar un periodo académico para poder hacer uso de esta función.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem20_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem21_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem19_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem22_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem20_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem23_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem21_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem24_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem22_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem19_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }
    }
}
