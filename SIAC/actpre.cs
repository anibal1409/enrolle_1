﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class actpre : Form
    {
        public actpre()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void actpre_Load(object sender, EventArgs e)
        {
            conexion.Open();
        }

        void cleart()
        {
            clearc();
            textBox1.Enabled = true;
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox16.Clear();
            textBox17.Clear();
            textBox99.Clear();
            dateTimePicker1.Value = DateTime.Now;
            ci = "";
            curso = "";
            codp = "";
            dis = "";

        }
        void clear()
        {
            textBox1.ReadOnly = true;
            textBox7.ReadOnly = true;
            comboBox2.Enabled = false;
            button4.Enabled = false;
        }

        void combo2(string com)
        {
            string ctc = "SELECT cod,modalidad FROM modalidad WHERE estatus = 'DISPONIBLE' AND periodo = " + com + " ORDER BY modalidad";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "modalidad";
            comboBox2.DataSource = dt;
        }
        void clearc()
        {
            textBox8.Clear();
            textBox9.Clear();
            textBox10.Clear();
            textBox11.Clear();
            textBox12.Clear();
            textBox13.Clear();
            textBox14.Clear();
            textBox15.Clear();
            ocultar();

        }
        
        
        void ocultar()
        {
            textBox3.ReadOnly = true;
            textBox4.ReadOnly = true;
            textBox5.ReadOnly = true;
            textBox6.ReadOnly = true;
            dateTimePicker1.Enabled = false;
        }

        private static void OnlyNumber(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        string ci = "";
        DateTime var;
        void buscar()
        {
            if (textBox7.Text != "")
            {
                ci = "";
                var cadena = textBox7.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox7.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        if (lee["estatus"].ToString() == "ACTIVO" || lee["estatus"].ToString() == "INACTIVO")
                        {
                            ci = textBox7.Text;
                            textBox6.Text = lee["nombre"].ToString();
                            textBox3.Text = lee["apellido"].ToString();
                            dateTimePicker1.Text = lee["fecha"].ToString();
                            textBox4.Text = lee["direc"].ToString();
                            textBox5.Text = lee["tlf"].ToString();
                            var = dateTimePicker1.Value.Date;
                            int edad = DateTime.Today.AddTicks(-var.Ticks).Year - 1;
                            textBox99.Text = edad.ToString();

                        }
                        else
                        {
                            MessageBox.Show("El estudiante con el documento de identidad '" + textBox7.Text + "' se encuentra actualmente suspendido. Comuníquese con el Coordinador Académico.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }

                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad '" + textBox7.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para poder realizar la busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }

        string codp, estu, curso, dcurso, pcurso;
        DateTime dia = new DateTime();
        void busca()
        {
            if (textBox1.Text != "")
            {
                codp = "";
                estu = "";
                string buscarc = "SELECT * FROM pre WHERE cod = " + textBox1.Text + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read())
                {
                    if (lee["estatus"].ToString() == "PENDIENTE")
                    {
                        textBox2.Text = lee["precio"].ToString();
                        codp = lee["cod"].ToString();
                        textBox7.Text = ci = estu = lee["estudiante"].ToString();
                        comboBox4.Text = lee["estatus"].ToString();
                        string buscarcx2 = "SELECT * FROM empleado WHERE ci = '" + lee["empleado"].ToString() + "';";
                        OleDbDataAdapter datax2 = new OleDbDataAdapter(buscarcx2, conexion);
                        OleDbCommand comandox2 = new OleDbCommand(buscarcx2, conexion);
                        OleDbDataReader leex2 = comandox2.ExecuteReader();
                        if (leex2.Read() == true)
                        {
                            textBox18.Text = "(" + lee["empleado"].ToString() + ")" + " " + leex2["nombre"].ToString() + " " + leex2["apellido"].ToString();
                        }
                        string buscarcx1 = "SELECT * FROM periodo WHERE cod = " + lee["periodo"].ToString() + ";";
                        OleDbDataAdapter datax1 = new OleDbDataAdapter(buscarcx1, conexion);
                        OleDbCommand comandox1 = new OleDbCommand(buscarcx1, conexion);
                        OleDbDataReader leex1 = comandox1.ExecuteReader();
                        if (leex1.Read() == true)
                        {
                            textBox16.Text = leex1["periodo"].ToString();
                        }
                        combo2(lee["periodo"].ToString());
                        curso = lee["modalidad"].ToString();
                        pcurso = lee["preciom"].ToString();
                        dia = DateTime.Parse(lee["fecha"].ToString());
                        textBox17.Text = dia.AddDays(10).ToString("dd-MM-yyyy");

                        comboBox2.SelectedIndex = int.Parse(lee["modalidad"].ToString());
                        string buscarcx = "SELECT * FROM modalidad WHERE cod = " + lee["modalidad"].ToString() + ";";
                        OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                        OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                        OleDbDataReader leex = comandox.ExecuteReader();
                        if (leex.Read() == true)
                        {
                            textBox8.Text = leex["dia"].ToString();
                            textBox9.Text = leex["desde"].ToString();
                            textBox10.Text = leex["hasta"].ToString();
                            textBox12.Text = leex["inicio"].ToString();
                            textBox11.Text = leex["fin"].ToString();
                            textBox13.Text = lee["preciom"].ToString();
                            textBox14.Text = leex["disponible"].ToString();
                            dcurso = leex["disponible"].ToString();
                            string buscarcxx = "SELECT * FROM empleado WHERE ci = '" + leex["profesor"].ToString() + "';";
                            OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
                            OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
                            OleDbDataReader leexx = comandoxx.ExecuteReader();
                            if (leexx.Read() == true)
                            {
                                textBox15.Text = leexx["nombre"].ToString() + " " + leexx["apellido"].ToString();
                            }

                        }
                        buscar();
                        comboBox2.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Esta preinscripción no puede ser modificada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox1.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("El código ingresado no pertenece a un preinscripción registrada en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            busca();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscar();
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox7_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buscar();
            }
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                busca();
            }
        }
        Double d = 0;
        string dis = "";
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedValue.ToString() != "0")
            {
                string buscarc = "SELECT * FROM modalidad WHERE cod = " + comboBox2.SelectedValue.ToString() + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    textBox8.Text = lee["dia"].ToString();
                    textBox9.Text = lee["desde"].ToString();
                    textBox10.Text = lee["hasta"].ToString();
                    textBox12.Text = lee["inicio"].ToString();
                    textBox11.Text = lee["fin"].ToString();
                    textBox13.Text = lee["precio"].ToString();
                    textBox14.Text = lee["disponible"].ToString();
                    d = Double.Parse(lee["disponible"].ToString()) - 1;
                    dis = d.ToString();
                    string buscarcx = "SELECT * FROM empleado WHERE ci = '" + lee["profesor"].ToString() + "';";
                    OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                    OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read() == true)
                    {
                        textBox15.Text = leex["nombre"].ToString() + " " + leex["apellido"].ToString();
                    }

                }
            }
            else
            {
                clearc();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (ci != "" && codp != "" && comboBox2.SelectedIndex != 0)
            {
                if (textBox14.Text == "0")
                {
                    MessageBox.Show("La disciplina '" + comboBox2.Text + "' no posee capacidad para agregar a otro estudiante.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {
                        if (curso == comboBox2.SelectedValue.ToString() && textBox13.Text == pcurso)
                        {
                            string insertar = "UPDATE  pre SET  estudiante = '" + ci + "', empleado = '" + estatica.Ci + "' WHERE cod = " + codp + " ";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("La preinscripción fue modificada con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            string insertar = "UPDATE  pre SET  estudiante = '" + ci + "', modalidad=" + comboBox2.SelectedValue.ToString() + ", preciom = '" + textBox13.Text + "', empleado = '" + estatica.Ci + "' WHERE cod = " + codp + " ";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();

                            string insertarx = "UPDATE  modalidad SET  disponible = '" + dis + "' WHERE cod = " + comboBox2.SelectedValue.ToString() + " ";
                            OleDbCommand cmdx = new OleDbCommand(insertarx, conexion);
                            cmdx.ExecuteNonQuery();
                            int x = int.Parse(dcurso) + 1;
                            string insertarxx = "UPDATE  modalidad SET  disponible = '" + x.ToString() + "' WHERE cod = " + curso + " ";
                            OleDbCommand cmdxx = new OleDbCommand(insertarxx, conexion);
                            cmdxx.ExecuteNonQuery();
                            MessageBox.Show("La preinscripción fue modificada con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }

                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar la modificación.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        
        private void button3_Click(object sender, EventArgs e)
        {

            cleart();

        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

    }
}
