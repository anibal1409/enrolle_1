﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class exo : Form
    {
        public exo()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void exo_Load(object sender, EventArgs e)
        {
            conexion.Open();
        }
        public void entrar()
        {

            if (textBox1.Text == "" && textBox2.Text == "" || textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("Debe ingresar sus credenciales para continuar con el registro.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (textBox1.Text == "")
                {
                    textBox1.Focus();
                }
                else
                {
                    textBox2.Focus();
                }
            }

            else
            {
                string buscarc = "SELECT * FROM usuario WHERE nombre = '" + textBox1.Text + "' AND pass = '" + textBox2.Text + "';";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    if (lee["estatus"].ToString() == "ACTIVO" && lee["nivel"].ToString() == "Coordinador Académico" || lee["nivel"].ToString() == "Administrador")
                    {
                        estatica.Usu = textBox1.Text;
                        button1.Visible = false;
                        textBox1.Clear();
                        textBox2.Clear();
                        textBox1.Visible = false;
                        textBox2.Visible = false;
                        textBox3.Visible = true;
                        label3.Visible = true;
                        button2.Visible = true;
                        label1.Visible = false;
                        label2.Visible = false;
                        
                            
                    }
                    else
                    {
                        MessageBox.Show("Anexo denegado para esta operación.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }


                }

                else
                {
                    MessageBox.Show("Nombre de usuario y/o contraseña incorrectos.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            entrar();
        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                entrar();
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox3.Text != "")
            {
               estatica.Obser = textBox3.Text;
                this.DialogResult = DialogResult.Yes;
            }
            else
            {
                MessageBox.Show("No se puede continuar con el registro del pago sin realizar un observación.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        
    }
}
