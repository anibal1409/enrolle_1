﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class connotas : Form
    {
        public connotas()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");

        private void connotas_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
        }
        void combo1()
        {
            string ctc = "SELECT * FROM periodo ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dtx = new DataTable();
            dtx = new DataTable();
            DATA.Fill(dtx);

            DataRow nuevaFila = dtx.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dtx.Rows.InsertAt(nuevaFila, 0);

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "periodo";
            comboBox3.DataSource = dtx;
        }
        string cie = "";
        void combo3()
        {
            if (textBox5.Text != "")
            {

                cie = "";
                var cadena = textBox5.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    cie = textBox5.Text;
                    string buscarcx = "SELECT nombre, apellido, cargo, estatus FROM empleado WHERE  ci = '" + cie + "'";
                    OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                    OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read())
                    {
                        if (leex["estatus"].ToString() == "ACTIVO")
                        {
                            textBox13.Text = leex["nombre"].ToString() + " " + leex["apellido"].ToString();

                            string buscarc = "SELECT cargo FROM cargo WHERE cod = '" + leex["cargo"].ToString() + "'";
                            OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                            OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                            OleDbDataReader lee = comando.ExecuteReader();
                            if (lee.Read())
                            {
                                textBox4.Text = lee["cargo"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("El documento de identidad '" + textBox5.Text + "' no se encuentra activo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            textBox5.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad '" + textBox5.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox5.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox5.Focus();
                }


            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para poder realizar una busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox5.Focus();
            }

        }
        DataTable dt = new DataTable();
        void combo2()
        {
            DataTable dt = new DataTable();
            string buscarcx = "SELECT * FROM pre WHERE estudiante= '" + ci + "' AND  estatus = 'CULMINADO';";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            while (leex.Read())
            {
                string ctc = "SELECT cod,modalidad FROM modalidad WHERE  cod = '" + leex["modalidad"].ToString() + "'";
                OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
                DATA.Fill(dt);
            }


            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "modalidad";
            comboBox2.DataSource = dt;
        }
        string ci = "";
        void buscare()
        {
            if (textBox2.Text != "")
            {
                if (textBox7.Text != "")
                {
                    ci = "";
                    var cadena = textBox7.Text;
                    if (cadena.Length >= 7 && cadena.Length <= 8)
                    {
                        string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox7.Text + "';";
                        OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                        OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                        OleDbDataReader lee = comando.ExecuteReader();
                        if (lee.Read() == true)
                        {
                            if (lee["estatus"].ToString() == "ACTIVO")
                            {
                                ci = textBox7.Text;
                                textBox6.Text = lee["nombre"].ToString() + " " + lee["apellido"].ToString();
                                combo2();
                            }
                            else
                            {
                                MessageBox.Show("El estudiante con el documento de identidad '" + textBox7.Text + "' no se encuentra activo actualmente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                textBox7.Focus();
                            }

                        }
                        else
                        {
                            MessageBox.Show("El documento de identidad '" + textBox7.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            textBox7.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad para poder realizar la busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un periodo académico para poder hacer uso de esta función", "Información)", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        void clear()
        {
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox8.Clear();
            textBox9.Clear();
            textBox13.Clear();
            textBox14.Clear();
            ci = "";
            cie = "";
            comboBox5.SelectedIndex = 0;
            dt = new DataTable();
            dt.Columns.Add("cod");
            dt.Columns.Add("modalidad");
            DataRow nuevaFila = dt.NewRow();
            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "modalidad";
            comboBox2.DataSource = dt;
            cod = "";

        }
        string per;
        void bm()
        {
            if (comboBox2.SelectedValue.ToString() != "0")
            {
                per = "";
                string buscarc = "SELECT * FROM modalidad WHERE cod = '" + comboBox2.SelectedValue.ToString() + "';";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    string buscarcx = "SELECT * FROM periodo WHERE cod = '" + lee["periodo"].ToString() + "';";
                    OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                    OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read() == true)
                    {
                        per = lee["periodo"].ToString();
                        textBox8.Text = leex["periodo"].ToString();
                    }
                    string buscarcxx = "SELECT * FROM pre WHERE estudiante = '" + ci + "' AND modalidad = '" + lee["cod"].ToString() + "';";
                    OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
                    OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
                    OleDbDataReader leexx = comandoxx.ExecuteReader();
                    if (leexx.Read() == true)
                    {

                        textBox9.Text = leexx["nota"].ToString();
                    }

                }
            }
            else
            {
                //clearc();
            }
        }

        string cod = "";
        void buscar()
        {
            if (textBox3.Text != "")
            {
                cod = "";
                string buscarc = "SELECT * FROM nota WHERE cod = '" + textBox3.Text + "';";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    cod = textBox3.Text;

                    textBox2.Text = lee["precio"].ToString();
                    textBox7.Text = lee["estudiante"].ToString();
                    buscare();
                    string ctc = "SELECT * FROM modalidad WHERE  cod = '" + lee["modalidad"].ToString() + "'";
                    OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
                    OleDbCommand comandox = new OleDbCommand(ctc, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read() == true)
                    {
                        comboBox3.SelectedValue = leex["periodo"].ToString();
                    }
                    DataTable dt = new DataTable();
                    DATA.Fill(dt);
                    /*DataRow nuevaFila = dt.NewRow();

                    nuevaFila["cod"] = 0;
                    nuevaFila["modalidad"] = "Seleccione una disciplina";

                    dt.Rows.InsertAt(nuevaFila, 0);*/
                    button8.Visible = true;
                    comboBox2.ValueMember = "cod";
                    comboBox2.DisplayMember = "modalidad";
                    comboBox2.DataSource = dt;
                    bm();
                    textBox5.Text = lee["suscribe"].ToString();
                    combo3();
                    cie = lee["suscribe"].ToString();
                    textBox14.Clear();
                    comboBox5.Text = lee["estatus"].ToString();
                    string buscarcxx = "SELECT nombre, apellido FROM empleado WHERE  ci = '" + lee["empleado"].ToString() + "'";
                    OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
                    OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
                    OleDbDataReader leexx = comandoxx.ExecuteReader();
                    if (leexx.Read())
                    {
                        textBox14.Text = "(" + lee["empleado"].ToString() + ") " + leexx["nombre"].ToString() + " " + leexx["apellido"].ToString();
                    }


                }
                else
                {
                    MessageBox.Show("El código ingresado no pertene a una constancia de estudio registrada en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cod = "";
                    button8.Visible = false;

                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código para poder realizar la busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        string x;

        private void button1_Click(object sender, EventArgs e)
        {
            x = textBox3.Text;

            clear();
            textBox3.Text = x;
            buscar();
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                x = textBox3.Text;

                clear();
                textBox3.Text = x;
                buscar();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            clear();
            button8.Visible = false;
        }
    }
}
