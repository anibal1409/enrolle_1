﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Drawing.Printing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;
using System.Globalization;

namespace SIAC
{
    public partial class listacerti : Form
    {
        public listacerti()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");

        private void listacerti_Load(object sender, EventArgs e)
        {
            conexion.Open();
            comboBox3.SelectedIndex = 0;
            comboBox1.SelectedIndex = 0;

        }

        string[,] comen;
        int can;
        DateTime a, b, c, d;
        void lista()
        {
            a = DateTime.Parse(dateTimePicker1.Value.ToString("dd-MM-yyyy"));
            b = DateTime.Parse(dateTimePicker2.Value.ToString("dd-MM-yyyy"));
            can = 0;
            comen = new string[6, 1000000];


                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                string buscarcxx = "SELECT * FROM certi WHERE estatus = '" + comboBox3.Text + "' AND tipo = '"+comboBox1.Text+"';";
                OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
                OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
                OleDbDataReader leexx = comandoxx.ExecuteReader();
                while (leexx.Read())
                {
                    c = DateTime.Parse(leexx["fecha"].ToString());
                    if (c >= a && c <= b)
                    {

                        comen[0, can] = leexx["cod"].ToString();
                        comen[1, can] = leexx["fecha"].ToString();
                        string buscarcx = "SELECT * FROM modalidad WHERE cod = " + leexx["modalidad"].ToString() + ";";
                        OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                        OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                        OleDbDataReader leex = comandox.ExecuteReader();
                        if (leex.Read())
                        {
                            comen[2, can] = "(" + leex["cod"].ToString() + ") " + leex["modalidad"].ToString();
                        }
                        string buscarc = "SELECT * FROM estudiante WHERE ci = '" + leexx["estudiante"].ToString() + "' ;";
                        OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                        OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                        OleDbDataReader lee = comando.ExecuteReader();
                        if (lee.Read())
                        {
                            comen[3, can] = "(" + lee["ci"].ToString() + ") " + lee["nombre"].ToString() + " " + lee["apellido"].ToString();
                        }
                        
                        comen[4, can] = leexx["tipo"].ToString();
                        comen[5, can] = leexx["pago"].ToString();                        
                        can++;
                    }

                }
                if (can > 1)
                {
                    string ax, bx, cx, dx, ex, fx, gx, hx, ix, jx;
                    for (int i = 0; i < can; i++)
                    {
                        for (int j = 0; j < can - 1; j++)
                        {
                            if (String.Compare(comen[1, j], comen[1, j + 1]) > 0)
                            {
                                ax = comen[0, j];
                                bx = comen[1, j];
                                cx = comen[2, j];
                                dx = comen[3, j];
                                ex = comen[4, j];
                                fx = comen[5, j];
                                comen[0, j] = comen[0, j + 1];
                                comen[1, j] = comen[1, j + 1];
                                comen[2, j] = comen[2, j + 1];
                                comen[3, j] = comen[3, j + 1];
                                comen[4, j] = comen[4, j + 1];
                                comen[5, j] = comen[5, j + 1];
                                comen[0, j + 1] = ax;
                                comen[1, j + 1] = bx;
                                comen[2, j + 1] = cx;
                                comen[3, j + 1] = dx;
                                comen[4, j + 1] = ex;
                                comen[5, j + 1] = fx;
                            }
                        }
                    }
                }

                if (can > 0)
                {
                    dataGridView1.Rows.Clear();
                    dataGridView1.Refresh();
                    for (int i = 0; i < can; i++)
                    {
                        dataGridView1.Rows.Add();
                        dataGridView1.Rows[i].Cells[0].Value = comen[0, i];
                        dataGridView1.Rows[i].Cells[1].Value = comen[1, i];
                        dataGridView1.Rows[i].Cells[2].Value = comen[2, i];
                        dataGridView1.Rows[i].Cells[3].Value = comen[3, i].ToUpper();
                        dataGridView1.Rows[i].Cells[4].Value = comen[4, i];
                        dataGridView1.Rows[i].Cells[5].Value = comen[5, i];

                    }
                }
        }


        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            lista();
            boton();

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            lista();
            boton();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            lista();
            boton();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lista();
            boton();
        }
        void boton()
        {
                button7.Visible = true;

        }
        #region crearPDF
        private void To_pdf()
        {
            string ab, bc;
            
            ab = comboBox3.Text.ToLower();
            if (comboBox1.Text == "REGULAR")
            {
                bc = comboBox1.Text.ToLower() + "es";
            }
            else
            {
                bc = comboBox1.Text.ToLower() + "s";
            }
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            Document doc = new Document(PageSize.LETTER, 20, 20, 20, 20);
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:";
            saveFileDialog1.Title = "Guardar Lista de Certificaciones " + myTI.ToTitleCase(ab) + " Estudiantes " + myTI.ToTitleCase(bc);
            saveFileDialog1.DefaultExt = "pdf";
            saveFileDialog1.Filter = "pdf Files (*.pdf)|*.pdf| All Files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.FileName = "ListaCertificaciones" + myTI.ToTitleCase(ab)+ "Estudiantes" + myTI.ToTitleCase(bc) + DateTime.Now.ToString("dd-MMM-yyyy"); ;
            string filename = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filename = saveFileDialog1.FileName;
            }
            if (filename.Trim() != "")
            {
                FileStream file = new FileStream(filename,
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite,
                FileShare.ReadWrite);
                PdfWriter wri = PdfWriter.GetInstance(doc, file);
                wri.PageEvent = new PageEventHelper();
                //PdfWriter.GetInstance(doc, file);

                doc.Open();
                string remito = "Generado por: (" + estatica.Ci + ") " + estatica.Nombre + " " + estatica.Ape + ", con ENROLL ONE.";
                string envio = "Fecha: " + DateTime.Now.ToString("dd MMMM yyyy, hh:mm:ss tt");

                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                iTextSharp.text.Font _standardFont2 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font cabe = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                // Creamos una tabla que contendrá el nombre, apellido y país
                // de nuestros visitante.
                PdfPTable tb1 = new PdfPTable(2);
                tb1.WidthPercentage = 100;


                //agregando una imagen
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("logo.jpg");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_CENTER;
                float percentage = 0.0f;
                percentage = 1000 / imagen.Width;
                imagen.ScalePercent(percentage * 100);
                //PdfImage img = new PdfImage(iTextSharp.text.Image.
                PdfPCell ff2 = new PdfPCell(imagen);
                ff2.BorderWidth = 0;
                //tb1.AddCell(imagen);


                // Configuramos el título de las columnas de la tabla
                PdfPCell ff1 = new PdfPCell(new Phrase("\nRepública Bolivariana de Venezuela\nCentro de Adiestramiento Profesional para el Desarrollo de los Sistemas Informáticos y Afines \nTelf.: 0291-8966131/ 0414-7679330\nMaturín - Estado Monagas\nRIF: J-406338516", _standardFont2));
                ff1.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff1.BorderWidth = 0;


                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);
                tb1.AddCell(ff1);


                // Configuramos el título de las columnas de la tabla

                Chunk chunk = new Chunk("Lista de Certificaciones " + myTI.ToTitleCase(ab) + " - Estudiantes " + myTI.ToTitleCase(bc), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 20, iTextSharp.text.Font.BOLD, BaseColor.BLACK));
                ff2 = new PdfPCell(new Paragraph(chunk));
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ff2.Padding = 4;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);

                ff2 = new PdfPCell(new Paragraph(envio, _standardFont2));
                ff2.BorderWidth = 0;
                ff2.Colspan = 2;
                ff2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                //tb1.AddCell(new Paragraph(chunk));



                // Añadimos las celdas a la tabla
                tb1.AddCell(ff2);

                float[] medidaCeldas = { 0.25f, 2.85f };

                // ASIGNAS LAS MEDIDAS A LA TABLA (ANCHO)
                tb1.SetWidths(medidaCeldas);

                // Finalmente, añadimos la tabla al documento PDF y cerramos el documento
                doc.Add(tb1);

                //doc.Add(new Paragraph("                       "));

                doc.Add(new Paragraph(remito, _standardFont2));
                doc.Add(new Paragraph("Busqueda: Desde " + dateTimePicker1.Text + " Hasta " + dateTimePicker2.Text, _standardFont2));
                doc.Add(new Paragraph("                       "));

                // Crea la imagen
                iTextSharp.text.Image imagen2 = iTextSharp.text.Image.GetInstance("fondo.jpg");
                // Cambia el tamaño de la imagen
                imagen2.ScaleToFit(700, 250);
                // Se indica que la imagen debe almacenarse como fondo
                imagen2.Alignment = iTextSharp.text.Image.UNDERLYING;
                // Coloca la imagen en una posición absoluta
                imagen2.SetAbsolutePosition(150, 280);
                // Imprime la imagen como fondo de página
                doc.Add(imagen2);

                if (dataGridView1.Rows.Count > 0)
                {
                    GenerarDocumento(doc);
                }



                doc.AddCreationDate();

                doc.Close();
                Process.Start(filename);//Esta parte se puede omitir, si solo se desea guardar el archivo, y que este no se ejecute al instante
            }

        }
        iTextSharp.text.Font _standardFont4 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        iTextSharp.text.Font _standardFont5 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        public void GenerarDocumento(Document document)
        {
            int i, j;
            PdfPTable datatable = new PdfPTable(dataGridView1.ColumnCount);
            datatable.DefaultCell.Padding = 3;
            float[] headerwidths = GetTamañoColumnas(dataGridView1);
            datatable.SetWidths(headerwidths);
            datatable.WidthPercentage = 100;
            datatable.DefaultCell.BorderWidth = 0.75f;
            datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            for (i = 0; i < dataGridView1.ColumnCount; i++)
            {
                datatable.AddCell(new Phrase(dataGridView1.Columns[i].HeaderText, _standardFont5));
            }
            datatable.HeaderRows = 1;
            datatable.DefaultCell.BorderWidth = 0.75f;
            for (i = 0; i < dataGridView1.Rows.Count; i++)
            {
                for (j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1[j, i].Value != null)
                    {
                        datatable.AddCell(new Phrase(dataGridView1[j, i].Value.ToString(), _standardFont4));//En esta parte, se esta agregando un renglon por cada registro en el datagrid
                    }
                }
                datatable.CompleteRow();
            }
            document.Add(datatable);
        }
        public float[] GetTamañoColumnas(DataGridView dg)
        {
            float[] values = new float[dg.ColumnCount];
            for (int i = 0; i < dg.ColumnCount - 1; i++)
            {
                values[i] = (float)dg.Columns[i].Width;
            }
            return values;

        }
        #endregion

        private void button7_Click(object sender, EventArgs e)
        {
            To_pdf();
        }

        
    }
}
