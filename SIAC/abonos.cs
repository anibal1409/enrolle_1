﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class abonos : Form
    {
        public abonos()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void abonos_Load(object sender, EventArgs e)
        {
            conexion.Open();
            bus();
            if (estatica.Nivel == "Administrador")
            {
                menuStrip1.Visible = true;
            }
            if (estatica.Nivel == "Asesor de Ventas")
            {
                //menuStrip4.Visible = true;
            }
            if (estatica.Nivel == "Coordinador Académico")
            {
                //menuStrip2.Visible = true;
            }
            if (estatica.Nivel == "Coordinador de Personal")
            {
                menuStrip3.Visible = true;
            }
        }
        string[,] comen;
        int can;
        string num;
        void bus()
        {
            can = 0;
            comen = new string[3, 1000000];
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            string buscarc = "SELECT * FROM abono ORDER BY abono;";
            OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
            OleDbCommand comando = new OleDbCommand(buscarc, conexion);
            OleDbDataReader lee = comando.ExecuteReader();
            while (lee.Read())
            {
                comen[0, can] = lee["cod"].ToString();
                comen[1, can] = lee["abono"].ToString();
                comen[2, can] = lee["valor"].ToString();
                can++;
            }
            
            if (can > 0)
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                for (int i = 0; i < can; i++)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells[0].Value = comen[0, i];
                    dataGridView1.Rows[i].Cells[1].Value = comen[1, i];
                    dataGridView1.Rows[i].Cells[2].Value = comen[2, i];

                }
            }

            string buscarcx = "SELECT TOP 1 * FROM abono ORDER BY cod DESC;";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                double ccod;
                ccod = Double.Parse(leex["cod"].ToString()) + 1;
                num = ccod.ToString();
            }
            else
            {
                num = "1";
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                if (IsNumeric(textBox2.Text))
                {
                    string buscarc = "SELECT * FROM abono WHERE abono LIKE '" + textBox1.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        MessageBox.Show("La asignación '" + textBox1.Text + "' ya se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox1.Focus();
                    }
                    else
                    {

                        try
                        {
                            string insertar = "INSERT INTO abono  VALUES (" + num + ",'" + textBox1.Text + "', '" + textBox2.Text + "', '"+estatica.Ci+"')";
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            clear();
                            MessageBox.Show("La asignación se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            bus();
                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox2.Focus();
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox1.Focus();
            bus();
        }
        public bool IsNumeric(string x)
        {

            double retNum;

            try
            {
                retNum = Double.Parse(x);
                return true;
            }
            catch
            {
                return false;

            }



        }

        private static void OnlyNumber(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            historial a = new historial();
            a.Show();
            this.Close();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea salir de su sesión?", "Notificación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                isesion a = new isesion();
                a.Show();
                this.Close();
            }
        }

        #region menu1

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            admin a = new admin();
            a.Show();
            this.Close();
        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cargarnota a = new cargarnota();
            a.Show();
            this.Close();
        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actnotae a = new actnotae();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            modalidad a = new modalidad();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            actmodalidad a = new actmodalidad();
            a.Show();
            this.Close();
        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mdisponibles a = new mdisponibles();
            a.Show();
            this.Close();
        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resumencurso a = new resumencurso();
            a.Show();
            this.Close();
        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listaestudiantes a = new listaestudiantes();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            tipom a = new tipom();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            acttipom a = new acttipom();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            forma a = new forma();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            actforma a = new actforma();
            a.Show();
            this.Close();
        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cambio a = new cambio();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            periodo a = new periodo();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            actperiodo a = new actperiodo();
            a.Show();
            this.Close();
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cerrar a = new cerrar();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            pre a = new pre();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            actpre a = new actpre();
            a.Show();
            this.Close();
        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            prepen a = new prepen();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            estudiante a = new estudiante();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actestudiante a = new actestudiante();
            a.Show();
            this.Close();
        }

        private void historialToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            historial a = new historial();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            actempleado a = new actempleado();
            a.Show();
            this.Close();
        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            trabajo a = new trabajo();
            a.Show();
            this.Close();
        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actividad a = new actividad();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            cargo a = new cargo();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            actcargo a = new actcargo();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            banco a = new banco();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            actbanco a = new actbanco();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            pago a = new pago();
            a.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            conpago a = new conpago();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            actbanco a = new actbanco();
            a.Show();
            this.Close();
        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            veripago a = new veripago();
            a.Show();
            this.Close();
        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listapv a = new listapv();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {
            precio a = new precio();
            a.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            conprecio a = new conprecio();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            actprecio a = new actprecio();
            a.Show();
            this.Close();
        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actpas a = new actpas();
            a.ShowDialog();
        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            abonos a = new abonos();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            actabonos a = new actabonos();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            dedu a = new dedu();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            actdedu a = new actdedu();
            a.Show();
            this.Close();
        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pagoe a = new pagoe();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem20_Click(object sender, EventArgs e)
        {
            notas a = new notas();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem18_Click(object sender, EventArgs e)
        {
            actnotas a = new actnotas();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem21_Click(object sender, EventArgs e)
        {
            certi2 a = new certi2();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem19_Click(object sender, EventArgs e)
        {
            actcerti2 a = new actcerti2();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem22_Click(object sender, EventArgs e)
        {
            certi1 a = new certi1();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem20_Click(object sender, EventArgs e)
        {
            actcerti1 a = new actcerti1();
            a.Show();
            this.Close();
        }

        private void listarCertificacionesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            listacerti a = new listacerti();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem23_Click(object sender, EventArgs e)
        {
            estudio a = new estudio();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem21_Click(object sender, EventArgs e)
        {
            actestudio a = new actestudio();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem24_Click(object sender, EventArgs e)
        {
            postu a = new postu();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem22_Click(object sender, EventArgs e)
        {
            actpostu a = new actpostu();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem19_Click(object sender, EventArgs e)
        {
            usuario a = new usuario();
            a.Show();
            this.Close();
        }

        private void modificarRestContraseñaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            actusuario a = new actusuario();
            a.Show();
            this.Close();
        }

        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea salir de su sesión?", "Notificación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                isesion a = new isesion();
                a.Show();
                this.Close();
            }
        }
        #endregion

        #region menu3

        private void toolStripMenuItem84_Click(object sender, EventArgs e)
        {
            admin a = new admin();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem115_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem116_Click(object sender, EventArgs e)
        {
            actempleado a = new actempleado();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem117_Click(object sender, EventArgs e)
        {
            trabajo a = new trabajo();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem118_Click(object sender, EventArgs e)
        {
            actividad a = new actividad();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem120_Click(object sender, EventArgs e)
        {
            cargo a = new cargo();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem121_Click(object sender, EventArgs e)
        {
            actcargo a = new actcargo();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem137_Click(object sender, EventArgs e)
        {
            actpas a = new actpas();
            a.ShowDialog();
        }

        private void toolStripMenuItem140_Click(object sender, EventArgs e)
        {
            abonos a = new abonos();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem141_Click(object sender, EventArgs e)
        {
            actabonos a = new actabonos();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem143_Click(object sender, EventArgs e)
        {
            dedu a = new dedu();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem144_Click(object sender, EventArgs e)
        {
            actdedu a = new actdedu();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem145_Click(object sender, EventArgs e)
        {
            pagoe a = new pagoe();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem164_Click(object sender, EventArgs e)
        {
            usuario a = new usuario();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem165_Click(object sender, EventArgs e)
        {
            actusuario a = new actusuario();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem166_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea salir de su sesión?", "Notificación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                isesion a = new isesion();
                a.Show();
                this.Close();
            }
        }
        #endregion

       


    }
}
