﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class actpago : Form
    {
        public actpago()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void actpago_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
            combo2();
        }

        void combo1()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Cliente' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "banco";
            comboBox2.DataSource = dt;
        }

        void combo2()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM banco WHERE uso = 'Instituto' OR uso = 'Ambos' ORDER BY banco";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["banco"] = "Seleccione un banco";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "banco";
            comboBox3.DataSource = dt;
        }
        string pa;
        private void button3_Click(object sender, EventArgs e)
        {
            if(textBox1.Text != "")
            {
                pa = "";
                string buscarc = "SELECT * FROM pago WHERE cod = " + textBox1.Text + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    pa = lee["cod"].ToString();
                    label13.Text = "Registrado: "+lee["registro"].ToString();
                    comboBox1.Text = lee["tipo"].ToString();
                    dateTimePicker1.Text = lee["fecha"].ToString();
                    textBox2.Text = lee["monto"].ToString();
                    textBox3.Text = lee["pago"].ToString();
                    comboBox2.Text = lee["origen"].ToString();
                    comboBox3.Text = lee["destino"].ToString();
                    textBox4.Text = lee["disponible"].ToString();
                    comboBox4.Text = lee["estatus"].ToString();
                    textBox5.Text = lee["obser"].ToString();
                    string buscarcx = "SELECT * FROM empleado WHERE ci = '" + lee["usu"].ToString() + "';";
                    OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
                    OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read() == true)
                    {
                        textBox6.Text = "(" + lee["usu"].ToString() + ")" + " " + leex["nombre"].ToString() + " " + leex["apellido"].ToString();
                    }
                    button1.Visible = true;
                    
                }
                else
                {
                    MessageBox.Show("El código ingresado no pertenece a un periodo académico registrado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            else
            {
                MessageBox.Show("Debe ingresar un código para poder realizar la busqueda", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        void clear()
        {
            pa = "";
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            comboBox4.SelectedIndex = 0; 
            dateTimePicker1.Value = DateTime.Now;
            label13.Text = "Registrado:";

        }

        public bool IsNumeric(string x)
        {
            double retNum;
            try
            {
                retNum = Double.Parse(x);
                return true;
            }
            catch
            {
                return false;
            }
        }
        int sw;
        private void button1_Click(object sender, EventArgs e)
        {
            if(pa != "" && comboBox1.Text != "")
            {
                sw = 0;
                if (comboBox1.Text == "Efectivo" && textBox2.Text != "" && textBox4.Text != "" && comboBox4.Text != "")
                {
                    if (IsNumeric(textBox2.Text))
                    {
                        try
                        {
                            string insertar = "UPDATE  pago SET  tipo = '" + comboBox1.Text + "', fecha = '" + dateTimePicker1.Text + "', monto = '" + textBox2.Text + "' , pago = '" + comboBox1.Text + "', origen = '" + comboBox1.Text + "' , destino = '" + comboBox1.Text + "', disponible = '" + textBox4.Text + "', estatus = '" + comboBox4.Text + "', obser = '" + textBox5.Text + "', usu = '" + estatica.Ci + "' WHERE cod = " + pa + " ";
                        
                            OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                            cmd.ExecuteNonQuery();
                            clear();
                            sw = 1;
                            MessageBox.Show("Los datos del pago fueron modificados con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);


                        }

                        catch (DBConcurrencyException ex)
                        {
                            MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }
                }
                if (comboBox1.Text == "Exoneración" && textBox4.Text != "" && textBox5.Text != "" && comboBox4.Text != "")
                {
                    try
                    {
                        string insertar = "UPDATE  pago SET  tipo = '" + comboBox1.Text + "', fecha = '" + dateTimePicker1.Text + "', monto = '0', pago = '" + comboBox1.Text + "', origen = '" + comboBox1.Text + "', destino = '" + comboBox1.Text + "', disponible = '" + textBox4.Text + "', estatus ='" + comboBox4.Text + "', obser = '" + textBox5.Text + "', usu = '" + estatica.Ci + "' WHERE cod = " + pa + ";";
                        OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                        cmd.ExecuteNonQuery();
                        clear();
                        sw = 1;
                        MessageBox.Show("Los datos del pago fueron modificados con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    }

                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                if (comboBox1.Text == "Deposito" && textBox4.Text != "" && textBox3.Text != "" && comboBox4.Text != "" && textBox2.Text != "" && comboBox3.SelectedIndex != 0)
                {
                    if (IsNumeric(textBox2.Text))
                    {
                    try
                    {
                        string insertar = "UPDATE  pago SET  tipo = '" + comboBox1.Text + "', fecha = '" + dateTimePicker1.Text + "', monto = '" + textBox2.Text + "', pago = '" + textBox3.Text + "', origen = '" + comboBox1.Text + "', destino = '" + comboBox3.Text + "', disponible = '" + textBox4.Text + "', estatus ='" + comboBox4.Text + "', obser = '" + textBox5.Text + "', usu = '" + estatica.Ci + "' WHERE cod = " + pa + " ";
                        OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                        cmd.ExecuteNonQuery();
                        clear();
                        sw = 1;
                        MessageBox.Show("Los datos del pago fueron modificados con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    }

                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                        }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }
                }
                if (comboBox1.Text == "Trans. Bancaria" || comboBox1.Text == "Pago Rápido" && textBox4.Text != "" && textBox3.Text != "" && comboBox4.Text != "" && textBox2.Text != "" && comboBox3.SelectedIndex != 0 && comboBox2.SelectedIndex != 0)
                {
                    if (IsNumeric(textBox2.Text))
                    {
                    try
                    {
                        string insertar = "UPDATE  pago SET  tipo = '" + comboBox1.Text + "', fecha = '" + dateTimePicker1.Text + "', monto = '" + textBox2.Text + "', pago = '" + textBox3.Text+ "', origen = '" + comboBox2.Text + "', destino = '" + comboBox3.Text + "', disponible = '" + textBox4.Text + "', estatus ='" + comboBox4.Text + "', obser = '" + textBox5.Text + "', usu = '" + estatica.Ci + "' WHERE cod = " + pa + " ";
                        OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                        cmd.ExecuteNonQuery();
                        clear();
                        sw = 1;
                        MessageBox.Show("Los datos del pago fueron modificados con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    }

                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                        }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }
                }
                if (comboBox1.Text == "Otro" && textBox4.Text != "" && textBox5.Text != "" && comboBox4.Text != "" && textBox2.Text != "" )
                {
                    if (IsNumeric(textBox2.Text))
                    {
                    try
                    {
                        string insertar = "UPDATE  pago SET  tipo = '" + comboBox1.Text + "', fecha = '" + dateTimePicker1.Text + "', monto = '" + textBox2.Text + "', pago = '" + textBox3.Text + "', origen = '" + comboBox2.Text + "', destino = '" + comboBox3.Text + "', disponible = '" + textBox4.Text + "', estatus ='" + comboBox4.Text + "', obser = '" + textBox5.Text + "', usu = '" + estatica.Ci + "' WHERE cod = " + pa + " ";
                        OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                        cmd.ExecuteNonQuery();
                        clear();
                        sw = 1;
                        MessageBox.Show("Los datos del pago fueron modificados con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    }

                    catch (DBConcurrencyException ex)
                    {
                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    }

                    else
                    {
                        MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                    }
                }
                if (sw == 0)
                {
                    if (comboBox1.Text == "Efectivo")
                    {
                        MessageBox.Show("Campos requeridos:\nTipo\nFecha\nMonto\nDisponible\nEstatus", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    if (comboBox1.Text == "Exoneración")
                    {
                        MessageBox.Show("Campos requeridos:\nTipo\nFecha\nDisponible\nEstatus\nObservación", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    if (comboBox1.Text == "Deposito")
                    {
                        MessageBox.Show("Campos requeridos:\nTipo\nFecha\nMonto\nN. de Referencia\nBanco de Destino\nDisponible\nEstatus", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    if (comboBox1.Text == "Trans. Bancaria" || comboBox1.Text == "Pago Rápido")
                    {
                        MessageBox.Show("Campos requeridos:\nTipo\nFecha\nMonto\nN. de Referencia\nBanco de Origen\nBanco de Destino\nDisponible\nEstatus", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    if (comboBox1.Text == "Otro")
                    {
                        MessageBox.Show("Campos requeridos:\nTipo\nFecha\nMonto\nDisponible\nEstatus\nObservación", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar la modificación.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem20_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem21_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem19_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem22_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem20_Click(object sender, EventArgs e)
        {

        }

        private void certificaciónRegularesToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem23_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem21_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem24_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem22_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem19_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        
    }
}
