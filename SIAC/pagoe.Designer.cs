﻿namespace SIAC
{
    partial class pagoe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(pagoe));
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem84 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem114 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem115 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem116 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem117 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem118 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem119 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem120 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem121 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem136 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem137 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem138 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem139 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem140 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem141 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem142 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem143 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem144 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem145 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem163 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem164 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem165 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem166 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.académicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarModificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarAEstudianteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disciplinasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.disponiblesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resumenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaDeEstudiantesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.formalizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cambioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.periodoAcadémicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preinscripciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.pendientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estudianteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empleadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeTrabajoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeActividadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.finanzasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bancoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.pagosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarMiContraseñaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deduccionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.reciboDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitudToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónDeCalificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónEgredadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.certificaciónRegularesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.listarCertificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.constanciaDeEstudioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.postulaciónParaPasantíasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.usuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarRestContraseñaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.menuStrip3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Location = new System.Drawing.Point(145, 84);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(24, 26);
            this.button3.TabIndex = 167;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 84);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(127, 26);
            this.textBox1.TabIndex = 166;
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 19);
            this.label1.TabIndex = 168;
            this.label1.Text = "Doc. de Identidad";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(172, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 19);
            this.label2.TabIndex = 170;
            this.label2.Text = "Nombre";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(175, 84);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(327, 26);
            this.textBox2.TabIndex = 169;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(505, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 19);
            this.label3.TabIndex = 172;
            this.label3.Text = "Cargo";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(508, 84);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(205, 26);
            this.textBox3.TabIndex = 171;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(238, 19);
            this.label5.TabIndex = 174;
            this.label5.Text = "Seleccione la asignación a agregar";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 158);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(347, 27);
            this.comboBox1.TabIndex = 173;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(361, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 19);
            this.label4.TabIndex = 176;
            this.label4.Text = "Valor unitario";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(365, 159);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(139, 26);
            this.textBox4.TabIndex = 175;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(506, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 19);
            this.label6.TabIndex = 178;
            this.label6.Text = "Cant. a pagar";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(659, 157);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 26);
            this.button1.TabIndex = 179;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.White;
            this.button4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button4.BackgroundImage")));
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.Location = new System.Drawing.Point(689, 157);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(24, 26);
            this.button4.TabIndex = 180;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(510, 159);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(139, 26);
            this.textBox5.TabIndex = 177;
            this.textBox5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox5_KeyPress);
            this.textBox5.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox5_KeyUp);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dataGridView1.Location = new System.Drawing.Point(12, 192);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(701, 99);
            this.dataGridView1.TabIndex = 181;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Descripción";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 250;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Bs.";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Cantidad";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 80;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Asignación";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 150;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(451, 27);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(113, 26);
            this.dateTimePicker1.TabIndex = 182;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(597, 27);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(116, 26);
            this.dateTimePicker2.TabIndex = 183;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(324, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(121, 19);
            this.label7.TabIndex = 184;
            this.label7.Text = "Periodo de pago:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(570, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 19);
            this.label8.TabIndex = 185;
            this.label8.Text = "al";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.Column5});
            this.dataGridView2.Location = new System.Drawing.Point(12, 382);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(701, 99);
            this.dataGridView2.TabIndex = 194;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Descripción";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 250;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Bs.";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Cantidad";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 80;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Deducción";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Visible = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.Location = new System.Drawing.Point(689, 347);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(24, 26);
            this.button2.TabIndex = 193;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button5.BackgroundImage")));
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.Location = new System.Drawing.Point(659, 347);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(24, 26);
            this.button5.TabIndex = 192;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(506, 327);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 19);
            this.label9.TabIndex = 191;
            this.label9.Text = "Cant. a deducir";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(510, 349);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(139, 26);
            this.textBox6.TabIndex = 190;
            this.textBox6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox6_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(361, 326);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 19);
            this.label10.TabIndex = 189;
            this.label10.Text = "Valor unitario";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(365, 349);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(139, 26);
            this.textBox7.TabIndex = 188;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(9, 326);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(234, 19);
            this.label11.TabIndex = 187;
            this.label11.Text = "Seleccione la deducción a agregar";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(12, 348);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(347, 27);
            this.comboBox2.TabIndex = 186;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(375, 294);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(158, 19);
            this.label12.TabIndex = 195;
            this.label12.Text = "Subtotal asignaciones:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(379, 484);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(154, 19);
            this.label15.TabIndex = 197;
            this.label15.Text = "Subtotal deducciones:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(502, 565);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(127, 19);
            this.label16.TabIndex = 206;
            this.label16.Text = "Banco de Destino";
            this.label16.Visible = false;
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(303, 583);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(197, 27);
            this.comboBox3.TabIndex = 205;
            this.comboBox3.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(142, 565);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(123, 19);
            this.label17.TabIndex = 204;
            this.label17.Text = "N. de Referencia";
            this.label17.Visible = false;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(142, 584);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(155, 26);
            this.textBox8.TabIndex = 203;
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox8.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(303, 565);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(121, 19);
            this.label18.TabIndex = 202;
            this.label18.Text = "Banco de Origen";
            this.label18.Visible = false;
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(506, 583);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(197, 27);
            this.comboBox4.TabIndex = 201;
            this.comboBox4.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(8, 562);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(108, 19);
            this.label19.TabIndex = 200;
            this.label19.Text = "Forma de pago";
            // 
            // comboBox5
            // 
            this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "",
            "Efectivo",
            "Deposito",
            "Trans. Bancaria",
            "Pago Rápido",
            "Cheque"});
            this.comboBox5.Location = new System.Drawing.Point(12, 584);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(124, 27);
            this.comboBox5.TabIndex = 199;
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(539, 484);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 19);
            this.label14.TabIndex = 198;
            this.label14.Text = "Valor";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(539, 294);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 19);
            this.label13.TabIndex = 196;
            this.label13.Text = "Valor";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(446, 523);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(170, 26);
            this.textBox9.TabIndex = 207;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(300, 526);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(136, 19);
            this.label20.TabIndex = 208;
            this.label20.Text = "TOTAL A PAGAR:";
            // 
            // button8
            // 
            this.button8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button8.BackgroundImage")));
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button8.Location = new System.Drawing.Point(619, 513);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(44, 44);
            this.button8.TabIndex = 209;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button6.BackgroundImage")));
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button6.Location = new System.Drawing.Point(669, 513);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(44, 44);
            this.button6.TabIndex = 210;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // menuStrip3
            // 
            this.menuStrip3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem84,
            this.toolStripMenuItem114,
            this.toolStripMenuItem136,
            this.toolStripMenuItem138,
            this.toolStripMenuItem163,
            this.toolStripMenuItem166});
            this.menuStrip3.Location = new System.Drawing.Point(0, 0);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Size = new System.Drawing.Size(735, 24);
            this.menuStrip3.TabIndex = 212;
            this.menuStrip3.Text = "menuStrip3";
            this.menuStrip3.Visible = false;
            // 
            // toolStripMenuItem84
            // 
            this.toolStripMenuItem84.Name = "toolStripMenuItem84";
            this.toolStripMenuItem84.Size = new System.Drawing.Size(49, 20);
            this.toolStripMenuItem84.Text = "Inicio";
            // 
            // toolStripMenuItem114
            // 
            this.toolStripMenuItem114.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem115,
            this.toolStripMenuItem116,
            this.toolStripMenuItem117,
            this.toolStripMenuItem118,
            this.toolStripMenuItem119});
            this.toolStripMenuItem114.Name = "toolStripMenuItem114";
            this.toolStripMenuItem114.Size = new System.Drawing.Size(71, 20);
            this.toolStripMenuItem114.Text = "Empleado";
            // 
            // toolStripMenuItem115
            // 
            this.toolStripMenuItem115.Name = "toolStripMenuItem115";
            this.toolStripMenuItem115.Size = new System.Drawing.Size(205, 22);
            this.toolStripMenuItem115.Text = "Registrar";
            // 
            // toolStripMenuItem116
            // 
            this.toolStripMenuItem116.Name = "toolStripMenuItem116";
            this.toolStripMenuItem116.Size = new System.Drawing.Size(205, 22);
            this.toolStripMenuItem116.Text = "Modificar";
            // 
            // toolStripMenuItem117
            // 
            this.toolStripMenuItem117.Name = "toolStripMenuItem117";
            this.toolStripMenuItem117.Size = new System.Drawing.Size(205, 22);
            this.toolStripMenuItem117.Text = "Constancia de Trabajo";
            // 
            // toolStripMenuItem118
            // 
            this.toolStripMenuItem118.Name = "toolStripMenuItem118";
            this.toolStripMenuItem118.Size = new System.Drawing.Size(205, 22);
            this.toolStripMenuItem118.Text = "Constancia de Actividad";
            // 
            // toolStripMenuItem119
            // 
            this.toolStripMenuItem119.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem120,
            this.toolStripMenuItem121});
            this.toolStripMenuItem119.Name = "toolStripMenuItem119";
            this.toolStripMenuItem119.Size = new System.Drawing.Size(205, 22);
            this.toolStripMenuItem119.Text = "Cargos";
            // 
            // toolStripMenuItem120
            // 
            this.toolStripMenuItem120.Name = "toolStripMenuItem120";
            this.toolStripMenuItem120.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem120.Text = "Registrar";
            // 
            // toolStripMenuItem121
            // 
            this.toolStripMenuItem121.Name = "toolStripMenuItem121";
            this.toolStripMenuItem121.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem121.Text = "Modificar";
            // 
            // toolStripMenuItem136
            // 
            this.toolStripMenuItem136.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem137});
            this.toolStripMenuItem136.Name = "toolStripMenuItem136";
            this.toolStripMenuItem136.Size = new System.Drawing.Size(46, 20);
            this.toolStripMenuItem136.Text = "Perfil";
            // 
            // toolStripMenuItem137
            // 
            this.toolStripMenuItem137.Name = "toolStripMenuItem137";
            this.toolStripMenuItem137.Size = new System.Drawing.Size(203, 22);
            this.toolStripMenuItem137.Text = "Modificar mi contraseña";
            // 
            // toolStripMenuItem138
            // 
            this.toolStripMenuItem138.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem139,
            this.toolStripMenuItem142,
            this.toolStripMenuItem145});
            this.toolStripMenuItem138.Name = "toolStripMenuItem138";
            this.toolStripMenuItem138.Size = new System.Drawing.Size(55, 20);
            this.toolStripMenuItem138.Text = "Salario";
            // 
            // toolStripMenuItem139
            // 
            this.toolStripMenuItem139.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem140,
            this.toolStripMenuItem141});
            this.toolStripMenuItem139.Name = "toolStripMenuItem139";
            this.toolStripMenuItem139.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem139.Text = "Asignaciones";
            // 
            // toolStripMenuItem140
            // 
            this.toolStripMenuItem140.Name = "toolStripMenuItem140";
            this.toolStripMenuItem140.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem140.Text = "Registrar";
            // 
            // toolStripMenuItem141
            // 
            this.toolStripMenuItem141.Name = "toolStripMenuItem141";
            this.toolStripMenuItem141.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem141.Text = "Modificar";
            // 
            // toolStripMenuItem142
            // 
            this.toolStripMenuItem142.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem143,
            this.toolStripMenuItem144});
            this.toolStripMenuItem142.Name = "toolStripMenuItem142";
            this.toolStripMenuItem142.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem142.Text = "Deducciones";
            // 
            // toolStripMenuItem143
            // 
            this.toolStripMenuItem143.Name = "toolStripMenuItem143";
            this.toolStripMenuItem143.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem143.Text = "Registrar";
            // 
            // toolStripMenuItem144
            // 
            this.toolStripMenuItem144.Name = "toolStripMenuItem144";
            this.toolStripMenuItem144.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem144.Text = "Modificar";
            // 
            // toolStripMenuItem145
            // 
            this.toolStripMenuItem145.Name = "toolStripMenuItem145";
            this.toolStripMenuItem145.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem145.Text = "Recibo de Pago";
            // 
            // toolStripMenuItem163
            // 
            this.toolStripMenuItem163.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem164,
            this.toolStripMenuItem165});
            this.toolStripMenuItem163.Name = "toolStripMenuItem163";
            this.toolStripMenuItem163.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem163.Text = "Usuario";
            // 
            // toolStripMenuItem164
            // 
            this.toolStripMenuItem164.Name = "toolStripMenuItem164";
            this.toolStripMenuItem164.Size = new System.Drawing.Size(221, 22);
            this.toolStripMenuItem164.Text = "Registrar";
            // 
            // toolStripMenuItem165
            // 
            this.toolStripMenuItem165.Name = "toolStripMenuItem165";
            this.toolStripMenuItem165.Size = new System.Drawing.Size(221, 22);
            this.toolStripMenuItem165.Text = "Modificar/Rest. Contraseña";
            // 
            // toolStripMenuItem166
            // 
            this.toolStripMenuItem166.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenuItem166.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolStripMenuItem166.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.toolStripMenuItem166.Name = "toolStripMenuItem166";
            this.toolStripMenuItem166.Size = new System.Drawing.Size(42, 20);
            this.toolStripMenuItem166.Text = "Salir";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem,
            this.académicoToolStripMenuItem,
            this.estudianteToolStripMenuItem,
            this.empleadoToolStripMenuItem,
            this.finanzasToolStripMenuItem,
            this.perfilToolStripMenuItem,
            this.salarioToolStripMenuItem,
            this.solicitudToolStripMenuItem,
            this.usuarioToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(735, 24);
            this.menuStrip1.TabIndex = 211;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.inicioToolStripMenuItem.Text = "Inicio";
            // 
            // académicoToolStripMenuItem
            // 
            this.académicoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calificacionesToolStripMenuItem,
            this.disciplinasToolStripMenuItem,
            this.formalizaciónToolStripMenuItem,
            this.periodoAcadémicoToolStripMenuItem,
            this.preinscripciónToolStripMenuItem});
            this.académicoToolStripMenuItem.Name = "académicoToolStripMenuItem";
            this.académicoToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.académicoToolStripMenuItem.Text = "Académico";
            // 
            // calificacionesToolStripMenuItem
            // 
            this.calificacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarModificarToolStripMenuItem,
            this.modificarAEstudianteToolStripMenuItem});
            this.calificacionesToolStripMenuItem.Name = "calificacionesToolStripMenuItem";
            this.calificacionesToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.calificacionesToolStripMenuItem.Text = "Calificaciones";
            // 
            // cargarModificarToolStripMenuItem
            // 
            this.cargarModificarToolStripMenuItem.Name = "cargarModificarToolStripMenuItem";
            this.cargarModificarToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.cargarModificarToolStripMenuItem.Text = "Cargar/Modificar";
            // 
            // modificarAEstudianteToolStripMenuItem
            // 
            this.modificarAEstudianteToolStripMenuItem.Name = "modificarAEstudianteToolStripMenuItem";
            this.modificarAEstudianteToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.modificarAEstudianteToolStripMenuItem.Text = "Modificar a Estudiante";
            // 
            // disciplinasToolStripMenuItem
            // 
            this.disciplinasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem4,
            this.modificarToolStripMenuItem3,
            this.disponiblesToolStripMenuItem,
            this.resumenToolStripMenuItem,
            this.listaDeEstudiantesToolStripMenuItem,
            this.tiposToolStripMenuItem});
            this.disciplinasToolStripMenuItem.Name = "disciplinasToolStripMenuItem";
            this.disciplinasToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.disciplinasToolStripMenuItem.Text = "Disciplina";
            // 
            // registrarToolStripMenuItem4
            // 
            this.registrarToolStripMenuItem4.Name = "registrarToolStripMenuItem4";
            this.registrarToolStripMenuItem4.Size = new System.Drawing.Size(182, 22);
            this.registrarToolStripMenuItem4.Text = "Registrar";
            // 
            // modificarToolStripMenuItem3
            // 
            this.modificarToolStripMenuItem3.Name = "modificarToolStripMenuItem3";
            this.modificarToolStripMenuItem3.Size = new System.Drawing.Size(182, 22);
            this.modificarToolStripMenuItem3.Text = "Modificar";
            // 
            // disponiblesToolStripMenuItem
            // 
            this.disponiblesToolStripMenuItem.Name = "disponiblesToolStripMenuItem";
            this.disponiblesToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.disponiblesToolStripMenuItem.Text = "Disponibles";
            // 
            // resumenToolStripMenuItem
            // 
            this.resumenToolStripMenuItem.Name = "resumenToolStripMenuItem";
            this.resumenToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.resumenToolStripMenuItem.Text = "Resumen";
            // 
            // listaDeEstudiantesToolStripMenuItem
            // 
            this.listaDeEstudiantesToolStripMenuItem.Name = "listaDeEstudiantesToolStripMenuItem";
            this.listaDeEstudiantesToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.listaDeEstudiantesToolStripMenuItem.Text = "Lista de Estudiantes";
            // 
            // tiposToolStripMenuItem
            // 
            this.tiposToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem5,
            this.modificarToolStripMenuItem4});
            this.tiposToolStripMenuItem.Name = "tiposToolStripMenuItem";
            this.tiposToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.tiposToolStripMenuItem.Text = "Tipos";
            // 
            // registrarToolStripMenuItem5
            // 
            this.registrarToolStripMenuItem5.Name = "registrarToolStripMenuItem5";
            this.registrarToolStripMenuItem5.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem5.Text = "Registrar";
            // 
            // modificarToolStripMenuItem4
            // 
            this.modificarToolStripMenuItem4.Name = "modificarToolStripMenuItem4";
            this.modificarToolStripMenuItem4.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem4.Text = "Modificar";
            // 
            // formalizaciónToolStripMenuItem
            // 
            this.formalizaciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem6,
            this.modificarToolStripMenuItem5,
            this.cambioToolStripMenuItem});
            this.formalizaciónToolStripMenuItem.Name = "formalizaciónToolStripMenuItem";
            this.formalizaciónToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.formalizaciónToolStripMenuItem.Text = "Formalización";
            // 
            // registrarToolStripMenuItem6
            // 
            this.registrarToolStripMenuItem6.Name = "registrarToolStripMenuItem6";
            this.registrarToolStripMenuItem6.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem6.Text = "Registrar";
            // 
            // modificarToolStripMenuItem5
            // 
            this.modificarToolStripMenuItem5.Name = "modificarToolStripMenuItem5";
            this.modificarToolStripMenuItem5.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem5.Text = "Modificar";
            // 
            // cambioToolStripMenuItem
            // 
            this.cambioToolStripMenuItem.Name = "cambioToolStripMenuItem";
            this.cambioToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.cambioToolStripMenuItem.Text = "Cambio";
            // 
            // periodoAcadémicoToolStripMenuItem
            // 
            this.periodoAcadémicoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem7,
            this.modificarToolStripMenuItem6,
            this.cerrarToolStripMenuItem});
            this.periodoAcadémicoToolStripMenuItem.Name = "periodoAcadémicoToolStripMenuItem";
            this.periodoAcadémicoToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.periodoAcadémicoToolStripMenuItem.Text = "Periodo Académico";
            // 
            // registrarToolStripMenuItem7
            // 
            this.registrarToolStripMenuItem7.Name = "registrarToolStripMenuItem7";
            this.registrarToolStripMenuItem7.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem7.Text = "Registrar";
            // 
            // modificarToolStripMenuItem6
            // 
            this.modificarToolStripMenuItem6.Name = "modificarToolStripMenuItem6";
            this.modificarToolStripMenuItem6.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem6.Text = "Modificar";
            // 
            // cerrarToolStripMenuItem
            // 
            this.cerrarToolStripMenuItem.Name = "cerrarToolStripMenuItem";
            this.cerrarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.cerrarToolStripMenuItem.Text = "Cerrar";
            // 
            // preinscripciónToolStripMenuItem
            // 
            this.preinscripciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem8,
            this.modificarToolStripMenuItem7,
            this.pendientesToolStripMenuItem});
            this.preinscripciónToolStripMenuItem.Name = "preinscripciónToolStripMenuItem";
            this.preinscripciónToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.preinscripciónToolStripMenuItem.Text = "Preinscripción";
            // 
            // registrarToolStripMenuItem8
            // 
            this.registrarToolStripMenuItem8.Name = "registrarToolStripMenuItem8";
            this.registrarToolStripMenuItem8.Size = new System.Drawing.Size(133, 22);
            this.registrarToolStripMenuItem8.Text = "Registrar";
            // 
            // modificarToolStripMenuItem7
            // 
            this.modificarToolStripMenuItem7.Name = "modificarToolStripMenuItem7";
            this.modificarToolStripMenuItem7.Size = new System.Drawing.Size(133, 22);
            this.modificarToolStripMenuItem7.Text = "Modificar";
            // 
            // pendientesToolStripMenuItem
            // 
            this.pendientesToolStripMenuItem.Name = "pendientesToolStripMenuItem";
            this.pendientesToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.pendientesToolStripMenuItem.Text = "Pendientes";
            // 
            // estudianteToolStripMenuItem
            // 
            this.estudianteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem,
            this.modificarToolStripMenuItem,
            this.historialToolStripMenuItem});
            this.estudianteToolStripMenuItem.Name = "estudianteToolStripMenuItem";
            this.estudianteToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.estudianteToolStripMenuItem.Text = "Estudiante";
            // 
            // registrarToolStripMenuItem
            // 
            this.registrarToolStripMenuItem.Name = "registrarToolStripMenuItem";
            this.registrarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem.Text = "Registrar";
            // 
            // modificarToolStripMenuItem
            // 
            this.modificarToolStripMenuItem.Name = "modificarToolStripMenuItem";
            this.modificarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem.Text = "Modificar";
            // 
            // historialToolStripMenuItem
            // 
            this.historialToolStripMenuItem.Name = "historialToolStripMenuItem";
            this.historialToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.historialToolStripMenuItem.Text = "Historial";
            // 
            // empleadoToolStripMenuItem
            // 
            this.empleadoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem9,
            this.modificarToolStripMenuItem8,
            this.constanciaDeTrabajoToolStripMenuItem,
            this.constanciaDeActividadToolStripMenuItem,
            this.cargosToolStripMenuItem});
            this.empleadoToolStripMenuItem.Name = "empleadoToolStripMenuItem";
            this.empleadoToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.empleadoToolStripMenuItem.Text = "Empleado";
            // 
            // registrarToolStripMenuItem9
            // 
            this.registrarToolStripMenuItem9.Name = "registrarToolStripMenuItem9";
            this.registrarToolStripMenuItem9.Size = new System.Drawing.Size(205, 22);
            this.registrarToolStripMenuItem9.Text = "Registrar";
            // 
            // modificarToolStripMenuItem8
            // 
            this.modificarToolStripMenuItem8.Name = "modificarToolStripMenuItem8";
            this.modificarToolStripMenuItem8.Size = new System.Drawing.Size(205, 22);
            this.modificarToolStripMenuItem8.Text = "Modificar";
            // 
            // constanciaDeTrabajoToolStripMenuItem
            // 
            this.constanciaDeTrabajoToolStripMenuItem.Name = "constanciaDeTrabajoToolStripMenuItem";
            this.constanciaDeTrabajoToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.constanciaDeTrabajoToolStripMenuItem.Text = "Constancia de Trabajo";
            // 
            // constanciaDeActividadToolStripMenuItem
            // 
            this.constanciaDeActividadToolStripMenuItem.Name = "constanciaDeActividadToolStripMenuItem";
            this.constanciaDeActividadToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.constanciaDeActividadToolStripMenuItem.Text = "Constancia de Actividad";
            // 
            // cargosToolStripMenuItem
            // 
            this.cargosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem10,
            this.modificarToolStripMenuItem9});
            this.cargosToolStripMenuItem.Name = "cargosToolStripMenuItem";
            this.cargosToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.cargosToolStripMenuItem.Text = "Cargos";
            // 
            // registrarToolStripMenuItem10
            // 
            this.registrarToolStripMenuItem10.Name = "registrarToolStripMenuItem10";
            this.registrarToolStripMenuItem10.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem10.Text = "Registrar";
            // 
            // modificarToolStripMenuItem9
            // 
            this.modificarToolStripMenuItem9.Name = "modificarToolStripMenuItem9";
            this.modificarToolStripMenuItem9.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem9.Text = "Modificar";
            // 
            // finanzasToolStripMenuItem
            // 
            this.finanzasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bancoToolStripMenuItem,
            this.pagosToolStripMenuItem,
            this.preciosToolStripMenuItem});
            this.finanzasToolStripMenuItem.Name = "finanzasToolStripMenuItem";
            this.finanzasToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.finanzasToolStripMenuItem.Text = "Finanzas";
            // 
            // bancoToolStripMenuItem
            // 
            this.bancoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem11,
            this.modificarToolStripMenuItem10});
            this.bancoToolStripMenuItem.Name = "bancoToolStripMenuItem";
            this.bancoToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.bancoToolStripMenuItem.Text = "Banco";
            // 
            // registrarToolStripMenuItem11
            // 
            this.registrarToolStripMenuItem11.Name = "registrarToolStripMenuItem11";
            this.registrarToolStripMenuItem11.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem11.Text = "Registrar";
            // 
            // modificarToolStripMenuItem10
            // 
            this.modificarToolStripMenuItem10.Name = "modificarToolStripMenuItem10";
            this.modificarToolStripMenuItem10.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem10.Text = "Modificar";
            // 
            // pagosToolStripMenuItem
            // 
            this.pagosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem12,
            this.consultarToolStripMenuItem,
            this.modificarToolStripMenuItem11,
            this.verificarToolStripMenuItem,
            this.listaToolStripMenuItem});
            this.pagosToolStripMenuItem.Name = "pagosToolStripMenuItem";
            this.pagosToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.pagosToolStripMenuItem.Text = "Pagos";
            // 
            // registrarToolStripMenuItem12
            // 
            this.registrarToolStripMenuItem12.Name = "registrarToolStripMenuItem12";
            this.registrarToolStripMenuItem12.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem12.Text = "Registrar";
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.consultarToolStripMenuItem.Text = "Consultar";
            // 
            // modificarToolStripMenuItem11
            // 
            this.modificarToolStripMenuItem11.Name = "modificarToolStripMenuItem11";
            this.modificarToolStripMenuItem11.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem11.Text = "Modificar";
            // 
            // verificarToolStripMenuItem
            // 
            this.verificarToolStripMenuItem.Name = "verificarToolStripMenuItem";
            this.verificarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.verificarToolStripMenuItem.Text = "Verificar";
            // 
            // listaToolStripMenuItem
            // 
            this.listaToolStripMenuItem.Name = "listaToolStripMenuItem";
            this.listaToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.listaToolStripMenuItem.Text = "Lista";
            // 
            // preciosToolStripMenuItem
            // 
            this.preciosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem13,
            this.consultarToolStripMenuItem1,
            this.modificarToolStripMenuItem12});
            this.preciosToolStripMenuItem.Name = "preciosToolStripMenuItem";
            this.preciosToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.preciosToolStripMenuItem.Text = "Precios";
            // 
            // registrarToolStripMenuItem13
            // 
            this.registrarToolStripMenuItem13.Name = "registrarToolStripMenuItem13";
            this.registrarToolStripMenuItem13.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem13.Text = "Registrar";
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(126, 22);
            this.consultarToolStripMenuItem1.Text = "Consultar";
            // 
            // modificarToolStripMenuItem12
            // 
            this.modificarToolStripMenuItem12.Name = "modificarToolStripMenuItem12";
            this.modificarToolStripMenuItem12.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem12.Text = "Modificar";
            // 
            // perfilToolStripMenuItem
            // 
            this.perfilToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificarMiContraseñaToolStripMenuItem});
            this.perfilToolStripMenuItem.Name = "perfilToolStripMenuItem";
            this.perfilToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.perfilToolStripMenuItem.Text = "Perfil";
            // 
            // modificarMiContraseñaToolStripMenuItem
            // 
            this.modificarMiContraseñaToolStripMenuItem.Name = "modificarMiContraseñaToolStripMenuItem";
            this.modificarMiContraseñaToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.modificarMiContraseñaToolStripMenuItem.Text = "Modificar mi contraseña";
            // 
            // salarioToolStripMenuItem
            // 
            this.salarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asignacionesToolStripMenuItem,
            this.deduccionesToolStripMenuItem,
            this.reciboDePagoToolStripMenuItem});
            this.salarioToolStripMenuItem.Name = "salarioToolStripMenuItem";
            this.salarioToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.salarioToolStripMenuItem.Text = "Salario";
            // 
            // asignacionesToolStripMenuItem
            // 
            this.asignacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem2,
            this.modificarToolStripMenuItem1});
            this.asignacionesToolStripMenuItem.Name = "asignacionesToolStripMenuItem";
            this.asignacionesToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.asignacionesToolStripMenuItem.Text = "Asignaciones";
            // 
            // registrarToolStripMenuItem2
            // 
            this.registrarToolStripMenuItem2.Name = "registrarToolStripMenuItem2";
            this.registrarToolStripMenuItem2.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem2.Text = "Registrar";
            // 
            // modificarToolStripMenuItem1
            // 
            this.modificarToolStripMenuItem1.Name = "modificarToolStripMenuItem1";
            this.modificarToolStripMenuItem1.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem1.Text = "Modificar";
            // 
            // deduccionesToolStripMenuItem
            // 
            this.deduccionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem3,
            this.modificarToolStripMenuItem2});
            this.deduccionesToolStripMenuItem.Name = "deduccionesToolStripMenuItem";
            this.deduccionesToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.deduccionesToolStripMenuItem.Text = "Deducciones";
            // 
            // registrarToolStripMenuItem3
            // 
            this.registrarToolStripMenuItem3.Name = "registrarToolStripMenuItem3";
            this.registrarToolStripMenuItem3.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem3.Text = "Registrar";
            // 
            // modificarToolStripMenuItem2
            // 
            this.modificarToolStripMenuItem2.Name = "modificarToolStripMenuItem2";
            this.modificarToolStripMenuItem2.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem2.Text = "Modificar";
            // 
            // reciboDePagoToolStripMenuItem
            // 
            this.reciboDePagoToolStripMenuItem.Name = "reciboDePagoToolStripMenuItem";
            this.reciboDePagoToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.reciboDePagoToolStripMenuItem.Text = "Recibo de Pago";
            // 
            // solicitudToolStripMenuItem
            // 
            this.solicitudToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.certificaciónDeCalificacionesToolStripMenuItem,
            this.certificaciónEgredadosToolStripMenuItem,
            this.certificaciónRegularesToolStripMenuItem,
            this.listarCertificacionesToolStripMenuItem,
            this.constanciaDeEstudioToolStripMenuItem,
            this.postulaciónParaPasantíasToolStripMenuItem});
            this.solicitudToolStripMenuItem.Name = "solicitudToolStripMenuItem";
            this.solicitudToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.solicitudToolStripMenuItem.Text = "Solicitud";
            // 
            // certificaciónDeCalificacionesToolStripMenuItem
            // 
            this.certificaciónDeCalificacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem14,
            this.modificarToolStripMenuItem13});
            this.certificaciónDeCalificacionesToolStripMenuItem.Name = "certificaciónDeCalificacionesToolStripMenuItem";
            this.certificaciónDeCalificacionesToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.certificaciónDeCalificacionesToolStripMenuItem.Text = "Certificación de Calificaciones";
            // 
            // registrarToolStripMenuItem14
            // 
            this.registrarToolStripMenuItem14.Name = "registrarToolStripMenuItem14";
            this.registrarToolStripMenuItem14.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem14.Text = "Registrar";
            // 
            // modificarToolStripMenuItem13
            // 
            this.modificarToolStripMenuItem13.Name = "modificarToolStripMenuItem13";
            this.modificarToolStripMenuItem13.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem13.Text = "Modificar";
            // 
            // certificaciónEgredadosToolStripMenuItem
            // 
            this.certificaciónEgredadosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem15,
            this.modificarToolStripMenuItem14});
            this.certificaciónEgredadosToolStripMenuItem.Name = "certificaciónEgredadosToolStripMenuItem";
            this.certificaciónEgredadosToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.certificaciónEgredadosToolStripMenuItem.Text = "Certificación Egredados";
            // 
            // registrarToolStripMenuItem15
            // 
            this.registrarToolStripMenuItem15.Name = "registrarToolStripMenuItem15";
            this.registrarToolStripMenuItem15.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem15.Text = "Registrar";
            // 
            // modificarToolStripMenuItem14
            // 
            this.modificarToolStripMenuItem14.Name = "modificarToolStripMenuItem14";
            this.modificarToolStripMenuItem14.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem14.Text = "Modificar";
            // 
            // certificaciónRegularesToolStripMenuItem
            // 
            this.certificaciónRegularesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem16,
            this.modificarToolStripMenuItem15});
            this.certificaciónRegularesToolStripMenuItem.Name = "certificaciónRegularesToolStripMenuItem";
            this.certificaciónRegularesToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.certificaciónRegularesToolStripMenuItem.Text = "Certificación Regulares";
            // 
            // registrarToolStripMenuItem16
            // 
            this.registrarToolStripMenuItem16.Name = "registrarToolStripMenuItem16";
            this.registrarToolStripMenuItem16.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem16.Text = "Registrar";
            // 
            // modificarToolStripMenuItem15
            // 
            this.modificarToolStripMenuItem15.Name = "modificarToolStripMenuItem15";
            this.modificarToolStripMenuItem15.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem15.Text = "Modificar";
            // 
            // listarCertificacionesToolStripMenuItem
            // 
            this.listarCertificacionesToolStripMenuItem.Name = "listarCertificacionesToolStripMenuItem";
            this.listarCertificacionesToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.listarCertificacionesToolStripMenuItem.Text = "Listar Certificaciones";
            // 
            // constanciaDeEstudioToolStripMenuItem
            // 
            this.constanciaDeEstudioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem17,
            this.modificarToolStripMenuItem16});
            this.constanciaDeEstudioToolStripMenuItem.Name = "constanciaDeEstudioToolStripMenuItem";
            this.constanciaDeEstudioToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.constanciaDeEstudioToolStripMenuItem.Text = "Constancia de Estudio";
            // 
            // registrarToolStripMenuItem17
            // 
            this.registrarToolStripMenuItem17.Name = "registrarToolStripMenuItem17";
            this.registrarToolStripMenuItem17.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem17.Text = "Registrar";
            // 
            // modificarToolStripMenuItem16
            // 
            this.modificarToolStripMenuItem16.Name = "modificarToolStripMenuItem16";
            this.modificarToolStripMenuItem16.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem16.Text = "Modificar";
            // 
            // postulaciónParaPasantíasToolStripMenuItem
            // 
            this.postulaciónParaPasantíasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem18,
            this.modificarToolStripMenuItem17});
            this.postulaciónParaPasantíasToolStripMenuItem.Name = "postulaciónParaPasantíasToolStripMenuItem";
            this.postulaciónParaPasantíasToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.postulaciónParaPasantíasToolStripMenuItem.Text = "Postulación para Pasantías";
            // 
            // registrarToolStripMenuItem18
            // 
            this.registrarToolStripMenuItem18.Name = "registrarToolStripMenuItem18";
            this.registrarToolStripMenuItem18.Size = new System.Drawing.Size(126, 22);
            this.registrarToolStripMenuItem18.Text = "Registrar";
            // 
            // modificarToolStripMenuItem17
            // 
            this.modificarToolStripMenuItem17.Name = "modificarToolStripMenuItem17";
            this.modificarToolStripMenuItem17.Size = new System.Drawing.Size(126, 22);
            this.modificarToolStripMenuItem17.Text = "Modificar";
            // 
            // usuarioToolStripMenuItem
            // 
            this.usuarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarToolStripMenuItem1,
            this.modificarRestContraseñaToolStripMenuItem});
            this.usuarioToolStripMenuItem.Name = "usuarioToolStripMenuItem";
            this.usuarioToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.usuarioToolStripMenuItem.Text = "Usuario";
            // 
            // registrarToolStripMenuItem1
            // 
            this.registrarToolStripMenuItem1.Name = "registrarToolStripMenuItem1";
            this.registrarToolStripMenuItem1.Size = new System.Drawing.Size(221, 22);
            this.registrarToolStripMenuItem1.Text = "Registrar";
            // 
            // modificarRestContraseñaToolStripMenuItem
            // 
            this.modificarRestContraseñaToolStripMenuItem.Name = "modificarRestContraseñaToolStripMenuItem";
            this.modificarRestContraseñaToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.modificarRestContraseñaToolStripMenuItem.Text = "Modificar/Rest. Contraseña";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.salirToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.salirToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // pagoe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(735, 625);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip3);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.comboBox5);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox1);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "pagoe";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Recibo de Pago";
            this.Load += new System.EventHandler(this.pagoe_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem84;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem114;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem115;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem116;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem117;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem118;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem119;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem120;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem121;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem136;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem137;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem138;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem139;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem140;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem141;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem142;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem143;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem144;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem145;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem163;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem164;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem165;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem166;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem académicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarModificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarAEstudianteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disciplinasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem disponiblesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resumenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaDeEstudiantesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tiposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem formalizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem cambioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem periodoAcadémicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem cerrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preinscripciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem pendientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estudianteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empleadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeTrabajoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeActividadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem finanzasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bancoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem pagosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem verificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem perfilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarMiContraseñaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deduccionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem reciboDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solicitudToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem certificaciónDeCalificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem certificaciónEgredadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem certificaciónRegularesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem listarCertificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem constanciaDeEstudioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem postulaciónParaPasantíasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem usuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem modificarRestContraseñaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
    }
}