﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class actperiodo : Form
    {
        public actperiodo()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void actperiodo_Load(object sender, EventArgs e)
        {
            conexion.Open();
        }

        string per;
        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                per = "";
                string buscarc = "SELECT * FROM periodo WHERE cod = " + textBox1.Text + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    if (lee["estatus"].ToString() == "ACTIVO" || lee["estatus"].ToString() == "INACTIVO")
                    {
                        per = lee["cod"].ToString();
                        textBox2.Text = lee["periodo"].ToString();
                        dateTimePicker1.Text = lee["inicio"].ToString();
                        dateTimePicker2.Text = lee["fin"].ToString();
                        comboBox1.Text = lee["estatus"].ToString();
                        button1.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("El código ingresado pertenece a un periodo culminado y por lo tanto no puede ser modificado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        button1.Visible = false;
                        per = "";
                    }
                }
                else
                {
                    MessageBox.Show("El código ingresado no pertenece a un periodo académico registrado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un código para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "")
            {
                per = "";
                string buscarc = "SELECT * FROM periodo WHERE periodo LIKE '" + textBox2.Text + "';";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    if (lee["estatus"].ToString() == "ACTIVO" || lee["estatus"].ToString() == "INACTIVO")
                    {
                        textBox1.Text= per = lee["cod"].ToString();
                        textBox2.Text = lee["periodo"].ToString();
                        dateTimePicker1.Text = lee["inicio"].ToString();
                        dateTimePicker2.Text = lee["fin"].ToString();
                        comboBox1.Text = lee["estatus"].ToString();
                        button1.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("El nombre ingresado pertenece a un culminado y por lo tanto no puede ser modificado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        button1.Visible = false;
                        per = "";
                    }
                }
                else
                {
                    MessageBox.Show("El nombre ingresado no pertenece a un periodo académico registrado.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar el nombre de un periodo académico  para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        void clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            comboBox1.Text = "";
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
            per = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (per != "" && comboBox1.Text != "" && textBox2.Text != "")
            {
                try
                {
                    string insertar = "UPDATE  periodo SET  periodo = '" + textBox2.Text + "', inicio = '" + dateTimePicker1.Text + "', fin = '" + dateTimePicker2.Text + "', estatus = '" + comboBox1.Text + "' WHERE cod = " + per + " ";
                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                    cmd.ExecuteNonQuery();
                    clear();
                    MessageBox.Show("Los datos del periodo fueron modificados con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);


                }

                catch (DBConcurrencyException ex)
                {
                    MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar la modificación.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem20_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem21_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem19_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem22_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem20_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem23_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem21_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem24_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem22_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem19_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }


        
    }
}
