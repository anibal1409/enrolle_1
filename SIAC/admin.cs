﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SIAC
{
    public partial class admin : Form
    {
        public admin()
        {
            InitializeComponent();
        }

        private void admin_Load(object sender, EventArgs e)
        {
            if (estatica.Nivel == "Administrador")
            {
                menuStrip1.Visible = true;
            }
            if (estatica.Nivel == "Asesor de Ventas")
            {
                menuStrip4.Visible = true;
            }
            if (estatica.Nivel == "Coordinador Académico")
            {
                menuStrip2.Visible = true;
            }
            if (estatica.Nivel == "Coordinador de Personal")
            {
                menuStrip3.Visible = true;
            }
            label1.Text = "Bienvenido, " + estatica.Nombre;
        }

        #region menu1

        private void modificarToolStripMenuItem17_Click(object sender, EventArgs e)
        {
            actpostu a = new actpostu();
            a.Show();
            this.Close();
        }
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea salir de su sesión?", "Notificación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                isesion a = new isesion();
                a.Show();
                this.Close();
            }
        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            estudiante a = new estudiante();
            a.Show();
            this.Close();
        }

        private void modificarRestContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actusuario a = new actusuario();
            a.Show();
            this.Close();
        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actpas a = new actpas();
            a.ShowDialog();
        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            historial a = new historial();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            usuario a = new usuario();
            a.Show();
            this.Close();
        }

        private void asignacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void deduccionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pagoe a = new pagoe();
            a.Show();
            this.Close();
        }

        private void disciplinasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            admin a = new admin();
            a.Show();
            this.Close();
        }

        

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cargarnota a = new cargarnota();
            a.Show();
            this.Close();
        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actnotae a = new actnotae();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            modalidad a = new modalidad();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            actmodalidad a = new actmodalidad();
            a.Show();
            this.Close();
        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mdisponibles a = new mdisponibles();
            a.Show();
            this.Close();
        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resumencurso a = new resumencurso();
            a.Show();
            this.Close();
        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listaestudiantes a = new listaestudiantes();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            tipom a = new tipom();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            acttipom a = new acttipom();
            a.Show();
            this.Close();
        }

        

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            forma a = new forma();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            actforma a = new actforma();
            a.Show();
            this.Close();
        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cambio a = new cambio();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            periodo a = new periodo();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            actperiodo a = new actperiodo();
            a.Show();
            this.Close();
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cerrar a = new cerrar();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            pre a = new pre();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            actpre a = new actpre();
            a.Show();
            this.Close();
        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            prepen a = new prepen();
            a.Show();
            this.Close();
        }
        

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actestudiante a = new actestudiante();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            actempleado a = new actempleado();
            a.Show();
            this.Close();
        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            trabajo a = new trabajo();
            a.Show();
            this.Close();
        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actividad a = new actividad();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            cargo a = new cargo();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            actcargo a = new actcargo();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            banco a = new banco();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            actbanco a = new actbanco();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            pago a = new pago();
            a.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            conpago a = new conpago();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            actpago a = new actpago(); 
            a.Show();
            this.Close();
        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            veripago a = new veripago();
            a.Show();
            this.Close();
        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listapv a = new listapv();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {
            precio a = new precio();
            a.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            conprecio a = new conprecio();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            actprecio a = new actprecio();
            a.Show();
            this.Close();
        }

        private void salarioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            abonos a = new abonos();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            actabonos a = new actabonos();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            dedu a = new dedu();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            actdedu a = new actdedu();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {
            notas a = new notas();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {
            actnotas a = new actnotas();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {
            certi2 a = new certi2();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {
            actcerti2 a = new actcerti2();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem16_Click(object sender, EventArgs e)
        {
            certi1 a = new certi1();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {
            actcerti1 a = new actcerti1();
            a.Show();
            this.Close();
        }

        private void listarCertificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listacerti a = new listacerti();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem17_Click(object sender, EventArgs e)
        {
            estudio a = new estudio();
            a.Show();
            this.Close();
        }

        private void modificarToolStripMenuItem16_Click(object sender, EventArgs e)
        {
            actestudio a = new actestudio();
            a.Show();
            this.Close();
        }

        private void registrarToolStripMenuItem18_Click(object sender, EventArgs e)
        {
            postu a = new postu();
            a.Show();
            this.Close();
        }
        #endregion


        #region menu2

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            admin a = new admin();
            a.Show();
            this.Close();
        }
        

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            cargarnota a = new cargarnota();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            actnotas a = new actnotas();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            modalidad a = new modalidad();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            actmodalidad a = new actmodalidad();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            mdisponibles a = new mdisponibles();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            resumencurso a = new resumencurso();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem11_Click(object sender, EventArgs e)
        {
            listaestudiantes a = new listaestudiantes();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            tipom a = new tipom();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem14_Click(object sender, EventArgs e)
        {
            acttipom a = new acttipom();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem16_Click(object sender, EventArgs e)
        {
            forma a = new forma();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem17_Click(object sender, EventArgs e)
        {
            actforma a = new actforma();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem18_Click(object sender, EventArgs e)
        {
            cambio a = new cambio();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem20_Click(object sender, EventArgs e)
        {
            periodo a = new periodo();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem21_Click(object sender, EventArgs e)
        {
            actperiodo a = new actperiodo();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem22_Click(object sender, EventArgs e)
        {
            cerrar a = new cerrar();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem24_Click(object sender, EventArgs e)
        {
            pre a = new pre();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem25_Click(object sender, EventArgs e)
        {
            actpre a = new actpre();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem26_Click(object sender, EventArgs e)
        {
            prepen a = new prepen();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem28_Click(object sender, EventArgs e)
        {
            estudiante a = new estudiante();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem29_Click(object sender, EventArgs e)
        {
            actestudiante a = new actestudiante();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem30_Click(object sender, EventArgs e)
        {
            historial a = new historial();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem41_Click(object sender, EventArgs e)
        {
            banco a = new banco();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem42_Click(object sender, EventArgs e)
        {
            actbanco a = new actbanco();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem44_Click(object sender, EventArgs e)
        {
            pago a = new pago();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem45_Click(object sender, EventArgs e)
        {
            conpago a = new conpago();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem46_Click(object sender, EventArgs e)
        {
            actpago a = new actpago();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem47_Click(object sender, EventArgs e)
        {
            veripago a = new veripago();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem48_Click(object sender, EventArgs e)
        {
            listapv a = new listapv();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem50_Click(object sender, EventArgs e)
        {
            precio a = new precio();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem51_Click(object sender, EventArgs e)
        {
            conprecio a = new conprecio();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem52_Click(object sender, EventArgs e)
        {
            actprecio a = new actprecio();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem54_Click(object sender, EventArgs e)
        {
            actpas a = new actpas();
            a.ShowDialog();
        }

        private void toolStripMenuItem65_Click(object sender, EventArgs e)
        {
            notas a = new notas();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem66_Click(object sender, EventArgs e)
        {
            actnotas a = new actnotas();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem68_Click(object sender, EventArgs e)
        {
            certi2 a = new certi2();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem69_Click(object sender, EventArgs e)
        {
            actcerti2 a = new actcerti2();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem71_Click(object sender, EventArgs e)
        {
            certi1 a = new certi1();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem72_Click(object sender, EventArgs e)
        {
            actcerti1 a = new actcerti1();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem73_Click(object sender, EventArgs e)
        {
            listacerti a = new listacerti();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem75_Click(object sender, EventArgs e)
        {
            estudio a = new estudio();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem76_Click(object sender, EventArgs e)
        {
            actestudio a = new actestudio();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem78_Click(object sender, EventArgs e)
        {
            postu a = new postu();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem79_Click(object sender, EventArgs e)
        {
            actpostu a = new actpostu();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem81_Click(object sender, EventArgs e)
        {
            usuario a = new usuario();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem82_Click(object sender, EventArgs e)
        {
            actusuario a = new actusuario();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem83_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea salir de su sesión?", "Notificación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                isesion a = new isesion();
                a.Show();
                this.Close();
            }
        }
        #endregion


        #region menu3
        private void toolStripMenuItem84_Click(object sender, EventArgs e)
        {
            admin a = new admin();
            a.Show();
            this.Close();
        }
        

        private void toolStripMenuItem115_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem116_Click(object sender, EventArgs e)
        {
            actempleado a = new actempleado();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem117_Click(object sender, EventArgs e)
        {
            trabajo a = new trabajo();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem118_Click(object sender, EventArgs e)
        {
            actividad a = new actividad();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem120_Click(object sender, EventArgs e)
        {
            cargo a = new cargo();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem121_Click(object sender, EventArgs e)
        {
            actcargo a = new actcargo();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem137_Click(object sender, EventArgs e)
        {
            actpas a = new actpas();
            a.ShowDialog();
        }

        private void toolStripMenuItem140_Click(object sender, EventArgs e)
        {
            abonos a = new abonos();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem141_Click(object sender, EventArgs e)
        {
            actabonos a = new actabonos();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem143_Click(object sender, EventArgs e)
        {
            dedu a = new dedu();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem144_Click(object sender, EventArgs e)
        {
            actdedu a = new actdedu();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem145_Click(object sender, EventArgs e)
        {
            pagoe a = new pagoe();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem164_Click(object sender, EventArgs e)
        {
            usuario a = new usuario();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem165_Click(object sender, EventArgs e)
        {
            actusuario a = new actusuario();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem166_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea salir de su sesión?", "Notificación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                isesion a = new isesion();
                a.Show();
                this.Close();
            }
        }
        #endregion


        #region menu4
        private void toolStripMenuItem167_Click(object sender, EventArgs e)
        {
            admin a = new admin();
            a.Show();
            this.Close();
        }

        

        private void toolStripMenuItem182_Click(object sender, EventArgs e)
        {
            forma a = new forma();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem183_Click(object sender, EventArgs e)
        {
            actforma a = new actforma();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem184_Click(object sender, EventArgs e)
        {
            cambio a = new cambio();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem190_Click(object sender, EventArgs e)
        {
            pre a = new pre();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem191_Click(object sender, EventArgs e)
        {
            actpre a = new actpre();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem192_Click(object sender, EventArgs e)
        {
            prepen a = new prepen();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem194_Click(object sender, EventArgs e)
        {
            estudiante a = new estudiante();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem195_Click(object sender, EventArgs e)
        {
            actestudiante a = new actestudiante();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem196_Click(object sender, EventArgs e)
        {
            historial a = new historial();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem210_Click(object sender, EventArgs e)
        {
            pago a = new pago();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem211_Click(object sender, EventArgs e)
        {
            conpago a = new conpago();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem217_Click(object sender, EventArgs e)
        {
            conprecio a = new conprecio();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem220_Click(object sender, EventArgs e)
        {
            actpas a = new actpas();
            a.ShowDialog();
        }

        private void toolStripMenuItem231_Click(object sender, EventArgs e)
        {
            notas a = new notas();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem232_Click(object sender, EventArgs e)
        {
            actnotas a = new actnotas();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem234_Click(object sender, EventArgs e)
        {
            certi2 a = new certi2();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem235_Click(object sender, EventArgs e)
        {
            actcerti2 a = new actcerti2();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem237_Click(object sender, EventArgs e)
        {
            certi1 a = new certi1();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem238_Click(object sender, EventArgs e)
        {
            actcerti1 a = new actcerti1();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem239_Click(object sender, EventArgs e)
        {
            listacerti a = new listacerti();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem241_Click(object sender, EventArgs e)
        {
            estudio a = new estudio();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem242_Click(object sender, EventArgs e)
        {
            actestudio a = new actestudio();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem244_Click(object sender, EventArgs e)
        {
            postu a = new postu();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem245_Click(object sender, EventArgs e)
        {
            actpostu a = new actpostu();
            a.Show();
            this.Close();
        }

        private void toolStripMenuItem249_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea salir de su sesión?", "Notificación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                isesion a = new isesion();
                a.Show();
                this.Close();
            }
        }
        #endregion

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        

    }
}
