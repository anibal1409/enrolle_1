﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class cambio : Form
    {
        public cambio()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void cambio_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
        }

        
        void combo1()
        {
            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox1.ValueMember = "cod";
            comboBox1.DisplayMember = "periodo";
            comboBox1.DataSource = dt;
        }

        string ci = "";
        void buscar()
        {
            if (textBox7.Text != "")
            {
                ci = "";
                var cadena = textBox7.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM estudiante WHERE ci = '" + textBox7.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                            ci = textBox7.Text;
                            textBox6.Text = lee["nombre"].ToString()+" "+lee["apellido"].ToString();
                            comboBox1.Enabled = true;

                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad '" + textBox7.Text + "' no se encuentra registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox7.Focus();   
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad válido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para poder realizar la búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        DataTable dtx = new DataTable();
        void combo2(string com)
        {
            string ctc = "SELECT cod,modalidad FROM modalidad WHERE estatus = 'DISPONIBLE' AND periodo = " + com + " ORDER BY modalidad";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "modalidad";
            comboBox2.DataSource = dt;
        }
        string[,] comen;
        int can;
        void combo3(string com)
        {
            string ctc = "SELECT * FROM pre WHERE estudiante = '" + ci + "' AND periodo = '" + comboBox1.SelectedValue.ToString() + "' AND estatus = 'VERIFICADA' ;";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            OleDbCommand comando = new OleDbCommand(ctc, conexion);
            OleDbDataReader lee = comando.ExecuteReader();

            DataTable dt = new DataTable();
            //DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();
            dt.Columns.Add("cod");
            dt.Columns.Add("modalidad");

            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";

            dt.Rows.InsertAt(nuevaFila, 0);
            can = 0;
            comen = new string[2, 1000000];
            while (lee.Read())
            {
                string ctcx = "SELECT * FROM modalidad WHERE cod = " + lee["modalidad"].ToString() + " ;";
                OleDbDataAdapter DATAx = new OleDbDataAdapter(ctcx, conexion);
                OleDbCommand comandox = new OleDbCommand(ctcx, conexion);
                OleDbDataReader leex = comandox.ExecuteReader();
                if (leex.Read())
                {
                    comen[0, can] = leex["cod"].ToString();
                    comen[1, can] = leex["modalidad"].ToString();
                    can++;
                }
            }
            if (can > 1)
            {
                string ax, bx, cx;
                for (int i = 1; i < can; i++)
                {
                    for (int j = 1; j < can - 1; j++)
                    {
                        if (String.Compare(comen[1, j], comen[1, j + 1]) > 0)
                        {
                            ax = comen[0, j];
                            bx = comen[1, j];
                            comen[0, j] = comen[0, j + 1];
                            comen[1, j] = comen[1, j + 1];
                            comen[0, j + 1] = ax;
                            comen[1, j + 1] = bx;
                        }
                    }
                }
            }

            if (can > 0)
            {
                for (int i = 0; i < can; i++)
                {
                    DataRow nuevaFilax = dt.NewRow();

                    nuevaFilax["cod"] = comen[0, i];
                    nuevaFilax["modalidad"] = comen[1, i];

                    dt.Rows.InsertAt(nuevaFilax, i+1); 
                    
                    
                    

                }
            }

            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "modalidad";
            comboBox3.DataSource = dt;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 0)
            {

                combo2(comboBox1.SelectedValue.ToString());
                combo3(comboBox1.SelectedValue.ToString());
                comboBox2.Enabled = true;
                comboBox3.Enabled = true;


            }
            else
            {
                clearc();
            }
        }

        void clearc()
        {
            comboBox2.Enabled = false;
            comboBox3.Enabled = false;
            dtx = new DataTable();
            dtx.Columns.Add("cod");
            dtx.Columns.Add("modalidad");
            DataRow nuevaFila = dtx.NewRow();
            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione una disciplina";

            dtx.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "modalidad";
            comboBox2.DataSource = dtx;
            comboBox3.ValueMember = "cod";
            comboBox3.DisplayMember = "modalidad";
            comboBox3.DataSource = dtx;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            buscar();
            combo1();
        }
        void cleare()
        {
            ci = "";
            textBox7.Clear();
            textBox6.Clear();
            textBox7.Focus();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            cleare();
            clear();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ctcx = "SELECT * FROM modalidad WHERE cod = " + comboBox3.SelectedValue.ToString() + " ;";
            OleDbDataAdapter DATAx = new OleDbDataAdapter(ctcx, conexion);
            OleDbCommand comandox = new OleDbCommand(ctcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read())
            {
                textBox2.Text = leex["disponible"].ToString();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ctcx = "SELECT * FROM modalidad WHERE cod = " + comboBox2.SelectedValue.ToString() + " ;";
            OleDbDataAdapter DATAx = new OleDbDataAdapter(ctcx, conexion);
            OleDbCommand comandox = new OleDbCommand(ctcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read())
            {
                textBox14.Text = leex["disponible"].ToString();
            }
        }
        void clear()
        {
            cleare();
            clearc();
            DataTable dtxx = new DataTable();
            dtxx.Columns.Add("cod");
            dtxx.Columns.Add("periodo");
            DataRow nuevaFila = dtxx.NewRow();
            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dtxx.Rows.InsertAt(nuevaFila, 0);

            comboBox1.ValueMember = "cod";
            comboBox1.DisplayMember = "periodo";
            comboBox1.DataSource = dtxx;
            comboBox1.Enabled = false;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (ci != "" &&  textBox14.Text != "")
            {
                try
                {
                    string ctcxx = "SELECT * FROM pre WHERE estudiante = '"+ci+"' AND periodo = "+comboBox1.SelectedValue.ToString()+" AND modalidad = " + comboBox2.SelectedValue.ToString() + ";";
                    OleDbDataAdapter DATAXX = new OleDbDataAdapter(ctcxx, conexion);
                    OleDbCommand comandoxx = new OleDbCommand(ctcxx, conexion);
                    OleDbDataReader leexx = comandoxx.ExecuteReader();
                    if (leexx.Read())
                    {
                        if (leexx["estatus"].ToString() == "VERIFICADO")
                        {
                            MessageBox.Show("El estudiante '"+ci+"' ya encuentra inscrito en la disciplina '"+comboBox2.Text+"'.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        if (leexx["estatus"].ToString() == "PENDIENTE")
                        {
                            MessageBox.Show("El estudiante '" + ci + "' tiene una preinscripción pendiente sobre la disciplina '" + comboBox2.Text + "'. Formalícela.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        double x = Double.Parse(textBox14.Text);
                        double y = Double.Parse(textBox2.Text);
                        if (x > 0)
                        {
                            try
                            {

                                string insertar = "UPDATE  pre SET  modalidad = " + comboBox2.SelectedValue.ToString() + " WHERE estudiante = '" + ci + "' AND periodo = " + comboBox1.SelectedValue.ToString() + " AND modalidad = " + comboBox3.SelectedValue.ToString() + "";
                                OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                x = x - 1;
                                insertar = "UPDATE  modalidad SET disponible = '" + x.ToString() + "'  WHERE cod = " + comboBox2.SelectedValue.ToString() + "";
                                cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();

                                y = y + 1;
                                insertar = "UPDATE  modalidad SET disponible = '" + y.ToString() + "'  WHERE cod = " + comboBox3.SelectedValue.ToString() + "";
                                cmd = new OleDbCommand(insertar, conexion);
                                cmd.ExecuteNonQuery();
                                MessageBox.Show("El cambio de disciplina se realizó con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                clear();
                            }
                            catch (DBConcurrencyException ex)
                            {
                                MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("La disciplina seleccionada tiene su capacidad al máximo, y esto impide realizar el cambio.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
                catch (DBConcurrencyException ex)
                {
                    MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el cambio.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem20_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem21_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem19_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem22_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem20_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem23_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem21_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem24_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem22_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem19_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        
    }
}
