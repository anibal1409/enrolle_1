﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class modalidad : Form
    {
        public modalidad()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void modalidad_Load(object sender, EventArgs e)
        {
            conexion.Open();
            cargar();
            combo1();
            combo2();
            comboBox3.SelectedIndex = 0;
            comboBox4.SelectedIndex = 0;
        }

        void cargar()
        {
            string buscarcx = "SELECT TOP 1 * FROM modalidad ORDER BY cod DESC;";
            OleDbDataAdapter datax = new OleDbDataAdapter(buscarcx, conexion);
            OleDbCommand comandox = new OleDbCommand(buscarcx, conexion);
            OleDbDataReader leex = comandox.ExecuteReader();
            if (leex.Read() == true)
            {
                double ccod;
                ccod = Double.Parse(leex["cod"].ToString()) + 1;
                textBox1.Text = ccod.ToString();
            }
            else
            {
                textBox1.Text = "1";
            }
        }

        void combo1()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM tipom ORDER BY modalidad";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["modalidad"] = "Seleccione un tipo de disciplina";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox1.ValueMember = "cod";
            comboBox1.DisplayMember = "modalidad";
            comboBox1.DataSource = dt;
        }

        void combo2()
        {

            DataTable tc = new DataTable();
            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' OR estatus = 'INACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dt = new DataTable();
            DATA.Fill(dt);

            DataRow nuevaFila = dt.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dt.Rows.InsertAt(nuevaFila, 0);

            comboBox2.ValueMember = "cod";
            comboBox2.DisplayMember = "periodo";
            comboBox2.DataSource = dt;
        }
        string ci = "";
        void buscar()
        {
            if (textBox5.Text != "")
            {
                ci = "";
                var cadena = textBox5.Text;
                if (cadena.Length >= 7 && cadena.Length <= 8)
                {
                    string buscarc = "SELECT * FROM empleado WHERE ci = '" + textBox5.Text + "';";
                    OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                    OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                    OleDbDataReader lee = comando.ExecuteReader();
                    if (lee.Read() == true)
                    {
                        if (lee["estatus"].ToString() == "ACTIVO")
                        {

                            ci = textBox5.Text;
                            textBox6.Text = lee["nombre"].ToString() + " " + lee["apellido"].ToString();
                            textBox7.Text = lee["profesion"].ToString();
                        }
                        else
                        {
                            MessageBox.Show("Este empleado no se encuentra activo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        MessageBox.Show("El documento de identidad ingresado no pertenece a ningun empleado registrado en el sistema.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un documento de identidad valido.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un documento de identidad para realizar una busqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.Text != "Seleccione un periodo académico")
            {
                ;
                string buscarc = "SELECT * FROM periodo WHERE cod = " + comboBox2.SelectedValue.ToString() + ";";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    textBox3.Text = lee["inicio"].ToString();
                    textBox4.Text = lee["fin"].ToString();
                }

            }
            else
            {
                textBox3.Clear();
                textBox4.Clear();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buscar();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            ci = "";
        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnlyNumber(e, false);
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox5_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buscar();
            }
        }

        private static void OnlyNumber(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        public bool IsNumeric(string x)
        {

            double retNum;

            try
            {
                retNum = Double.Parse(x);
                return true;
            }
            catch
            {
                return false;

            }



        }
        void clear()
        {
            textBox1.Clear();
            cargar();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox8.Clear();
            ci = "";
            y = 0;
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            comboBox4.SelectedIndex = 0;
            numericUpDown1.Value = 1;
            numericUpDown2.Value = 1;
            numericUpDown3.Value = 40;
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
            dateTimePicker3.Value = DateTime.Now;
            dateTimePicker4.Value = DateTime.Now;

        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && comboBox1.Text != "Seleccione un tipo de modalidad" && textBox2.Text != "" && comboBox2.Text != "Seleccione un periodo académico"  && comboBox3.Text != "Seleccione un día" && ci != "" && textBox8.Text != "" && comboBox4.Text != "Seleccione un estatus")
            {
                if (IsNumeric(textBox8.Text))
                {
                    DateTime a,b;
                    if (dateTimePicker1.Value < dateTimePicker2.Value)
                    {
                        a = DateTime.Parse(textBox3.Text);
                        b = DateTime.Parse(textBox4.Text);
                        if(String.Compare(dateTimePicker3.Value.ToString("dd-MM-yyy"), dateTimePicker4.Value.ToString("dd-MM-yyy")) <=0)
                        {
                            if (String.Compare(dateTimePicker3.Value.ToString("dd-MM-yyy"), textBox3.Text) >= 0)
                            {
                                if (String.Compare(dateTimePicker4.Value.ToString("dd-MM-yyy"), textBox4.Text) <= 0)
                                {
                                    try
                                    {
                                        string insertar = "INSERT INTO modalidad VALUES (" + textBox1.Text + "," + comboBox1.SelectedValue.ToString() + ",'" + textBox2.Text + "'," + comboBox2.SelectedValue.ToString() + ", '" + comboBox3.Text + "', '" + dateTimePicker1.Text + "', '" + dateTimePicker2.Text + "', '" + dateTimePicker3.Text + "', '" + dateTimePicker4.Text + "', '" + numericUpDown1.Value.ToString() + "', '" + numericUpDown2.Value.ToString() + "', '" + numericUpDown3.Value.ToString() + "', '" + ci + "', '" + textBox8.Text + "', '" + comboBox4.Text + "', '" + numericUpDown3.Value.ToString() + "')";
                                        OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                                        cmd.ExecuteNonQuery();
                                        //clear();
                                        MessageBox.Show("La disciplina se registró con éxito.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    }

                                    catch (DBConcurrencyException ex)
                                    {
                                        MessageBox.Show("Error de concurrencia:\n" + ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Error al guardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("La fecha de finalización no puede ser mayor que la fecha de fin del periodo académico seleccionado. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    dateTimePicker4.Focus();
                                }
                            }
                            else
                            {
                                MessageBox.Show("La fecha de inicio no puede ser menor que la fecha de inicio del periodo académico seleccionado. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                dateTimePicker3.Focus();
                            }
                        }
                        else
                        {
                            MessageBox.Show("La fecha de inicio no puede ser mayor que la fecha de finalización. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            dateTimePicker3.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("La hora de salida no puede ser menor o igual que la hora de entrada. Verifíquela.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        dateTimePicker2.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("El monto ingresado no es válido. Verifíquelo.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox8.Focus();
                }
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos para realizar el registro.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        int y = 0;
        void horas()
        {
            TimeSpan a = dateTimePicker4.Value.Date - dateTimePicker3.Value.Date;
            int x = (Int32.Parse(a.Days.ToString()) / 7);
            numericUpDown2.Value = x;
            y = dateTimePicker2.Value.Hour - dateTimePicker1.Value.Hour;
            if (dateTimePicker2.Value.Hour > dateTimePicker1.Value.Hour && x > 0)
            {

                
                numericUpDown1.Value = x * y;


            }
            else
            {
                numericUpDown1.Value = 1;
            }
        }

        private void dateTimePicker4_ValueChanged(object sender, EventArgs e)
        {
            horas();
            
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            horas();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            horas();
        }

        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {
            horas();
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            if (y > 0)
            {
                numericUpDown1.Value = y * numericUpDown2.Value;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
