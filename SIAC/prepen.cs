﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class prepen : Form
    {
        public prepen()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        private void prepen_Load(object sender, EventArgs e)
        {
            conexion.Open();
            combo1();
        }
        void combo1()
        {
            string ctc = "SELECT * FROM periodo WHERE estatus = 'ACTIVO' ORDER BY inicio";
            OleDbDataAdapter DATA = new OleDbDataAdapter(ctc, conexion);
            DataTable dtx = new DataTable();
            dtx = new DataTable();
            DATA.Fill(dtx);

            DataRow nuevaFila = dtx.NewRow();

            nuevaFila["cod"] = 0;
            nuevaFila["periodo"] = "Seleccione un periodo académico";

            dtx.Rows.InsertAt(nuevaFila, 0);

            comboBox1.ValueMember = "cod";
            comboBox1.DisplayMember = "periodo";
            comboBox1.DataSource = dtx;
        }

        string[,] comen;
        int can;
        void lista()
        {
            can = 0;
            comen = new string[4, 1000000];
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            string buscarcxx = "SELECT * FROM pre WHERE periodo = '" + comboBox1.SelectedValue.ToString() + "' AND estatus = 'PENDIENTE';";
            OleDbDataAdapter dataxx = new OleDbDataAdapter(buscarcxx, conexion);
            OleDbCommand comandoxx = new OleDbCommand(buscarcxx, conexion);
            OleDbDataReader leexx = comandoxx.ExecuteReader();
            while (leexx.Read())
            {


                comen[0, can] = leexx["cod"].ToString();
                string buscarcx1 = "SELECT * FROM estudiante WHERE ci = '" + leexx["estudiante"].ToString() + "';";
                OleDbDataAdapter datax1 = new OleDbDataAdapter(buscarcx1, conexion);
                OleDbCommand comandox1 = new OleDbCommand(buscarcx1, conexion);
                OleDbDataReader leex1 = comandox1.ExecuteReader();
                if (leex1.Read())
                {
                    comen[1, can] = "(" + leexx["estudiante"].ToString() + ") " + leex1["nombre"].ToString() + leex1["apellido"].ToString();
                    comen[2, can] = leex1["tlf"].ToString();
                }
                string buscarcx2 = "SELECT * FROM modalidad WHERE cod = '" + leexx["modalidad"].ToString() + "';";
                OleDbDataAdapter datax2 = new OleDbDataAdapter(buscarcx2, conexion);
                OleDbCommand comandox2 = new OleDbCommand(buscarcx2, conexion);
                OleDbDataReader leex2 = comandox2.ExecuteReader();
                if (leex2.Read())
                {
                    comen[3, can] = leex2["modalidad"].ToString();
                }
                can++;


            }
            if (can > 1)
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();



                string ax, bx, cx, dx, ex;
                for (int i = 0; i < can; i++)
                {
                    for (int j = 0; j < can - 1; j++)
                    {
                        if (String.Compare(comen[3, j], comen[3, j + 1]) > 0)
                        {
                            ax = comen[0, j];
                            bx = comen[1, j];
                            cx = comen[2, j];
                            dx = comen[3, j];
                            comen[0, j] = comen[0, j + 1];
                            comen[1, j] = comen[1, j + 1];
                            comen[2, j] = comen[2, j + 1];
                            comen[3, j] = comen[3, j + 1];
                            comen[0, j + 1] = ax;
                            comen[1, j + 1] = bx;
                            comen[2, j + 1] = cx;
                            comen[3, j + 1] = dx;
                        }
                    }
                }
            }

            if (can > 0)
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                for (int i = 0; i < can; i++)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells[0].Value = comen[0, i];
                    dataGridView1.Rows[i].Cells[1].Value = comen[1, i];
                    dataGridView1.Rows[i].Cells[2].Value = comen[2, i];
                    dataGridView1.Rows[i].Cells[3].Value = comen[3, i];
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 0)
            {
                lista();
            }
            else
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
            }
        }

        private void inicioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cargarModificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarAEstudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void disponiblesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaDeEstudiantesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void cambioToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }

        private void pendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void constanciaDeActividadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem11_Click(object sender, EventArgs e)
        {

        }

        private void verificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem12_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarMiContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void reciboDePagoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem13_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem14_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem15_Click(object sender, EventArgs e)
        {

        }

        private void listarCertificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem16_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem18_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem17_Click(object sender, EventArgs e)
        {

        }

        private void registrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarRestContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
