﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SIAC
{
    public partial class contra : Form
    {
        public contra()
        {
            InitializeComponent();
        }
        OleDbConnection conexion = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\datesiac.accdb; Jet OLEDB:Database Password=antontesis");
        
        private void contra_Load(object sender, EventArgs e)
        {
            conexion.Open();
        }
        string fecha, fechai, nivel, user;
        int ver = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            ver = 0;
            if(textBox1.Text != "" && comboBox1.SelectedIndex != 0 && dateTimePicker1.Text != DateTime.Now.ToString("dd-MM-yyyy"))
            {
                string buscarc = "SELECT * FROM usuario WHERE nombre = '" + textBox1.Text + "';";
                OleDbDataAdapter data = new OleDbDataAdapter(buscarc, conexion);
                OleDbCommand comando = new OleDbCommand(buscarc, conexion);
                OleDbDataReader lee = comando.ExecuteReader();
                if (lee.Read() == true)
                {
                    user = textBox1.Text;
                    nivel = lee["nivel"].ToString();
                    string buscar = "SELECT * FROM empleado WHERE ci = '" + textBox1.Text + "';";
                    OleDbDataAdapter datax = new OleDbDataAdapter(buscar, conexion);
                    OleDbCommand comandox = new OleDbCommand(buscar, conexion);
                    OleDbDataReader leex = comandox.ExecuteReader();
                    if (leex.Read() == true)
                    {
                            
                        fecha = leex["naci"].ToString();
                        fechai = leex["fechai"].ToString();
                    }
                }
                if (fecha == dateTimePicker1.Text && dateTimePicker2.Text == fechai && nivel == comboBox1.Text)
                {
                    ver = 1;
                    label1.Visible = false;
                    label2.Visible = false;
                    label4.Visible = false;
                    label7.Visible = false;
                    textBox1.Visible = false;
                    dateTimePicker1.Visible = false;
                    dateTimePicker2.Visible = false;
                    comboBox1.Visible = false;
                    button1.Visible = false;
                    label5.Visible = true;
                    label6.Visible = true;
                    button2.Visible = true;
                    textBox2.Visible = true;
                    textBox3.Visible = true;
                    checkBox1.Visible = true;
                }
                if( ver == 0)
                {
                    MessageBox.Show("Los datos ingresados son incorrectos.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else
            {
                MessageBox.Show("Debe completar todos los campos para poder continuar.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "" && textBox3.Text != "")
            {
                if (textBox2.Text == textBox3.Text)
                {
                    string insertar = "UPDATE  usuario SET  pass = '" + textBox2.Text + "' WHERE nombre = '" + user + "' ";
                    OleDbCommand cmd = new OleDbCommand(insertar, conexion);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("La contraseña fue modificada con éxito. ", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
                else
                {
                    MessageBox.Show("La nueva contraseña y la confirmación son diferentes.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox2.Focus();
                }
            }
            else
            {
                MessageBox.Show("Debe completar todos los campos para poder continuar.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text == textBox2.Text)
            {
                textBox2.BackColor = Color.Cyan;
                textBox3.BackColor = Color.Cyan;
            }
            else
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
            if (textBox2.Text == "" || textBox2.Text == " " || textBox2.Text == "  " || textBox2.Text == "  ")
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
            if (textBox2.Text == "" || textBox3.Text == " " || textBox3.Text == "  " || textBox3.Text == "  ")
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text == textBox2.Text)
            {
                textBox2.BackColor = Color.Cyan;
                textBox3.BackColor = Color.Cyan;
            }
            else
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
            if (textBox2.Text == "" || textBox2.Text == " " || textBox2.Text == "  " || textBox2.Text == "  ")
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
            if (textBox2.Text == "" || textBox3.Text == " " || textBox3.Text == "  " || textBox3.Text == "  ")
            {
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                textBox2.PasswordChar = '\0';
                textBox3.PasswordChar = '\0';
            }
            else
            {
                textBox2.PasswordChar = '*';
                textBox3.PasswordChar = '*';

            }
        }
    }
}
